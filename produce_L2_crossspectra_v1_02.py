'''
    Author:  mickael.lebars@ifremer.fr
    Purpose: exploit script data driven L2 to produce xspec
    Creation:  2016-10-19
    Arguments:    
    Outputs:     
    Dependencies:
    History: Based on antoine Graouzel script to produce spectrum
    --------------------------------------------------------------
    Date:
    Author:
    Modification:
    --------------------------------------------------------------
'''
import os
import logging
import sys
import datetime
import glob
import shutil
import fnmatch

import matplotlib

import re
from dateutil import rrule
from operator import or_
from generate_listing_v1_02 import createlisting

rep_trigger = '/home/cercache/project/mpc-sentinel1/workspace/trigger_chains/xspec_L2_plots/'

SAT = {"S1A":'sentinel-1a',
       "S1B":'sentinel-1b'}
ROOT_OUT = '/home/cercache/project/mpc-sentinel1/analysis/s1_data_analysis/L2_V2/'

LOG_FOLDER = '/home/cercache/project/mpc-sentinel1/analysis/s1_data_analysis/L2_V2/LOGS/'

modes = {'SAT':['satfull','satreal','satimaginary'],'WW3':['ww3']}

#ssher script used to connect using ssh on distant computer
ssher = '/home/losafe/users/agrouaze/PROGRAMMES/EXPLOIT/cron_ssher.py'
gogoscript = '/home/cersat5/tools/gogolist/bin/gogolist.py'
script = '/home3/homedir7/perso/satwave/sources_en_exploitation/wavetoolbox/runner_plot_spectra_v2.bash'



def generatecommandline(options,listing):    
    """
    #Return the command line used to launch gogolist
    """
    platform = options.platform
    if not platform:
        platform ="UNK"
    options_gogo = "--mem=10G --name "+platform+"_WVL2_xspec --background --split-max-jobs 100 --streaming --stdin -e "+script
    #print "python "+ssher+" cerhouse1 "+gogoscript+' '+options_gogo

    return "python "+ssher+" cerhouse1 '"+"cat "+listing+"|"+gogoscript+' '+options_gogo+"'"

def parsearguments(parser):
    """
    # Parse the arguments and create an object options that contains it 
    """
    parser.add_option("-b","--beginningdate",
                        action="store", type="string",
                        dest="begdate", metavar="string",
                        help="starting date YYYYMMDD [default=begining of mission]")
    parser.add_option("-e","--endingdate",
                        action="store", type="string",
                        dest="enddate", metavar="string",
                        help="ending date YYYYMMDD [default=current day]")
    parser.add_option("-q","--quiet",
                        action="store_false", default=True,dest="quiet",
                        help="quiet mode [OPTIONAL default=True]")
    parser.add_option("-g","--gogolist",
                        action="store_false", default=True,dest="gogo",
                        help="turn off gogolist [OPTIONAL default=True]")
    parser.add_option("-p","--platform",
                        action="store", type="choice",choices=['S1A','S1B'],
                        dest="platform",
                        help="satellite: S1A or S1B [mandatory]")
    parser.add_option("-s","--source",
                        action="store", type="choice",choices=['SAT','WW3','BUOY','CFO'],
                        dest="source",
                        help="source: SAT or WW3 or BUOY [mandatory]")
    parser.add_option("-v","--verbose",
                      action='store_true',default=False,
                      help="verbose mode [default is quiet]")
    parser.add_option("-f","--file",
                    action="store", type="string",
                    dest="file",metavar="string",
                    help="file: file that contains data to generate")
    parser.add_option("-r","--redo-all",
                      action='store_true',default=False,dest='redo',
                      help="redo all plot even the one existing [default is to produce only the one missing]")
    (options, args) = parser.parse_args()
    return options

if __name__ == "__main__":
    from optparse import OptionParser
    
    #Get the options--------
    parser = OptionParser()
    options = parsearguments(parser)
    #-----------------------
      
    #Configure the logger -----------
    root = logging.getLogger()
    if root.handlers:
        for handler in root.handlers:
            root.removeHandler(handler) 
    if options.verbose:
        logging.basicConfig(level=logging.DEBUG)
    else:
        logging.basicConfig(level=logging.INFO)
    #----------------------------------- 
    if options.file:
        listingpath = options.file
    else:
        listingpath = createlisting(options) 
    
    if options.gogo:
        meta_sentence = generatecommandline(options, listingpath)
    else:
        meta_sentence = "cat "+listingpath+"|"+script
        #print listingpath    
    os.system(meta_sentence)
    logging.info('exit spectra python')

