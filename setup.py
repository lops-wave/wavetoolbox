from setuptools import setup, find_packages
files = ["/*"]
setup(
    name='wavetoolbox',
    package_dir={'wavetoolbox': 'wavetoolbox',
                 'wavetoolbox.SpectrumReader_V2':"wavetoolbox.SpectrumReader_V2"},
    #packages=find_packages(),
    package_data = {'package' : files },
    scripts = ["SpectrumReader_V2.py"],
    url='https://gitlab.ifremer.fr/lops-wave/wavetoolbox',
    use_scm_version=True,
    setup_requires=['setuptools_scm'],
    install_requires=[
        'matplotlib',
        'netCDF4',
        'numpy',
        'xarray',
            ],
    license='GPL',
    author='Antoine Grouazel',
    author_email='antoine.grouazel@ifremer.fr',
    description='Lib to read/display/write spectral data (insitu model satellite)'
)
