# encoding: utf-8
"""

original file from cerbere.science.wave
=========================================

Routines for computing properties of ocean wave data

:copyright: Copyright 2013 Ifremer / Cersat.
:license: Released under GPL v3 license, see :ref:`license`.

.. sectionauthor:: Jeff Piolle <jfpiolle@ifremer.fr>
.. codeauthor:: Jeff Piolle <jfpiolle@ifremer.fr>
"""
import numpy
g = 9.81

def wavelength2period(wlength, depth=1e6):
    k = 2. * numpy.pi / wlength
    km = 363.
    omega = numpy.sqrt(g * k * (numpy.tanh(k * depth) + (k / km) * (k / km)))
    period = 2. * numpy.pi / omega
    return period

def period2wavelength(period):
    '''return wavelength in [m]
    inumpyut period (in [sec])'''
    wavelength = (g*period**2)/(2. * numpy.pi)
    return wavelength

def r1r2_to_sth1sth2(theta1, theta2, r1, r2):
    if r1 == None:
        stheta1 = None
    else:
        stheta1 = numpy.degrees(numpy.sqrt(abs(2. * (1 - r1))))
    if r2 == None:
        stheta2 = None
    else:
        stheta2 = numpy.degrees(numpy.sqrt(abs(0.5 * (1 - r2))))
    return (theta1, theta2, stheta1, stheta2)


def moments2dirspread(a1, b1, a2, b2):
    """
    compute spreading for 
    a1,b1,a2,b2 coef given for instance in NDBC buoys
    returns theta1, theta2, stheta1, stheta2
    
    """
    
    if a1 == None or b1 == None:
        r1 = None
        theta1 = None
    else:
        r1 = numpy.sqrt(a1 * a1 + b1 * b1)
        theta1 = numpy.fmod(numpy.degrees(numpy.arctan2(b1, a1)), 360)
    if a2 == None or b2 == None:
        r2 = None
        theta2 = None
    else:
        r2 = numpy.sqrt(a2 * a2 + b2 * b2)
        theta2 = numpy.fmod(numpy.degrees(0.5 * numpy.arctan2(b2, a2)), 180)
    return r1r2_to_sth1sth2(theta1, theta2, r1, r2)

def buoy_spectrum2d(sf,a1,a2,b1,b2, dirs = numpy.arange(0,365,10)):
    """
    purpose: compute spectrum(f,theta) from coef ai,bi
    author: Wang He
    # Maximum entropy method to estimate the Directional Distribution
    
    # Maximum Entropy Method - Lygre & Krogstad (1986 - JPO)
    # Eqn. 13:
    # phi1 = (c1 - c2c1*)/(1 - abs(c1)^2)
    # phi2 = c2 - c1phi1
    # 2piD = (1 - phi1c1* - phi2c2*)/abs(1 - phi1exp(-itheta) -phi2exp(2itheta))^2
    # c1 and c2 are the complex fourier coefficients
    
    # inumpyuts : dirs in degrees
    """
    nfreq = sf.size
    nbin = dirs.size
    
    c1 = a1+1j*b1
    c2 = a2+1j*b2
    p1 = (c1-c2*c1.conjugate())/(1.-abs(c1)**2)
    p2 = c2-c1*p1
    
    # numerator(2D) : x
    x = 1.-p1*c1.conjugate()-p2*c2.conjugate()
    x = numpy.tile(numpy.real(x),(nbin,1)).T
    
    # denominator(2D): y
    a = dirs*numpy.pi/180.
    e1 = numpy.tile(numpy.cos(a)-1j*numpy.sin(a),(nfreq,1))
    e2 = numpy.tile(numpy.cos(2*a)-1j*numpy.sin(2*a),(nfreq,1))
    
    p1e1 = numpy.tile(p1,(nbin,1)).T*e1
    p2e2 = numpy.tile(p2,(nbin,1)).T*e2
    
    y = abs(1-p1e1-p2e2)**2
    
    D = x/(y*2*numpy.pi)
    
    # normalizes the spreading function,
    # so that int D(theta,f) dtheta = 1 for each f  
    
    dth = dirs[1]-dirs[0]
    tot = numpy.tile(numpy.sum(D, axis=1)*dth/180.*numpy.pi,(nbin,1)).T
    D = D/tot
    
    sp2d = numpy.tile(sf,(nbin,1)).T*D
    
    return sp2d,D


def spfphi2kphi(spfphi,f):
    """
    convert sp(f,phi)  -> sp(k,phi)
    """
    k = (2*numpy.pi*f)**2/g
    dk = numpy.zeros(k.size)
    dk[0] = k[1]-k[0]
    dk[-1] = k[-1]-k[-2]
    dk[1:-1] = (k[2:]-k[0:-2])/2.
    
    df = numpy.zeros(f.size)
    df[0] = f[1]-f[0]
    df[-1] = f[-1]-f[-2]
    df[1:-1] = (f[2:]-f[0:-2])/2.
    
    spkphi = []
    for sp in spfphi:
        df2kdk = (numpy.tile(df/(k*dk), (sp.shape[1], 1))).T
        sp2 = sp*df2kdk
        spkphi.append(sp2)
    
    return spkphi,k

def spfphi2kphib(spfphi,f):
    """
    convert sp(f,phi)  -> sp(k,phi)
    """
    k = (2*numpy.pi*f)**2/g
    dk = numpy.zeros(k.size)
    dk[0] = k[1]-k[0]
    dk[-1] = k[-1]-k[-2]
    dk[1:-1] = (k[2:]-k[0:-2])/2.
    
    df = numpy.zeros(f.size)
    df[0] = f[1]-f[0]
    df[-1] = f[-1]-f[-2]
    df[1:-1] = (f[2:]-f[0:-2])/2.
    
    spkphi = []
    for sp in spfphi:
        df2kdk = (numpy.tile(df/(k*dk), (sp.shape[1], 1))).T
        sp2 = sp*df2kdk
        spkphi.append(sp2)
    
    return spkphi,k

def spkphi2fphi(spkphi,k):

    f = numpy.sqrt(k*g)/2/numpy.pi
    
    dk,df = numpy.zeros(k.size),numpy.zeros(f.size)
    dk[0],dk[-1],dk[1:-1] = k[1]-k[0],k[-1]-k[-2],(k[2:]-k[0:-2])/2.
    
    df[0],df[-1],df[1:-1] = f[1]-f[0],f[-1]-f[-2],(f[2:]-f[0:-2])/2.
    
    
    if spkphi.shape[0] == f.size:
        kdk2df = (numpy.tile(k*dk/df, (spkphi.shape[1], 1))).T
    else:
        kdk2df = (numpy.tile(k*dk/df, (spkphi.shape[0],1)))
       
    spfphi = spkphi*kdk2df
    
    return spfphi,f

def spfrom2to(sp_in,phi):
    """
    change convention of the direction (from -> to) 
    """
    phi2 = (180.+phi)%360
    ind = numpy.argsort(phi2)
    
    sp_out = []
    for sp in sp_in:
        sp_out.append(sp[:,ind])
    
    return sp_out

def th2ab(theta1,theta2,stheta1,stheta2):
    """
    # convert theta & stheta(globwave data used) to a1,b1,a2,b2
    """
    r1 = 1-0.5*(stheta1/180.*numpy.pi)**2
    r2 = 1-2.0*(stheta2/180.*numpy.pi)**2
    a1 = r1*numpy.cos(theta1/180.*numpy.pi)
    b1 = r1*numpy.sin(theta1/180.*numpy.pi)
    
    a2 = r2*numpy.cos(2*theta2/180.*numpy.pi)
    b2 = r2*numpy.sin(2*theta2/180.*numpy.pi) 
    
    return a1,a2,b1,b2

def ar2ab(alpha1,alpha2,r1,r2):
    """
    # convert alpha & r(ndbc data used) to a1,b1,a2,b2
    """
    a1 = r1*numpy.cos(alpha1/180.*numpy.pi)
    b1 = r1*numpy.sin(alpha1/180.*numpy.pi)
    a2 = r2*numpy.cos(2*alpha2/180.*numpy.pi)
    b2 = r2*numpy.sin(2*alpha2/180.*numpy.pi)
    
    return a1,a2,b1,b2   

KEY_VARIABLES = ['wave_frequency_spectrum_spectral_density','wave_directional_spectrum_spectral_density','significant_wave_height',\
                 'average_wave_period','dominant_wave_direction','dominant_wave_period',\
                 'maximum_wave_height','maximum_wave_steepness','maximum_height_wave_period',\
                 'average_wave_direction','dominant_wave_spreading','wind_wave_significant_wave_height',\
                 'swell_significant_wave_height','wind_wave_average_wave_period','swell_average_wave_period'\
                 'wind_wave_average_wave_direction','swell_average_wave_direction']
DIRECTIONAL_VARIABLES = ['wave_directional_spectrum_spectral_density','dominant_wave_direction','average_wave_direction','dominant_wave_spreading','wind_wave_average_wave_direction','swell_average_wave_direction']

