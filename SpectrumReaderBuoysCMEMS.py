import abc
import numpy as np
import netCDF4 as nc
from dateutil import parser
from netCDF4 import Dataset
import Spectrum as spc
import logging
import spectrumUtil as spUtil
import os


class AbstractSpectrumReader() :
    __metaclass__ = abc.ABCMeta

    @abc.abstractmethod
    def read_spectrum ( ncfile,mode="satfull",index=None ) :
        pass



class SpectrumReaderBuoyCMEMS(AbstractSpectrumReader) :
    @staticmethod
    def read_spectrum ( ncfile,mode="buoycmems",index=None,dataName='UNK') :
        """
        Function that return a spectrum object from a netcdf file
        ncf: Net CDF File already open
        path: The path to access the file

        NB: If path and ncf are given priority is given to the path

        """
        if isinstance(ncfile,str) :
            fileToRead = Dataset(ncfile,'r')
        else :
            fileToRead = ncfile
        phi = np.arange(0,350,10)
        mywind = SpectrumReaderBuoyCMEMS.read_wind_cmems(fileToRead,index)
        mywsys = SpectrumReaderBuoyCMEMS.read_wsys_cmems(fileToRead,index)
        myspec_data = SpectrumReaderBuoyCMEMS.read_spec_data_cmems(fileToRead,index,mode,phi=phi)
        mymeta = SpectrumReaderBuoyCMEMS.read_meta_cmems(fileToRead,index,mode)

        if isinstance(ncfile,str) :
            fileToRead.close()
        try:
            dataName = os.path.basename(ncfile)
        except:
            dataName = ncfile.getncattr('wmo_platform_code')
        return spc.Spectrum(myspec_data,mywsys,mywind,mymeta, dataName=dataName)

    @staticmethod
    def read_wind_cmems ( ncf,index ) :
        wind = {}

        return wind

    @staticmethod
    def read_wsys_cmems ( ncf,index ) :
        lon,lat = None,None
        lat = np.squeeze(ncf.variables['LATITUDE'][index])
        lon = np.squeeze(ncf.variables['LONGITUDE'][index])
        wsys = {'lon' : lon,'lat' : lat}

        return wsys

    @staticmethod
    def read_spec_data_cmems ( ncf,index,mode='buoycmems',phi=np.arange(0,350,10),con='to' ) :
        logging.debug('index: %s %s',index,type(index))
        time = nc.num2date(ncf.variables['TIME'][:],ncf.variables['TIME'].units)
        time = time[int(index)]
        logging.debug('time: %s',time)
        logging.debug('vars: %s',ncf.variables.keys())
        logging.debug('theta1: %s',ncf.variables['THETA1'])
        logging.debug('theta1: %s',ncf.variables['THETA1'][:])
        c1 = ncf.variables['THETA1'][index,:]  # theta1
        c2 = ncf.variables['THETA2'][index,:]  # theta2
        c3 = ncf.variables['STHETA1'][index,:]  # stheta1
        c4 = ncf.variables['STHETA2'][index,:]  # stheta2

        # convert theta & stheta to a1,b1,a2,b2
        a1,a2,b1,b2 = spUtil.th2ab(c1,c2,c3,c4)
        if mode == "buoycmems" :
            f,sf = SpectrumReaderBuoyCMEMS.read_spec_data_ndbc_cmems(ncf,index)
        else :
            f,sf = SpectrumReaderBuoyCMEMS.read_spec_data_cdip_cmems(ncf,index)

        spfphitmp,d = spUtil.buoy_spectrum2d(sf,a1,a2,b1,b2,dirs=phi)
        spfphi = [spfphitmp]
        # spfphi2kphi wait an array of values

        sp2d,k = spUtil.spfphi2kphi(spfphi,f)

        if con == 'to' :
            pass
            #sp2d = spUtil.spfrom2to(sp2d,phi)
        # sp2d is an array of values : so we use the first and only element
        spec_data = spc.Spec_data(k=k,phi=phi,sp=sp2d[0],start_time=time,)

        return spec_data

    @staticmethod
    def read_spec_data_ndbc_cmems ( ncf,index ) :
        #f = np.squeeze(ncf.variables['FREQUENCY'][index,:])
        f = np.squeeze(ncf.variables['FREQ'][index,:]) #fix SISMER 2021 to have xarray compliant format
        sf = ncf.variables['VSPEC1D'][index,:]
        return f,sf

    @staticmethod
    def read_spec_data_cdip_cmems ( ncf,index ) :
        f = np.squeeze(ncf.variables['central_frequency'][index,:])
        sf = ncf.variables['sea_surface_variance_spectral_density'][index,:]
        return f,sf

    @staticmethod
    def read_meta_cmems ( ncf,index,mode='buoycmems' ) :

        if mode == "buoycmems" :
            mymeta = spc.Meta(type=mode)
        else :
            mymeta = SpectrumReaderBuoyCMEMS.read_meta_cdip_cmems(ncf,index,mode)
        return mymeta

    @staticmethod
    def read_meta_cdip_cmems ( ncf,index,mode ) :
        meta = {}
        meta['type'] = mode
        meta['wp'] = ncf.variables['dominant_wave_period'][index]
        meta['dirp'] = np.squeeze(ncf.variables['dominant_wave_direction'][index])
        meta['hst'] = np.squeeze(ncf.variables['significant_wave_height'][index])

        return meta

    @staticmethod
    def read_wsys_ndbc ( ncf,index ) :
        lon,lat = None,None
        lat = np.squeeze(ncf.variables['LATITUDE'][index])
        lon = np.squeeze(ncf.variables['LONGITUDE'][index])
        wsys = {'lon' : lon,'lat' : lat}

        return wsys
