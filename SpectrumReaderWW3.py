# coding: utf-8

import abc
import numpy as np
import os
os.environ['HDF5_USE_FILE_LOCKING'] = 'FALSE' # to fix issue when reading nc files on /home/ref-marc
import netCDF4 as nc
from dateutil import parser
from netCDF4 import Dataset
import Spectrum as spc
import logging
import spectrumUtil as spUtil
import os
import traceback

try:
    from get_ww3grid_fromS1measu_L2_fullpath import get_path_ww3grid,get_closest_gridpoint  # colocation
    from reference_oswk import reference_oswK_954m_60pts
    from ocean_wave_spectrum_partitioning_IPF_proto import partition_spectrum #quality
    from interpolation_polar_spectrum import interp_polar_spectrum #already in mpc git qualitycheck
    import interpolation_polar_spectrum
    from reconstruct_2D_partition_label_matrix import reconstruct_ww3_partition_mask_from_partitioning #quality
    from convert_partitioning_output_2_wsys_polar_spectral_plot import convert_partitioning_output_2_wsys,convert_partitioning_output_2_wsys_fromxa_struct
    from partition_wv_xassignment import compute_ww3_partition_params
    from get_ww3spectra_file_from_l2measu import get_l2ocn_from_ww3spectra
    from get_oswl2ocn_param import get_oswl2ocn_param #data_collect
    # from get_ww3grid_fromS1measu_L2_fullpath import get_partitions_params_ww3grid,get_path_ww3grid,get_closest_gridpoint
    flag_partitioning = True
except:
    print('impossible to load librairies for partitioning in the WW3 spectra such as ocean_wave_spectrum_partitioning_IPF_proto')
    print('traceback',traceback.format_exc())
    print('this traceback is only warning (only matters if we consider reading WW3 spectra)')
    flag_partitioning=False
class AbstractSpectrumReader():
    
    __metaclass__ = abc.ABCMeta
    
    @abc.abstractmethod
    def read_spectrum(self,ncfile,mode="satfull",index=None):
        pass

#-----------------------------------------------
# Reader for WW3 File 
#-----------------------------------------------

class SpectrumReaderWw3(AbstractSpectrumReader):

#   @staticmethod
    def read_spectrum(self,ncf=None,mode='ww3',dataName='UNK',filepath=None):
        """
        read_spectrum_ww3: Function that return a spectrum object from a WW3 prevision file
        ncf: Net CDF File already open
        path: The path to access the file

        NB: If path and ncf are given priority is given to the path

        examples:

        ex1:
        nc1 = Dataset('path/to/ncfile.nc', 'r')
        myspectrum = read_spectrum_ww3(nc1)
        nc1.close()

        ex2:
        myspectrum = read_spectrum_ww3(path='path/to/ncfile.nc')

        """
        if filepath is not None:
            self.fullpath = filepath
        else:
            self.fullpath = None

        myspec_data = self.read_spec_data(ncf)
        mywsys = self.read_wsys(ncf)
        self.mywsys = mywsys
        mywind = self.read_wind(ncf)
        mymeta = self.read_meta(ncf,mode)
#         myspec_data = SpectrumReaderWw3.read_spec_data(ncf)
#         mywsys = SpectrumReaderWw3.read_wsys(ncf)
#         mywind = SpectrumReaderWw3.read_wind(ncf)
#         mymeta = SpectrumReaderWw3.read_meta(ncf,mode)
        return spc.Spectrum(myspec_data,mywsys,mywind,mymeta,dataName=dataName)
   
#     @staticmethod
    def read_wsys(self,ncf):
        wsys = {}
        # wsys['lon'] = ncf.variables['lon'][:]
        # logging.debug('SpectrumReaderWw3 | wsys["lon"] = %s',wsys['lon'])
        wsys['lon'] = self.lon
        # wsys['lat'] = ncf.variables['lat'][:]
        wsys['lat'] = self.lat
        if self.tmp_wsys != {}:
            wsys['hs'] = self.tmp_wsys['hs']
            wsys['wl'] = self.tmp_wsys['wl']
            wsys['dirad'] = self.tmp_wsys['dirad']
        wsys['hsgrid'] = self.hsgrid
        return wsys
    
    
#     def do_partitioning(ncf):

#         return complet_partitions_sorted,wave_integrated_values
    
#     @staticmethod
    def read_wind(self,ncf):
        """
        since Mickael Accensi wind in colocated WW3 spectra a in FROM convention 'example: +90° mean coming from east'
        #which is the meteorological convention also used in the new IPF2.90 since 13march2018
        """
        wind={}
        wind['U'] = ncf.variables['wind_speed_model_u'][:]
        wind['U'] = wind['U'][0]
        wind['V'] = ncf.variables['wind_speed_model_v'][:]
        wind['V'] = wind['V'][0]
        speed = np.sqrt(wind['U']**2+wind['V']**2)
#         if True:#patch agrouaze Sept 2019
#             wind['U'] = -wind['U']
#             wind['V'] = -wind['V']
        if True:#patch agrouaze Sept 2019: (90-val+180)%360 correction sans explication qui vient de tests sur  http://grougrou1.ifremer.fr:8888/notebooks/workspace_leste/cfosat_perso/verif_windir_computation.ipynb#
#             wdir = (90-np.arctan2(wind['U'],wind['V']) * 180 / np.pi + 180.) % 360
            wdir = (np.arctan2(wind['V'],wind['U']) * 180 / np.pi + 180.) % 360 #en inversant V et U comme dans la doc numpy on retombe bien sur la bonne direction
            #Il faut mettre le V d abord (ie la composent meridional) et en second le U (ie composante zonale)
            wind['V'] = speed*np.cos(np.radians(wdir))
            wind['U'] = speed*np.sin(np.radians(wdir))
        else:
            wdir = (np.arctan2(wind['U'],wind['V']) * 180 / np.pi) % 360
        wind['dir'] = wdir
        return wind

#     @staticmethod
    def read_meta(self,ncf,mode):

        meta={}
        meta['type'] = mode
        return meta

#     @staticmethod
    def read_spec_data(self,ncf):
        ww3gridseek = True
        my_data = {}
        my_data['k'] = ncf.variables['k'][:]
        my_data['phi'] = ncf.variables['phi'][:]
        self.lon  =  float(ncf.variables['lon'][:])
        self.lat =  float(ncf.variables['lat'][:])
        area = ncf.variables['area'][:]
        #to look exactly to the spectra I X assign with S1 WV I want to apply also the interpolation step (new added Sept 2020)
   
        k_sar = reference_oswK_954m_60pts
        phi_sar = np.arange(0,360,5)
        ww3_spec = ncf.variables['polSpec'][:,:,0]
        ww3_spec_interp = interp_polar_spectrum(k_sar, phi_sar,ww3_spec, my_data['k'], my_data['phi'])
        # add a fix for WW3 spectra that contain value below 0
        ww3_spec_interp = np.ma.masked_where(ww3_spec_interp < 0,ww3_spec_interp,copy=True)
        #ww3_spec_interp = ww3_spec_interp.filled(np.nan)
        my_data['sp'] = ww3_spec_interp
        my_data['k'] = k_sar
        my_data['phi'] = phi_sar
        #previously I was simply looking at the spectra without inteprolation on SAR grid (k,phi)
        #my_data['sp'] = ncf.variables['polSpec'][:].squeeze() #*area added by agrouaze Nov 2019 to have reasonable Hs at the partitioning step
        #partitioning
        #valeur utilisees pour les cross assignment des bulletins mensuels Dec 2019
        noise_thr = 2.
        #noise_thr = 0.1
        npartitions=5
        #npartitions=2
        merge_thr = 4. #
        #merge_thr = 15.00 #
        discard_thr = 0.
        #discard_thr = 0.01
        #test avec une autre valeur de discard
#        merge_thr = 2
#        discard_thr = 8
        # discard_thr = 0.022
        if flag_partitioning:
            #avant sept 2020 les params integraux dans le wavesys etaient directement utilises pour l affichage
            # ces params etaient errones
            # mais apres sept 2020
            #je reprends le partitioning et j utilise compute_ww3_partition_params() pour recalculer de la bonne facon les params integraux
            wavesys = partition_spectrum(my_data['sp'],my_data['k'], my_data['phi'], npartitions=npartitions, twod=True, symphi=False,
                           klow=None, khigh=None,
                           noise_thr=noise_thr, merge_thr=merge_thr,
                           discard_thr=discard_thr, polar=True, alfah=None, merge_close=False,
                           rel_peak_high=False, spec_std0=None, rem=None,area=None)
            #complet_partitions_sorted = reconstruct_ww3_partition_mask_from_partitioning(my_data['sp']*area,wavesys)
            #ici 9sept2020 je pense que pour coller avec la procedure de Xassignment auto avec S1WV
            # il faut utilier la methode
            # compute_ww3_partition_params(option_version,smooth_peak,ww3_part,ww3_spec_interp,npartitions,sar,phi_sar,k_sar,s1a_spec)
            fsar = get_l2ocn_from_ww3spectra(self.fullpath)
            sar = get_oswl2ocn_param(fsar,get_spec=True).copy()
            ww3_struct = compute_ww3_partition_params(option_version='v2',smooth_peak=False,ww3_part=wavesys,ww3_spec_interp=ww3_spec_interp,
                                                      npartitions=npartitions,sar=sar,phi_sar=phi_sar,
                                         k_sar=k_sar,s1a_spec=ww3_spec_interp)#{le s1a_spec argument ne sert a rien sauf a initialiser une variable avec les bonnes dims 72x60}

            complet_partitions_sorted = reconstruct_ww3_partition_mask_from_partitioning(ww3_spec_interp,wavesys)
        #     complet_partitions_sorted = partition_wv_xassignment.reconstruct_ww3_partition_mask_from_partitioning(complet_matrice_sorted,ww3_part)
            #TODO: voir si les params de ww3_struct sont coherent avec les Xassignment automatiques et si oui remplacer wavesys par le contenu de ww3_struct
            # for ppi,ww3Ipart in enumerate(ww3_struct):
            #     print(ppi,'ww3_struct wl_eff %1.1fm'%ww3Ipart.wl_eff)
            #     print(ppi,'ww3_struct dirad_eff %1.1f$^o$'%np.degrees(ww3Ipart.dirad_eff))
            #     print(ppi,'ww3_struct wl %1.1fm' % ww3Ipart.wl)
            #     print(ppi,'ww3_struct dirad %1.1f$^o$' % np.degrees(ww3Ipart.dirad))
            #     print(ppi,'ww3_struct wl_part %1.1fm' % ww3Ipart.wl_part)
            #     print(ppi,'ww3_struct dirad_part %1.1f$^o$' % np.degrees(ww3Ipart.dirad_part))
                # for vv in ww3Ipart:
                #     print(vv,ww3Ipart[vv])
            #if False:
            #    wave_integrated_values = convert_partitioning_output_2_wsys(wavesys)
            #else:
            wave_integrated_values = convert_partitioning_output_2_wsys_fromxa_struct ( ww3_struct )
            # end partitioning
            my_data['part'] = complet_partitions_sorted
        else:
            logging.info('no partitioning for WW3 spectra')
            wave_integrated_values = {}


        
        t = ncf.variables['time'][:]
        logging.info('time WW3 %s',t)
        u = ncf.variables['time'].units
        my_data['start_time'] = nc.num2date(t,u)
        my_data['start_time'] = my_data['start_time'][0]
        logging.debug('start_time %s',my_data['start_time'])
        t = ncf.variables['sar_time'][:]
        u = ncf.variables['sar_time'].units
        my_data['sar_time'] = nc.num2date(t,u)
        my_data['sar_time'] = my_data['sar_time'][0]
#         my_data['tmp_wsys'] = wave_integrated_values
        logging.debug('start_time %s', my_data['start_time'])
        #add hs from closest ww3 grid point
        if self.fullpath is not None:
            res_file = get_path_ww3grid(measuL2WV_fullpath = self.fullpath)
            try:
                handler_ww3gridfile = Dataset(res_file)
                idlat,idlon = get_closest_gridpoint(self.lon,self.lat,handler_ww3gridfile)
                my_data['hsgrid'] = handler_ww3gridfile.variables['hs'][0,idlat,idlon]

                handler_ww3gridfile.close()
            except:
                logging.info('impossible to read %s (%s)',res_file,traceback.format_exc())
                my_data['hsgrid'] = np.nan
        else:
            my_data['hsgrid'] = np.nan
        logging.debug('ww3 hs from grid = %sm',my_data['hsgrid'])
        self.tmp_wsys = wave_integrated_values
        self.hsgrid = my_data['hsgrid']
        return my_data
#-----------------------------------------------
# End of Reader for WW3 File 
#-----------------------------------------------




