import abc
import datetime
import numpy as np
from dateutil import parser
from netCDF4 import Dataset
import Spectrum as spc
import logging
import spectrumUtil as spUtil
import os

class AbstractSpectrumReader():
    
    __metaclass__ = abc.ABCMeta
    
    @abc.abstractmethod
    def read_spectrum(ncfile,mode="satfull",index=None):
        pass



#-----------------------------------------------
# Reader for Sat File 
#-----------------------------------------------

class SpectrumReaderSat(AbstractSpectrumReader):

    def read_spectrum(self,ncf=None,mode='satfull',dataName='UNK',filepath=None):
        """
        read_spectrum_sat: Function that return a spectrum object from a satellite file
        ncf: Net CDF File already open
        mode: The mode of data satellite:
                satfull: Full data
                satreal: Real Part
                satimaginary: Imaginary Part
        path: The path to access the file

        NB: If path and ncf are given priority is given to the path

        examples:

        ex1:
        nc1 = Dataset('path/to/ncfile.nc', 'r')
        myspectrum = read_spectrum_sat(nc1,mode="satfull")
        nc1.close()

        ex2:
        myspectrum = read_spectrum_sat(path='path/to/ncfile.nc',mode="satimaginary")

        """
        self.filepath = filepath
        #index is useless for now
        myspec_data = self.read_spec_data(ncf,mode)
        mywsys = self.read_wsys(ncf)
        mywind = self.read_wind(ncf)
        logging.debug('read_spectrum | mywind = %s',mywind)
        mymeta = self.read_meta(ncf,mode)

        return spc.Spectrum(myspec_data,mywsys,mywind,mymeta,dataName=dataName)
     

    def read_spectrum_list(self,ncf=None):
        """
        read_spectrum_list_sat: Function that return an array with the spectrum objects from a satelite file
        it will return the following:
            - Imaginary part
            - Real Part
            - Full
        ncf: Net CDF File already open or fil path


        examples:

        ex1:
        nc1 = Dataset('path/to/ncfile.nc', 'r')
        myspectra = read_spectrum_list_sat(nc1)
        nc1.close()

        ex2:
        myspectrum = read_spectrum_list_sat(path='path/to/ncfile.nc')

        """
        spectrumdict={}
        modes = ['satfull','satreal','satimaginary']
        if isinstance(ncf, str):
            self.handler = Dataset(ncf, 'r')
            print('debug SpectrumReaderSentinel file',ncf)
            self.filepath = ncf 
        else:
            self.handler = ncf

        for mode in modes:
            myspectrum = self.read_spectrum(ncf=self.handler,mode=mode)
#             myspectrum = SpectrumReaderSat.read_spectrum(ncf=fileToRead,mode=mode,dataName=dataName)
            spectrumdict[mode] = myspectrum
        if isinstance(self.handler, str):
            self.handler.close()
        return spectrumdict
    
    @staticmethod
    def read_wsys(ncf):
        """
        read_wsys_sat: Function that return a wave system dictionary from a sat file
        """
        wsys = {}
        wsys['hs'] = ncf.variables['oswHs'][:][0][0]
        wsys['wl'] = ncf.variables['oswWl'][:][0][0]
        wsys['dirad'] = ncf.variables['oswDirmet'][:][0][0]*np.pi/180.
        wsys['lon'] = ncf.variables['oswLon'][:][0][0]
        wsys['lat'] = ncf.variables['oswLat'][:][0][0]
        if 'oswQualityFlagPartition' in ncf.variables:
            wsys['qcpartition'] = ncf.variables['oswQualityFlagPartition'][:][0][0]
        else:
            wsys['qcpartition'] = np.ones((5,))*np.nan
        # logging.debug('qc partition = %s',wsys['qcpartition'])
        return wsys

    def read_wind(self,ncf):
        """
        read_wind_sat: Function that return a wind dictionary from a sat file
        oswWindDirection:long_name = "SAR Wind direction (meteorological convention=clockwise direction from where the wind comes respect to North)"
        note: avant IPF 2.91 la convention etait erronee
        """
        wind={}
#         wind['U'] = ncf.variables['oswWindSpeed'][:][0][0]*np.cos(90- ncf.variables['oswWindDirection'][:][0][0]*np.pi/180.) #original version from MLB ?
#         wind['V'] = ncf.variables['oswWindSpeed'][:][0][0]*np.sin(90- ncf.variables['oswWindDirection'][:][0][0]*np.pi/180.)
#         oswWindDirection:long_name = "SAR Wind direction (meteorological convention=clockwise direction from where the wind comes respect to North)"
        dirwind = ncf.variables['oswWindDirection'][:][0][0] # note: oswWindDirection exactly equal to oiswWindDirection
        if self.filepath is None:
            try:
                datedt = datetime.datetime.strptime(ncf.getncattr('firstMeasurementTime'),'%Y-%m-%dT%H:%M:%S.%fZ')
                
            except:
                datedt = datetime.datetime.strptime(ncf.getncattr('firstMeasurementTime'),'%Y-%m-%dT%H:%M:%SZ')
        else:
            datedt = datetime.datetime.strptime(os.path.basename(self.filepath).split('-')[4],'%Y%m%dt%H%M%S')
        if datedt < datetime.datetime(2018,3,13) :  # date of convention change with IPF 2.90
            value = (ncf.variables['oswWindDirection'][0][
                         0].squeeze() + 180.) % 360.  # to have a value in the TO convention
        else :
            value = ncf.variables['oswWindDirection'][0][0].squeeze()
        logging.debug('ESA direction given in convention FROM %s',dirwind)
        dirwind = np.mod(dirwind+180,360)
        logging.debug('direction given in convention TO %s',dirwind)
        # if False:
        #     wind['U'] = ncf.variables['oswWindSpeed'][:][0][0]*np.cos(np.radians(dirwind)) #correction A. Grouazel Sept 2019
        #     wind['V'] = ncf.variables['oswWindSpeed'][:][0][0]*np.sin(np.radians(dirwind))
        # else:
        if 'oswEcmwfWindSpeed' not in ncf.variables.keys():
            variable_wind_speed = "oswWindSpeed" #because on old IPF version there was no oswEcmwfWindSpeed variable
        else:
            variable_wind_speed = 'oswEcmwfWindSpeed' #change asked by A.Mouche June 2020 to align filters in xwaves web app and png generated/displayed
        wind['U'] = ncf.variables[variable_wind_speed][:][0][0]*np.cos(np.pi/2-np.radians(dirwind)) #correction A. Grouazel Sept 2019
        wind['V'] = ncf.variables[variable_wind_speed][:][0][0]*np.sin(np.pi/2-np.radians(dirwind))
        wind['dir'] = dirwind
        wind['varname'] = variable_wind_speed
        logging.debug('u = %s v = %s',wind['U'],wind['V'])
        return wind


    @staticmethod
    def read_meta(ncf,mode):
        meta={}
        meta['type'] = mode
        meta['nv']  = ncf.variables['oswNv'][:][0][0]
        meta['snr'] = ncf.variables['oswSnr'][:][0][0]
        meta['azc'] = ncf.variables['oswAzCutoff'][:][0][0]
        meta['inc'] = ncf.variables['oswIncidenceAngle'][:][0][0]
        meta['nrcs'] = ncf.variables['oswNrcs'][:][0][0]
        meta['tra'] =  ncf.variables['oswHeading'][:][0][0]

        return meta


    @staticmethod
    def read_spec_data(ncf,mode):
        """
        Function getting spectrum data from a netcdf4 Object

        ncf: NetCDF4 open object
        mode: Type of data. help determine how to parse to get the data
            satimaginary,satfull,satreal:Fichier Satelite 
                satimaginary: imaginary part
                satreal: real part
                satfull: full
            w:Fichier WW3

        """
        my_data = {}
        my_data['part'] = ncf.variables['oswPartitions'][:][0][0]
        my_data['k'] = ncf.variables['oswK'][:]
        my_data['phi'] = ncf.variables['oswPhi'][:]

#         print('mode1',mode,my_data['phi'],type(my_data['phi']))
        if isinstance(my_data['phi'],np.ma.core.MaskedArray) or True: #je ne comprend pas a quoi sert ce test...
            my_data['phi'] = np.ma.array(my_data['phi'])
            my_data['phi'].mask = np.zeros(my_data['phi'].shape)
            #special patch for oswK [agrouaze 11/06/2016]
            # (values set to the fill value do not have the attribute fill=True thus they are not retrieved as masked values)
            my_data['k'] = np.ma.masked_where(my_data['k']>=9.96920e+36, my_data['k'], copy=False)
            if mode == "satfull":
                tmptmp =ncf.variables['oswPolSpec'][:][0][0]
                my_data['sp'] = np.ma.masked_where(tmptmp >= 10000,tmptmp,copy = False)
            if not isinstance(my_data['k'].mask,np.ndarray):
                my_data['k'].mask = np.zeros(my_data['k'].shape)

                kmask = my_data['k'].mask==False
                phimask = my_data['phi'].mask==False
                logging.debug('phimask %s',phimask.shape)
                logging.debug('kmask %s',kmask.shape)

                my_data['k'] = my_data['k'][kmask]
                my_data['phi'] = my_data['phi'][phimask]
                my_data['part'] = my_data['part'][phimask,:]
                my_data['part'] = my_data['part'][:,kmask]

                if mode=="satfull":
                    my_data['sp'] = my_data['sp'][phimask,:]
                    my_data['sp'] = my_data['sp'][:,kmask]
                    logging.debug('sp %s',my_data['sp'].shape)

                if mode == "satreal":
                    my_data['sp'] = ncf.variables['oswQualityCrossSpectraRe'][:][0][0]
                    my_data['sp'] = my_data['sp'][phimask,:]
                    my_data['sp'] = my_data['sp'][:,kmask]
                if mode == "satimaginary":
                    my_data['sp'] = ncf.variables['oswQualityCrossSpectraIm'][:][0][0]
                    my_data['sp'] = my_data['sp'][phimask,:]
                    my_data['sp'] = my_data['sp'][:,kmask]
                #recuperation sar_start_time
                my_data['start_time'] = parser.parse(ncf.getncattr('firstMeasurementTime'))
        else:
            logging.info('rien')
        return my_data    
#-----------------------------------------------
# End of Reader for Sat File 
#-----------------------------------------------
