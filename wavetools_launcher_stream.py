import matplotlib
matplotlib.use('Agg')
from SpectrumReader_V2 import SpectrumReader
import logging
import sys
import traceback
import os, socket
from datetime import datetime
import collections
import time
import traceback
import resource
if __name__ == "__main__":
    root = logging.getLogger()
    if root.handlers:
        for handler in root.handlers:
            root.removeHandler(handler)
    import argparse
    parser = argparse.ArgumentParser(description='generate png spectra')
    parser.add_argument('--verbose', action='store_true',default=False)
    parser.add_argument('--overwrite', action='store_true',default=False,help='overwrite existing png')
    parser.add_argument('--style',action='store',default='v2',help='version of spectra representation v2 (mpl contourf) or v3 (xarray scatter)')
    args = parser.parse_args()
    if args.verbose:
        logging.basicConfig(level=logging.DEBUG,format='%(asctime)s %(levelname)-5s %(message)s',
                    datefmt='%d/%m/%Y %H:%M:%S')
    else:
        logging.basicConfig(level=logging.INFO,format='%(asctime)s %(levelname)-5s %(message)s',
                    datefmt='%d/%m/%Y %H:%M:%S')
    year = str(datetime.now().year)
    month = str(datetime.now().month).zfill(2)
    day = str(datetime.now().day).zfill(2)
    #filesinerror = '/home/datawork-cersat-public/project/mpc-sentinel1/analysis/s1_data_analysis/L2_V2/wavetools_error/' + year + month + day + '_filesinerror_' + socket.gethostname() + '_' + str(os.getpid()) + '.log'
    filesinerror = '/home1/scratch/satwave/L2_V2/wavetools_error/' + year + month + day + '_filesinerror_' + socket.gethostname() + '_' + str(
        os.getpid()) + '.log'
    if not os.path.exists(os.path.split(filesinerror)[0]):
        os.makedirs(os.path.split(filesinerror)[0])
    root = logging.getLogger()
    if root.handlers:
        for handler in root.handlers:
            root.removeHandler(handler)
    nbsuccess = 0
    nberror = 0
    logging.debug('start plot')
    cpt = collections.defaultdict(int)
    t0 = time.time()
    for line in sys.stdin:
        cpt['treated_line'] += 1
        if cpt['treated_line']%100==0:
            elsaped = time.time()-t0
            logging.info('elapsed time %1.1f seconds line=%s success=%s errors=%s',elsaped,cpt['treated_line'],nbsuccess,nberror)
        logging.debug('line: %s',line)
        #Ignore the first line of errors files
        if not line.startswith("FileResume:"):
            try:
                linecontent = line.split()
                input_file = linecontent[0]
                output_file = linecontent[1]  # the file to create S1A_WV_OCN__2S/
                mode = linecontent[2]
                output_dir, filename = os.path.split(output_file) 
                output_file, extension = os.path.splitext(output_file)
                if not os.path.exists(output_dir):
                    os.makedirs(output_dir, 0o0775)
                if os.path.exists(output_file+'.png') and args.overwrite:
                    os.remove(output_file+'.png')
                instance = SpectrumReader()

                currentSpectrum = instance.read_spectrum(input_file, mode)
                status = currentSpectrum.write(output_file+'.png',version=args.style) #add .png at the end agrouaze august 2018 fix missing .png at the end of the filenames
                cpt[status] += 1
                nbsuccess += 1
            except Exception as e:
                logging.error('exception occured:')
                logging.error(traceback.format_exc())
                filewriter = open(filesinerror, 'a')
                filewriter.write(line)
                filewriter.close()
                logging.error("problem generate spectrum for:" + input_file)
                nberror += 1
    logging.info('counter %s',cpt)
    logging.info("{} generated successfully, {} not generated".format(nbsuccess, nberror))
    logging.info('all product in error can be find in %s',filesinerror)
    logging.info('peak memory usage: %s kilobytes',resource.getrusage(resource.RUSAGE_SELF).ru_maxrss)