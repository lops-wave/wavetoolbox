"""
code adapted from plot_spec_V2.py
use xarray to plot the spectra as a pcolormesh or scatter rather than contourf

"""
import os
import traceback
import numpy as np
import matplotlib.pyplot as plt
import matplotlib as mpl
from mpl_toolkits.axes_grid1 import make_axes_locatable
import itertools
import xarray
import pdb
import logging
import matplotlib.patheffects as path_effects
from add_range_azimuth_axis import add_labeled_image_axis
from add_azimuth_cutoff_lines_on_polar_spec_fig import add_azimuth_cutoff_lines
#Function that create a colorMap from a rgb file
def getColorMap( rgbFile = "medspiration.rgb" ):
    '''
    Load a RGB palette provided in ascii file
    '''
    colors = []
    nbCol=0
    for line in open( rgbFile ):
        r,g,b = [int(c) for c in line.split()]
        colors.append( [r/255.,g/255.,b/255.] )
        nbCol += 1
    return( mpl.colors.ListedColormap(colors, name="custom", N=nbCol) )

cdict = {'red': ((0., 1, 1),
                 (0.05, 1, 1),
                 (0.11, 0, 0),
                 (0.66, 1, 1),
                 (0.89, 1, 1),
                 (1, 0.5, 0.5)),
         'green': ((0., 1, 1),
                   (0.05, 1, 1),
                   (0.11, 0, 0),
                   (0.375, 1, 1),
                   (0.64, 1, 1),
                   (0.91, 0, 0),
                   (1, 0, 0)),
         'blue': ((0., 1, 1),
                  (0.05, 1, 1),
                  (0.11, 1, 1),
                  (0.34, 1, 1),
                  (0.65, 0, 0),
                  (1, 0, 0))}

my_cmap = mpl.colors.LinearSegmentedColormap('my_colormap',cdict,256)

ccc = ['yellow','black','red','blue','green','orange']

#initialize the spectrum
def init_sp(k,phi,spec):
    #logging.debug('k %s phi %s spec %s',k.shape,phi.shape,spec.shape)
    if phi is not None and np.shape(spec)[0]!=len(phi):
        spec = spec.T

    # # adding 360deg
    if True: #agrouaze test for cfosat L2 cwwic nov 2017
        ind = np.argsort(phi)
        phi = np.sort(phi)    
        phi = np.append(phi,360)
        sp = spec[ind,:]
        sp = np.concatenate((sp,sp[0:1,:]), axis=0)
        sp = np.ma.filled(sp,fill_value=np.nan)#to avoid masked array in IPF 002.90 S1 products
    else:
        logging.debug('on fait pas de manip sur le spectre')
        sp = spec

    # converting the direction to rad
    phi = phi/180.0*np.pi

    radius,thetas = np.meshgrid(k,phi)
    return thetas,radius,sp

#Draw the cardinal directions and the main circle
def init_ax(ax,title_font_size,title):
    ax.set_title(title,fontsize=title_font_size)
    logging.debug('set fontitle %s %s',title,title_font_size)
    # Go clockwise
    ax.set_theta_direction(-1)
    # Start from the top
    ax.set_theta_offset(np.pi/2)

    ax.set_aspect('equal', adjustable='box', anchor='C')
    ax.radii_angle = 90. # position of label name on radial axis
    ax.theta_angles = np.arange(0, 360, 90)
    ax.theta_labels = ['N', 'E', 'S', 'W']
    try:
        ax.set_thetagrids(angles=ax.theta_angles, labels=ax.theta_labels,frac=1.05)
    except:
        ax.set_thetagrids(angles=ax.theta_angles, labels=ax.theta_labels)
    ax.set_axis_off
    ax.set_frame_on(False)
    ax.xaxis.set_visible(True)

    return ax     

#Draw radial circle  that will help user to estimate value
def circle_plot(ax,r,freq=0):
    # Radial Circles and their label
    theta = 2*np.pi*np.arange(360)/360.
    labels = []

    if freq == 0:
        for i in r:
            plt.plot(theta, np.arange(360)*0 + 2*np.pi/i,'--k')
            labels.append(str(i)+' m')
        ax.set_rgrids([2*np.pi/i for i in r], labels=labels, angle=45.)

    if freq == 1:
        for i in r:
            plt.plot(theta, np.arange(360)*0 + np.sqrt(2*np.pi/i*9.81/(2*np.pi)**2),'--k')
            labels.append(str(i)+' m')
        ax.set_rgrids([np.sqrt(2*np.pi/i*9.81/(2*np.pi)**2) for i in r], labels=labels, angle=45.)
        
#Show the real axes of the drawing
#used with satellite
def AzRaAx_add(ax,tra,cut_off,meta=None):
    if tra is None:
        return
    ax.plot([(tra-180)*np.pi/180.,(tra)*np.pi/180.],[2*np.pi/cut_off,2*np.pi/cut_off],zorder=100000,color='black',lw=1.5)
    ax.plot([(tra-90)*np.pi/180.,(tra+90)*np.pi/180.],[2*np.pi/cut_off,2*np.pi/cut_off],zorder=100000,color='black',lw=1.5)
    ax.annotate('Range',xy=((90+tra+2)*np.pi/180.,2*np.pi/cut_off),color='black',zorder=1000000)
    ax.annotate('Azimuth',xy=((tra-2)*np.pi/180.,2*np.pi/cut_off),color='black',zorder=1000000)

    if meta.azc is not None:
        R = 2*np.pi/cut_off
        Azc = 2*np.pi/meta.azc
        ttt = tra-180.
        theta0 = np.arcsin(Azc/R)
        theta1 = (np.pi/2.)-theta0
        theta2 = -theta1
        theta3 =  theta1 + ttt*np.pi/180.
        theta4 = -theta1 + ttt*np.pi/180.
        ax.plot([theta3,theta4],[2*np.pi/cut_off,2*np.pi/cut_off],zorder=100000,color='grey',lw=1.5,alpha=0.5)

        theta0 = np.arcsin(Azc/R) 
        theta1 = (np.pi/2.)+theta0
        theta2 = -theta1 
        theta3 =  theta1 + ttt*np.pi/180.
        theta4 = -theta1 + ttt*np.pi/180.
        ax.plot([theta3,theta4],[2*np.pi/cut_off,2*np.pi/cut_off],zorder=100000,color='grey',lw=1.5,alpha=0.5)

def ax_invisible(ax):
    """
    hide axes and contour
    :param ax:
    :return:
    """
    ax.set_frame_on(False)
    ax.xaxis.set_visible(False)
    ax.yaxis.set_visible(False)     

#add to the figure the spectrum
def add_wsys_contour_sp(ax_plot,k,phi,spi,cut_off,c_filled = '0.75'):    
    # boundary of each wave system

    sys = np.zeros_like(spi.sp,dtype = np.float)           
    sys[spi.sp>0]=1   

    thetas, radius, sys2 = init_sp(k,phi,sys)

    ax_plot.contour(thetas, radius, sys2,colors=c_filled,levels = [0],linewidths=2)
    
    ax_plot.set_rmax(rmax=2*np.pi/cut_off)     

def add_wsys_label(ax_legend,wsys,j,c_txt='k',x_txt = 0,fs_legend = 12,hs = 1,meta=None):
    """
    Legend of each wave system
    """


    try:
        _Hs  = 'Hs'  + ': {:3.2f}'.format(wsys.hs[j]) + ' m'
        ax_legend.annotate(_Hs,xy=(x_txt, 0.3),color=c_txt, fontsize=fs_legend,xycoords='axes fraction')
    except:
        logging.error('%s',traceback.format_exc())
        pass
    try:
        _Dir = 'Dir' + ': {:3.2f}'.format(wsys.dirad[j]*180.0/np.pi) + ' $^\circ$'
        ax_legend.annotate(_Dir,xy=(x_txt, 0.6),color=c_txt, fontsize=fs_legend,xycoords='axes fraction')    
    except:
        logging.error('%s',traceback.format_exc())
        pass
    try:
        _Wl  = 'Wl'  + ': {:3.2f}'.format(wsys.wl[j]) + ' m'
        ax_legend.annotate(_Wl,xy=(x_txt, 0.9),color=c_txt, fontsize=fs_legend,xycoords='axes fraction')
    except:
        logging.error('%s',traceback.format_exc())
        pass

    if hasattr(wsys,"qcpartition"): #wsys['qcpartition']
        valtmp = wsys.qcpartition[j]

        if np.ma.is_masked(valtmp) or np.isnan(valtmp):
            valtmp2 = 'masked'
        else:
            valtmp2 = qc_vals[valtmp]
        qcval = 'QC : '+valtmp2
        ax_legend.annotate(qcval,xy = (x_txt,1.2),color = c_txt,fontsize = fs_legend,xycoords = 'axes fraction')
    else:
        logging.debug('no QC')

    
    if j==0:
        
        _Lon = 'Lon :???'
        _Lat = 'Lat :???'
        try:
            _Lon = 'Lon : {:3.2f}'.format(float(wsys.lon)) + ' $^o$'
            ax_legend.annotate(_Lon,xy=(x_txt, -0.1),color='k', fontsize=fs_legend,xycoords='axes fraction')
        except:
            pass
        try:
            _Lat = 'Lat : {:3.2f}'.format(float(wsys.lat)) + ' $^o$'
            ax_legend.annotate(_Lat,xy=(x_txt, -0.4),color='k', fontsize=fs_legend,xycoords='axes fraction')
        except:
            pass
        if hasattr(wsys,"hsgrid"):
            hsgridstr = r'$Hs_{grid}$'+' : {:3.2f}'.format(wsys.hsgrid)+' m'
            ax_legend.annotate(hsgridstr,xy = (0.205,-0.1),color = 'k',fontsize = fs_legend,xycoords = 'axes fraction')
        
        if meta.type[0] == 's' or meta.type == 'cfosat':

            try:
                _SnR = 'SnR : {:3.2f}'.format(meta.snr)
                ax_legend.annotate(_SnR,xy=(0.205, -0.1),color='k', fontsize=fs_legend,xycoords='axes fraction')
            except:
                pass
            try:
                _Nv  = 'Nv  : {:3.2f}'.format(meta.nv)
                ax_legend.annotate(_Nv,xy=(0.205, -0.4),color='k', fontsize=fs_legend,xycoords='axes fraction')
            except:
                pass
            try:
                _Nrcs  = 'NRCS  : {:3.2f}'.format(meta.nrcs) + ' dB'
                ax_legend.annotate(_Nrcs,xy=(0.375, -0.1),color='k', fontsize=fs_legend,xycoords='axes fraction')
            except:
                pass
            try:
                _Tra   = 'Track : {:3.2f}'.format(meta.tra) + ' $^o$'
                ax_legend.annotate(_Tra ,xy=(0.375, -0.4),color='k', fontsize=fs_legend,xycoords='axes fraction')
            except:
                pass
            try:
                _Azc  = 'Az. Cut Off : {:3.2f}'.format(meta.azc) + ' m'
                ax_legend.annotate(_Azc,xy=(0.65, -0.1),color='k', fontsize=fs_legend,xycoords='axes fraction')
            except:
                pass
            try:
                _inc  = 'Incidence   : {:3.2f}'.format(meta.inc) + ' $^o$'
                ax_legend.annotate(_inc,xy=(0.65, -0.4),color='k', fontsize=fs_legend,xycoords='axes fraction')
            except:
                pass
    if meta.type[0] == 'b' or meta.type =='ww3':
            try:
                _wp = r'$T_p$'+' : {:3.2f}'.format(meta.wp)+' s'
                ax_legend.annotate(_wp,xy=(0.205, -0.1),color='k', fontsize=fs_legend,xycoords='axes fraction')
            except:
                pass
            try:
                _dirp  = r'$\Phi_p$'+' : {:3.2f}'.format(meta.dirp)
                ax_legend.annotate(_dirp,xy=(0.205, -0.4),color='k', fontsize=fs_legend,xycoords='axes fraction')
            except:
                pass
            try:
                _Nrcs  = r'$Hs_t$'+' : {:3.2f}'.format(meta.hst) 
                ax_legend.annotate(_Nrcs,xy=(0.375, -0.1),color='k', fontsize=fs_legend,xycoords='axes fraction')
            except:
                pass




#add text elements to the legend
#Specific code because generic one will cost too much for no strong added value
qc_vals = {0:'very_good',1:'good',2:'medium',3:'low',4:'poor'}
def add_wsys_label_old(ax_legend,wsys,j,c_txt='k',x_txt = 0,fs_legend = 12,hs = 1,meta=None):
    """

    :param ax_legend:
    :param wsys:
    :param j: indice de la partition (int) 0 a N
    :param c_txt:
    :param x_txt:
    :param fs_legend:
    :param hs: je ne vois pas a quoi ca sert...
    :param meta:
    :return:
    """
    # Legend of each wave system
    
    try:
        _Hs  = 'Hs'  + ': {:3.2f}'.format(wsys.hs[j]) + ' m'
        ax_legend.annotate(_Hs,xy=(x_txt, 0.3),color=c_txt, fontsize=fs_legend,xycoords='axes fraction')
    except:
        _Hs = None
    if hasattr(wsys, 'dirad') and wsys.dirad is not None:
        _Dir = 'Dir' + ': {:3.2f}'.format(wsys.dirad[j]*180.0/np.pi) + ' $^\circ$'
        ax_legend.annotate(_Dir,xy=(x_txt, 0.6),color=c_txt, fontsize=fs_legend,xycoords='axes fraction')    
            
    if hasattr(wsys, 'wl') and wsys.wl is not None:
        _Wl  = 'Wl'  + ': {:3.2f}'.format(wsys.wl[j]) + ' m'
        ax_legend.annotate(_Wl,xy=(x_txt, 0.9),color=c_txt, fontsize=fs_legend,xycoords='axes fraction')
    if hasattr(wsys,"qcpartition"): #wsys['qcpartition']
        qcval = 'QC : '+qc_vals[wsys.qcpartition[j]]
        ax_legend.annotate(qcval,xy = (x_txt,1.2),color = c_txt,fontsize = fs_legend,xycoords = 'axes fraction')
    else:
        logging.info('no QC')
    if j==0:
        
        _Lon = 'Lon :???'
        _Lat = 'Lat :???'
        if hasattr(wsys,'lon') and wsys.lon is not None:
            _Lon = 'Lon : {:3.2f}'.format(float(wsys.lon)) + ' $^o$'
        if hasattr(wsys,'lat') and wsys.lat is not None:
            _Lat = 'Lat : {:3.2f}'.format(float(wsys.lat)) + ' $^o$'
        if hasattr(wsys,"hsgrid"):
            hsgridstr = r'$Hs_{grid}$'+' : {:3.2f}'.format(wsys.hsgrid)
            ax_legend.annotate(hsgridstr,xy = (0.205,-0.1),color = 'k',fontsize = fs_legend,xycoords = 'axes fraction')
        ax_legend.annotate(_Lon,xy=(x_txt, -0.1),color='k', fontsize=fs_legend,xycoords='axes fraction')
        ax_legend.annotate(_Lat,xy=(x_txt, -0.4),color='k', fontsize=fs_legend,xycoords='axes fraction')
        
        if meta.type[0] == 's':
            if hasattr(meta,'snr') and meta.snr is not None:
                _SnR = 'SnR : {:3.2f}'.format(meta.snr)
                ax_legend.annotate(_SnR,xy=(0.205, -0.1),color='k', fontsize=fs_legend,xycoords='axes fraction')
            if hasattr(meta,'nv') and meta.nv is not None:
                _Nv  = 'Nv  : {:3.2f}'.format(meta.nv)
                ax_legend.annotate(_Nv,xy=(0.205, -0.4),color='k', fontsize=fs_legend,xycoords='axes fraction')
            if hasattr(meta,'nrcs') and meta.nrcs is not None:
                _Nrcs  = 'NRCS  : {:3.2f}'.format(meta.nrcs) + ' dB'
                ax_legend.annotate(_Nrcs,xy=(0.375, -0.1),color='k', fontsize=fs_legend,xycoords='axes fraction')
            if hasattr(meta,'tra') and meta.tra is not None:
                _Tra   = 'Track : {:3.2f}'.format(meta.tra) + ' $^o$'
                ax_legend.annotate(_Tra ,xy=(0.375, -0.4),color='k', fontsize=fs_legend,xycoords='axes fraction')
            if hasattr(meta,'azc') and meta.azc is not None:
                _Azc  = 'Az. Cut Off : {:3.2f}'.format(meta.azc) + ' m'
                ax_legend.annotate(_Azc,xy=(0.65, -0.1),color='k', fontsize=fs_legend,xycoords='axes fraction')
            if hasattr(meta,'inc') and meta.inc is not None:
                _inc  = 'Incidence   : {:3.2f}'.format(meta.inc) + ' $^o$'
                ax_legend.annotate(_inc,xy=(0.65, -0.4),color='k', fontsize=fs_legend,xycoords='axes fraction')
    if meta.type[0] == 'b':
            if hasattr(meta,'wp') and meta.wp is not None:
                _wp = r'$T_p$'+' : {:3.2f}'.format(meta.wp)+' s'
                ax_legend.annotate(_wp,xy=(0.205, -0.1),color='k', fontsize=fs_legend,xycoords='axes fraction')
            if hasattr(meta,'dirp') and meta.dirp is not None:
                _dirp  = r'$\Phi_p$'+' : {:3.2f}'.format(meta.dirp)
                ax_legend.annotate(_dirp,xy=(0.205, -0.4),color='k', fontsize=fs_legend,xycoords='axes fraction')
            if hasattr(meta,'hst') and meta.hst is not None:
                _Nrcs  = r'$Hs_t$'+' : {:3.2f}'.format(meta.hst) 
                ax_legend.annotate(_Nrcs,xy=(0.375, -0.1),color='k', fontsize=fs_legend,xycoords='axes fraction')
  
#Create and return the colorbar
def add_colorbar(fig,ax_cb,label,vmax = None,vmin = None,colormap = my_cmap,nrow = 1,ncolumn = 1):

    # rect = [0.2,0.1,0.8,1.0]
    # rect = [0.17,0.1,0.75,1.0]
    # ax_plot = plt.gca()
    # ax_cb = add_subplot_axes(ax_plot,rect)
    #
    # ax_invisible(ax_cb)
    divider = make_axes_locatable(ax_cb)
    ax_cb = divider.append_axes('right', size='3%', pad=0.3)

    norm = mpl.colors.Normalize(vmin=vmin,vmax=vmax)
    cb = mpl.colorbar.ColorbarBase(ax_cb,cmap=colormap,norm=norm,orientation='vertical', format='%2.2f')
    cb.set_label(label,fontsize=18)
    for l in cb.ax.yaxis.get_ticklabels() :
        l.set_weight("bold")
        l.set_fontsize(14)
    return fig

def init_colorbar_area():
    """
    because I want the wind speed quiver in the spectrum area to be over the colobar -> I need to initiate this subplot before the spectrum one
    :return:
    """
    rect = [0.17,0.1,0.75,1.0]
    ax_plot = plt.gca()
    ax_cb = add_subplot_axes(ax_plot,rect)

    ax_invisible(ax_cb)
    return ax_cb

#Function that init the Legend area under the graph and hide the contour
def init_legend_area(fig,nrow=1,ncolumn=1):
        ax_plot = plt.gca()
        rect = [0.0,-9.6,1.2,1.8]
        ax_legend = add_subplot_axes(ax_plot,rect)
        ax_legend.set_axis_off()
        return ax_legend

#Function that will add pair (label/value) of non empty item in the legend area    
def fill_legend_area(ax_legend,myspectrum,part_u,fs_legend,hs_on):
    #check the lenght of lon value
    #to avoid bad formated files??
    if hasattr(myspectrum,'wsys')and myspectrum.wsys is not None and hasattr(myspectrum.wsys,'lon'):
        lon = myspectrum.wsys.lon
    else: 
        lon=0
    logging.debug('lon : %s',lon)
    if hasattr(myspectrum,'wsys') and (lon< 9.96921e+36):
        #used to force legend rendering without pu
        if len(part_u)<1:
            j=0
            add_wsys_label(ax_legend,myspectrum.wsys,j,x_txt = j*0.2,fs_legend = fs_legend,hs = hs_on,meta=myspectrum.meta)
            ax_invisible(ax_legend)
            
        for j,iwsys in enumerate(range(1,len(part_u)+1)): #modif agrouaze because wsys is always 5 but unique partition values can be solely 1
            #change the color
            c_txt = ccc[np.int(part_u[j])]

            if c_txt is None:
                c_txt='k' #by default it is print in black
            #Used to show the differents value under the graph
            add_wsys_label(ax_legend,myspectrum.wsys,j,c_txt=c_txt,x_txt = j*0.2,fs_legend = fs_legend,hs = hs_on,meta=myspectrum.meta)
            ax_invisible(ax_legend)

def init_plot_area(fig,nrow=1,ncolumn=1,title=''):
    ax_plot = fig.add_subplot(nrow, ncolumn,1, polar=True)
    plt.suptitle(title)
    ax_invisible(ax_plot)
    if True:
        divider = make_axes_locatable(ax_plot)
        # ax_plot = divider.append_axes('top',size='400%') #original
        ax_plot = divider.append_axes('top', size='500%',pad=0)
    else:
        pass
        # rect = [0.0,-9.6,1.2,1.8]
        # ax_legend = add_subplot_axes(ax_plot,rect)
        # ax_legend.set_axis_off()
    return ax_plot

#Function that add diffrents basic aelements: cardinal direction, title, radial circle
def add_basic_elements_to_plot(ax_plot,title,fs_title,circleradius,freq):
    #add the cardinal direction and the main figure
    ax_plot = init_ax(ax_plot,title_font_size=fs_title,title=title)
    #The current Figure is updated to the parent of ax_plot
    plt.sca(ax_plot)
    #add the title to the plot
    # plt.title(title, fontsize=fs_title)
    ax_plot.set_title(title, fontsize=fs_title)

    # Create Radial Circle on the graph    
    circle_plot(ax_plot,circleradius,freq= freq)

    return ax_plot

#Function that add a wind Vector to ax_plot
def add_wind_to_plot(myspectrum,ax_plot,cut_off,fs_legend):
    scriptpath = os.path.realpath(__file__)
    U = None
    V = None
    #Test to ignore empty array
    if myspectrum.wind and hasattr(myspectrum.wind,"U")and hasattr(myspectrum.wind,"V"):
        U = myspectrum.wind.U #zonal
        V = myspectrum.wind.V #meridional
        logging.debug('get U and V from myspectrum')
    else:
        logging.debug('myspectrum.wind = %s',myspectrum.wind)
    
    # plot the wind vector
    if U and V:

        #a condition que les U et V aient ete calculer comme ca:U = 10*np.cos(np.pi/2-np.radians(dirwind))
        wspd = np.sqrt(U**2+V**2)
        x = np.arctan2(U,V) #position in radians
        y = 0.5*2*np.pi/cut_off #distance from the center of the plot
        logging.debug('x=%s y=%s',x,y)
        logging.debug('x in deg = %s',np.degrees(x))
        ax_plot.quiver(x,y,
                        U/wspd,V/wspd,
                        scale=5,width=0.015,color='magenta',edgecolor='k')

#         ax_plot.annotate(r'$U10_TO$ = {:3.2f}'.format(wspd)+ ' m/s',
        if 'dir' in dir(myspectrum.wind):
            dire = myspectrum.wind.dir
        else:
            dire = np.nan
        try:
            fixedradius = 0.9*2*np.pi/cut_off
            fixeddir = 0.1
            radius = 0.9*2*np.pi/cut_off
            variable_dir = np.arctan2(U,V)
            if 'varname' in dir(myspectrum.wind):
                varname = myspectrum.wind.varname
                str = r'$U10_{TO}$ = %3.2f'%(wspd)+ ' m/s\n$dir_{%s}$=%3.1f$^o$'%(varname,myspectrum.wind.dir)
            else:
                str = r'$U10_{TO}$ = %3.2f' % (wspd) + ' m/s\ndir=%3.1f$^o$' % (myspectrum.wind.dir)
                varname = None
            texto = ax_plot.annotate(str,
                        xy=(fixeddir,fixedradius),color='magenta',fontsize=fs_legend)
            # texto.set_path_effects([path_effects.Stroke(linewidth=2.0, foreground='black'),
            #            path_effects.Normal()])
        except:
            logging.error('cant add the direction on the plot as annotation')
            logging.error('%s',traceback.format_exc())
            ax_plot.annotate(r'$U10_{TO}$ = %3.2f'%(wspd)+ ' m/s',
                        xy=(np.arctan2(U,V),0.9*2*np.pi/cut_off),color='red',zorder=1000000)
        logging.debug('quiver added')
    else:
        logging.debug('quiver not added')
    return ax_plot
        
#Draw the spectrum and the contours
def add_spectrum_to_plot(fig,myspectrum,vmin,vmax,colormap,cb_label,nrow,ncolumn,cmap,ax_cb):
    part_u = []
    canPlotContour = False
    hasphiandk = False
    try:
        if myspectrum.spec_data.phi is not None and myspectrum.spec_data.k is not None:
            hasphiandk = True
            if np.amax(myspectrum.spec_data.sp)>np.amin(myspectrum.spec_data.sp):        
                canPlotContour = True      
    except:
        canPlotContour = False
        hasphiandk = False
    if canPlotContour:
        levels = list(np.linspace(vmin,vmax,100))
        #draw the spectrum
        cc = myspectrum.spec_data.sp
        xx = myspectrum.spec_data.k
        yy = np.radians(myspectrum.spec_data.phi)
        if cc.shape[0]==len(xx):
            spex_xarray = xarray.DataArray(cc,coords=[xx,yy],dims=['k','phi'])
        else:
            spex_xarray = xarray.DataArray(cc.T, coords=[xx, yy], dims=['k','phi'])
        ind_k_ok = slice(0,-1)
        ax = plt.gca()
        #rect = [0.2,0.1,0.8,1.0]
        #ax = add_subplot_axes(ax0,rect,subplot_kw=dict(projection='polar'))
        qm = spex_xarray[{'k': ind_k_ok}].plot (ax=ax,cmap=cmap,vmin=vmin,vmax=vmax,antialiased=True,add_colorbar=False)
        ax.set_ylabel('',fontsize = 18,rotation = 0)
        ax.set_xlabel('',fontsize = 18,rotation = 0)
        ax.set_theta_direction(-1)
        ax.set_theta_offset(np.pi/2.)
        ax.set_theta_zero_location('N')
        ax.grid(linewidth = 1,linestyle='--')
        #ax.set_rgrids(np.arange(0,500,50)[::-1])
        #ax.set_rgrids(np.arange(0,0.6,0.1))
        #ax.set_thetagrids(np.arange(0,365,5))
        ax.set_axis_on()
        
    if hasphiandk and hasattr(myspectrum,'spec_data') and hasattr(myspectrum.spec_data,'sp') and \
                myspectrum.spec_data.sp is not None:
                #check if there is one or more partitions if so show them
                logging.debug('part brut = %s',myspectrum.spec_data.part)
                if hasattr(myspectrum.spec_data,'part') and myspectrum.spec_data.part is not None:
                    myspectrum.spec_data.part = myspectrum.spec_data.part+1
                    part_u = np.unique(myspectrum.spec_data.part)
                    part_u = part_u[abs(part_u)<50]
                    logging.debug('part_u shape before removing masked values: %s %s',part_u.shape,part_u)
                    if isinstance(part_u,np.ma.core.masked_array):
                        if isinstance(part_u.mask,np.ndarray):
                            logging.debug('part_u.mask==False -> %s',part_u.mask==False)
#                             part_u = part_u[part_u.mask==False]
                            part_u = np.array(part_u[(part_u<200)&(part_u>0)], dtype=float)
                    logging.debug('part_u: %s',part_u)
                    if isinstance(part_u,np.float):# or isinstance(part_u,np.float):
                        part_u = np.array([part_u])
                    if len(part_u)==1:
                        logging.debug('only one value of partitionning: %s',part_u)
                    #draw each coutour

                    for cpt in part_u:
                        logging.debug('cpt= %s',cpt)
                        ppart=myspectrum.spec_data.part*0
                        ppart[myspectrum.spec_data.part==cpt]=cpt
                        try:
                            logging.debug('partition cpt = %s -> color = %s',cpt,ccc[np.int(cpt)])
                        except:
                            pass
                        if canPlotContour:
                            plt.contour(*init_sp(myspectrum.spec_data.k,myspectrum.spec_data.phi,ppart),levels=[0],colors=ccc[np.int(cpt)])
                #adding the colorbar
                fig = add_colorbar(fig,ax_cb,cb_label,vmax = vmax,vmin = vmin,colormap= colormap,nrow=nrow,ncolumn=ncolumn)
    else:
        logging.warning('spectra values not available')
    return fig,part_u



def add_subplot_axes(ax,rect,axisbg='w'):
    """
    tentative to debug the ww3spectra new version dont les composant sont tous imbriques
    Dec 2019
    agrouaze
    https://stackoverflow.com/questions/17458580/embedding-small-plots-inside-subplots-in-matplotlib
    rect [x,y,width,height]
    """
    fig = plt.gcf()
    box = ax.get_position()
    width = box.width
    height = box.height
    inax_position  = ax.transAxes.transform(rect[0:2])
    transFigure = fig.transFigure.inverted()
    infig_position = transFigure.transform(inax_position)    
    x = infig_position[0]
    y = infig_position[1]
    width *= rect[2]
    height *= rect[3]  # <= Typo was here
    subax = fig.add_axes([x,y,width,height]) #,axisbg=axisbg
    x_labelsize = subax.get_xticklabels()[0].get_size()
    y_labelsize = subax.get_yticklabels()[0].get_size()
    x_labelsize *= rect[2]**0.5
    y_labelsize *= rect[3]**0.5
    subax.xaxis.set_tick_params(labelsize=x_labelsize)
    subax.yaxis.set_tick_params(labelsize=y_labelsize)
    return subax
        
# Function that will generate a figure for a spectrum with all elements.
def spec_plot(myspectrum,spectrumstyle,cut_off = 200, 
                freq = 0,c_txt=None,
                spi_list = None,sp_log = 0,
                k0=None,phi0=None):
    
    part_u = []
    logging.debug('start spec_plot')
    if sp_log :
        myspectrum.spec_data.sp[myspectrum.spec_data.sp>0] = np.log(myspectrum.spec_data.sp[myspectrum.spec_data.sp>0])
        myspectrum.spec_data.sp[myspectrum.spec_data.sp<0] = 0
        cb_label = cb_label+'(nature log)'
    
    vmin = spectrumstyle['vmin']
    vmax = spectrumstyle['vmax']

    #determine the spectrum minimum and maximum value 
    if hasattr(myspectrum,'spec_data') and hasattr(myspectrum.spec_data,'sp'): 
        if vmin is not None:
            myspectrum.spec_data.sp[myspectrum.spec_data.sp<vmin] = 0
        else:
            vmin = np.min(myspectrum.spec_data.sp)
        if vmax is not None:
            myspectrum.spec_data.sp[myspectrum.spec_data.sp>vmax] = 0
        else:
            vmax = np.max(myspectrum.spec_data.sp[myspectrum.spec_data.sp<10000])
    fig = plt.figure(figsize=(spectrumstyle['xsize']*spectrumstyle['ncolumn'],
                               spectrumstyle['ysize']*spectrumstyle['nrow']))
    #fig = plt.figure(figsize=(9,6),dpi=120) #pour datavore compliance
    #create the main structure

    ax_plot = init_plot_area(fig,spectrumstyle['nrow'],spectrumstyle['ncolumn'],myspectrum.title)
    ax_cb = init_colorbar_area()
    if False:
        ax_txt = fig.add_subplot(spectrumstyle['nrow'],spectrumstyle['ncolumn'],1)
    else:
        rect = [0.0,0.9,0.8,0.1]
        # rect = [0.0,0.9,0.8,0.5]
        ax_txt = add_subplot_axes(ax_plot,rect)
        ax_txt.set_axis_off()

    #create the area for the legend(bottom of the representation)
    ax_legend = init_legend_area(fig,nrow=spectrumstyle['nrow'],ncolumn=spectrumstyle['ncolumn'])
    #add circles, cardinal direction, and title
    logging.debug('title %s',myspectrum.title)
    # spectrumstyle['titlesize']
    ax_plot = add_basic_elements_to_plot(ax_plot,myspectrum.title,29,
                                         spectrumstyle['circleradius'],freq)
   
    # if there is a range & azimuth axis add it
    if hasattr(myspectrum.meta, 'tra'):
        #AzRaAx_add(ax_plot,myspectrum.meta.tra,cut_off,meta=myspectrum.meta) # deprecated since there was an error it is not arcsin  but arctan (corrected in May 2022)
        add_labeled_image_axis(ax=ax_plot, heading=myspectrum.meta.tra, k=myspectrum.spec_data.k[myspectrum.spec_data.k<(2*np.pi)/cut_off])
        add_azimuth_cutoff_lines(ax_plot, tra=myspectrum.meta.tra, limit_wl_plot=cut_off, azc=myspectrum.meta.azc)
    #TODO reduce the parameters with only style
    plt.sca(ax_plot) #test
    fig, part_u = add_spectrum_to_plot(fig,myspectrum,vmin,vmax,
                                        spectrumstyle['colormap'],spectrumstyle['colorbarTitle']
                                       ,spectrumstyle['nrow'],spectrumstyle['ncolumn'],cmap=spectrumstyle['colormap'],ax_cb=ax_cb)

    if k0 is not None and phi0 is not None:
        markersize = 3
        plt.plot(phi0/180.*np.pi,k0,c = 'k',marker='o',alpha = 0.9,markersize=markersize)    
    
    fill_legend_area(ax_legend,myspectrum,part_u,spectrumstyle['legendsize'],spectrumstyle['hs'])#ax_legend
    if spi_list is not None:    
        colors = itertools.cycle(['0.75',"y", "g", "c","m","r"])
        for j,spi in enumerate(spi_list):
            c = next(colors) 
            add_wsys_contour_sp(ax_plot,myspectrum.spec_data.k,myspectrum.spec_data.phi,spi,cut_off,c_filled = c)
    ax_plot = add_wind_to_plot(myspectrum,ax_plot,cut_off,fs_legend=spectrumstyle['legendsize'])

    if freq == 0: 
        ax_plot.set_rmax(rmax=2*np.pi/cut_off)
        ax_plot.set_rmin(0)# never forget this rmin with xarray plot
    if freq ==1:
        ax_plot.set_rmax(rmax=np.sqrt(2*np.pi/cut_off*9.81/(2*np.pi)**2))
        ax_plot.set_rmin(0) # never forget this rmin with xarray plot
        plt.sca(ax_plot)
    return fig

