import abc
import numpy as np
import netCDF4 as nc
from dateutil import parser
from netCDF4 import Dataset
import Spectrum as spc
import logging
import spectrumUtil as spUtil
import os


class AbstractSpectrumReader():
    __metaclass__ = abc.ABCMeta

    @abc.abstractmethod
    def read_spectrum(ncfile, mode="satfull", index=None):
        pass


# -----------------------------------------------
# Reader for Buoys Globwave File (Ndbc and cdip)
# -----------------------------------------------

class SpectrumReaderBuoyGlobwave(AbstractSpectrumReader):
    @staticmethod
    def read_spectrum(ncf, index, mode="buoyglobwavendbc", phi=np.arange(0, 365, 10), dataName='UNK',filepath=None):
        """
        read_spectra_globwave: Function that return a spectrum object from a globwave file
        ncf: Net CDF File already open
        path: The path to access the file

        NB: If path and ncf are given priority is given to the path

        examples:

        ex1:
        nc1 = Dataset('path/to/ncfile.nc', 'r')
        myspectrum = read_spectrum_globwave(nc1,mode="satfull")
        nc1.close()

        ex2:
        myspectrum = read_spectrum_globwave(path='path/to/ncfile.nc',mode="satimaginary")

        """
        if isinstance(ncf, str):
            fileToRead = Dataset(ncf, 'r')
        else:
            fileToRead = ncf

        mywind = SpectrumReaderBuoyGlobwave.read_wind_globwave(fileToRead, index)
        mywsys = SpectrumReaderBuoyGlobwave.read_wsys_globwave(fileToRead, index)
        myspec_data = SpectrumReaderBuoyGlobwave.read_spec_data_globwave(fileToRead, index, mode, phi=phi)
        mymeta = SpectrumReaderBuoyGlobwave.read_meta_globwave(fileToRead, index, mode)

        if isinstance(ncf, str):
            fileToRead.close()

        return spc.Spectrum(myspec_data, mywsys, mywind, mymeta, dataName=dataName)

    @staticmethod
    def read_wind_globwave(ncf, index):
        wind = {}

        return wind

    @staticmethod
    def read_wsys_globwave(ncf, index):
        lon, lat = None, None
        lat = np.squeeze(ncf.variables['lat'][:])
        lon = np.squeeze(ncf.variables['lon'][:])
        wsys = {'lon': lon, 'lat': lat}

        return wsys

    @staticmethod
    def read_spec_data_globwave(ncf, index, mode='buoyglobwavendbc', phi=np.arange(0, 365, 10), con='to'):
        time = nc.num2date(ncf.variables['time'][:], ncf.variables['time'].units)
        time = time[int(index)]

        c1 = ncf.variables['theta1'][index, :]  # theta1
        c2 = ncf.variables['theta2'][index, :]  # theta2
        c3 = ncf.variables['stheta1'][index, :]  # stheta1
        c4 = ncf.variables['stheta2'][index, :]  # stheta2

        # convert theta & stheta(globwave data used) to a1,b1,a2,b2
        a1, a2, b1, b2 = spUtil.th2ab(c1, c2, c3, c4)
        if mode == "buoyglobwavendbc":
            f, sf = SpectrumReaderBuoyGlobwave.read_spec_data_ndbc_globwave(ncf, index)
        else:
            f, sf = SpectrumReaderBuoyGlobwave.read_spec_data_cdip_globwave(ncf, index)

        spfphitmp, d = spUtil.buoy_spectrum2d(sf, a1, a2, b1, b2, dirs=phi)
        spfphi = [spfphitmp]
        # spfphi2kphi wait an array of values

        sp2d, k = spUtil.spfphi2kphi(spfphi, f)

        if con == 'to':
            sp2d = spUtil.spfrom2to(sp2d, phi)
        # sp2d is an array of values : so we use the first and only element
        spec_data = spc.Spec_data(k=k, phi=phi, sp=sp2d[0], start_time=time, )

        return spec_data

    @staticmethod
    def read_spec_data_ndbc_globwave(ncf, index):
        f = np.squeeze(ncf.variables['wave_directional_spectrum_central_frequency'][:])
        sf = ncf.variables['spectral_wave_density'][index, :]
        return f, sf

    @staticmethod
    def read_spec_data_cdip_globwave(ncf, index):
        f = np.squeeze(ncf.variables['central_frequency'][index, :])
        sf = ncf.variables['sea_surface_variance_spectral_density'][index, :]
        return f, sf

    @staticmethod
    def read_meta_globwave(ncf, index, mode='buoyglobwavendbc'):

        if mode == "buoyglobwavendbc":
            mymeta = spc.Meta(type=mode)
        else:
            mymeta = SpectrumReaderBuoyGlobwave.read_meta_cdip_globwave(ncf,index,mode)
        return mymeta

    @staticmethod
    def read_meta_cdip_globwave(ncf,index,mode):
        meta={}
        meta['type'] = mode
        meta['wp'] = ncf.variables['dominant_wave_period'][index]
        meta['dirp'] = np.squeeze(ncf.variables['dominant_wave_direction'][index])
        meta['hst'] = np.squeeze(ncf.variables['significant_wave_height'][index])
    
        return meta

    @staticmethod
    def read_wsys_ndbc(ncf, index):
        lon, lat = None, None
        lat = np.squeeze(ncf.variables['latitude'][:])
        lon = np.squeeze(ncf.variables['longitude'][:])
        wsys = {'lon': lon, 'lat': lat}

        return wsys

# -----------------------------------------------
# End of Reader for Buoys Globwave File (Ndbc and cdip)
# -----------------------------------------------
