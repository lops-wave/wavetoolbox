# coding: utf-8
"""
author: Antoine Grouazel
"""
from matplotlib import pyplot as plt
import numpy as np
import xarray
import logging
import os
import traceback
import matplotlib
from plot_spec_V2 import init_legend_area
import matplotlib.patheffects as path_effects
cdict = {'red': ((0., 1, 1),
                 (0.05, 1, 1),
                 (0.11, 0, 0),
                 (0.66, 1, 1),
                 (0.89, 1, 1),
                 (1, 0.5, 0.5)),
         'green': ((0., 1, 1),
                   (0.05, 1, 1),
                   (0.11, 0, 0),
                   (0.375, 1, 1),
                   (0.64, 1, 1),
                   (0.91, 0, 0),
                   (1, 0, 0)),
         'blue': ((0., 1, 1),
                  (0.05, 1, 1),
                  (0.11, 1, 1),
                  (0.34, 1, 1),
                  (0.65, 0, 0),
                  (1, 0, 0))}

sp_cmap = matplotlib.colors.LinearSegmentedColormap('my_colormap',cdict,256)
ccc = ['black','red','magenta','green','orange','cyan','blue','grey','yellow']
def do_polar_spectra_plot_l2s_swim_averaged(xx,yy0,cc,ind_k_ok,title,fig=None,ax=None,max_wl=200.0,trackangle=None,
                cut_off=None,levels=None,vmax=None,vmin=None,cmap=None,no_cb=False,no_circle=False,toradians=True,
                                            mask_partitions=None,wsys=None,typespectra='s1',u=None,v=None,
                                            add_legend=False,titlefs=18):
    """
    inspired from Nouguier's snippet Sept 2019
    :args:
        xx (ndarray): vector of k wavenumber vectorized 1D
        yy0(ndarray): vector of phi vectorized 1D in degrees
        cc (ndarray): matrix 2D of the spectrum (phi,k)
        ind_k_ok (slice): to subset in the k dimension (could be slice(None,None,None) if not needed)
        typespectra (str): s1 or cfosat or ww3
    """
    if fig is None:
        fig = plt.figure(figsize=(10,10))
    if toradians:
        yy = np.radians(yy0)
    else:
        yy = yy0
    logging.debug('cc %s',cc.shape)
    logging.debug('xx.shape %s',xx.shape)
    logging.debug('yy.shape %s',yy.shape)
#     spex_xarray = xarray.DataArray(cc, coords=[xx, yy0], dims=['k', 'phi'])
    spex_xarray = xarray.DataArray(cc.T, coords=[xx, yy], dims=['k','phi'])
    if ax is None:
        ax = plt.subplot(1,1,1,projection='polar')
        if add_legend:
            ax_legend = init_legend_area(fig,nrow=1,ncolumn=1)
        plt.sca(ax)
#     plt.title(title)
    if cmap is None:
        cmap = sp_cmap
    if True:
        titi = ax.set_xticks(np.radians(np.arange(0,360,30)))
#         print(dir(titi))
#         for ti in titi:
#             print(dir(ti))
#             lab = ti.get_label()
#             print(dir(lab))
#             ti.set_font_size(10)
#         ax.set_xticks_fontsize(10)
        ax.tick_params(axis='both', which='major', labelsize=7)
        ax.tick_params(axis='both', which='minor', labelsize=8)
    else:
        ax.set_xticks(np.radians(np.arange(-180,180,45)))
    if False:
        qm = spex_xarray[{'k':slice(0,40)}].plot(cmap=cmap) #oriinal
    else:
        qm = spex_xarray[{'k':ind_k_ok}].plot(cmap=cmap,vmin=vmin,vmax=vmax,antialiased=True) #ajouter les vmin vmax et levesl=60
    ax.set_theta_direction(-1)
    ax.set_theta_offset(np.pi/2.)
    ax.set_theta_zero_location('N')
    # ax.set_rgrids(np.arange(0,0.18,0.02), angle=-55)
    ax.set_ylabel('', fontsize=18,rotation=0)
    ax.yaxis.set_label_coords(0.88,1)
#     ax.yaxis.set_label_coords(2*np.pi/700,2*np.pi/50)
    ax.grid(True)
    ax.set_xlabel('', fontsize=20)
#     ax.tick_params(labelsize=10)
    # qm.set_clim([0,100])
    ax.set_title(title, fontsize=titlefs)
    if True:
        qm.colorbar.remove() 
#         fig.xlabel.remove()
    if no_circle is False:
        circle_plot(ax,[50,100,200,400],freq=0)
       
    #     plt.ylabel('phi_geo [deg]')
    #     plt.xlabel('k linear [m-1]')
    if trackangle:
        AzRaAx_add(ax,trackangle,cut_off,wsys=None)
    ax.set_rmax(2*np.pi/max_wl)
    #     plt.colorbar()
    
    #     divider = make_axes_locatable(ax)
    #     cax = divider.append_axes("right", size="5%", pad=0.05)
    
    #     plt.colorbar(im, cax=cax)
#     add_wsys_contour_wsys(ax,cc,xx,yy0,mask_partitions,cut_off,c_filled = '0.75')
    if mask_partitions is not None:
        add_wsys_contour_wsys_v2(cc,xx,yy0,mask_partitions)
    #add the infos for each swell system
    if u is not None and v is not None:
        add_wind_to_plot(u,v,ax,cut_off)
    if add_legend:
        logging.debug('wsys = %s',wsys)
        wrapper_wavesystem_infos(wsys,mask_partitions,ax_legend,typespectra,fs_legend=12,hs_on=True)
    if no_cb is False:
        plt.colorbar(qm,fraction=0.046, pad=0.14)
    if  fig is None:
        plt.show()
        
        
def circle_plot(ax,r,freq=0):
    # Radial Circles and their label
    theta = 2*np.pi*np.arange(360)/360.
    
    labels = []

    if freq==0:
        for i in r:
            plt.plot(theta, np.arange(360)*0 + 2*np.pi/i,'--k')
            labels.append(str(i)+' m')
        texto = ax.set_rgrids([2*np.pi/i for i in r], labels=labels, angle=55.,fontsize=10) #agrouaze put -45 to specific case in order to see a label

    if freq==1:
        for i in r:
            plt.plot(theta, np.arange(360)*0 + np.sqrt(2*np.pi/i*9.81/(2*np.pi)**2),'--k')
            labels.append(str(i)+' m')
        ax.set_rgrids([np.sqrt(2*np.pi/i*9.81/(2*np.pi)**2) for i in r], labels=labels, angle=45.,fontsize=10)
        
        
        
def AzRaAx_add(ax,tra,cut_off,wsys=None):
    ax.plot([(tra-180)*np.pi/180.,(tra)*np.pi/180.],[2*np.pi/cut_off,2*np.pi/cut_off],zorder=100000,color='black',lw=1.5)
    ax.plot([(tra-90)*np.pi/180.,(tra+90)*np.pi/180.],[2*np.pi/cut_off,2*np.pi/cut_off],zorder=100000,color='black',lw=1.5)
    ax.annotate('Range',xy=((90+tra+2)*np.pi/180.,2*np.pi/cut_off),color='black',zorder=1000000)
    ax.annotate('Azimuth',xy=((tra-2)*np.pi/180.,2*np.pi/cut_off),color='black',zorder=1000000)

    if wsys is not None:
        R = 2*np.pi/cut_off
        Azc = 2*np.pi/wsys['Azc']
        ttt = tra-180.
        theta0 = np.arcsin(Azc/R)
        theta1 = (np.pi/2.)-theta0
        theta2 = -theta1
        theta3 =  theta1 + ttt*np.pi/180.
        theta4 = -theta1 + ttt*np.pi/180.
        #ax.plot([theta0],[2*np.pi/cut_off],'o', zorder=100000,color='green',lw=1.5)
        #ax.plot([theta1],[2*np.pi/cut_off],'o', zorder=100000,color='blue',lw=1.5)
        #ax.plot([theta1+ttt*np.pi/180.],[2*np.pi/cut_off],'s', zorder=100000,color='blue',lw=1.5)
        #ax.plot([theta2],[2*np.pi/cut_off],'o',zorder=100000,color='red',lw=1.5)
        #ax.plot([theta2+ttt*np.pi/180.],[2*np.pi/cut_off],'s',zorder=100000,color='red',lw=1.5)
        #ax.plot([theta1,theta2],[2*np.pi/cut_off,2*np.pi/cut_off],zorder=100000,color='grey',lw=1.5)
        ax.plot([theta3,theta4],[2*np.pi/cut_off,2*np.pi/cut_off],zorder=100000,color='grey',lw=1.5,alpha=0.5)

        theta0 = np.arcsin(Azc/R) 
        theta1 = (np.pi/2.)+theta0
        theta2 = -theta1 
        theta3 =  theta1 + ttt*np.pi/180.
        theta4 = -theta1 + ttt*np.pi/180.
        #ax.plot([theta3],[2*np.pi/cut_off],'o', zorder=100000,color='blue',lw=1.5)
        #ax.plot([theta4],[2*np.pi/cut_off],'o',zorder=100000,color='red',lw=1.5)
        ax.plot([theta3,theta4],[2*np.pi/cut_off,2*np.pi/cut_off],zorder=100000,color='grey',lw=1.5,alpha=0.5)
        
def add_wsys_contour_wsys(ax_plot,sp,k,phi,iwsys,cut_off,c_filled = '0.75'):    
    # boundary of each wave system
    isys = iwsys.isystem 
    if len(isys)>0:
        sys = np.zeros_like(sp,dtype = np.float)
        sys[isys]=1            
        thetas, radius, sys2 = init_sp(k,phi,sys)

        sys2[sys2==0]= np.nan

        ax_plot.contourf(thetas, radius, sys2,colors=c_filled,alpha = 0.8)
    
    ax_plot.set_rmax(rmax=2*np.pi/cut_off) 
    
def add_wsys_contour_wsys_v2(sp,k,phi,part):
    """
    version agrouaze Nov 2019
    
    """
    if np.amax(sp)>np.amin(sp):    
        # contour    
        
        
        #levels = [0.] + list(np.linspace(vmin,vmax,50))
        #levels[1] = 20.
#         levels = list(np.linspace(vmin,vmax,100))
#         plt.contourf(*init_sp(k,phi,sp),cmap=colormap,levels = levels)
        logging.debug('partition: %s',np.unique(part))
        if part is not None:
#             part = part+1
            pu = np.unique(part)
            pu = pu[pu<200]
#             print "is finite",np.isfinite(pu)
#             pu = pu.filled(np.nan)
            pu = pu.filled(-99999)
#             print pu
            pu = pu[np.isfinite(pu) & (pu>=0)]
#             pu = pu[np.isfinite(pu) ]
            logging.debug('pu: %s',pu)
            if len(pu)==1:
                logging.warning('only one value of partitioning: %s',pu)
            for cpt in pu:
                
                ppart=part*0-2 #je met du moins 2 par defaut pour ne pas utiliser le zero
                ppart[part==(cpt)%len(pu)] = cpt
                #ppart[ppart==cpt] = cpt+2
                logging.debug('contenu de ppart = %s',np.unique(ppart))
#                 ppart[part==cpt+1]=cpt+1 #essaie agrouaze pour corriger le pb de couleur des contours
                print('je cree un matrice contenant des -1 et des %s'%cpt)
#                 print 'cpt',cpt
                thethas,radius,spec = init_sp(k,phi,ppart)
                
#                 print 'thethas',thethas.shape
#                 print 'radius',radius.shape
#                 print 'spec',spec.shape,type(spec)
#                 plt.contour(thethas,radius,spec,levels=[0],colors=ccc[np.int(cpt)])
                indice_for_wsys = np.int(cpt) #-1 before, je pense qui ne faut pas y toucher car wsys ira tjrs de 0 à N
                print( 'cpt = ',cpt)
                color_parto = ccc[int(cpt)]
                levels_to_plot=  [cpt-0.1,cpt+0.1]
                #levels_to_plot = [cpt-0.1]
                #levels_to_plot = [cpt+0.1]
                print( 'cpt=',cpt,"spec unique",np.unique(spec),'contour ploted',levels_to_plot,'indice_for_wsys',indice_for_wsys,"color_parto",color_parto)
                plt.contour(thethas,radius,spec,levels=levels_to_plot,colors=color_parto) #new tentative from agrouaze to make the partition contours not intersecting
#                 plt.contour(thethas,radius,spec,levels=[cpt,cpt+1],colors=ccc[np.int(cpt)]) #new tentative from agrouaze to make the partition contours not intersecting
                #add a point on the peak values of each partition
                
#                 print cpt,np.degrees(wsys['dirad'][indice_for_wsys]),2.*np.pi/wsys['wl'][indice_for_wsys]
#                 print(cpt,'color=',color_parto)
#                 plt.plot(wsys['dirad'][indice_for_wsys],2.*np.pi/wsys['wl'][indice_for_wsys],'.',ms=25,markeredgecolor='k',color=color_parto)#,color=[np.int(cpt)])
#                 plt.text(wsys['dirad'][indice_for_wsys],2.*np.pi/wsys['wl'][indice_for_wsys],wsys['original_label'][indice_for_wsys],color=color_parto)#,color=[np.int(cpt)])
                #print ccc[np.int(cpt)]

        # colorbar
        if False:
            norm = mpl.colors.Normalize(vmin=vmin,vmax=vmax)
            cb = mpl.colorbar.ColorbarBase(ax_cb,cmap=colormap,norm=norm,orientation='vertical', format='%.0e')
            cb.set_label(cb_lable)
            
def wrapper_wavesystem_infos(wsys,part,ax,typespectra,fs_legend,hs_on=True):
    pu = np.unique(part)
    pu = pu[pu<200]
    pu = pu.filled(-99999)
    pu = pu[np.isfinite(pu) & (pu>=0)]
    logging.debug('pu not ordered = %s',pu)
    logging.info('wsys = %s wsys["lon"] = %s',wsys,wsys['lon'])
    if wsys is not None and wsys['lon']< 9.96921e+36:
    
    # colors = itertools.cycle(['0.75',"y", "g", "c","m","r"])
#         if len(wsys['hs'])==len(pu):
#             for j,iwsys in enumerate(wsys['hs']):
        for j,iwsys in enumerate(range(1,len(pu)+1)): #modif agrouaze because wsys is always 5 but unique partition values can be solely 1
            # c = next(colors)
            c = 'k'
#             print 'pu',pu
            
            c_txt = ccc[np.int(pu[j])]#-1 added by agrouaze to make the contours match the text
            logging.debug('texte color pu[j] = %s c_txt=%s j=%s direction wave = %sdeg',pu[j],c_txt,j,np.degrees(wsys['dirad'][j]))
            # add_wsys_contour_wsys(ax_plot,sp,k,phi,iwsys,cut_off,c_filled = c)
            
            if c_txt is None:
                add_wsys_label(ax,wsys,typespectra,j,c_txt=c,x_txt = j*0.2,fs_legend = fs_legend,hs = hs_on)
            else:
                add_wsys_label(ax,wsys,typespectra,j,c_txt=c_txt,x_txt = j*0.2,fs_legend = fs_legend,hs = hs_on)
            ax_invisible(ax)
    else:
        logging.info('wsys = %s wsys["lon"] = %s',wsys,wsys['lon'])
            
def ax_invisible(ax):
    ax.set_frame_on(False)
    ax.xaxis.set_visible(False)
    ax.yaxis.set_visible(False)     

def add_wsys_label(ax_legend,wsys,typespectra,j,c_txt='k',x_txt = 0,fs_legend = 12,hs = 1):
    """
    :args:
        typespectra (str): ww3 or s1 or cfosat
        j (bool): ?
    """
#     print("add_wsys_label",wsys)
    # Legend of each wave system
    _Hs  = 'Hs'  + ': {:3.2f}'.format(wsys['hs'][j]) + ' m'
    _Wl  = 'Wl'  + ': {:3.2f}'.format(wsys['wl'][j]) + ' m'
    _Dir = 'Dir' + ': {:3.2f}'.format(wsys['dirad'][j]*180.0/np.pi) + ' $^\circ$'
    #_Hs  = 'Hs'  + ': {:3.2f}'.format(iwsys['hs) + ' m'
    #_Wl  = 'Wl'  + ': {:3.2f}'.format(iwsys.wl) + ' m'
    #_Dir = 'Dir' + ': {:3.2f}'.format(iwsys.dirad) + ' $^\circ$'
    
    if hs:
        texto =ax_legend.annotate(_Hs,xy=(x_txt, 0.3),color=c_txt, fontsize=fs_legend,xycoords='axes fraction')
        texto.set_path_effects([path_effects.Stroke(linewidth=1, foreground='black'),path_effects.Normal()])

    texto =ax_legend.annotate(_Dir,xy=(x_txt, 0.6),color=c_txt, fontsize=fs_legend,xycoords='axes fraction')    
    texto.set_path_effects([path_effects.Stroke(linewidth=1, foreground='black'),path_effects.Normal()])
    texto =ax_legend.annotate(_Wl,xy=(x_txt, 0.9),color=c_txt, fontsize=fs_legend,xycoords='axes fraction')
    texto.set_path_effects([path_effects.Stroke(linewidth=1, foreground='black'),path_effects.Normal()])

    if j==0:
        _Lon = 'Lon : {:3.2f}'.format(wsys['lon']) + ' deg'
        _Lat = 'Lat : {:3.2f}'.format(wsys['lat']) + ' deg'
        texto =ax_legend.annotate(_Lon,xy=(x_txt, -0.1),color='k', fontsize=fs_legend,xycoords='axes fraction')
        texto.set_path_effects([path_effects.Stroke(linewidth=1, foreground='black'),path_effects.Normal()])
        texto =ax_legend.annotate(_Lat,xy=(x_txt, -0.4),color='k', fontsize=fs_legend,xycoords='axes fraction')
        texto.set_path_effects([path_effects.Stroke(linewidth=1, foreground='black'),path_effects.Normal()])
        if typespectra in ['cfosat','s1']:
            _SnR = 'SnR : {:3.2f}'.format(wsys['SnR'])
            _Nv  = 'Nv  : {:3.2f}'.format(wsys['Nv'])
            texto =ax_legend.annotate(_SnR,xy=(0.2, -0.1),color='k', fontsize=fs_legend,xycoords='axes fraction')
            texto.set_path_effects([path_effects.Stroke(linewidth=1, foreground='black'),path_effects.Normal()])
            texto =ax_legend.annotate(_Nv,xy=(0.2, -0.4),color='k', fontsize=fs_legend,xycoords='axes fraction')
            texto.set_path_effects([path_effects.Stroke(linewidth=1, foreground='black'),path_effects.Normal()])
    
            _Nrcs  = 'NRCS  : {:3.2f}'.format(wsys['Nrcs']) + ' dB'
            _Tra   = 'Track : {:3.2f}'.format(wsys['Tra']) + ' deg'
            texto =ax_legend.annotate(_Nrcs,xy=(0.375, -0.1),color='k', fontsize=fs_legend,xycoords='axes fraction')
            texto.set_path_effects([path_effects.Stroke(linewidth=1, foreground='black'),path_effects.Normal()])
            texto =ax_legend.annotate(_Tra ,xy=(0.375, -0.4),color='k', fontsize=fs_legend,xycoords='axes fraction')
            texto.set_path_effects([path_effects.Stroke(linewidth=1, foreground='black'),path_effects.Normal()])
    
            _Azc  = 'Az. Cut Off : {:3.2f}'.format(wsys['Azc']) + ' m'
            _inc  = 'Incidence   : {:3.2f}'.format(wsys['Inc']) + ' deg'
            texto =ax_legend.annotate(_Azc,xy=(0.6, -0.1),color='k', fontsize=fs_legend,xycoords='axes fraction')
            texto.set_path_effects([path_effects.Stroke(linewidth=1, foreground='black'),path_effects.Normal()])
            texto =ax_legend.annotate(_inc,xy=(0.6, -0.4),color='k', fontsize=fs_legend,xycoords='axes fraction')
            texto.set_path_effects([path_effects.Stroke(linewidth=1, foreground='black'),path_effects.Normal()])

def add_wind_to_plot(U,V,ax_plot,cut_off):
#     print('myspectrum',myspectrum)
#     print('add_wind_to_plot')
    scriptpath = os.path.realpath(__file__)
#     print("Script path is : " + scriptpath)
#     U = None
#     V = None
    #Test to ignore empty array
#     if myspectrum.wind and hasattr(myspectrum.wind,"U")and hasattr(myspectrum.wind,"V"):
#         U = myspectrum.wind.U #zonal
#         V = myspectrum.wind.V #meridional
#         logging.debug('get U and V from myspectrum')
#     else:
#         logging.debug('myspectrum.wind = %s',myspectrum.wind)
    
    # plot the wind vector
    if U and V:
        if False: #ca marche pour mon cas mais je suis quasiment sur que ca va faire nimp dans les autres cas
            wspd = np.sqrt(U**2+V**2)
            x = np.arctan2(-U,-V) #position in radians
            y = 0.5*2*np.pi/cut_off #distance from the center of the plot
            logging.debug('x=%s y=%s',x,y)
            logging.debug('x in deg = %s',np.degrees(x))
            ax_plot.quiver(x,y,
                            -U/wspd,-V/wspd,
                            scale=5,width=0.015,color='red')
        else:
            #a condition que les U et V aient ete calculer comme ca:U = 10*np.cos(np.pi/2-np.radians(dirwind))
            wspd = np.sqrt(U**2+V**2)
            x = np.arctan2(U,V) #position in radians
            logging.debug('add_wind_to_plot | x=%s',x)
            y = 0.5*2*np.pi/cut_off #distance from the center of the plot
            logging.debug('x=%s y=%s',x,y)
            logging.debug('x in deg = %s',np.degrees(x))
            ax_plot.quiver(x,y,
                            U/wspd,V/wspd,
                            scale=5,width=0.015,color='red')

#         ax_plot.annotate(r'$U10_TO$ = {:3.2f}'.format(wspd)+ ' m/s',
#         if 'dir' in dir(myspectrum.wind):
#             dire = myspectrum.wind.dir
        dire = wspd*np.arctan2(U,V)
#         else:
#             dire = np.nan
        try:
            texto = ax_plot.annotate(r'$U10_{TO}$ = %3.2f'%(wspd)+ ' m/s\ndir=%3.1f$^o$'%(dire),
                        xy=(np.arctan2(U,V),0.9*2*np.pi/cut_off),color='yellow',zorder=1000000)
            texto.set_path_effects([path_effects.Stroke(linewidth=3, foreground='black'),
                       path_effects.Normal()])
        except:
            logging.error('cant add the direction on the plot as annotation')
            logging.error('%s',traceback.format_exc())
            ax_plot.annotate(r'$U10_{TO}$ = %3.2f'%(wspd)+ ' m/s',
                        xy=(np.arctan2(U,V),0.9*2*np.pi/cut_off),color='yellow',zorder=1000000)
        logging.debug('quiver added')
    else:
        logging.debug('quiver not added')
    return ax_plot

def init_sp(k,phi,spec):
    logging.debug('init_sp k %s phi %s spec %s',k.shape,phi.shape,spec.shape)
    if np.shape(spec)[0]!=len(phi):
        spec = spec.T

    # # adding 360deg
    ind = np.argsort(phi)
    phi = np.sort(phi)    
    phi = np.append(phi,360)
    sp = spec[ind,:]
    sp = np.concatenate((sp,sp[0:1,:]), axis=0)

    #print np.min(sp)
    # converting the direction to rad
    phi = phi/180.0*np.pi

    radius,thetas = np.meshgrid(k,phi)

    return thetas,radius,sp
