import abc
import numpy as np
import netCDF4 as nc
from dateutil import parser
from netCDF4 import Dataset
import Spectrum as spc
import logging
import spectrumUtil as spUtil
import os

from SpectrumReaderBuoys import SpectrumReaderBuoyGlobwave
from SpectrumReaderBuoysCMEMS import SpectrumReaderBuoyCMEMS
from SpectrumReaderSentinel import SpectrumReaderSat
from SpectrumReaderWW3 import SpectrumReaderWw3

class AbstractSpectrumReader():
    
    __metaclass__ = abc.ABCMeta
    
#     @abc.abstractmethod
    def read_spectrum(self,ncfile,mode="satfull",index=None):
        pass
    
instww3 = SpectrumReaderWw3()
insts1 = SpectrumReaderSat()
instbuoy = SpectrumReaderBuoyGlobwave()
instcmems = SpectrumReaderBuoyCMEMS()
readerdictionary ={
    "satreal":insts1.read_spectrum,
    "satfull":insts1.read_spectrum,
    "satimaginary":insts1.read_spectrum,
    "ww3":instww3.read_spectrum,
    "buoyglobwavendbc":instbuoy.read_spectrum,
    "buoyglobwavecdip":instbuoy.read_spectrum,
    "buoycmems":instcmems.read_spectrum,
    
}    


#-----------------------------------------------
# Generic Reader that call the good reader
#-----------------------------------------------
class SpectrumReader(AbstractSpectrumReader):

    def __init__(self):
        self.fullpath = None
#     @staticmethod
    def read_spectrum(self,ncfile,mode=None,index=None,path=None):
        dataName='UNK'
        
        if isinstance(ncfile, str):
#           dataName = SpectrumReader.selectDataName(ncfile)
            self.fullpath = ncfile
            dataName = self.selectDataName(ncfile)
            fileToRead = Dataset(ncfile, 'r') 
            if not mode:
#               mode = SpectrumReader.selectmodebyname(ncfile)
                mode = self.selectmodebyname(ncfile)
        else:
            fileToRead = ncfile
#             SpectrumReader.selectmodebydata(ncfile)
            self.selectmodebydata(ncfile)
        if index is not None:
            myspectrum = readerdictionary[mode](fileToRead,index=index,mode=mode,dataName=dataName)    
        else:
            fullpath = None
            if isinstance(ncfile,str):
                fullpath = ncfile
            else:
                if path is not None:
                    fullpath = path
            myspectrum = readerdictionary[mode](fileToRead,mode=mode,dataName=dataName,filepath=fullpath)
        if isinstance(ncfile, str):
            fileToRead.close()
            
        #myspectrum.dataName=dataName
        #myspectrum.title = myspectrum.generateSpectrumTitle()
        
        return myspectrum

#     @staticmethod
    def selectDataName(self,name):
        if 'globwave' in name and 'cdip' in name:
            return 'buoyglobwavecdip'
        elif 'globwave' in name and 'ndbc' in name:
            return 'buoyglobwavendbc'
        elif 'ww3' in name:
            return 'WW3'
        elif 's1a' in name:
            return 'S1A'
        elif 's1b' in name:
            return 'S1B'
        elif 'CMEMS' in name:
            return 'buoycmems'
        else:
            logging.info('default data name: UNKNOWN')
            return 'UNKNOWN'
        


#     @staticmethod
    def selectmodebyname(self,name):
        if 'globwave' in name and 'cdip' in name:
            return 'buoyglobwavecdip'
        elif 'globwave' in name and 'ndbc' in name:
            return 'buoyglobwavendbc'
        elif 'ww3' in name:
            return 'ww3'
        elif 's1a' in name or 's1b' in name:
            print('ambiguous situation will return the full spectrum mode:satfull')
            return 'satfull'
        else:
            print('impossible to find mode from name')
            return 'unknown'
#     @staticmethod
    def selectmodebydata(self,data):
        print(data.title)
#-----------------------------------------------
# End of Generic Reader 
#-----------------------------------------------



