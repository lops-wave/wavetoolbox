import sys

sys.path.append('/home3/homedir7/perso/mlebars/workspace/git/wavetoolbox')
import check_generation_and_indexation_V1 as check_gen
import os
mission_start_s1a = "20140101"
import datetime
from dateutil.relativedelta import relativedelta

import generate_listing_v1_02 as generate_listing

fileRepoWavetool = "/home3/homedir7/perso/satwave/sources_en_exploitation/wavetoolbox/"
#fileRepoMpc = "/home3/homedir7/perso/satwave/sources_en_exploitation/mpc-sentinel/mpc-sentinel/mpcsentinellibs/indexation_in_hbase/"
fileRepoMpc = "/home3/homedir7/perso/mlebars/workspace/git/mpc-sentinel/mpc-sentinel/mpcsentinellibs/"


pngBaseList = None
pngFullPathList = None
ncBaseList = None
ncFullPathList = None
ww3NcBaseList = None
ww3NcFullPathList = None
ww3NcFilesOnWater = None
ncBaseFromTrack = None
indexedFullPathList = None
l1XmlBaseList = None
l1XmlFullPathList = None
l1RoughnessBaseList = None
l1RoughnessFullPathList = None
geolocalizedElementsFromCalvalIndex = None
updateDataBeforeProcess = None

picklerPathA='/home/cercache/users/mlebars/output_test/checkgeneration_result/picklers/sat-a'
picklerPathB='/home/cercache/users/mlebars/output_test/checkgeneration_result/picklers/sat-b'

platform='S1A'
platformB='S1B'

dictdbcolnames = {'L1':'cf:fpath',
                  'roughness':'cf:ql:l1_roughness',
                  'a':{'ww3_V2': 'cf:ql:'+platform+'_WV_OCN__2S_ww3'+'_V2',
                  'imaginary_V2':'cf:ql:'+platform+'_WV_OCN__2S_imaginarycrossspectra_'+platform.lower()+'_V2',
                  'real_V2':'cf:ql:'+platform+'_WV_OCN__2S_realcrossspectra_'+platform.lower()+'_V2',
                  'full_V2':'cf:ql:'+platform+'_WV_OCN__2S_ocean_swell_spectra_'+platform.lower()+'_V2'
                       
                  },'b':{'ww3_V2': 'cf:ql:'+platformB+'_WV_OCN__2S_ww3'+'_V2',
                  'imaginary_V2':'cf:ql:'+platformB+'_WV_OCN__2S_imaginarycrossspectra_'+platformB.lower()+'_V2',
                  'real_V2':'cf:ql:'+platformB+'_WV_OCN__2S_realcrossspectra_'+platformB.lower()+'_V2',
                  'full_V2':'cf:ql:'+platformB+'_WV_OCN__2S_ocean_swell_spectra_'+platformB.lower()+'_V2'
                  }}


### Function To Check and Reprocess L2 Data 
def reprocessL2(currentMonth,picklerPath,satellite,updateDataBeforeProcess):
    now = datetime.datetime.now() 
    tmpListing = '/home/cercache/project/mpc-sentinel1/analysis/s1_data_analysis/L2_V2/listing/xspec_l2_'+now.strftime('%Y%m%d%H%M%S')+'.tmp'
    repout = generate_listing.getoutputfolder('S1'+satellite.upper())
    print "updateDataBeforeProcess",updateDataBeforeProcess
    print repout
    elementsToProcess=0
    #########################################################
    ### Part To Generate PNG 
    #########################################################
    if updateDataBeforeProcess:
        ncBaseList,ncFullPathList = check_gen.writeListFromNCOnHardDrive(currentMonth,picklerPath,satellite)
        pngBaseList,pngFullPathList = check_gen.writeListFromPNGOnHardDrive(currentMonth,picklerPath,satellite)
    else:
        ncBaseList,ncFullPathList = check_gen.retrieveListFromNCOnHardDrive(currentMonth,picklerPath)
        pngBaseList,pngFullPathList = check_gen.retrieveListFromPNGOnHardDrive(currentMonth,picklerPath)
    #Make the test only on "real" not on imaginary or full but the three will be processed
    notGeneratedElements = list(set(ncBaseList) - set(pngBaseList[dictdbcolnames[satellite]['real_V2']]))
    print ("notGeneratedElements:",len(notGeneratedElements))
    
    linesToGenerate=[]
    tmpFile = open(tmpListing,'w')

    for currentKey in notGeneratedElements:
        inputfile = ncFullPathList[currentKey]
        linesToGenerate = generate_listing.getoutputlines(inputfile,repout,'SAT')
        for currentline in linesToGenerate:
            tmpFile.write(currentline+"\n")
            elementsToProcess+=1
    tmpFile.close()
    os.chmod(tmpListing,0775)
    print "%d L2 NC To generate in PNG"%(elementsToProcess)
    if elementsToProcess>0:
        os.system('cat '+tmpListing+'|'+fileRepoWavetool+'runner_plot_spectra_v2.bash')
        #Update Data
        pngBaseList,pngFullPathList = check_gen.writeListFromPNGOnHardDrive(currentMonth,picklerPath,satellite)
    os.remove(tmpListing)
    #########################################################
    ###END of Part To Generate PNG
    #########################################################
    
    
    #########################################################
    ### Part To Index PNG 
    #########################################################
    
    if updateDataBeforeProcess:
        indexedFullPathList = check_gen.writePathListInIndex(currentMonth,picklerPath,satellite)
    else:
        indexedFullPathList = check_gen.retrievePathListInIndex(currentMonth,picklerPath)
    
    elementsToProcess = 0
    tmpFile = open(tmpListing,'w')
    for elementType in ['real_V2','imaginary_V2','full_V2']:
        notIndexedElements = list(set(pngFullPathList[dictdbcolnames[satellite][elementType]]) - set(indexedFullPathList[dictdbcolnames[satellite][elementType]]))
        elementsToProcess += len(notIndexedElements)
        for currentElement in notIndexedElements:
            tmpFile.write(currentElement+"\n")
    tmpFile.close()
    os.chmod(tmpListing,0775)
    
    print "%d L2 PNG To index "%(elementsToProcess)
    if elementsToProcess>0:
        os.system('cat '+tmpListing+'|'+fileRepoMpc+'indexation_in_hbase/index_sentinel-1a-ng-Quicklooks_RoughnessL1_and_L2_V2.py')
        #Update Data
        indexedFullPathList = check_gen.writePathListInIndex(currentMonth,picklerPath,satellite)
    os.remove(tmpListing)
    
    #########################################################
    ### End of Part To Index PNG 
    #########################################################

def reprocessL1(currentMonth,picklerPath,satellite,updateDataBeforeProcess):
    #########################################################
    ### Part To Check and Reprocess L1 Data 
    #########################################################
    
    now = datetime.datetime.now() 
    tmpListing = '/home/cercache/project/mpc-sentinel1/analysis/s1_data_analysis/L2_V2/listing/xspec_l2_'+now.strftime('%Y%m%d%H%M%S')+'.tmp'
    
    if updateDataBeforeProcess:
        l1XmlBaseList,l1XmlFullPathList,l1XmlFullPathDict = check_gen.writeListFromLevel1XmlOnHardDrive(currentMonth,picklerPath,satellite)
        indexedFullPathList = check_gen.writePathListInIndex(currentMonth,picklerPath,satellite)
    else:
        l1XmlBaseList,l1XmlFullPathList,l1XmlFullPathDict = check_gen.retrieveListFromLevel1XmlOnHardDrive(currentMonth,picklerPath)
        indexedFullPathList = check_gen.retrievePathListInIndex(currentMonth,picklerPath)
        
    notIndexedElements = list(set(l1XmlFullPathList) - set(indexedFullPathList[dictdbcolnames['L1']]))
    
    tmpFile = open(tmpListing,'w')
    elementsToProcess=0
    for currentElement in notIndexedElements:
        currentElement = currentElement.replace(".tiff",".xml").replace('/measurement','/annotation')
        tmpFile.write(currentElement+"\n")
        elementsToProcess+=1
    tmpFile.close()
    os.chmod(tmpListing,0775)
    
    print "%d Xml To index "%(elementsToProcess)
    print "element Launched",fileRepoMpc+'indexation_in_hbase/index_sentinel_L1_base.py'
    if elementsToProcess>0:
        os.system('cat '+tmpListing+'|'+fileRepoMpc+'indexation_in_hbase/index_sentinel_L1_base.py')
        #Update Data
        print(currentMonth,picklerPath,satellite)
        indexedFullPathList = check_gen.writePathListInIndex(currentMonth,picklerPath,satellite)
    os.remove(tmpListing)
    #if len(notIndexedElements)>0:
    #    print notIndexedElements
        #Update Data
    
    #########################################################
    ### End of Part To Check and Reprocess L1 Data 
    #########################################################

def reprocessRoughness(currentMonth,picklerPath,satellite,updateDataBeforeProcess):
    #########################################################
    ### Part To Check and Reprocess ROUGHNESS Data 
    #########################################################
    now = datetime.datetime.now() 
    tmpListing = '/home/cercache/project/mpc-sentinel1/analysis/s1_data_analysis/L2_V2/listing/xspec_l2_'+now.strftime('%Y%m%d%H%M%S')+'.tmp'

    elementsToProcess=0
    #########################################################
    ### Part To Generate roughness PNG 
    #########################################################
    if updateDataBeforeProcess:
        l1XmlBaseList,l1XmlFullPathList,l1XmlFullPathDict = check_gen.writeListFromLevel1XmlOnHardDrive(currentMonth,picklerPath,satellite)
        l1RoughnessBaseList,l1RoughnessFullPathList,l1RoughnessFullPathDict = check_gen.writeListFromLevel1RoughnessOnHardDrive(currentMonth,picklerPath,satellite)
    else:
        l1XmlBaseList,l1XmlFullPathList,l1XmlFullPathDict = check_gen.retrieveListFromLevel1XmlOnHardDrive(currentMonth,picklerPath)
        l1RoughnessBaseList,l1RoughnessFullPathList,l1RoughnessFullPathDict = check_gen.retrieveListFromLevel1RoughnessOnHardDrive(currentMonth,picklerPath)
    
    notGeneratedElements = list(set(l1XmlBaseList) - set(l1RoughnessBaseList))
    print ("notGeneratedElements:",len(notGeneratedElements))
    inputList=[]
    filesInError=[]
    tmpFile = open(tmpListing,'w')

    for currentKey in notGeneratedElements:
        currentElement = l1XmlFullPathDict[currentKey].replace(".xml",".tiff").replace('/annotation','/measurement')
        if os.path.exists(currentElement):
            inputList.append(currentElement)
            tmpFile.write(currentElement+"\n")
        else:
            filesInError.append(currentElement)
    elementsToProcess = len(inputList)
    print("FilesInError:", len(filesInError))
    tmpFile.close()
    if elementsToProcess>0:
        os.system(fileRepoMpc+'graphics/produce_roughness_png_from_L1.py --listing '+tmpListing)
        #produce_roughness.SequentialProcess(inputList)
        #Update Data
        l1RoughnessBaseList,l1RoughnessFullPathList,l1RoughnessFullPathDict = check_gen.writeListFromLevel1RoughnessOnHardDrive(currentMonth,picklerPath,satellite)
    #########################################################
    ### End of Part To Generate roughness PNG 
    #########################################################
    
    #########################################################
    ### Part To Index Roughness PNG 
    #########################################################
    
    if updateDataBeforeProcess:
        indexedFullPathList = check_gen.writePathListInIndex(currentMonth,picklerPath,satellite)
    else:
        indexedFullPathList = check_gen.retrievePathListInIndex(currentMonth,picklerPath)
    
    notIndexedElements = list(set(l1XmlFullPathList) - set(indexedFullPathList[dictdbcolnames['roughness']]))
    
    tmpFile = open(tmpListing,'w')
    elementsToProcess=0
    for currentElement in notIndexedElements:
        currentElement = currentElement.replace(".tiff",".xml").replace('/measurement','/annotation')
        tmpFile.write(currentElement+"\n")
        #print currentElement
        elementsToProcess+=1
    tmpFile.close()
    os.chmod(tmpListing,0775)
    
    #print "%d Xml To index "%(elementsToProcess)
    if elementsToProcess>0:
        print ('cat '+tmpListing+'|'+fileRepoMpc+'indexation_in_hbase/index_sentinel-1a-ng-Quicklooks_RoughnessL1_and_L2.py')
        os.system('cat '+tmpListing+'|'+fileRepoMpc+'indexation_in_hbase/index_sentinel-1a-ng-Quicklooks_RoughnessL1_and_L2.py')
        #Update Data
        indexedFullPathList = check_gen.writePathListInIndex(currentMonth,picklerPath,satellite)
    os.remove(tmpListing)

    #########################################################
    ### End of Part To Index Roughness PNG 
    #########################################################

def reprocessWW3Part1(currentMonth,picklerPath,satellite,updateDataBeforeProcess):
    now = datetime.datetime.now() 
    tmpListing = '/home/cercache/project/mpc-sentinel1/analysis/s1_data_analysis/L2_V2/listing/xspec_l2_'+now.strftime('%Y%m%d%H%M%S')+'.tmp'
    repout = generate_listing.getoutputfolder('S1'+satellite.upper())
    elementsToProcess=0
    #########################################################
    ### Part To Index in calval 
    #########################################################

    if updateDataBeforeProcess:
        ncBaseList,ncFullPathList = check_gen.writeListFromNCOnHardDrive(currentMonth,picklerPath,satellite)
        geolocalizedElementsFromCalvalIndex,notGeolocalizedElementsFromCalvalIndex = check_gen.writeNCFileNameFromCalvalIndex(currentMonth,picklerPath)
    else:
        ncBaseList,ncFullPathList = check_gen.retrieveListFromNCOnHardDrive(currentMonth,picklerPath)
        geolocalizedElementsFromCalvalIndex,notGeolocalizedElementsFromCalvalIndex = check_gen.retrieveListGeolocalizedElementsFromCalvalIndex(currentMonth,picklerPath)

    
    notIndexedElements = list(set(ncBaseList) - set(geolocalizedElementsFromCalvalIndex) - set(notGeolocalizedElementsFromCalvalIndex))
    print ("not Indexed elements:",len(notIndexedElements))
    
    linesToGenerate=[]
    tmpFile = open(tmpListing,'w')
    for currentKey in notIndexedElements:
        tmpFile.write(ncFullPathList[currentKey]+"\n")
        elementsToProcess+=1
    tmpFile.close()
    os.chmod(tmpListing,0775)
    print "%d NC L2 To Index in Calval"%(elementsToProcess)
    if elementsToProcess>0:
        os.system('cat '+tmpListing+'|'+fileRepoMpc+'indexation_in_hbase/index_sentinel-l2_v1d.py wv')
        geolocalizedElementsFromCalvalIndex,notGeolocalizedElementsFromCalvalIndex = check_gen.writeNCFileNameFromCalvalIndex(currentMonth,picklerPath)
    os.remove(tmpListing)
    #########################################################
    ### END of Part To Index in calval
    #########################################################
    
    
    #########################################################
    ### Part To Generate TrackFile 
    #########################################################
    if updateDataBeforeProcess:
        ncBaseFromTrack,ncBaseFromTrackOnWater = check_gen.writeFilesFromTrack(currentMonth,picklerPath)
    else:
        ncBaseFromTrack,ncBaseFromTrackOnWater = check_gen.retrieveFilesFromTrack(currentMonth,picklerPath)
    
    notInTrackElements = list(set(geolocalizedElementsFromCalvalIndex) - set(ncBaseFromTrack))
    print len(geolocalizedElementsFromCalvalIndex),len(ncBaseFromTrack)
    if len(notInTrackElements)>0:
        print "launch trackExtraction",len(notInTrackElements)
        os.system("python "+ fileRepoMpc + "colocation/ww3spectra/produce_trackfile_from_index.py -l L2 -s S1"+satellite.upper())
        ncBaseFromTrack,ncBaseFromTrackOnWater = check_gen.writeFilesFromTrack(currentMonth,picklerPath)

    #########################################################
    ### End of Part To Index PNG 
    #########################################################
    print "Process WW3 part 1"

def reprocessWW3Part2(currentMonth,picklerPath,satellite,updateDataBeforeProcess):
    now = datetime.datetime.now() 
    tmpListing = '/home/cercache/project/mpc-sentinel1/analysis/s1_data_analysis/L2_V2/listing/xspec_l2_'+now.strftime('%Y%m%d%H%M%S')+'.tmp'
    repout = generate_listing.getoutputfolder('S1'+satellite.upper())
    elementsToProcess=0
    #########################################################
    ### Part To Generate PNG 
    #########################################################
    if updateDataBeforeProcess :
        ww3NcBaseList,ww3NcFullPathList = check_gen.writeListFromWW3OnHardDrive(currentMonth,picklerPath,satellite)
        pngBaseList,pngFullPathList = check_gen.writeListFromPNGOnHardDrive(currentMonth,picklerPath,satellite)
    else:
        ww3NcBaseList,ww3NcFullPathList = check_gen.retrieveListFromWW3OnHardDrive(currentMonth,picklerPath)
        pngBaseList,pngFullPathList = check_gen.retrieveListFromPNGOnHardDrive(currentMonth,picklerPath)
    
    notGeneratedElements = list(set(ww3NcBaseList) - set(pngBaseList[dictdbcolnames[satellite]['ww3_V2']]))
    print ("notGeneratedElements:",len(notGeneratedElements))
    
    linesToGenerate=[]
    tmpFile = open(tmpListing,'w')
    for currentKey in notGeneratedElements:
        inputfile = ww3NcFullPathList[currentKey]
        linesToGenerate = generate_listing.getoutputlines(inputfile,repout,'WW3')
        for currentline in linesToGenerate:
            tmpFile.write(currentline+"\n")
            elementsToProcess+=1
    tmpFile.close()
    os.chmod(tmpListing,0775)
    print "%d WW3 NC To generate in PNG"%(elementsToProcess)
    if elementsToProcess>0:
        os.system('cat '+tmpListing+'|'+fileRepoWavetool+'runner_plot_spectra_v2.bash')
        #Update Data
        pngBaseList,pngFullPathList = check_gen.writeListFromPNGOnHardDrive(currentMonth,picklerPath,satellite)
    os.remove(tmpListing)
    #########################################################
    ###END of Part To Generate PNG
    #########################################################
    
    
    #########################################################
    ### Part To Index PNG 
    #########################################################
    
    if updateDataBeforeProcess:
        indexedFullPathList = check_gen.writePathListInIndex(currentMonth,picklerPath,satellite)
    else:
        indexedFullPathList = check_gen.retrievePathListInIndex(currentMonth,picklerPath)
    
    elementsToProcess = 0
    tmpFile = open(tmpListing,'w')
    
    notIndexedElements = list(set(pngFullPathList[dictdbcolnames[satellite]['ww3_V2']]) - set(indexedFullPathList[dictdbcolnames[satellite]['ww3_V2']]))
    elementsToProcess += len(notIndexedElements)
    for currentElement in notIndexedElements:
        tmpFile.write(currentElement+"\n")
    tmpFile.close()
    os.chmod(tmpListing,0775)
    
    print "%d WW3 PNG To index "%(elementsToProcess)
    if elementsToProcess>0:
        os.system('cat '+tmpListing+'|'+fileRepoMpc+'indexation_in_hbase/index_sentinel-1a-ng-Quicklooks_RoughnessL1_and_L2_V2.py')
        #Update Data
        indexedFullPathList = check_gen.writePathListInIndex(currentMonth,picklerPath,satellite)
    os.remove(tmpListing)
    
    #########################################################
    ### End of Part To Index PNG 
    #########################################################
    print "Process WW3 part 2"

def cmdlineparser(parser):
    """
    define the options of the scripts and set func depending of the inputs
    """
    #OBJCHOICES = ('WV', 'SAFE')

    #parser.add_argument('-d', '--debug', action='store_true', default=False, help='Debug logs')
    #parser.add_argument('--ww3colocprovenance', action='store', default=None,choices=['SATGLOB','REJEU2016','SENTINEL'], help='read the coloc WW3 from a specific directory = specific run [optional]')
    #parser.add_argument('--indexname', action='store', default=None, help='give a different indexname for special purpose',required=False)
    #parser.add_argument('-m','--mode', action='store',default=['both'],nargs=1, help='determine the storage mode hbase/phoenix/both by default it\'s both')
    #parser.add_argument('data_type', type=str.upper, choices=OBJCHOICES)
    parser.add_option("-b","--beginningdate",
                        action="store", type="string",
                        dest="begdate", metavar="string",
                        help="starting date YYYYMM [default=begining of mission]")
    parser.add_option("-e","--endingdate",
                        action="store", type="string",
                        dest="enddate", metavar="string",
                        help="ending date YYYYMM [default=current day]")
    parser.add_option("-m","--mode",
                        action="store", type="string",
                        dest="mode", metavar="string",
                        help="Mode [L2,L1,ROUGHNESS,ALL]")
    parser.add_option("-u","--updateDataBeforeProcess",
                        action="store_true", 
                        dest="updateDataBeforeProcess", default=False,
                        help="updateDataBeforeProcess determine if using already existing data or refresh detection")
    parser.add_option("-s","--satellite",
                        action="store", type="string",
                        dest="satellite", metavar="string",
                        help="Mode [S1A,S1B,ALL]")
    
    (options, args) = parser.parse_args()

    return options

def main():
    
    from optparse import OptionParser
    
    #Get the options--------
    parser = OptionParser()
    options = cmdlineparser(parser)

    if options.begdate == None:
        startdate = mission_start_s1a
        print "noStartDate"
    else:
        startdate = datetime.datetime.strptime(options.begdate,'%Y%m')
    if options.satellite == "S1A":
        satellite = ["S1A"]
    elif options.satellite == "S1B":
        satellite=["S1B"]
    else:
        satellite=["S1A","S1B"]

    if options.enddate == None :
        enddate = startdate
        print "noEndDate"
    else:
        enddate = datetime.datetime.strptime(options.enddate,'%Y%m')

    print satellite
    if options.mode == None :
        options.mode="L2"
        print "USING L2 By Default mode"
    print "mode",options.mode
    print "time interval",startdate,enddate
    
    updateDataBeforeProcess = options.updateDataBeforeProcess

    while startdate<=enddate:
        currentMonth= str(startdate.year) + str(startdate.month).zfill(2)
        for currentSatellite in satellite:
            if currentSatellite == "S1A":
                picklerPath = picklerPathA
                sat="a"
            elif currentSatellite == "S1B":
                picklerPath = picklerPathB
                sat="b"
            if options.mode == "L2":
                print "reprocessL2",currentMonth,picklerPath,sat
                reprocessL2(currentMonth,picklerPath,sat,updateDataBeforeProcess)
            if options.mode == "L1":
                print "reprocessL1",currentMonth,picklerPath,sat
                reprocessL1(currentMonth,picklerPath,sat,updateDataBeforeProcess)
            if options.mode == "ROUGHNESS":
                print "reprocessROUGHNESS",currentMonth,picklerPath,sat
                reprocessRoughness(currentMonth,picklerPath,sat,updateDataBeforeProcess)
            if options.mode == "WW3PART1":
                print "reprocessWW3PART1",currentMonth,picklerPath,sat
                reprocessWW3Part1(currentMonth,picklerPath,sat,updateDataBeforeProcess)
            if options.mode == "WW3PART2":
                print "reprocessWW3PART2",currentMonth,picklerPath,sat
                reprocessWW3Part2(currentMonth,picklerPath,sat,updateDataBeforeProcess)
            if options.mode == "ALL":
                print "reprocessL1",currentMonth,picklerPath,sat
                reprocessL1(currentMonth,picklerPath,sat,updateDataBeforeProcess)
                print "reprocessROUGHNESS",currentMonth,picklerPath,sat
                reprocessRoughness(currentMonth,picklerPath,sat,updateDataBeforeProcess)
                print "reprocessL2",currentMonth,picklerPath,sat
                reprocessL2(currentMonth,picklerPath,sat,updateDataBeforeProcess)
                #Not tested Enough
                #print "reprocessWW3PART1",currentMonth,picklerPath,sat
                #reprocessWW3Part1(currentMonth,picklerPath,sat,updateDataBeforeProcess)
                print "reprocessWW3PART2",currentMonth,picklerPath,sat
                reprocessWW3Part2(currentMonth,picklerPath,sat,updateDataBeforeProcess)
            startdate = startdate + relativedelta(months=+1)
        
    sys.exit(0)

    

if __name__ == '__main__':
    main()
    