#!/bin/bash
# prun-> pbs -> ->cat lst | wavetools_launcher_stream.py
echo 'attention le job prun va commencer'
echo 'fasten your seat belt, the job will start in a minute'
listing=/home1/scratch/agrouaze/prun/S1l2spectragen/listing_1.txt
listing=/home1/scratch/agrouaze/prun/S1l2spectragen/listing_2.txt
listing=/home1/scratch/agrouaze/prun/S1l2spectragen/listing_2015_v2.txt
listing=/home1/scratch/agrouaze/prun/S1l2spectragen/listing_2016_v1.txt #2245674 lines
listing=/home1/scratch/agrouaze/prun/S1l2spectragen/listing_2016_v2.txt # 3774 lines
listing=/home1/scratch/agrouaze/prun/S1l2spectragen/listing_2017_v1.txt # 2100000 lines
listing=/home1/scratch/agrouaze/prun/S1l2spectragen/listing_2018_v1.txt # 2300000 lines
listing=/home1/scratch/agrouaze/prun/S1l2spectragen/listing_s1a_2019_v1.txt # 2269506 lines
listing=/home1/scratch/agrouaze/prun/S1l2spectragen/listing_s1a_2020_v1.txt # 1045000 lines
listing=/home1/scratch/agrouaze/prun/S1l2spectragen/listing_s1b_2016_v1.txt # 1187000 lines
listing=/home1/scratch/agrouaze/prun/S1l2spectragen/listing_s1b_2017_v1.txt # 2500000 lines
listing=/home1/scratch/agrouaze/prun/S1l2spectragen/listing_s1b_2018_v1.txt # 2430000 lines
listing=/home1/scratch/agrouaze/prun/S1l2spectragen/listing_S1B_2019_v2.txt # 2430000 lines
listing=/home1/scratch/agrouaze/prun/S1l2spectragen/listing_S1B_2020_v2.txt # 2430000 lines
listing=/home1/datawork/satwave/data/sentinel1/ww3spectraplot_2015.txt
listing=/home1/datawork/satwave/data/sentinel1/ww3spectraplot_2016S1A.txt
listing=/home1/datawork/satwave/data/sentinel1/ww3spectraplot_2016S1B.txt #64000 missing a revoir
listing=/home1/datawork/satwave/data/sentinel1/ww3spectraplot_2017S1A.txt
listing=/home1/datawork/satwave/data/sentinel1/ww3spectraplot_2017S1B.txt
listing=/home1/datawork/satwave/data/sentinel1/ww3spectraplot_2018S1A.txt
listing=/home1/datawork/satwave/data/sentinel1/ww3spectraplot_2018S1B.txt # 134k rate
listing=/home1/datawork/satwave/data/sentinel1/ww3spectraplot_2019S1A.txt
listing=/home1/datawork/satwave/data/sentinel1/ww3spectraplot_2019S1B.txt
echo 'dollar1 '$1
if [ "$1" = 'list' ]; then
   echo 'redo input listing '
   . /appli/anaconda/2.7/etc/profile.d/conda.sh
   conda activate /home1/datawork/agrouaze/conda_envs/py3_dev_datawork/
   #python /home1/datahome/agrouaze/git/wavetoolbox/generate_listing_v1_02.py -q -g -b 20150101 -e 20151231 -p S1A -s SAT -r -o $listing --version-outputdir V3
   python /home1/datahome/agrouaze/git/wavetoolbox/generate_listing_v1_02.py -q -g -b 20150101 -e 20151231 -p S1A -s WW3 -r -o $listing --version-outputdir V3
   conda deactivate
else
   echo 'no listing to redo'
fi
echo $listing
/appli/prun/bin/prun --split-max-jobs=2000 --mem=301m --name S1l2spectragen --max-time=28:30:00 --background -e /home1/datahome/satwave/sources_en_exploitation2/wavetoolbox/create_sentinel1_spectra_l2_png.pbs $listing
echo  'va voir dans les logs '
echo 'ou bien va voir ici /home/datawork-cersat-public/project/mpc-sentinel1/analysis/s1_data_analysis/L2_V3/WV/'
