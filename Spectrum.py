#from plot_spec_V2 import plot_spec_generic
from plot_spec_V2 import spec_plot as spec_plot_v2
from plot_spec_V2 import getColorMap
import logging
import matplotlib.pyplot as plt
import matplotlib as mpl
import os
import numpy as np

titledict = {'satimaginary':' Ocean Wave X Spectrum (Im) \n',
             'satreal':' Ocean Wave X Spectrum (Re) \n',
             'satfull':' Ocean Wave Spectrum \n',
             'ww3':' Ocean Wave Spectrum \n',
             'bgn':' \n',
             'bgc':' \n',
             'cfosat':'CFO Sat\n'
             }

cdict = {'red': ((0., 1, 1),
                 (0.05, 1, 1),
                 (0.11, 0, 0),
                 (0.66, 1, 1),
                 (0.89, 1, 1),
                 (1, 0.5, 0.5)),
         'green': ((0., 1, 1),
                   (0.05, 1, 1),
                   (0.11, 0, 0),
                   (0.375, 1, 1),
                   (0.64, 1, 1),
                   (0.91, 0, 0),
                   (1, 0, 0)),
         'blue': ((0., 1, 1),
                  (0.05, 1, 1),
                  (0.11, 1, 1),
                  (0.34, 1, 1),
                  (0.65, 0, 0),
                  (1, 0, 0))}
my_cmap = mpl.colors.LinearSegmentedColormap('my_colormap',cdict,256)

palette_path = 'palettes/wave_spec.pal'
palette_path = os.path.join(os.path.dirname(__file__), palette_path)

def init_spectrum_style_default():
    spectrumStyle={'nrow':1,'ncolumn':1,'xsize':9,'ysize':6,
                   'titlecolor':"black", 'titlesize':12,'legendsize':10,'markersize':10,
                   'colormap':my_cmap, 'circleradius':[50,100,200,400],# 'legend_style':None,
                    'vmin':None,'vmax':None, 'colorbarTitle':'Spectral energy [$m^4$]',
                    'hs':1}
    return spectrumStyle

def init_spectrum_style(mode=None):
    
    tmpstyle = init_spectrum_style_default()
    if mode == "ww3":
        cmapw = getColorMap(rgbFile = palette_path )   
        tmpstyle['colormap'] = cmapw
    elif mode == "satimaginary":
        tmpstyle['colormap'] = mpl.cm.PuOr
    elif mode == "satfull":
        cmapw = getColorMap(rgbFile = palette_path )   
        tmpstyle['colormap'] = cmapw
    elif mode == "cfosat":
        cmapw = getColorMap(rgbFile = palette_path )   
        tmpstyle['colormap'] = cmapw
    return tmpstyle



class Wind:
    """
    Class that contain a wind element
    """
    U  = None
    V  = None
    
    def __init__(self, *initial_data, **kwargs):
        for dictionary in initial_data:
            for key in dictionary:
                setattr(self, key, dictionary[key])
        for key in kwargs:
            setattr(self, key, kwargs[key])


class Spec_data:
    """
    Class that contain the spec data of a spectrum
    part=partitionnement
    start_time=start date and time 
    sp= the spectrum
    k = wave number
    phi = wave angle
    """
    part = None
    start_time = None
    sp = None
    k = None
    phi = None
    
    def __init__(self, *initial_data, **kwargs):
        for dictionary in initial_data:
            for key in dictionary:
                setattr(self, key, dictionary[key])
        for key in kwargs:
            setattr(self, key, kwargs[key])
class Wsys:
    """
    Class that contains the wave system
    """
    hs = None
    wl = None
    dirad = None
    lon = None
    lat = None

    def __init__(self, *initial_data, **kwargs):
        for dictionary in initial_data:
            for key in dictionary:
                setattr(self, key, dictionary[key])
        for key in kwargs:
            setattr(self, key, kwargs[key])
        
class Meta:
    """
        class to store and get the type of data 
        and specific elements 
    """
    #dataType = None
    
    def __init__(self, *initial_data, **kwargs):
        for dictionary in initial_data:
            for key in dictionary:
                setattr(self, key, dictionary[key])
        for key in kwargs:
            setattr(self, key, kwargs[key])

class Spectrum:
    wind = None
    wsys = None
    spec_data = None
    meta = None
    dataName = 'UNK'
    title = None
    
    def __init__(self,myspec_data,mywsys,mywind,mymeta,title=None,dataName=dataName):
        
        self.dataName=dataName
        
        if isinstance(mywind,Wind):
            self.wind=mywind
        else:
            self.wind = Wind(mywind)
        if isinstance(mywsys,Wsys):
            self.wsys = mywsys
        else:
            self.wsys = Wsys(mywsys)
        if isinstance(myspec_data,Spec_data):
            self.spec_data = myspec_data
        else:
            self.spec_data = Spec_data(myspec_data)
        if isinstance(mymeta,Meta):
            self.meta = mymeta
        else:
            self.meta = Meta(mymeta)
        #if no title as been given generate one
        if not title:
            self.title = self.generateSpectrumTitle()
        else:
            self.title = title
    
    def generate(self,style=None,cut_off=50,version='v2'):
        """
            Function that will call the generation of a spectrum figure then return it
        :args:
            version (str): v2 is mpl + contourf v3 is xarray + scatter
        """
        if not style:
            style = init_spectrum_style(self.meta.type)

        if version=='v2':
            fct_spec_plot = spec_plot_v2
        elif version=='v3':
            from plot_spec_V3 import spec_plot as spec_plot_v3
            fct_spec_plot = spec_plot_v3
        else:
            logging.error('%s is a version of spectra dsisplay unknown not handle',style['version'])

        if self.meta.type[0] == "s": 
        #Test for imaginary part
            if hasattr(self.spec_data,'k'):
                if isinstance(self.spec_data.k,np.ma.core.MaskedArray):
                    return fct_spec_plot(self,style,cut_off = cut_off,
                              sp_log = None)
                else:
                    plt.annotate('Spectrum cannot be displayed because of unusable values',xy=(0.375, 0.5),color=(1., .4, 0.))
        else:
            return fct_spec_plot(self,style, cut_off = cut_off,
                          sp_log = None)
        return None
    
    def generate_v2(self,style=None,cut_off=50):
        """
        version Dec 2019 to use polar_plot_xarray instead of the classic spec_plot()
        :param style:
        :param cut_off:
        :return:
        """

    
    def show(self,style=None,cut_off=50,version='v2'):
        """
            Function that will call the generation of a spectrum figure then show it
        """
        myFig = self.generate(style=style,cut_off=cut_off,version=version)
        #myFig = plot_spec_generic(self,mycolormap=mycolormap,dateFormat=dateFormat)
        plt.show()
        if myFig:
            plt.close(myFig)

    def write(self,outputfile,showresult=False,style=None,
               overwrite=False,dpi=100,version='v2'):
        if not(overwrite) and (os.path.isfile(outputfile)):
            pass
            logging.debug('file:'+outputfile+' already exist!')
            status = 'already_exist_no_overwrite'
        else:
            myFig = self.generate(style=style,version=version)
            if 'eps' in outputfile:
                myFig.savefig(outputfile,format='eps', dpi=dpi)
            else:
                myFig.savefig(outputfile,format='png', dpi=dpi)
            if showresult:
                plt.show()
            plt.close(myFig)
            if os.path.exists(outputfile):
                status = 'overwritten'
            else:
                status = 'written'
        return status

    def generateSpectrumTitle(self,dateFormat="%Y-%m-%d %H:%M:%S"):
        mydate = ""
        if hasattr(self,'spec_data') and hasattr(self.spec_data,'start_time') and self.spec_data.start_time is not None:
            mydate = self.spec_data.start_time.strftime(dateFormat)
        
        if self.meta.type in titledict:
            return self.dataName + titledict[self.meta.type] + mydate
        else:
            return 'Default name \n' + mydate
    

    
        
    
   
