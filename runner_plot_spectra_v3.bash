#!/bin/bash
#runner to create the plot for ww3 spectra (colocated with track SAR)
#agrouaze June 2020
# arguments waited are listings like this:
# input_filename.nc output_filename.png type_of_spectra_to_produce
#/home/datawork-cersat-public/project/mpc-sentinel1/data/esa/sentinel-1a/L2/WV/S1A_WV_OCN__2S/2020/174/S1A_WV_OCN__2SSV_20200622T230628_20200622T232253_033136_03D6BA_4EC5.SAFE/measurement/s1a-wv1-ocn-vv-20200622t231249-20200622t231252-033136-03d6ba-027.nc /home/datawork-cersat-public/project/mpc-sentinel1/analysis/s1_data_analysis/L2_V2/WV/S1A_WV_OCN__2S/2020/174/S1A_WV_OCN__2SSV_20200622T230628_20200622T232253_033136_03D6BA_4EC5.SAFE/xspec/ocean_swell_spectra_s1a-wv1-ocn-vv-20200622t231249-20200622t231252-033136-03d6ba-027.png satfull
#/home/datawork-cersat-public/project/mpc-sentinel1/data/esa/sentinel-1a/L2/WV/S1A_WV_OCN__2S/2020/174/S1A_WV_OCN__2SSV_20200622T230628_20200622T232253_033136_03D6BA_4EC5.SAFE/measurement/s1a-wv1-ocn-vv-20200622t231249-20200622t231252-033136-03d6ba-027.nc /home/datawork-cersat-public/project/mpc-sentinel1/analysis/s1_data_analysis/L2_V2/WV/S1A_WV_OCN__2S/2020/174/S1A_WV_OCN__2SSV_20200622T230628_20200622T232253_033136_03D6BA_4EC5.SAFE/xspec/realcrossspectra_s1a-wv1-ocn-vv-20200622t231249-20200622t231252-033136-03d6ba-027.png satreal
#/home/datawork-cersat-public/project/mpc-sentinel1/data/esa/sentinel-1a/L2/WV/S1A_WV_OCN__2S/2020/174/S1A_WV_OCN__2SSV_20200622T230628_20200622T232253_033136_03D6BA_4EC5.SAFE/measurement/s1a-wv1-ocn-vv-20200622t231249-20200622t231252-033136-03d6ba-027.nc /home/datawork-cersat-public/project/mpc-sentinel1/analysis/s1_data_analysis/L2_V2/WV/S1A_WV_OCN__2S/2020/174/S1A_WV_OCN__2SSV_20200622T230628_20200622T232253_033136_03D6BA_4EC5.SAFE/xspec/imaginarycrossspectra_s1a-wv1-ocn-vv-20200622t231249-20200622t231252-033136-03d6ba-027.png satimaginary
#test

# export PYTHONPATH=/home1/datahome/agrouaze/git/mpc/qualitycheck:/home1/datahome/agrouaze/git/mpc/data_collect:/home1/datahome/agrouaze/git/cfosat-calval-exe:/home1/datahome/agrouaze/git/mpc/colocation:/home1/datahome/agrouaze/git/mpc/colocation/ww3spectra:/home1/datahome/agrouaze/git/cerform
# echo "/home/datawork-cersat-public/project/mpc-sentinel1/data/esa/sentinel-1a/L2/WV/S1A_WV_OCN__2S/2020/174/S1A_WV_OCN__2SSV_20200622T230628_20200622T232253_033136_03D6BA_4EC5.SAFE/measurement/s1a-wv1-ocn-vv-20200622t231249-20200622t231252-033136-03d6ba-027.nc /tmp/project/mpc-sentinel1/analysis/s1_data_analysis/L2_V2/WV/S1A_WV_OCN__2S/2020/174/S1A_WV_OCN__2SSV_20200622T230628_20200622T232253_033136_03D6BA_4EC5.SAFE/xspec/ocean_swell_spectra_s1a-wv1-ocn-vv-20200622t231249-20200622t231252-033136-03d6ba-027.png satfull" | bash /home1/datahome/agrouaze/git/wavetoolbox/runner_plot_spectra_v3.bash
echo 'start generation of png for xwaves app'

#source /home1/datawork/agrouaze/condabase/bin/activate /home1/datawork/agrouaze/conda_envs/py3_wavetoolbox_exploit/
#which python
#python /home1/datahome/agrouaze/git/wavetoolbox/wavetools_launcher_stream.py --overwrite --style v3 $*
myvariable=$(whoami)
if [[ "${myvariable}" == "agrouaze" ]]; then
    echo 'je suis agrouaze'
    export PYTHONPATH=/home1/datahome/agrouaze/git/mpc/graphics/
    export PYTHONPATH=/home1/datahome/agrouaze/git/cfosat-calval-exe/:$PYTHONPATH
    export PYTHONPATH=/home1/datahome/agrouaze/git/mpc/colocation:$PYTHONPATH
    export PYTHONPATH=/home1/datahome/agrouaze/git/mpc/colocation/ww3spectra:$PYTHONPATH
    export PYTHONPATH=/home1/datahome/agrouaze/git/mpc/qualitycheck:$PYTHONPATH
    export PYTHONPATH=/home1/datahome/agrouaze/git/mpc/data_collect:$PYTHONPATH
    export PYTHONPATH=/home1/datahome/agrouaze/git/cerform/:$PYTHONPATH

    exe=/home1/datahome/agrouaze/git/wavetoolbox/wavetools_launcher_stream.py
else
  export PYTHONPATH=/home1/datahome/satwave/sources_en_exploitation2/mpc/graphics/
  export PYTHONPATH=/home1/datahome/satwave/sources_en_exploitation2/cfosat-calval-exe/:$PYTHONPATH
  export PYTHONPATH=/home1/datahome/satwave/sources_en_exploitation2/mpc/colocation:$PYTHONPATH
  export PYTHONPATH=/home1/datahome/satwave/sources_en_exploitation2/mpc/colocation/ww3spectra:$PYTHONPATH
  export PYTHONPATH=/home1/datahome/satwave/sources_en_exploitation2/mpc/qualitycheck:$PYTHONPATH
  export PYTHONPATH=/home1/datahome/satwave/sources_en_exploitation2/mpc/data_collect:$PYTHONPATH
  export PYTHONPATH=/home1/datahome/satwave/sources_en_exploitation2/cerform:$PYTHONPATH

  exe=/home1/datahome/satwave/sources_en_exploitation2/wavetoolbox/wavetools_launcher_stream.py
fi
pybin=/home1/datawork/agrouaze/conda_envs/py3_wavetoolbox_exploit/bin/python
$pybin $exe --style v3 $*
echo 'job finished'