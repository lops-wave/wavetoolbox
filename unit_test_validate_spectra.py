"""
tests to do with old env: . /home/cercache/tools/environments/scientific_toolbox_cloudphys_precise/bin/activate.sh
broken env: /home1/datawork/agrouaze/conda_envs/py2.7_dev_ww3spectra_plot_from_airflow_worker_ubuntu14.04 (xarray and python version not compatible)
latest env: source activate /home1/datawork/agrouaze/conda_envs/py3_wavetoolbox_exploit/
"""

import netCDF4
import sys
import logging
# sys.path.append('/home1/datahome/agrouaze/git/mpc/graphics')
# sys.path.append('/home1/datahome/agrouaze/git/mpc/qualitycheck')
# sys.path.append('/home1/datahome/agrouaze/git/mpc/colocation')
# sys.path.append('/home1/datahome/agrouaze/git/cerform')
# sys.path.append('/home1/datahome/agrouaze/git/cfosat-calval-exe')
import Spectrum as spc
import os
from SpectrumReaderWW3 import SpectrumReaderWw3
from SpectrumReaderSentinel import SpectrumReaderSat
def do_simple_spectra(filepath1,title,dirout=None,mode='satfull'):
    nc=netCDF4.Dataset(filepath1)
    inst = SpectrumReaderSat()
    spectra = inst.read_spectrum(nc,dataName=title,mode=mode,filepath=filepath1)
    # print(dir(spectra))
    # print(spectra.spec_data.sp)
    style0 = spc.init_spectrum_style(mode)
    if False:
        spectra.wind = None
#     style = {'legendsize':20,'vmin':None,'vmax':None}
    style = {'nrow':1,'ncolumn':1,'xsize':9,'ysize':6,
               'titlecolor':"black", 'titlesize':20,'legendsize':11,'markersize':10,
               'colormap':style0['colormap'], 'circleradius':[50,100,200,400],# 'legend_style':None,
                'vmin':None,'vmax':None, 'colorbarTitle':'Spectral energy [$m^4$]',
                'hs':1,'colorbarlabelfontsize':10,'annotationfontsize':15,'cutoff':50}
    #spectra.show(style=style,cut_off=style['cutoff'])
#     outputfile = os.path.join(dirout,os.path.basename(filepath1).replace('.nc','.eps'))
    outputfile = os.path.join(dirout,mode+os.path.basename(filepath1).replace('.nc','.png'))
    if True:
        spectra.write(outputfile,showresult=False,style=style,
               overwrite=True,dpi=500)
    logging.info("outputfile %s",outputfile)


def do_simple_spectra_ww3(filepath1,title,dirout=None,mode='ww3'):
    nc=netCDF4.Dataset(filepath1)
    inst = SpectrumReaderWw3()
    spectra = inst.read_spectrum(nc,dataName=title,mode=mode,path=filepath1)
    # print(dir(spectra))
    # print(spectra.spec_data.sp)
    style0 = spc.init_spectrum_style(mode)
    #if False:
    #    spectra.wind = None
#     style = {'legendsize':20,'vmin':None,'vmax':None}
    style = {'nrow':1,'ncolumn':1,'xsize':9,'ysize':6,
               'titlecolor':"black", 'titlesize':20,'legendsize':11,'markersize':10,
               'colormap':style0['colormap'], 'circleradius':[50,100,200,400],# 'legend_style':None,
                'vmin':None,'vmax':None, 'colorbarTitle':'Spectral energy [$m^4$]',
                'hs':1,'colorbarlabelfontsize':10,'annotationfontsize':15,'cutoff':50}
    #spectra.show(style=style,cut_off=style['cutoff'])
#     outputfile = os.path.join(dirout,os.path.basename(filepath1).replace('.nc','.eps'))
    outputfile = os.path.join(dirout,os.path.basename(filepath1).replace('.nc','.png'))
    if True:
        spectra.write(outputfile,showresult=False,style=style,
               overwrite=True,dpi=500)
    logging.info("outputfile %s",outputfile)

if __name__ =='__main__':
    import argparse

    parser = argparse.ArgumentParser(description = 'test wavetoolbox')
    parser.add_argument('--verbose',action = 'store_true',default = False)
    args = parser.parse_args()
    if args.verbose :
        logging.basicConfig(level = logging.DEBUG,format = '%(asctime)s %(levelname)-5s %(message)s',
                            datefmt = '%d/%m/%Y %H:%M:%S')
    else :
        logging.basicConfig(level = logging.INFO,format = '%(asctime)s %(levelname)-5s %(message)s',
                            datefmt = '%d/%m/%Y %H:%M:%S')
    filepath1 = '/home/datawork-cersat-public/project/mpc-sentinel1/data/esa/sentinel-1b/L2/WV/S1B_WV_OCN__2S/2020/060/S1B_WV_OCN__2SSV_20200229T020943_20200229T024440_020477_026CCB_86C0.SAFE/measurement/s1b-wv2-ocn-vv-20200229t024310-20200229t024313-020477-026ccb-138.nc'
    datestr = os.path.basename(filepath1).split('-')[4]
    dirout = '/tmp/testsp'
    if True:
        do_simple_spectra(filepath1,title='S1 OCN wave spectra\n %s'%datestr,dirout = dirout,mode = 'satfull')
        do_simple_spectra(filepath1,title = 'S1 Re image spectra\n %s' % datestr,dirout = dirout,mode = 'satreal')
        do_simple_spectra(filepath1,title = 'S1 Im image spectra\n %s' % datestr,dirout = dirout,mode = 'satimaginary')
    ffww3 = '/home/cercache/project/mpc-sentinel1/data/colocation/sentinel-1b/sar-model/ww3spectra/201905/netCDF_L2_daily/20190502/ww3-wv1-ocn-vv-20190502t001225-20190502t001228-016057-01e33a-001.nc'
    ffww3 = '/home/datawork-cersat-public/project/mpc-sentinel1/data/colocation/sentinel-1b/sar-model/ww3spectra/202002/netCDF_L2_daily/20200229/ww3-wv2-ocn-vv-20200229t024310-20200229t024313-020477-026ccb-138.nc'
    do_simple_spectra_ww3(ffww3,'test ww3',dirout = dirout,mode = 'ww3')
