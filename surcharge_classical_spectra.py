"""
purpose: draw spectra with plot_spec_V3 as usual but add information such as the low freq masked pixels of the peak param from WW3 grid
for S1 and WW3 spectra
creation: Sept 2020
author: Antoine Grouazel
"""

import sys
sys.path.append('/home1/datahome/agrouaze/git/mpc/data_collect')
sys.path.append('/home1/datahome/agrouaze/git/mpc/qualitycheck/')
sys.path.append('/home1/datahome/agrouaze/git/mpc/graphics/')
sys.path.append('/home1/datahome/agrouaze/git/mpc/colocation/')
sys.path.append('/home1/datahome/agrouaze/git/wavetoolbox/')
sys.path.append('/home1/datahome/agrouaze/git/cerform/')
sys.path.append('/home1/datahome/agrouaze/git/cfosat-calval-exe/')
import logging

import os
import netCDF4
import matplotlib.pyplot as plt
import numpy as np
import xarray as xr
import matplotlib.image as mpimg
import get_roughness_path_from_measu
import get_fullpath_from_subpath_dailyL2F
import get_quicklook_L2spectrapath_from_measu
from SpectrumReader_V2 import SpectrumReader
import get_ww3spectrum_file_fromL2S1measu
from get_ww3grid_fromS1measu_L2_fullpath import get_partitions_params_ww3grid,get_path_ww3grid,get_closest_gridpoint
from Spectrum import init_spectrum_style_default,init_spectrum_style

newstyle = init_spectrum_style_default()
newstyle['xsize'] = 10
newstyle['ysize'] = 9
newstyle['legendsize'] = 12
newstyleIm = init_spectrum_style('satimaginary')
newstyleIm['xsize'] = 10
newstyleIm['ysize'] = 9
newstyleIm['legendsize'] = 12

def get_lowfreq_mask(nc,plots=False):
    """

    :param nc:
    :param plots:
    :return:
    masklowfreq np.array 2D of float
    phi in degrees clockwise relative to geo north
    """
    parts = nc.variables['oswPartitions'][:].squeeze()
    print('parts',parts.shape)
    print(np.unique(parts))
    if plots:
        plt.pcolor(parts)
        plt.colorbar()
        plt.show()
    masklowfreq = parts==-1
    final = np.ones(masklowfreq.shape)*np.nan
    inds = np.where(masklowfreq)
    final[inds] = 1
    k = nc.variables['oswK'][:]
    phi = nc.variables['oswPhi'][:]
    return final,k,phi

def add_greyarea_for_low_freq_filter(lowfreq2dmat,k,phi,ax,cutoff):
    cc = lowfreq2dmat
    # cc = cc.filled(np.nan)
    xx = k
    yy = np.radians(phi)

    spex_xarray = xr.DataArray(cc.T,coords=[xx,yy],dims=['k','phi'])
    ind_k_ok = slice(0,-1)
    if False:
        qm = spex_xarray[{'k' : ind_k_ok}].plot(ax=ax,cmap='Greys',vmin=None,vmax=None,antialiased=True,add_colorbar=False,alpha=0.5,)
        qm.axes.grid(False)
        qm.axes.set_ylabel('',fontsize=18,rotation=0)
        qm.axes.set_xlabel('',fontsize=18,rotation=0)
        qm.axes.set_rmax(2 * np.pi / cutoff)
    else:
        mx,my = np.meshgrid(xx,yy)
        plt.pcolormesh(my,mx,lowfreq2dmat,cmap='viridis',alpha=0.5)
        # plt.plot(my.ravel(),mx.ravel(),lowfreq2dmat.ravel(),'s',color='grey')
    # qm.set_xlabels("")
    # qm.set_ylabels("")
    # qm.axes.set_visible(False)
    # print(dir(qm))
    # plt.colorbar(qm)


def sucharge_spectra_figures(pathL2,subdf,onthefly=True,cutoff=50,newstyle=newstyle,newstyleIm=newstyleIm):
    """

    :param pathL2:
    :param subdf: dataframe pandas build using L2F files
    :return:
    """
    display_ww3_ony_the_fly = True
    #print('debug',subdf,subdf['fname']==os.path.basename(pathL2))
    ii = np.where(subdf['fname']==os.path.basename(pathL2))[0][0]
    #print('ii',ii)
    plt.figure(figsize=(10,8),dpi=200)
    # plt.subplot(1,2,1)
    supath_iL1 = str(subdf['subpath_l1'][ii])
    subpath_i = str(subdf['subpath'][ii])
    fp = get_fullpath_from_subpath_dailyL2F.get_full_path(subpath_i)

    fp_l1 = get_fullpath_from_subpath_dailyL2F.get_full_path(supath_iL1,L1=True)

    roughn_path,flagexi = get_roughness_path_from_measu.get_roughness_from_measu(fp)
    img = mpimg.imread(roughn_path)
    plt.imshow(img)
    # plt.subplot(1,2,2)
    if onthefly is False :
        quicklook_type = 'ocean_swell_spectra'
        producttype = os.path.basename(os.path.abspath(os.path.join(os.path.dirname(fp),os.pardir)))[0 :14]
        spectrapath = get_quicklook_L2spectrapath_from_measu.getQuicklook(producttype,fp,quicklook_type,
                                                                          atlernative_version_spectra='V3')
        print(spectrapath)
        valsy = subdf['oswXA_wl_ww3spec_firstSARpartition_s1'][ii], \
                subdf['oswXA_wl_ww3spec_firstSARpartition_ww3'][ii],np.degrees(
            subdf['oswXA_dirad_ww3spec_firstSARpartition_s1'][ii]),np.degrees(
            subdf['oswXA_dirad_ww3spec_firstSARpartition_ww3'][ii])
        plt.title(
            'first partition peak wavelength SAR = %1.1fm WW3 = %1.1fm\n peak direction SAR = %1.1f$^o$ WW3 = %1.1f$^o$' % (
                valsy))
        img = mpimg.imread(spectrapath)
        plt.imshow(img)
        plt.show()
    else:
        # on the fly OCN wave spectra
        instance = SpectrumReader()
        print('fp',fp)
        currentSpectrum = instance.read_spectrum(fp,'satfull',path=None)
        currentSpectrum.path = fp
        fig = currentSpectrum.generate(style=newstyle,cut_off=cutoff,version='v3')
        ax_list = fig.axes
        print('axes',ax_list)
        print('fig',fig)
        plt.sca(ax_list[1])  # je reprend l'axe polaire
        #add recup du mask de low freq filter
        nc = netCDF4.Dataset(fp)
        lowfreq2dmat,k,phi = get_lowfreq_mask(nc,plots=False)
        nc.close()
        add_greyarea_for_low_freq_filter(lowfreq2dmat,k,phi,ax_list[1],cutoff)
        # currentSpectrum.show(version='v3',cut_off=cutoff)
        plt.show()



        #version sans le mask low freq plotted
        # on the fly
        # currentSpectrum = instance.read_spectrum(fp,'satfull',path=None)
        # fig = currentSpectrum.generate(style=newstyle,cut_off=cutoff,version='v3')
        # plt.show()

    if display_ww3_ony_the_fly is False :  # je ne veut pas voir le spectre WW3 pre genere tant que je nai pas fait le reprocessing
        plt.subplot(1,3,3)
        quicklook_type = 'ww3spectra'
        plt.title('WW3 first partition peak wavelength = %1.1fm' %
                  subdf['oswXA_wl_ww3spec_firstSARpartition_ww3'][ii])
        producttype = os.path.basename(os.path.abspath(os.path.join(os.path.dirname(fp),os.pardir)))[
                      0 :14]
        spectrapath = get_quicklook_L2spectrapath_from_measu.getQuicklook(producttype,fp,quicklook_type,
                                                                          atlernative_version_spectra='V3')
        if os.path.exists(spectrapath) :
            img = mpimg.imread(spectrapath)
            plt.imshow(img)

    if display_ww3_ony_the_fly:
        instance = SpectrumReader()
        logging.debug('start wavetoolbox read WW3 spectra + delineation and so on...')
        ww3spectra,_ = get_ww3spectrum_file_fromL2S1measu.get_ww3spectra_l2_filepath(fp,
                                                                                     use_new_procedure=None)
        currentSpectrum = instance.read_spectrum(ww3spectra,mode='ww3',path=ww3spectra)
        currentSpectrum.path = ww3spectra
        print('ww3 sp data',currentSpectrum.spec_data.sp.max())
        fig = currentSpectrum.generate(style=newstyle,cut_off=cutoff,version='v3')
        ax_list = fig.axes
        print('axes',ax_list)
        print('fig',fig)
        plt.sca(ax_list[1]) #je reprend l'axe polaire
        valsy = subdf['oswXA_wl_ww3spec_firstSARpartition_s1'][ii], \
                subdf['oswXA_wl_ww3spec_firstSARpartition_ww3'][ii],np.degrees(
            subdf['oswXA_dirad_ww3spec_firstSARpartition_s1'][ii]),np.degrees(
            subdf['oswXA_dirad_ww3spec_firstSARpartition_ww3'][ii])
        str_xainfos = 'X-assignment first SAR partition\npeak wavelength SAR = %1.1fm WW3 = %1.1fm\npeak direction SAR = %1.1f$^o$ WW3 = %1.1f$^o$' % (
                valsy)
        plt.annotate(str_xainfos,xy=(.025, .7), xycoords='figure fraction',fontsize=9)
        #add infos grom grid
        ww3gridseek = True
        if ww3gridseek :
            # trouver la grille WW3 la plus proche
            already_matched_partition = []
            ww3gridfile = get_path_ww3grid(measuL2WV_fullpath=fp)
            logging.debug('ww3file : %s',ww3gridfile)
            print(ww3gridfile)
            print(ww3spectra)
            nc = netCDF4.Dataset(ww3gridfile)
            idlat,idlon = get_closest_gridpoint(subdf['oswLon'][ii],subdf['oswLat'][ii],nc)
            paramsww3g,already_matched_partition = get_partitions_params_ww3grid(nc,idlat,idlon,already_matched_partition)
            for uy in range(len(paramsww3g['hs'])):
                radius = 2*np.pi/paramsww3g['wl'][uy]
                theta = np.radians(paramsww3g['dir'][uy])
                # plt.plot(radius,theta,'r.')
                plt.plot(theta,radius,'r.',markeredgecolor='k',ms=13)
                plt.annotate('hsgridww3=%1.1fm'%paramsww3g['hs'][uy],xy=(theta,radius),xycoords='data')



        plt.show()
        # plt.title('test')
        # plt.plot(0.5,0,'r.',ms=25)
        # currentSpectrum.show(version='v3',cut_off=cutoff)

    #add imaginary part cross spectra
    if onthefly is False:
        #pregeenrated
        plt.figure(figsize=(15,12),dpi=200)
        quicklook_type = 'xspec_imaginary_part'
        producttype = os.path.basename(os.path.abspath(os.path.join(os.path.dirname(fp),os.pardir)))[0 :14]
        spectrapath = get_quicklook_L2spectrapath_from_measu.getQuicklook(producttype,fp,quicklook_type,
                                                                          atlernative_version_spectra='V3')
        img = mpimg.imread(spectrapath)
        plt.imshow(img)
    else:
        #on the fly
        instance = SpectrumReader()
        print('fp',fp)
        currentSpectrum = instance.read_spectrum(fp,'satimaginary',path=None)
        currentSpectrum.path = fp

        currentSpectrum.show(version='v3',cut_off=cutoff,style=newstyleIm)

    #add real part cross spectra (OCN version)
    if onthefly is False:
        #pregeenrated
        plt.figure(figsize=(15,12),dpi=200)
        quicklook_type = 'xspec_real_part'
        producttype = os.path.basename(os.path.abspath(os.path.join(os.path.dirname(fp),os.pardir)))[0 :14]
        spectrapath = get_quicklook_L2spectrapath_from_measu.getQuicklook(producttype,fp,quicklook_type,
                                                                          atlernative_version_spectra='V3')
        img = mpimg.imread(spectrapath)
        plt.imshow(img)
    else:
        #on the fly
        instance = SpectrumReader()
        print('fp',fp)
        currentSpectrum = instance.read_spectrum(fp,'satreal',path=None)
        currentSpectrum.path = fp

        currentSpectrum.show(version='v3',cut_off=cutoff,style=newstyle)
