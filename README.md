# wavetoolbox

Python wavetoolbox lib provides script to read/display spectral wave surfacce data.
Supported data:
* WW3 numerical model 2D netcdf files
* NDBC Globwave buoys 2D netcdf files
* CMEMS insitu buoys 2D netcdf files
* Sentinel-1 WV 2D netcdf files
* CFOSAT SWIM L2 CWWIC 2D netcdf files

# Installation


So insallation in a conda environement is recommended.


```
conda create -n wavetoolboxenv python=3
conda activate wavetoolboxenv
conda install xarray numpy matplotlib netCDF4
```

then :
```
pip install git+https://gitlab.ifremer.fr/lops-wave/wavetoolbox.git
```

or , for development installation:
```
git clone https://gitlab.ifremer.fr/lops-wave/wavetoolbox.git
cd wavetoolbox
pip install -r requirements.txt
pip install -e .
```
