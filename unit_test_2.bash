#!/bin/bash
#test to check env, sources and output png in exploitation conditions
# June 2020
version=$1
user=`whoami`
echo 'user= '$user
echo 'version= '$version
if [ "$user" == "satwave" ]; then
  export PYTHONPATH=/home1/datahome/satwave/sources_en_exploitation2/mpc/qualitycheck:/home1/datahome/satwave/sources_en_exploitation2/mpc/data_collect:/home1/datahome/satwave/sources_en_exploitation2/cfosat-calval-exe:/home1/datahome/satwave/sources_en_exploitation2/mpc/colocation:/home1/datahome/satwave/sources_en_exploitation2/mpc/colocation/ww3spectra:/home1/datahome/satwave/sources_en_exploitation2/cerform
else
  export PYTHONPATH=/home1/datahome/agrouaze/git/mpc/qualitycheck:/home1/datahome/agrouaze/git/mpc/data_collect:/home1/datahome/agrouaze/git/cfosat-calval-exe:/home1/datahome/agrouaze/git/mpc/colocation:/home1/datahome/agrouaze/git/mpc/colocation/ww3spectra:/home1/datahome/agrouaze/git/cerform

fi

#scriptlisting=/home1/datahome/agrouaze/git/wavetoolbox/generate_listing_v1_02.py
#list="/tmp/tmpListingL2_S1B.lst"
#python ${scriptlisting} --beginningdate 20200623 --platform S1B -s SAT -q -g --output-filepath $list
case "$version" in
    2)
        echo "Starting version2"
        exe=/home1/datahome/agrouaze/git/wavetoolbox/runner_plot_spectra_v2.bash
        list=/home1/datahome/agrouaze/git/wavetoolbox/example_listing_s1.txt
        ;;
    3)
        echo "Starting version3"
        exe=/home1/datahome/agrouaze/git/wavetoolbox/runner_plot_spectra_v3.bash
        list=/home1/datahome/agrouaze/git/wavetoolbox/example_listing_s1_v3.txt
        ;;
    4)
        echo 'starting version3 but on WW3 listing'
        exe=/home1/datahome/agrouaze/git/wavetoolbox/runner_plot_spectra_v3.bash
        list=/home1/datahome/agrouaze/git/wavetoolbox/example_listing_s1_v3_ww3.txt
        ;;
    5)
        echo 'starting version2 but on WW3 listing'
        exe=/home1/datahome/agrouaze/git/wavetoolbox/runner_plot_spectra_v2.bash
        list=/home1/datahome/agrouaze/git/wavetoolbox/example_listing_s1_v3_ww3.txt
        ;;
    *)
        echo "Usage: unit_test_2.bash {2|3}"
        exit 1
    ;;
esac


#exe=/home1/datahome/satwave/sources_en_exploitation2/wavetoolbox/runner_plot_spectra_v3.bash
#echo "/home/datawork-cersat-public/project/mpc-sentinel1/data/esa/sentinel-1a/L2/WV/S1A_WV_OCN__2S/2020/174/S1A_WV_OCN__2SSV_20200622T230628_20200622T232253_033136_03D6BA_4EC5.SAFE/measurement/s1a-wv1-ocn-vv-20200622t231249-20200622t231252-033136-03d6ba-027.nc /tmp/project/mpc-sentinel1/analysis/s1_data_analysis/L2_V2/WV/S1A_WV_OCN__2S/2020/174/S1A_WV_OCN__2SSV_20200622T230628_20200622T232253_033136_03D6BA_4EC5.SAFE/xspec/ocean_swell_spectra_s1a-wv1-ocn-vv-20200622t231249-20200622t231252-033136-03d6ba-027.png satfull" | bash $exe

echo 'listing '$list
head -n 2 $list
cat $list | bash $exe
echo 'finito test'