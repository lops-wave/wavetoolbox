import abc
import numpy as np
import netCDF4 as nc
from dateutil import parser
from netCDF4 import Dataset
import Spectrum as spc
import logging
import spectrumUtil as spUtil
import os

class AbstractSpectrumReader():
    
    __metaclass__ = abc.ABCMeta
    
    @abc.abstractmethod
    def read_spectrum(ncfile,mode="satfull",index=None):
        pass



#-----------------------------------------------
# Reader for Sat File 
#-----------------------------------------------

class SpectrumReaderSat(AbstractSpectrumReader):
    @staticmethod
    def read_spectrum(ncf=None,mode='satfull',dataName='UNK'):
        """
        read_spectrum_sat: Function that return a spectrum object from a satelite file
        ncf: Net CDF File already open
        mode: The mode of data satelite:
                satfull: Full data
                satreal: Real Part
                satimaginary: Imaginary Part
        path: The path to access the file

        NB: If path and ncf are given priority is given to the path

        examples:

        ex1:
        nc1 = Dataset('path/to/ncfile.nc', 'r')
        myspectrum = read_spectrum_sat(nc1,mode="satfull")
        nc1.close()

        ex2:
        myspectrum = read_spectrum_sat(path='path/to/ncfile.nc',mode="satimaginary")

        """
        #index is useless for now
        myspec_data = SpectrumReaderSat.read_spec_data(ncf,mode)
        mywsys = SpectrumReaderSat.read_wsys(ncf)
        mywind = SpectrumReaderSat.read_wind(ncf)
        mymeta = SpectrumReaderSat.read_meta(ncf,mode)

        return spc.Spectrum(myspec_data,mywsys,mywind,mymeta,dataName=dataName)
     
    @staticmethod
    def read_spectrum_list(ncf=None):
        """
        read_spectrum_list_sat: Function that return an array with the spectrum objects from a satelite file
        it will return the following:
            - Imaginary part
            - Real Part
            - Full
        ncf: Net CDF File already open or fil path


        examples:

        ex1:
        nc1 = Dataset('path/to/ncfile.nc', 'r')
        myspectra = read_spectrum_list_sat(nc1)
        nc1.close()

        ex2:
        myspectrum = read_spectrum_list_sat(path='path/to/ncfile.nc')

        """
        spectrumdict={}
        modes = ['satfull','satreal','satimaginary']
        if isinstance(ncf, basestring):
            fileToRead = Dataset(ncf, 'r') 
        else:
            fileToRead = ncf

        for mode in modes:
            myspectrum = SpectrumReaderSat.read_spectrum(ncf=fileToRead,mode=mode,dataName=dataName)
            spectrumdict[mode] = myspectrum
        if isinstance(ncf, basestring):
            fileToRead.close()
        return spectrumdict
    
    @staticmethod
    def read_wsys(ncf):
        """
        read_wsys_sat: Function that return a wave system dictionary from a sat file
        """
        wsys = {}
        wsys['hs'] = ncf.variables['oswHs'][:][0][0]
        wsys['wl'] = ncf.variables['oswWl'][:][0][0]
        wsys['dirad'] = ncf.variables['oswDirmet'][:][0][0]*np.pi/180.
        wsys['lon'] = ncf.variables['oswLon'][:][0][0]
        wsys['lat'] = ncf.variables['oswLat'][:][0][0]
        return wsys

    @staticmethod
    def read_wind(ncf):
        """
        read_wind_sat: Function that return a wind dictionary from a sat file
        """
        wind={}
        wind['U'] = ncf.variables['oswWindSpeed'][:][0][0]*np.cos(90- ncf.variables['oswWindDirection'][:][0][0]*np.pi/180.)
        wind['V'] = ncf.variables['oswWindSpeed'][:][0][0]*np.sin(90- ncf.variables['oswWindDirection'][:][0][0]*np.pi/180.)
        return wind


    @staticmethod
    def read_meta(ncf,mode):
        meta={}
        meta['type'] = mode
        meta['nv']  = ncf.variables['oswNv'][:][0][0]
        meta['snr'] = ncf.variables['oswSnr'][:][0][0]
        meta['azc'] = ncf.variables['oswAzCutoff'][:][0][0]
        meta['inc'] = ncf.variables['oswIncidenceAngle'][:][0][0]
        meta['nrcs'] = ncf.variables['oswNrcs'][:][0][0]
        meta['tra'] =  ncf.variables['oswHeading'][:][0][0]

        return meta


    @staticmethod
    def read_spec_data(ncf,mode):
        """
        Function getting spectrum data from a netcdf4 Object

        ncf: NetCDF4 open object
        mode: Type of data. help determine how to parse to get the data
            satimaginary,satfull,satreal:Fichier Satelite 
                satimaginary: imaginary part
                satreal: real part
                satfull: full
            w:Fichier WW3

        """
        my_data = {}
        my_data['part'] = ncf.variables['oswPartitions'][:][0][0]
        my_data['k'] = ncf.variables['oswK'][:]
        my_data['phi'] = ncf.variables['oswPhi'][:]

        if not isinstance(my_data['phi'],np.ma.core.MaskedArray):
            my_data['phi'] = np.ma.array(my_data['phi'])
            my_data['phi'].mask = np.zeros(my_data['phi'].shape)
            #special patch for oswK [agrouaze 11/06/2016]
            # (values set to the fill value do not have the attribute fill=True thus they are not retrieved as masked values)
            my_data['k'] = np.ma.masked_where(my_data['k']>=9.96920e+36, my_data['k'], copy=False)
            if mode == "satfull":
                my_data['sp'] = ncf.variables['oswPolSpec'][:][0][0]

            if not isinstance(my_data['k'].mask,np.ndarray):
                my_data['k'].mask = np.zeros(my_data['k'].shape)

                kmask = my_data['k'].mask==False
                phimask = my_data['phi'].mask==False
                logging.debug('phimask %s',phimask.shape)
                logging.debug('kmask %s',kmask.shape)

                my_data['k'] = my_data['k'][kmask]
                my_data['phi'] = my_data['phi'][phimask]
                my_data['part'] = my_data['part'][phimask,:]
                my_data['part'] = my_data['part'][:,kmask]

                if mode=="satfull":
                    my_data['sp'] = my_data['sp'][phimask,:]
                    my_data['sp'] = my_data['sp'][:,kmask]
                    logging.debug('sp %s',my_data['sp'].shape)

                if mode == "satreal":
                    my_data['sp'] = ncf.variables['oswQualityCrossSpectraRe'][:][0][0]
                    my_data['sp'] = my_data['sp'][phimask,:]
                    my_data['sp'] = my_data['sp'][:,kmask]
                if mode == "satimaginary":
                    my_data['sp'] = ncf.variables['oswQualityCrossSpectraIm'][:][0][0]
                    my_data['sp'] = my_data['sp'][phimask,:]
                    my_data['sp'] = my_data['sp'][:,kmask]
                #recuperation sar_start_time
                my_data['start_time'] = parser.parse(ncf.getncattr('firstMeasurementTime'))
        return my_data    
#-----------------------------------------------
# End of Reader for Sat File 
#-----------------------------------------------

#-----------------------------------------------
# Reader for WW3 File 
#-----------------------------------------------

class SpectrumReaderWw3(AbstractSpectrumReader):
    @staticmethod
    def read_spectrum(ncf=None,mode='ww3',dataName='UNK'):
        """
        read_spectrum_ww3: Function that return a spectrum object from a WW3 prevision file
        ncf: Net CDF File already open
        path: The path to access the file

        NB: If path and ncf are given priority is given to the path

        examples:

        ex1:
        nc1 = Dataset('path/to/ncfile.nc', 'r')
        myspectrum = read_spectrum_ww3(nc1)
        nc1.close()

        ex2:
        myspectrum = read_spectrum_ww3(path='path/to/ncfile.nc')

        """
        myspec_data = SpectrumReaderWw3.read_spec_data(ncf)
        mywsys = SpectrumReaderWw3.read_wsys(ncf)
        mywind = SpectrumReaderWw3.read_wind(ncf)
        mymeta = SpectrumReaderWw3.read_meta(ncf,mode)
        return spc.Spectrum(myspec_data,mywsys,mywind,mymeta,dataName=dataName)
   
    @staticmethod
    def read_wsys(ncf):
        wsys = {}
        wsys['lon'] = ncf.variables['lon'][:]
        wsys['lon'] = float(wsys['lon'])
        wsys['lat'] = ncf.variables['lat'][:]
        wsys['lat'] = float(wsys['lat'])
        return wsys
    @staticmethod
    def read_wind(ncf):
        wind={}
        wind['U'] = ncf.variables['wind_speed_model_u'][:]
        wind['U'] = wind['U'][0]
        wind['V'] = ncf.variables['wind_speed_model_v'][:]
        wind['V'] = wind['V'][0]
        return wind
    @staticmethod
    def read_meta(ncf,mode):

        meta={}
        meta['type'] = mode
        return meta
    @staticmethod
    def read_spec_data(ncf):
        my_data = {}
        my_data['k'] = ncf.variables['k'][:]
        my_data['phi'] = ncf.variables['phi'][:]
        my_data['sp'] = ncf.variables['polSpec'][:].squeeze()

        t = ncf.variables['time'][:]
        u = ncf.variables['time'].units
        my_data['start_time'] = nc.num2date(t,u)
        my_data['start_time'] = my_data['start_time'][0]
        
        t = ncf.variables['sar_time'][:]
        u = ncf.variables['sar_time'].units
        my_data['sar_time'] = nc.num2date(t,u)
        my_data['sar_time'] = my_data['sar_time'][0]

        return my_data
#-----------------------------------------------
# End of Reader for WW3 File 
#-----------------------------------------------


#-----------------------------------------------
# Reader for Buoys Globwave File (Ndbc and cdip)
#-----------------------------------------------

class SpectrumReaderBuoyGlobwave(AbstractSpectrumReader):
    @staticmethod
    def read_spectrum(ncf,index,mode="buoyglobwavendbc",phi = np.arange(0,365,10),dataName='UNK'):
        """
        read_spectra_globwave: Function that return a spectrum object from a globwave file
        ncf: Net CDF File already open
        path: The path to access the file

        NB: If path and ncf are given priority is given to the path

        examples:

        ex1:
        nc1 = Dataset('path/to/ncfile.nc', 'r')
        myspectrum = read_spectrum_globwave(nc1,mode="satfull")
        nc1.close()

        ex2:
        myspectrum = read_spectrum_globwave(path='path/to/ncfile.nc',mode="satimaginary")

        """
        if isinstance(ncf, basestring):
            fileToRead = Dataset(ncf, 'r') 
        else:
            fileToRead = ncf

        mywind = SpectrumReaderBuoyGlobwave.read_wind_globwave(fileToRead,index)
        mywsys = SpectrumReaderBuoyGlobwave.read_wsys_globwave(fileToRead,index)
        myspec_data = SpectrumReaderBuoyGlobwave.read_spec_data_globwave(fileToRead,index,mode,phi = phi)
        mymeta = SpectrumReaderBuoyGlobwave.read_meta_globwave(fileToRead,index,mode)

        if isinstance(ncf, basestring):
            fileToRead.close()

        return spc.Spectrum(myspec_data,mywsys,mywind,mymeta,dataName=dataName)
    
    @staticmethod
    def read_wind_globwave(ncf,index):
        wind={}

        return wind
    @staticmethod
    def read_wsys_globwave(ncf,index):
        lon,lat =None,None
        lat = np.squeeze(ncf.variables['lat'][:])
        lon = np.squeeze(ncf.variables['lon'][:])
        wsys={'lon':lon,'lat':lat}

        return wsys
    @staticmethod
    def read_spec_data_globwave(ncf,index,mode='buoyglobwavendbc',phi = np.arange(0,365,10),con='to'):
        time = nc.num2date( ncf.variables['time'][:], ncf.variables['time'].units )
        time = time[index]

        c1 = ncf.variables['theta1'][index,:]     # theta1  
        c2 = ncf.variables['theta2'][index,:]     # theta2 
        c3 = ncf.variables['stheta1'][index,:]    # stheta1 
        c4 = ncf.variables['stheta2'][index,:]    # stheta2 

        # convert theta & stheta(globwave data used) to a1,b1,a2,b2
        a1,a2,b1,b2 = spUtil.th2ab(c1,c2,c3,c4)
        if mode == "buoyglobwavendbc":
            f,sf = SpectrumReaderBuoyGlobwave.read_spec_data_ndbc_globwave(ncf,index)
        else:
            f,sf = SpectrumReaderBuoyGlobwave.read_spec_data_cdip_globwave(ncf,index)

        spfphitmp,d = spUtil.buoy_spectrum2d(sf,a1,a2,b1,b2, dirs = phi)
        spfphi=[spfphitmp]
        #spfphi2kphi wait an array of values

        sp2d,k =  spUtil.spfphi2kphi(spfphi,f)

        if con=='to':
            sp2d = spUtil.spfrom2to(sp2d,phi)
        #sp2d is an array of values : so we use the first and only element
        spec_data = spc.Spec_data(k=k,phi=phi,sp=sp2d[0],start_time=time,)

        return spec_data
    
    @staticmethod
    def read_spec_data_ndbc_globwave(ncf,index):
        f = np.squeeze(ncf.variables['wave_directional_spectrum_central_frequency'][:])
        sf = ncf.variables['spectral_wave_density'][index,:]    
        return f,sf
    
    @staticmethod
    def read_spec_data_cdip_globwave(ncf,index):
        f = np.squeeze(ncf.variables['central_frequency'][index,:])
        sf = ncf.variables['sea_surface_variance_spectral_density'][index,:]
        return f,sf

    @staticmethod
    def read_meta_globwave(ncf,index,mode='buoyglobwavendbc'):

        if mode == "buoyglobwavendbc":
            mymeta = spc.Meta(type=mode)
        else:
            mymeta = SpectrumReaderBuoyGlobwave.read_meta_cdip_globwave(ncf,index,mode)
    	return mymeta

    @staticmethod
    def read_meta_cdip_globwave(ncf,index,mode):
    	meta={}
        meta['type'] = mode
    	meta['wp'] = ncf.variables['dominant_wave_period'][index]
    	meta['dirp'] = np.squeeze(ncf.variables['dominant_wave_direction'][index])
    	meta['hst'] = np.squeeze(ncf.variables['significant_wave_height'][index])
	
        return meta

    @staticmethod
    def read_wsys_ndbc(ncf,index):
        lon,lat =None,None
        lat = np.squeeze(ncf.variables['latitude'][:])
        lon = np.squeeze(ncf.variables['longitude'][:])
        wsys={'lon':lon,'lat':lat}

        return wsys

#-----------------------------------------------
# End of Reader for Buoys Globwave File (Ndbc and cdip)
#-----------------------------------------------


readerdictionary ={
    "satreal":SpectrumReaderSat.read_spectrum,
    "satfull":SpectrumReaderSat.read_spectrum,
    "satimaginary":SpectrumReaderSat.read_spectrum,
    "ww3":SpectrumReaderWw3.read_spectrum,
    "buoyglobwavendbc":SpectrumReaderBuoyGlobwave.read_spectrum,
    "buoyglobwavecdip":SpectrumReaderBuoyGlobwave.read_spectrum
    
}    


#-----------------------------------------------
# Generic Reader that call the good reader
#-----------------------------------------------
class SpectrumReader(AbstractSpectrumReader):
    
    @staticmethod
    def read_spectrum(ncfile,mode=None,index=None):
        dataName='UNK'
        
        if isinstance(ncfile, basestring):
            dataName = SpectrumReader.selectDataName(ncfile)
            fileToRead = Dataset(ncfile, 'r') 
            if not mode:
                mode = SpectrumReader.selectmodebyname(ncfile)
        else:
            fileToRead = ncfile
            SpectrumReader.selectmodebydata(ncfile)
        if index is not None:
            myspectrum = readerdictionary[mode](fileToRead,index=index,mode=mode,dataName=dataName)    
        else:
            myspectrum = readerdictionary[mode](fileToRead,mode=mode,dataName=dataName)    
	
        if isinstance(ncfile, basestring):
            fileToRead.close()
            
        #myspectrum.dataName=dataName
        #myspectrum.title = myspectrum.generateSpectrumTitle()
        
        return myspectrum

    @staticmethod
    def selectDataName(name):
        if 'globwave' in name and 'cdip' in name:
            return 'buoyglobwavecdip'
        elif 'globwave' in name and 'ndbc' in name:
            return 'buoyglobwavendbc'
        elif 'ww3' in name:
            return 'WW3'
        elif 's1a' in name:
            return 'S1A'
        elif 's1b' in name:
            return 'S1B'
        


    @staticmethod
    def selectmodebyname(name):
        if 'globwave' in name and 'cdip' in name:
            return 'buoyglobwavecdip'
        elif 'globwave' in name and 'ndbc' in name:
            return 'buoyglobwavendbc'
        elif 'ww3' in name:
            return 'ww3'
        elif 's1a' in name or 's1b' in name:
            print 'ambiguous situation will return the full spectrum mode:satfull'
            return 'satfull'
    @staticmethod
    def selectmodebydata(data):
        print data.title
#-----------------------------------------------
# End of Generic Reader 
#-----------------------------------------------



