"""
Nov 2021
Antoine Grouazel
script to generate a listing that will be used to backlog the missing roughness nice display png figures for WV S1
"""
from shared_information import DIR_SENTINEL_AGGREGATION,VERSION_DAILY
import os
import time
import pdb
import resource
import logging
import datetime
import glob
import xarray
import get_fullpath_from_subpath_dailyL2F
OUTPUTDIR = '/home1/scratch/agrouaze'

def read_L2D_to_get_measu_tiff_without_roughness(satellite,year=datetime.datetime.today().strftime('%Y'),write_log=True):
    """

    :param satellite: S1A or S1B .. str
    :return:
    """
    pattern = os.path.join(DIR_SENTINEL_AGGREGATION,
            'dump_l2_s1_wv_ocn_v8_10_daily_v'+VERSION_DAILY+satellite+'%s.nc'%year)
    logging.info('pattern %s',pattern)
    pot = glob.glob(pattern)
    output_L1_tiff_missing_roughness = os.path.join(OUTPUTDIR,'L1_tiff_without_roughness_image_%s_%s.txt'%(satellite,year))
    if write_log:
        fid = open(output_L1_tiff_missing_roughness,'w')
    cpt_missing_that_could_be_done = 0
    cpt_missing_that_doesnt_exist_anymore = 0
    if len(pot)>0:
        file_agg = pot[0]
        logging.info('file found: %s',file_agg)
        ds = xarray.open_dataset(file_agg)
        ds.load()
        ROUGHNESSFLAG = ds['ROUGHNESSFLAG'].values
        logging.info('obs without roughness : %s/%s (%1.1f%%)',(ROUGHNESSFLAG==0).sum(),len(ROUGHNESSFLAG),100.*(ROUGHNESSFLAG==0).sum()/len(ROUGHNESSFLAG))
        logging.info('obs with roughness : %s/%s (%1.1f%%)',(ROUGHNESSFLAG == 1).sum(),len(ROUGHNESSFLAG),100.*(ROUGHNESSFLAG==1).sum()/len(ROUGHNESSFLAG))
        sub_backlog = ds.where(ds['ROUGHNESSFLAG']==0,drop=True)
        if (ROUGHNESSFLAG==0).sum()>0:
            SUBPATH_L1 = sub_backlog['SUBPATH_L1'].values
            mask = SUBPATH_L1!='                                                                                                                                              '
            SUBPATH_L1_EXIST = SUBPATH_L1[mask]
            for ll in SUBPATH_L1_EXIST:
                ll2 = get_fullpath_from_subpath_dailyL2F.get_full_path(ll,L1=True)
                if os.path.exists(ll2):
                    if write_log:
                        fid.write(ll2+'\n')
                    cpt_missing_that_could_be_done += 1
                else:
                    cpt_missing_that_doesnt_exist_anymore += 1
            # pdb.set_trace()
    else:
        logging.info('no %s %s aggregation',satellite,year)
    if write_log:
        fid.close()
    logging.info('listing of missing roughness is here : %s',output_L1_tiff_missing_roughness)
    logging.info('cpt_missing_that_could_be_done : %s',cpt_missing_that_could_be_done)
    logging.info('cpt_missing_that_doesnt_exist_anymore : %s',cpt_missing_that_doesnt_exist_anymore)
if __name__ == '__main__':
    import argparse
    tinit = time.time()
    root = logging.getLogger()
    if root.handlers:
        for handler in root.handlers:
            root.removeHandler(handler)

    parser = argparse.ArgumentParser(description='backlog roughness')
    parser.add_argument('--verbose', action='store_true', default=False)
    parser.add_argument('--outputdir', default=OUTPUTDIR,
                        help='folder where the log will be written [optional ,default= %s]'%OUTPUTDIR,
                        required=False)
    args = parser.parse_args()
    fmt = '%(asctime)s %(levelname)s %(filename)s(%(lineno)d) %(message)s'
    if args.verbose:
        logging.basicConfig(level=logging.DEBUG, format=fmt,
                            datefmt='%d/%m/%Y %H:%M:%S')
    else:
        logging.basicConfig(level=logging.INFO, format=fmt,
                            datefmt='%d/%m/%Y %H:%M:%S')
    satellite = 'S1B'
    year = '202'
    for year in range(2014,2022):
        read_L2D_to_get_measu_tiff_without_roughness(satellite,year=str(year),write_log=False)
    logging.info('done in %1.3f min',(time.time() - tinit) / 60.)
    logging.info('peak memory usage: %s Mbytes',resource.getrusage(resource.RUSAGE_SELF).ru_maxrss / 1024.)
