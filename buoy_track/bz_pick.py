import cPickle,bz2

def zip_save(filename,*objects):
  with bz2.BZ2File(filename, 'wb', compresslevel=9) as f:
  # with gzip.GzipFile(filename, 'wb') as f:    
    for obj in objects: cPickle.dump(obj, f,2)
def zip_load(filename):
  with bz2.BZ2File(filename, 'rb', compresslevel=9) as f:
  # with gzip.GzipFile(filename, 'rb') as f:   
    data = cPickle.load(f)
  return data