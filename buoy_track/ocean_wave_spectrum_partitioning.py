#!/usr/bin/env python
# coding=utf-8
"""
"""


import numpy as np
from skimage.feature import peak_local_max
from skimage.morphology import watershed
from skimage.segmentation import relabel_sequential
from skimage.measure import regionprops

from netCDF4 import Dataset
import matplotlib.pyplot as plt
import pdb

import os


def compute_wave_parameters(labels, slope_spec, k, dk, phi, dphi, lon, lat):
    """
    """
    nlabels = labels.max()
    params = np.zeros((5, nlabels), dtype='float')
    props = regionprops(labels, slope_spec)
    # print datetime.utcnow()
    # lon_func = interp1d(np.arange(lon.size), lon, kind='cubic') # DATELINE ! # SLOW
    # lat_func = interp1d(np.arange(lat.size), lat, kind='cubic') # SLOW
    # print datetime.utcnow()
    for lab in range(nlabels):
        crd = props[lab].coords
        spec_lab = slope_spec[crd[:, 0], crd[:, 1]]
        # HS : use all energy in partition
        # heigth_spec = slope_spec / k**2
        # -> energy = height_spec * k * dk * dphi = slope_spec / k * dk * dphi
        norm = np.deg2rad(dphi[crd[:, 0]]) / k[crd[:, 1]] * dk[crd[:, 1]]
        energy = spec_lab * norm
        params[2, lab] = 4. * np.sqrt(energy.sum())
        # lambda / dir / lon / lat :  keep most energetic parts of partition
        indene = np.where(spec_lab >= spec_lab.max() / 2.)
        crd = crd[indene[0], :]
        spec_lab = slope_spec[crd[:, 0], crd[:, 1]]
        spec_lab_sum = spec_lab.sum()
        k_lab = (k[crd[:, 1]] * spec_lab).sum() / spec_lab_sum
        params[0, lab] = 2. * np.pi / k_lab
        params[1, lab] = (phi[crd[:, 0]] * spec_lab).sum() / spec_lab_sum
        # params[3, lab] = (lon[crd[:, 0]] * spec_lab).sum() / spec_lab_sum
        # params[4, lab] = (lat[crd[:, 0]] * spec_lab).sum() / spec_lab_sum
        ctr = (crd[:, 0] * spec_lab).sum() / spec_lab_sum
        # params[3, lab] = lon_func(ctr)
        # params[4, lab] = lat_func(ctr)
        params[3, lab] = lon[np.round(ctr).astype('int')]
        params[4, lab] = lat[np.round(ctr).astype('int')]
    # from scipy.interpolate import splprep, splev
    # tck = splprep([lon, lat], u=phi, k=5)[0]
    # #import pdb ; pdb.set_trace()
    # tlon, tlat = splev(np.linspace(phi.min(), phi.max(), num=phi.size*10), tck)
    # plt.plot(lon, lat, '+k')
    # plt.plot(tlon, tlat, '+r')
    # import pdb ; pdb.set_trace()

    return params

def  compute_connectivity(lab1, lab2, spec, labels, peaks_ind, noise_levels):
    """
    """
    dpath = np.sqrt((peaks_ind[lab1, 0] - peaks_ind[lab2, 0]) ** 2 +\
                    (peaks_ind[lab1, 1] - peaks_ind[lab2, 1]) ** 2)
    npath = np.ceil(dpath / 1.) + 1
    xpath = np.linspace(peaks_ind[lab1, 0], peaks_ind[lab2, 0], num=npath)
    ypath = np.linspace(peaks_ind[lab1, 1], peaks_ind[lab2, 1], num=npath)
    xpath = np.round(xpath).astype('int')
    ypath = np.round(ypath).astype('int')
    lpath = labels[xpath, ypath]
    if np.unique(lpath).size > 2:
        con1 = np.inf
        con2 = np.inf
    else:
        vpath = spec[xpath, ypath]
        vind = vpath.argmin()
        vmin = vpath[vind]
#        noi = noise_levels[xpath[vind], ypath[vind]]
#        dmax = spec[peaks_ind[lab1, 0], peaks_ind[lab1, 1]] - vmin
#        con1 = dmax / noi
#        dmax = spec[peaks_ind[lab2, 0], peaks_ind[lab2, 1]] - vmin
#        con2 = dmax / noi
        dmax = spec[peaks_ind[lab1, 0], peaks_ind[lab1, 1]] - vmin
        con1 = dmax / vmin
        dmax = spec[peaks_ind[lab2, 0], peaks_ind[lab2, 1]] - vmin
        con2 = dmax / vmin							
    return (con1, con2)
def  spec_dist(lab1, lab2, phi, k, peaks_ind):
    
    D1=phi[peaks_ind[lab1, 0]]%360;D2=phi[peaks_ind[lab2, 0]]%360;T1=2*np.pi/np.sqrt(k[peaks_ind[lab1, 1]]*9.8);T2=2*np.pi/np.sqrt(k[peaks_ind[lab2, 1]]*9.8);
    tmp_delta_dir=np.array(abs(D1-D2))
    tmp_delta_dir[tmp_delta_dir>180] = 360-tmp_delta_dir[tmp_delta_dir>180] 
    S=1/60.*(tmp_delta_dir+2*abs(T1-T2)/(T1+T2)*250)
    return (S, S)


def partition_spectrum(spec, k, phi, twod=True, symphi=False,
                       klow=None, khigh=None,
                       noise_level=0.,
                       noise_thr=0., merge_thr1=0., merge_thr2=0.,
                       discard_thr=0., discard_dphi=360.):
    """
    """
    kstart = 0 if klow is None else np.searchsorted(k, klow, side='left')
    kstop = k.size if khigh is None else np.searchsorted(k, khigh, side='right')
    noise_levels, _ = np.broadcast_arrays(noise_level, spec)
    # warning : do not update noise_levels (view), otherwise use next line
    #noise_levels, _ = np.broadcast_arrays(np.array(noise_level, copy=True), spec)

    # Duplicate spectrum if 2D
    nphi = spec.shape[0]
    nphih = nphi / 2
    if twod == True:
        spec = np.vstack((spec[-nphih:, :], spec, spec[0:nphih, :]))
        phi = np.hstack((phi[-nphih:] - 360, phi, phi[0:nphih] + 360))
        noise_levels = np.vstack((noise_levels[-nphih:, :], noise_levels,
                                  noise_levels[0:nphih, :]))

    # Apply watershed
    mask = spec > noise_thr * noise_levels
    mask[:, 0:kstart] = False
    mask[:, kstop:] = False
    peaks_ind = peak_local_max(spec, min_distance=1, threshold_abs=0,
                               threshold_rel=0, exclude_border=False,
                               indices=True, num_peaks=np.inf,
                               footprint=None, labels=mask)
    nlabels = peaks_ind.shape[0]
    markers = np.zeros(spec.shape, dtype='int')
    markers[peaks_ind[:, 0], peaks_ind[:, 1]] = np.arange(1, nlabels + 1)
    labels = watershed(-spec, markers, mask=mask)
    if nlabels == 0:
        if twod == True:
            labels = labels[nphih:-nphih, :]
        return labels
#    pdb.set_trace()
    # Merge partitions
    # Adjacency matrix
    adj_mat = np.zeros((nlabels + 1, nlabels + 1), dtype='bool')
    adj_mat[labels[:, :-1], labels[:, 1:]] = True # left-right pairs
    adj_mat[labels[:, 1:], labels[:, :-1]] = True # right-left pairs
    adj_mat[labels[:-1, :], labels[1:, :]] = True # top-bottom pairs
    adj_mat[labels[1:, :], labels[:-1, :]] = True # bottom-top pairs
    adj_mat = adj_mat[1:, 1:] # remove useless entries
    adj_mat[np.arange(nlabels), np.arange(nlabels)] = False
    # Connectivity matrix
    con_mat = np.zeros((nlabels, nlabels), dtype='float') + np.inf
#    vmin_mat = np.zeros((nlabels, nlabels), dtype='float') + np.inf
    dist_mat = np.zeros((nlabels, nlabels), dtype='float') + np.inf    
    for lab1 in range(nlabels):
        neighbours = np.where(adj_mat[lab1, 0:lab1] == True)[0]
        for lab2 in neighbours:
            con1, con2= compute_connectivity(lab1, lab2, spec, labels,
                                              peaks_ind, noise_levels)        
            S1, S2 = spec_dist(lab1, lab2, phi, k, peaks_ind)
                               
            con_mat[lab1, lab2] = con1
            con_mat[lab2, lab1] = con2       
            dist_mat[lab1, lab2] = S1
            dist_mat[lab2, lab1] = S2            
    # Merge
    # First get labels coordinates
    # (regionprops should be much faster than np.where for each label)
    coords = [prop.coords for prop in regionprops(labels)]
    # Then loop on connectivities
    # TO CHECK : no con_mat and con_mat_tmp ???!!!???
    while dist_mat.min() != np.inf:
        dist_argmin = (dist_mat + dist_mat.transpose()).argmin()
        lab1, lab2 = np.unravel_index(dist_argmin, dist_mat.shape)
        con1, con2 = con_mat[[lab1, lab2], [lab2, lab1]]
#        pdb.set_trace()
        if con1 > con2:
            con1, con2 = con2, con1
            lab1, lab2 = lab2, lab1

        # decide if merge is needed, otherwise go to next iteration
        # lab1/lab2 connectivity is then made infinite
        # but adjacency is kept because if lab1 (resp. lab2) is further merged
        # with another label we have to know its neighbours
#        if con1 > max[merge_thr1*noise_level,merge_thr1*0.1*vmin]:
#        if con1 > merge_thr1:    
        if con1 > merge_thr1/(1.0-merge_thr1):   
            con_mat[[lab1, lab2], [lab2, lab1]] = np.inf
            dist_mat[[lab1, lab2], [lab2, lab1]] = np.inf
            continue
        # don't merge symmetric partitions
        if symphi == True and peaks_ind[lab1, 1] == peaks_ind[lab2, 1] and \
           abs(peaks_ind[lab1, 0] - peaks_ind[lab2, 0]) == nphih:
            con_mat[[lab1, lab2], [lab2, lab1]] = np.inf
            dist_mat[[lab1, lab2], [lab2, lab1]] = np.inf            
            continue
        # merge lab1 into lab2
        labels[coords[lab1][:, 0], coords[lab1][:, 1]] = lab2 + 1
        coords[lab2] = np.vstack((coords[lab2], coords[lab1]))
        coords[lab1] = None
        # keep trace of lab1 neighbours different than lab2
        lab1_neighbours = np.where(adj_mat[lab1, :] == True)[0]
        lab1_neighbours = lab1_neighbours[np.where(lab1_neighbours != lab2)]
        # remove lab1 adjacency / connectivity as it does not exist anymore
        adj_mat[lab1, :] = False
        adj_mat[:, lab1] = False
        con_mat[:, lab1] = np.inf
        con_mat[lab1, :] = np.inf
        dist_mat[:, lab1] = np.inf
        dist_mat[lab1, :] = np.inf
        # update lab2 adjacency / connectivity if lab1 had neighbours
        if lab1_neighbours.size != 0:
            adj_mat[lab2, lab1_neighbours] = True
            adj_mat[lab1_neighbours, lab2] = True
            lab2_neighbours = np.where(adj_mat[lab2, :] == True)[0]
            for lab3 in lab2_neighbours:
                con2, con3 = compute_connectivity(lab2, lab3, spec, labels,
                                                  peaks_ind, noise_levels)
                S2, S3 = spec_dist(lab2, lab3, phi, k, peaks_ind)                                  
                con_mat[lab2, lab3] = con2
                con_mat[lab3, lab2] = con3
                dist_mat[lab2, lab3] = S2
                dist_mat[lab3, lab2] = S3    
    labels, _, _ = relabel_sequential(labels)

    # Discard partitions
    for prop in regionprops(labels, intensity_image=spec): 
        crd = prop.coords
        energy_label = spec[crd[:, 0], crd[:, 1]].sum()
        ctr = np.round(prop.weighted_centroid[0]).astype('int')
#        pdb.set_trace()
        ind = np.where(abs(phi-phi[ctr]) <= discard_dphi / 2.)[0] # BOF
        norm = discard_dphi / (phi[ind[-1]] - phi[ind[0]])
        energy_around = spec[ind, kstart:kstop].sum() * norm
        #print norm, energy_around, energy_label / energy_around * 100
        if energy_label / energy_around <= discard_thr / 100.:
            labels[crd[:, 0], crd[:, 1]] = 0
    labels, _, _ = relabel_sequential(labels)

    # Cut duplicated parts of 2D
    if twod == True:
        same = np.unique(labels[nphih, :] + labels[-nphih, :] * 1j)
        same = same[np.where((same.real != 0) & (same.imag != 0))]
#        pdb.set_trace()
#        print np.unique(same.real).size != same.size
#        print np.unique(same.imag).size != same.size
##       pdb.set_trace()
        if np.unique(same.real).size != same.size or \
           np.unique(same.imag).size != same.size:
#           raise Exception
#           print same
           return None
        for lab1, lab2 in zip(same.real, same.imag):
            labels[np.where(labels == lab1)] = lab2
        labels = labels[nphih:-nphih, :]
        labels, _, _ = relabel_sequential(labels)

    return labels


##############################################################################
##############################################################################
##############################################################################


def test_partitioning():
    """
    """
    # Get L2 spectrum and partitions
    # l2path = '/home/cercache/project/mpc-sentinel1/data/esa/sentinel-1a/L2/WV/S1A_WV_OCN__2S/2015/202/'\
    #          'S1A_WV_OCN__2SSV_20150721T044826_20150721T045731_006904_00954A_39FA.SAFE/measurement/'\
    #          's1a-wv1-ocn-vv-20150721t044826-20150721t044829-006904-00954a-001.nc'
    l2path = '/home/cercache/project/mpc-sentinel1/data/esa/sentinel-1a/L2/WV/S1A_WV_OCN__2S/2015/235/'\
             'S1A_WV_OCN__2SSV_20150823T005907_20150823T011433_007383_00A271_57A5.SAFE/measurement/'\
             's1a-wv2-ocn-vv-20150823t005922-20150823t005925-007383-00a271-002.nc'
    print l2path
    dset = Dataset(l2path)
    #spec = dset.variables['oswPolSpec'][0, 0, :, :]
    spec = dset.variables['oswQualityCrossSpectraRe'][0, 0, :, :]
    part = dset.variables['oswPartitions'][0, 0, :, :]
    k = dset.variables['oswK'][:]
    phi = dset.variables['oswPhi'][:]
    dset.close()
    #import pdb ; pdb.set_trace()

    # New partitoning
    # labels = partition_spectrum(spec, k, phi, twod=True, klow=None, khigh=None,
    #                             noise_level=100.,
    #                             noise_thr=1., # relative to noise_level
    #                             merge_thr1=2., # relative to noise_level
    #                             merge_thr2=np.inf, # relative to noise_level
    #                             discard_thr=5., # in %
    #                             discard_dphi=360.) # in degrees
    labels = partition_spectrum(spec, k, phi, twod=True, symphi=True,
                                klow=None, khigh=None,
                                noise_level=50.,
                                noise_thr=1., # relative to noise_level
                                merge_thr1=5., # relative to noise_level
                                merge_thr2=np.inf, # relative to noise_level
                                discard_thr=5., # in %
                                discard_dphi=360.) # in degrees

    # Plot
    plt.subplot(1, 3, 1)
    plt.imshow(np.ma.masked_equal(part, 255), origin='lower', interpolation='nearest')
    plt.colorbar(shrink=0.5)
    plt.title('L2 partitioning')
    plt.subplot(1, 3, 2)
    plt.imshow(spec, origin='lower', interpolation='nearest', cmap=getColorMap('wind.pal'))
    plt.colorbar(shrink=0.5)
    plt.title('Wave spectrum')
    plt.subplot(1, 3, 3)
    plt.imshow(np.ma.masked_equal(labels, 0), origin='lower', interpolation='nearest')
    plt.colorbar(shrink=0.5)
    plt.title('New partitioning')
    plt.show()


def test_partitioning_ww3():
    """
    """
    from bugtest.get_oswl2ocn_param import get_oswl2ocn_param
    from bugtest.get_ww3spec import get_ww3spec
    from bugtest.interpolation_polar_spectrum import interp_polar_spectrum
    
    l2path = 'bugtest/s1a-wv1-ocn-vv-20160101t181405-20160101t181408-009304-00d70c-015.nc'
    param = get_oswl2ocn_param(l2path, get_spec=True)
    part = param['oswPartitions']
    sp  = param['oswspec']
    k   = param['oswk']
    phi = param['oswphi']
    tra = np.squeeze(param['oswHeading'][0][0])

    ww3path = 'bugtest/ww3-wv1-ocn-vv-20160101t181405-20160101t181408-009304-00d70c-015.nc'
    prm = get_ww3spec(ww3path)
    spw  = prm['sp_ww3']
    kw   = prm['k_ww3']
    phiw = prm['phi_ww3']
    ww3_spec_interp = interp_polar_spectrum(k, phi, spw, kw, phiw)

    # WARNING : ww3_spec_interp make partition_spectrum failed at
    # noise_levels, _ = np.broadcast_arrays(noise_level, spec)
    # Maybe because ww3_spec_interp is a view
    # To be investigated, in the mean time we make a copy
    spec = ww3_spec_interp.copy()
    # \WARNING

    ww3_part = partition_spectrum(spec, k, phi, noise_level=2,
                                  noise_thr=1.,#1., # relative to noise_level
                                  merge_thr1=5,#5., # relative to noise_level
                                  merge_thr2=np.inf, # relative to noise_level
                                  discard_thr=1., #0., # in %
                                  discard_dphi=360.) # in degrees
    plt.figure()
    plt.subplot(1, 2, 1)
    plt.imshow(np.ma.masked_equal(spec, 0), origin='lower', interpolation='nearest', cmap=getColorMap('wind.pal'))
    plt.colorbar(shrink=0.5)
    plt.title('Wave spectrum')
    plt.subplot(1, 2, 2)
    plt.imshow(ww3_part, origin='lower', interpolation='nearest')
    plt.colorbar(shrink=0.5)
    plt.title('Partitioning')
    #plt.show()

    ww3_part = partition_spectrum(spec, k, phi, noise_level=0.1,
                                  noise_thr=1.,#1., # relative to noise_level
                                  merge_thr1=5,#5., # relative to noise_level
                                  merge_thr2=np.inf, # relative to noise_level
                                  discard_thr=1., #0., # in %
                                  discard_dphi=360.) # in degrees
    plt.figure()
    plt.subplot(1, 2, 1)
    plt.imshow(np.ma.masked_equal(spec, 0), origin='lower', interpolation='nearest', cmap=getColorMap('wind.pal'))
    plt.colorbar(shrink=0.5)
    plt.title('Wave spectrum')
    plt.subplot(1, 2, 2)
    plt.imshow(ww3_part, origin='lower', interpolation='nearest')
    plt.colorbar(shrink=0.5)
    plt.title('Partitioning')
    plt.show()


# MODIFICATIONS FROM SWIM ORIGINAL CODE :
# - symphi argument (updated in swim code)
# - return labels before merging if no partitions are found, eg if noise level too high (not updated in swim code)
