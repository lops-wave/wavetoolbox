import os
import glob
from datetime import datetime
import netCDF4 as netcdf
import numpy as np
from utils import buoy_spectrum2d, spfphi2kphi, th2ab,ar2ab,spfrom2to
from data_path import cdip_globwave_Path, ndbc_globwave_Path,ndbc_nrt_Path,ndbc_archive_Path
import pdb
import copy

ndbc_nrt_wind_Path ='/home/cercache/project/globwave/data/provider/ndbc/meteo/nrt/'

def spectrum_mean(lon,lat,time,f,k,sf,sp_list,buoy_flag):
    buoy_flag=np.array(buoy_flag)
    mean_sp_list=[]
    mean_sp_list.append((sp_list[0]+sp_list[1])/2)				
    for idx in range(1,len(time)-1):
       if np.all(buoy_flag[idx-1:idx+2]==0):
          mean_sp_list.append((sp_list[idx-1]+sp_list[idx]+sp_list[idx+1])/3)
       else:
          mean_sp_list.append(sp_list[idx])   
    mean_sp_list.append((sp_list[-2]+sp_list[-1])/2)											
    return mean_sp_list
    
def read_nc_wind(id,time_wd):
	filelist = findFilename(id, time_wd,provider ='ndbc_nrt_wind')
	if filelist!=[]:
		time_ind,filelist = GetTimeInd (filelist,time_wd)
		if all([ti==() for ti in time_ind]):
			return None,None,None
		else:
			time,wspd,wdir = Getallwind(filelist,time_ind)
			return time,wspd,wdir
	else:
		return None,None,None		

def Getallwind(filelist,time_ind):
	# import pdb; pdb.set_trace() 
	if all([ti==() for ti in time_ind]):
		print 'No data during the period'
		return None,None,None
#	print filelist[0]	     
	time,wspd,wdir = Getwind(filelist[0],np.array(time_ind[0]))    
	for buoyFile,ind in zip(filelist[1::],time_ind[1::]):
		# print buoyFile
		itime,iwspd,iwdir = Getwind(buoyFile,np.array(ind))

		time = np.array(list(time)+list(itime))
		wspd = np.concatenate((wspd,iwspd), axis=0)
		wdir = np.concatenate((wdir,iwdir), axis=0)
	
	return time,wspd,wdir
 
def Getwind(filepath, index):

	data = netcdf.Dataset( filepath, 'r' )
	time = netcdf.num2date( data.variables['time'][:], data.variables['time'].units )
	time = time[index]
	wspd = data.variables['wind_speed'][index]
	if data.variables.has_key('wind_direction'):   
		wdir = data.variables['wind_direction'][index]
		wspd = data.variables['wind_speed'][index]
	else:
		wdir=[]
		wspd=[]
	data.close()

	return time,wspd,wdir

def read_buoy_nc(id,time_wd,provider,phi = np.arange(0,365,10),con = 'to',smooth=False):
	filelist = findFilename(id, time_wd,provider =provider)
	if filelist!=[]:
		time_ind,filelist = GetTimeInd (filelist,time_wd)
#		pdb.set_trace()
		if all([ti==() for ti in time_ind]):
			#raise Exception ("ERROR : No data found !!!  " )
                        if provider=='ndbc_globwave_no_spectrum':
                                return None, None, None,None, None, None,None,None
                        else:
                                return None, None, None, None, None, None, None,None
		else:
                        if provider=='ndbc_globwave_no_spectrum':
                                #lon,lat = Getlonlat(filelist,time_ind,provider =provider)
                                time,hs,hsq,hsqd,Tp,Tpq,Tpqd,lon,lat = GetAllData(filelist,time_ind,provider =provider)
                                #pdb.set_trace()
                        else:
                                #pdb.set_trace()
                                lon,lat = Getlonlat(filelist,time_ind,provider =provider)
                                f,sf,time,c1,c2,c3,c4,flag = GetAllData(filelist,time_ind,provider =provider)
                                if time is None:
                                    return None,None,None,None,None,None,None,None

	else:
		#raise Exception ("ERROR : No file found !!!  " )
                return None, None, None,None, None, None,None, None

        if provider!='ndbc_globwave_no_spectrum':
                spfphi = Get2dSpec(sf,c1,c2,c3,c4,phi,f,smooth,provider = provider)
                sp2d,k = spfphi2kphi(spfphi,f)

                if con=='to':
                        sp2d = spfrom2to(sp2d,phi)
                return lon,lat,time,f,k,sf,sp2d,flag

        else:
                return lon,lat,time,hs,hsq,hsqd,Tp,Tpq,Tpqd
                

def findFilename( id, time_wd, provider ='cdip_globwave'):   
   filelist = []  

   if time_wd['type'] == 't0dt':
   		time_wd2 = {'type':'t1t2',
					'startDate': time_wd['time0']-time_wd['dt_max'],
					'stopDate': time_wd['time0']+time_wd['dt_max']}
   else:
   		time_wd2 = time_wd
   	   
   startDate,stopDate = time_wd2['startDate'],time_wd2['stopDate']



   if provider == 'ndbc_globwave' :
      archivePath = ndbc_globwave_Path
      filepattern = 'WMO'+id+'*_spectrum.nc'


   if provider == 'ndbc_globwave_no_spectrum' :
      archivePath = ndbc_globwave_Path
      filepattern = 'WMO'+id+'*.nc'


   if provider == 'cdip_globwave' :
      archivePath = cdip_globwave_Path
      if len(id)==5:      filepattern = 'WMO'+id+'*.nc'
      if len(id)==3:      filepattern = id+'_*.nc'

   if provider == 'ndbc_nrt' :
      archivePath = ndbc_nrt_Path
      filelist = [os.path.join( archivePath, id+'w9999.nc' )]

   if provider == 'ndbc_arc' :	   		
   	   archivePath = ndbc_archive_Path
   	   filelist = [os.path.join(archivePath,str(s),id+'w'+str(s)+'.nc') for s in np.arange(startDate.year,stopDate.year+1)]
       
   if provider == 'ndbc_nrt_wind' :
	   if id=='32012':
            id = '32st0'
	   filepattern ='*'+id+'*.nc'
	   archivePath = ndbc_nrt_wind_Path
	   curDate = datetime(startDate.year,startDate.month,1)        
	   while curDate < stopDate:
	       monthDir = os.path.join(archivePath, str(curDate.year), "%02d" % curDate.month )
	       if os.path.exists(monthDir):
	       		filelist = filelist+glob.glob(monthDir+'/'+filepattern)

	       if curDate.month == 12:
	           curDate = curDate.replace(year=curDate.year+1,month=1)
	       else:
	           curDate = curDate.replace(month=curDate.month+1)     

   if provider == 'cdip_globwave' or provider == 'ndbc_globwave' or  provider == 'ndbc_globwave_no_spectrum':
   	   curDate = datetime(startDate.year,startDate.month,1)        
	   while curDate < stopDate:
	       monthDir = os.path.join(archivePath, str(curDate.year), "%02d" % curDate.month )
	       if os.path.exists(monthDir):
	       		#import pdb; pdb.set_trace() 
	       		filelist = filelist+glob.glob(monthDir+'/'+filepattern)

	       if curDate.month == 12:
	           curDate = curDate.replace(year=curDate.year+1,month=1)
	       else:
	           curDate = curDate.replace(month=curDate.month+1)

   filelist.sort()
   #import pdb;pdb.set_trace()
   if time_wd['type'] == 't0dt' and len(filelist)>1 :
	   file_ind,time = [],[]
	   for i,f in enumerate(filelist):
		   data = netcdf.Dataset( f, 'r' )
		   t = list(netcdf.num2date( data.variables['time'][:], data.variables['time'].units ))
		   time = time+t
		   file_ind = file_ind+ [f]*len(t)
		   data.close()
	   time = np.array(time)
	   time0,dt_max= time_wd['time0'],time_wd['dt_max']
	   dt = min(abs(time-time0))
	   if dt<dt_max :
		   filelist = [file_ind[np.argmin(abs(time-time0))]	]	   
	   else:
		   filelist = []

   _filelist=[]
   if provider == 'ndbc_globwave_no_spectrum':
        for _file in filelist:
                if str.split(_file,'_')[-1] != 'spectrum.nc':
                        _filelist.append(_file)
#        pdb.set_trace()
        return _filelist

#   pdb.set_trace()
   return filelist


def GetTimeInd (filelist,time_wd):
	if filelist==[]:
		print 'No file exists!'
		return [()]

	time_ind ,filelist2= [],[]

	for buoyFile in filelist:

		data = netcdf.Dataset( buoyFile, 'r' )
		time = netcdf.num2date( data.variables['time'][:], data.variables['time'].units )
		data.close()

		# check the time in the time window or not
		if time_wd['type'] == 't1t2':
		   # GET the spectrum data in [startDate,stopDate]
		   startDate,stopDate = time_wd['startDate'],time_wd['stopDate'] 
		   dt1,dt2 = np.array(zip(*[((t-startDate).total_seconds(),(stopDate-t).total_seconds()) for t in time]))
		   index = tuple(np.where((dt1>0) * (dt2>0))[0])
		if time_wd['type'] == 't0dt':         
		   # GET the spectrum data for the given time0, within the time difference less than dt_max(unit:hours)
		   time0,dt_max= time_wd['time0'],time_wd['dt_max']
		   dt = min(abs(time-time0))
		   if dt<dt_max :
		      # import pdb; pdb.set_trace() 
		      index = tuple([np.argmin(abs(time-time0))])
		   else:
		      index = ()
		
		if index != ():  
			time_ind.append(index)
			filelist2.append(buoyFile)

	return time_ind,filelist2

def Getlonlat(filelist,time_ind,provider = 'cdip_globwave'):
	lon,lat = [],[]
	for buoyFile,ind in  zip(filelist,time_ind):
		data = netcdf.Dataset( buoyFile, 'r' )
		if provider == 'ndbc_nrt' or provider == 'ndbc_arc':
			lat = lat + [np.squeeze(data.variables['latitude'][:])]*len(ind)
			lon = lon + [np.squeeze(data.variables['longitude'][:])]*len(ind)

		if provider == 'ndbc_globwave' or provider == 'cdip_globwave' or provider=='ndbc_globwave_no_spectrum':
			lat = lat + [np.squeeze(data.variables['lat'][:])]*len(ind)
			lon = lon+ [np.squeeze(data.variables['lon'][:])]*len(ind)
	data.close()

	return np.array(lon), np.array(lat)

def Getdata(filepath, index,provider = 'cdip_globwave'):
	data = netcdf.Dataset( filepath, 'r' )
	time = netcdf.num2date( data.variables['time'][:], data.variables['time'].units )
   
	time = time[index]
        
        if provider=='ndbc_globwave_no_spectrum':
                try:
                        hs   = np.squeeze(data.variables['significant_wave_height'][index]).reshape((len(index),-1)) 
                except KeyError:
                        return None, None, None, None, None, None, None, None, None
                #try:
                hsq  = np.squeeze(data.variables['significant_wave_height_qc_level'][index]).reshape((len(index),-1)) 
                #catch e
                #return None, None, None, None, None, None, None, None, None
                #try:
                hsqd = np.squeeze(data.variables['significant_wave_height_qc_details'][index]).reshape((len(index),-1)) 
                #catch e
                #return None, None, None, None, None, None, None, None, None
                try:
                        Tp   = np.squeeze(data.variables['dominant_wave_period'][index]).reshape((len(index),-1)) 
                except KeyError:
                        return None, None, None, None, None, None, None, None, None
                try:
                        Tpq  = np.squeeze(data.variables['dominant_wave_period_qc_level'][index]).reshape((len(index),-1)) 
                except KeyError:
                        return None, None, None, None, None, None, None, None, None
                try:
                        Tpqd = np.squeeze(data.variables['dominant_wave_period_qc_details'][index]).reshape((len(index),-1)) 
                except KeyError:
                        return None, None, None, None, None, None, None, None, None
                
                lat  = [np.squeeze(data.variables['lat'][:])]*len(hs)
                lon  = [np.squeeze(data.variables['lon'][:])]*len(hs)
                
                #pdb.set_trace()
                data.close()

                return time,hs,hsq,hsqd,Tp,Tpq,Tpqd,lon,lat
        
        else:
                #pdb.set_trace()
                if provider == 'ndbc_nrt' or provider =='ndbc_arc' :
                        flag=[]
                        # import pdb; pdb.set_trace()  
                        f = np.squeeze(data.variables['frequency'][:])
                        sf = data.variables['spectral_wave_density'][index,:].reshape((len(index),-1))
                        c1 = np.squeeze(data.variables['mean_wave_dir'][index,:]).reshape((len(index),-1)) 		# alpha 1
                        c2 = np.squeeze(data.variables['principal_wave_dir'][index,:]).reshape((len(index),-1))	# alpha 2
                        c3 = np.squeeze(data.variables['wave_spectrum_r1'][index,:]).reshape((len(index),-1))	# r1
                        c4 = np.squeeze(data.variables['wave_spectrum_r2'][index,:]).reshape((len(index),-1))   # r2
                        try:
                            for dx in np.arange(time.shape[0]):                           
                               if (np.all(c1.data[dx,:]==999.0))|(np.all(c2.data[dx,:]==999.0))|(np.all(c3.data[dx,:]==999.0))|(np.all(c4.data[dx,:]==999.0)):
                                    flag.append(1)
                               else:
                                    flag.append(0)
                        except:
                            for dx in np.arange(time.shape[0]):                           
                               if (np.all(c1[dx,:]==999.0))|(np.all(c2[dx,:]==999.0))|(np.all(c3[dx,:]==999.0))|(np.all(c4[dx,:]==999.0)):
                                    flag.append(1)
                               else:
                                    flag.append(0)
                if provider == 'cdip_globwave' :
                        # import pdb; pdb.set_trace()  
                        f = np.squeeze(data.variables['central_frequency'][index[0],:])
                        sf = data.variables['sea_surface_variance_spectral_density'][index,:]
                if provider == 'ndbc_globwave' : 
                        # import pdb; pdb.set_trace()  
                        f = np.squeeze(data.variables['wave_directional_spectrum_central_frequency'][:])
                        sf = data.variables['spectral_wave_density'][index,:]

                if provider == 'cdip_globwave' or provider == 'ndbc_globwave': 
                        flag=[]                         
                        if data.variables.has_key('stheta1'):                    
                            c1 = data.variables['stheta1'][index,:]    # stheta1 
                            c2 = data.variables['stheta2'][index,:]    # stheta2 
                            c3 = data.variables['theta1'][index,:]     # theta1  
                            c4 = data.variables['theta2'][index,:]     # theta2 
                            try:
                                for dx in np.arange(time.shape[0]):                           
                                   if np.all(c1.data[dx,:]==9.96921*10**36):
                                        flag.append(1)
                                   else:
                                        flag.append(0)
                            except:
                                for dx in np.arange(time.shape[0]):                           
                                   if np.all(c1[dx,:]==9.96921*10**36):
                                        flag.append(1)
                                   else:
                                        flag.append(0)                            
                        else:
                            data.close()
                            return None, None, None, None, None, None, None, None
                data.close()
                #pdb.set_trace()     
                return f,sf,time,c1,c2,c3,c4,flag

def GetAllData(filelist,time_ind,provider = 'cdip_globwave'):
        
        if provider=='ndbc_globwave_no_spectrum':
                #pdb.set_trace()
                if all([ti==() for ti in time_ind]):
                        print 'No data during the period'
                        return None,None,None,None,None,None,None,None

                time,hs,hsq,hsqd,Tp,Tpq,Tpqd,lon,lat = Getdata(filelist[0],np.array(time_ind[0]),provider=provider)

                for buoyFile,ind in zip(filelist[1::],time_ind[1::]):
                         itime,ihs,ihsq,ihsqd,iTp,iTpq,iTpqd,ilon,ilat = Getdata(buoyFile,np.array(ind),provider = provider)

                         if itime is not None:
                                 time = np.array(list(time)+list(itime))
                                 hs   = np.concatenate((hs,ihs), axis=0)
                                 hsq  = np.concatenate((hsq,ihsq), axis=0)
                                 hsqd = np.concatenate((hsqd,ihsqd), axis=0)
                                 Tp   = np.concatenate((Tp,iTp), axis=0)
                                 Tpq  = np.concatenate((Tpq,iTpq), axis=0)
                                 Tpqd = np.concatenate((Tpqd,iTpqd), axis=0)
                                 lon  = np.concatenate((lon,ilon), axis=0)
                                 lat = np.concatenate((lat,ilat), axis=0)
                         
                return time,hs,hsq,hsqd,Tp,Tpq,Tpqd,lon,lat
        else:


                if all([ti==() for ti in time_ind]):
                        print 'No data during the period'
                        return None,None,None,None,None,None,None,None
                #pdb.set_trace()
                f,sf,time,c1,c2,c3,c4,flag = Getdata(filelist[0],np.array(time_ind[0]),provider = provider)
                if time is None:
                        return None,None,None,None,None,None,None ,None                    
                for buoyFile,ind in zip(filelist[1::],time_ind[1::]):
                        # print buoyFile
                        _,isf,itime,ic1,ic2,ic3,ic4,iflag = Getdata(buoyFile,np.array(ind),provider = provider)
                        if itime is None:
                            continue
                        time = np.array(list(time)+list(itime))
#                        if len(sf)!=len(isf):
#                            continue
                        sf = np.concatenate((sf,isf), axis=0)
                        c1 = np.concatenate((c1,ic1), axis=0)
                        c2 = np.concatenate((c2,ic2), axis=0)
                        c3 = np.concatenate((c3,ic3), axis=0)
                        c4 = np.concatenate((c4,ic4), axis=0)
                        flag = np.concatenate((flag,iflag), axis=0) 
                return f,sf,time,c1,c2,c3,c4,flag


def Get2dSpec(sf,c1,c2,c3,c4,phi,f,smooth,provider = 'cdip_globwave'):
	spfphi = []
	
	for vsf,vc1,vc2,vc3,vc4 in zip(sf,c1,c2,c3,c4):
		# import pdb; pdb.set_trace() 
		# convert  to a1,b1,a2,b2
		if provider == 'cdip_globwave' or provider == 'ndbc_globwave':
			a1,a2,b1,b2 = th2ab(vc1,vc2,vc3,vc4)
		if provider	 == 'ndbc_nrt' or provider =='ndbc_arc':
			a1,a2,b1,b2 = ar2ab(vc1,vc2,vc3,vc4)
		
		v,_ = buoy_spectrum2d(vsf,a1,a2,b1,b2,f,smooth,dirs = phi)
		spfphi.append(v)

	return spfphi

if __name__ == '__main__':
	from datetime import timedelta 
	phi = np.arange(0,365,10)
# # test of cdip globwave format
#	id = '165'
#	time_wd = {'type':'t0dt','time0': datetime(2014,5,1,0,2),'dt_max':timedelta(hours = 0.5)}
	# time_wd = {'type':'t1t2','startDate': datetime(2014,5,28,0,2),'stopDate':datetime(2014,6,5,0,52)}
#	lon,lat,time,f,k,sf,sp_list = read_buoy_nc(id,time_wd,phi = phi,con = 'to', provider = 'cdip_globwave')

# test of ndbc globwave formate
#	id = '51100'
#	time_wd = {'type':'t1t2','startDate': datetime(2014,5,2,0,2),'stopDate':datetime(2014,5,8,0,52)}

#	lon,lat,time,f,k,sf,sp_list = read_buoy_nc(id,time_wd,phi = phi,con = 'to', provider = 'ndbc_globwave')


# # test of ndbc nrt 
	id = '46060'
#	time_wd = {'type':'t1t2','startDate': datetime(2015,5,28,0,2),'stopDate':datetime(2015,6,5,0,52)}
	time_wd = {'type':'t1t2','startDate': datetime(2015,8,22,0,2),'stopDate':datetime(2015,9,9,0,52)}
#	lon,lat,time,f,k,sf,sp_list = read_buoy_nc(id,time_wd,phi = phi,con = 'to', provider = 'ndbc_nrt')
	lon,lat,time,hs,hsq,hsqd,Tp,Tpq,Tpqd = read_buoy_nc(id,time_wd,phi = phi,con = 'to', provider = 'ndbc_globwave_no_spectrum')

        import pdb
        pdb.set_trace()
