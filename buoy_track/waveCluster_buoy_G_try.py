# -*- coding: utf-8 -*-
"""
Created on Fri Mar 31 16:52:11 2017

@author: xwang
"""
import matplotlib.pyplot as plt
import numpy as np
from datetime import datetime,timedelta
import pdb
from buoy_and_SAR_storm_plot import plot_Partition 
import os
import matplotlib as mpl
from buoy_nc_reader import read_buoy_nc
from data_path import cdip_globwave_Path, ndbc_globwave_Path,ndbc_nrt_Path,ndbc_archive_Path
from palette import *
from ocean_wave_spectrum_partitioning import partition_spectrum
import matplotlib.dates as mdates
import heapq
from scipy import stats
import copy
from get_oswl2ocn_param import get_oswl2ocn_param
 
def track_buoy(hs_buoy,wl_buoy,dir_buoy,time_buoy,parts_buoy,id,time,f,k,sf,sp_list,wtime,wspd,wdir,Method,start_thr=1):
    timelist=sorted(np.unique(time_buoy))
    hs_buoy=np.array(hs_buoy)
    recordlist = [];
    for tid in range(0,len(timelist)):
        recordlist.append([])
        ind=np.where(np.array(time_buoy)==timelist[tid])[0]
        hssort_ind = np.argsort(hs_buoy[ind])[::-1]
        wind_ind=np.where(wtime==timelist[tid])[0]
        for sort_ind in range(0,len(ind)):
             sysind=hssort_ind[sort_ind]
             if len(wind_ind)!=0:
                  tmp_delta_dir=abs(dir_buoy[ind[sysind]]-(wdir[wind_ind]+180)%360)
                  tmp_delta_dir[tmp_delta_dir>180]=360-tmp_delta_dir[tmp_delta_dir>180] 
                  tmp_fp=np.sqrt(9.81/2/np.pi/wl_buoy[ind[sysind]])
                  u_min = g/tmp_fp/2/np.pi/1.2											
                  if (abs(tmp_delta_dir)<30)&(wspd[wind_ind]>u_min*2./3):           
                      recordlist[tid].append({ 'time':(timelist[tid]-timelist[0]).total_seconds()/3600\
                                 ,'datetime':timelist[tid], 'fp':tmp_fp\
                                         , 'hs':hs_buoy[ind[sysind]], 'dir':dir_buoy[ind[sysind]] \
                                            ,'parts':parts_buoy[ind[sysind]]  , 'sysid':0,'wind_flag':1,'thr':start_thr})
                  else:
                      recordlist[tid].append({ 'time':(timelist[tid]-timelist[0]).total_seconds()/3600\
                                 ,'datetime':timelist[tid], 'fp':tmp_fp\
                                         , 'hs':hs_buoy[ind[sysind]], 'dir':dir_buoy[ind[sysind]] \
                                           ,'parts':parts_buoy[ind[sysind]]    , 'sysid':0,'wind_flag':0,'thr':start_thr})      
             else:
                  recordlist[tid].append({ 'time':(timelist[tid]-timelist[0]).total_seconds()/3600\
                                      ,'datetime':timelist[tid], 'fp':np.sqrt(9.81/2/np.pi/wl_buoy[ind[sysind]])\
                                         , 'hs':hs_buoy[ind[sysind]], 'dir':dir_buoy[ind[sysind]] \
                                            ,'parts':parts_buoy[ind[sysind]] , 'sysid':0,'wind_flag':-1,'thr':start_thr}) 
  
    [recordlist,sysid]=track_criteria(recordlist,delta_fp_max=0.003,delta_fp_min=-0.001,delta_dir_max=30,sysid=1 )     
    [recordlist,sysid]=track_criteria(recordlist,delta_fp_max=0.006,delta_fp_min=-0.003,delta_dir_max=45,sysid=sysid )   
    plot_middle(recordlist,'1_raw',id,time,f,k,sf,sp_list,Method)
    
    print '1st connect'
    [recordlist,sysid]=track_connect(recordlist,sysid,id,time,f,k,sf,sp_list,wtime,wspd,wdir,start_thr)
    plot_middle(recordlist,'2_1st connect',id,time,f,k,sf,sp_list,Method)  
    print 'forward grow'    
    [recordlist,sysid]=track_buoy_grow_forward(recordlist,sysid,meanlen=2) 
    plot_middle(recordlist,'3_forward grow',id,time,f,k,sf,sp_list,Method)
    print 'backward grow'    
    [recordlist,sysid]=track_buoy_grow_backward(recordlist,sysid,meanlen=2) 
    plot_middle(recordlist,'4_backward grow',id,time,f,k,sf,sp_list,Method)  
    print 'repartition'
    track_buoy_grow_consistent(recordlist,sysid,id,time,f,k,sf,sp_list,wtime,wspd,wdir,meanlen=3)
    plot_middle(recordlist,'5_repartition',id,time,f,k,sf,sp_list,Method) 
    print 'track_more' 
    [recordlist,sysid]=track_criteria(recordlist,delta_fp_max=0.006,delta_fp_min=-0.003,delta_dir_max=45,sysid=sysid )  
    plot_middle(recordlist,'6_track_more',id,time,f,k,sf,sp_list,Method)     
    print '2st connect'     
    [recordlist,sysid]=track_connect(recordlist,sysid,id,time,f,k,sf,sp_list,wtime,wspd,wdir,start_thr)  
    plot_middle(recordlist,'7_2st connect',id,time,f,k,sf,sp_list,Method) 			
    ##  renumber   				
    print 'forward grow'    
    [recordlist,sysid]=track_buoy_grow_forward(recordlist,sysid,meanlen=2) 
    plot_middle(recordlist,'8_forward grow',id,time,f,k,sf,sp_list,Method)
    print 'backward grow'    
    [recordlist,sysid]=track_buoy_grow_backward(recordlist,sysid,meanlen=2) 
    plot_middle(recordlist,'9_backward grow',id,time,f,k,sf,sp_list,Method)    
	    
    print 'repartition'
    track_buoy_grow_consistent(recordlist,sysid,id,time,f,k,sf,sp_list,wtime,wspd,wdir,meanlen=2)				
    [recordlist,sysid]=repartition2(recordlist,sysid,id,time,f,k,sf,sp_list,wtime,wspd,wdir)			
    plot_middle(recordlist,'10_repartition',id,time,f,k,sf,sp_list,Method) 
    
    sss = zip(*[(recordlist[tid][prtid],tid,prtid) for tid in range(0,len(recordlist)) for prtid in range(0,len(recordlist[tid]))])
    btimelist = np.array([sss[0][x]['datetime'] for x in range(0,len(sss[0]))])
    bfplist =np.array([sss[0][x]['fp'] for x in range(0,len(sss[0]))])
    bhslist = np.array([sss[0][x]['hs'] for x in range(0,len(sss[0]))])
    bdirlist = np.array([sss[0][x]['dir'] for x in range(0,len(sss[0]))])
    bsyslist= np.array([sss[0][x]['sysid'] for x in range(0,len(sss[0]))])
    bwindlist= np.array([sss[0][x]['wind_flag'] for x in range(0,len(sss[0]))])
    thr= np.array([sss[0][x]['thr'] for x in range(0,len(sss[0]))]) 
    partlist=[sss[0][x]['parts'] for x in range(0,len(sss[0]))]      
    ##  renumber   
    num=1                
    for  sys1id in range(1,sysid):
        fp=bfplist[np.where(bsyslist == sys1id)[0]]
        if np.max(1./fp)>12:
           bsyslist[np.where(bsyslist == sys1id)[0]]=num
           num=num+1
        else:
           bsyslist[np.where(bsyslist == sys1id)[0]]=0     
        
    return btimelist, bfplist, bhslist, bdirlist, bsyslist,bwindlist,thr,partlist


def repartition2(recordlist,sysid,id,time,f,k,sf,sp_list,wtime,wspd,wdir):      
    
    sss = zip(*[(recordlist[tid][prtid],tid,prtid) for tid in range(0,len(recordlist)) for prtid in range(0,len(recordlist[tid]))])
    btimelist = np.array([sss[0][x]['datetime'] for x in range(0,len(sss[0]))])
    btimelist_tid=np.array([sss[0][x]['time'] for x in range(0,len(sss[0]))])
    bfplist =np.array([sss[0][x]['fp'] for x in range(0,len(sss[0]))])
    bhslist = np.array([sss[0][x]['hs'] for x in range(0,len(sss[0]))])
    bdirlist = np.array([sss[0][x]['dir'] for x in range(0,len(sss[0]))])
    bsyslist = np.array([sss[0][x]['sysid'] for x in range(0,len(sss[0]))])  
    bwindlist = np.array([sss[0][x]['wind_flag'] for x in range(0,len(sss[0]))])  
    thrlist = np.array([sss[0][x]['thr'] for x in range(0,len(sss[0]))])             
    flag_merge=np.zeros_like(bsyslist)
    ##  near partitions hs diferent and D 
    for  sys1id in range(1,sysid):
         sys_timelist=np.copy(btimelist_tid[np.where(bsyslist == sys1id)[0]])
         sys_timelist_date=np.copy(btimelist[np.where(bsyslist == sys1id)[0]])
         sys_fplist=np.copy(bfplist[np.where(bsyslist == sys1id)[0]])
         sys_dirlist=np.copy(bdirlist[np.where(bsyslist == sys1id)[0]])
         sys_hslist=np.copy(bhslist[np.where(bsyslist == sys1id)[0]])  
         sys_thrlist=np.copy(thrlist[np.where(bsyslist == sys1id)[0]])  									
         timelist=sorted(np.unique(sys_timelist))
         for tid in range(0,len(timelist)):
            count_partitions=np.where(btimelist_tid== timelist[tid])[0]
            if len(count_partitions)==1:    
                  continue      			
            count_partitions=np.where((btimelist_tid== timelist[tid])&(bsyslist ==0))[0]	
            if len(count_partitions)==0:    
                  continue 												
            ind=np.where(sys_timelist==timelist[tid])[0]
            tmp_mean_ind=abs(np.arange(0,len(sys_timelist))-ind)
            mean_ind=np.argsort(tmp_mean_ind)[:6]
            mean_ind=np.delete(mean_ind,[ind],None)  
            
            tmp_dir=np.mean(sys_dirlist[mean_ind])
            tmp_delta_dir=abs(sys_dirlist[ind]-tmp_dir)
            tmp_delta_dir[tmp_delta_dir>180]=360-tmp_delta_dir[tmp_delta_dir>180]
            tmp_fp=np.mean(sys_fplist[mean_ind])
            tmp_hs=np.mean(sys_hslist[mean_ind])
            Db=(np.log(sys_fplist[ind])-np.log(tmp_fp))**2*9\
                                        + (tmp_delta_dir/180.0)**2
            HsD=(tmp_hs-sys_hslist[ind])/tmp_hs												
#            if HsD<0.2:
#			     continue			
            sys_ind0=np.where((btimelist_tid== timelist[tid])&(bsyslist ==0))[0]                               
            tmp_delta_dir=abs(bdirlist[sys_ind0]-tmp_dir)
            tmp_delta_dir[tmp_delta_dir>180]=360-tmp_delta_dir[tmp_delta_dir>180]                                          
            Dsys0=(np.log(bfplist[sys_ind0])-np.log(tmp_fp))**2*9\
                                            + (tmp_delta_dir/180.0)**2
#            if sys_timelist_date[ind]==datetime(2016,4,28,19):
#				pdb.set_trace()																																												
            if ~np.any((Dsys0-Db)<0.2):
                continue           												
            time_ind=np.where(time==sys_timelist_date[ind])[0]  ## find the time of buoy spectrum
#            if sys_timelist_date[ind]==datetime(2016,4,29,6):
#				pdb.set_trace()
            parts=bsyslist[np.where(btimelist_tid== timelist[tid])[0]]  ## find the all partition sysid in time timelist[tid]
            [hs_buoy,dir_buoy,fp_buoy,D1,Hs1,buoy_parts]=buoy_partition(id,time[time_ind],f,k,sf,sp_list[time_ind],sys_thrlist[ind]*0.05,Db,HsD,tmp_dir,tmp_hs,tmp_fp,'repartition')                              
            for thr in range(sys_thrlist[ind]+1,20):
                 [hs_buoy,dir_buoy,fp_buoy,D,Hs,buoy_parts]=buoy_partition(id,time[time_ind],f,k,sf,sp_list[time_ind],thr*0.05,Db,HsD,tmp_dir,tmp_hs,tmp_fp,'repartition')
                 if hs_buoy is None:																			
                     continue
                 if (len(D)!=len(D1))&(len(np.intersect1d(HsD,Hs))==1): ##combine others break																
                     break     
                 Dsys_ind1=None;Dsys_ind2=None;		
#                 if sys_timelist_date[ind]==datetime(2016,4,28,19):
#                    	pdb.set_trace()																		
                 if len(np.intersect1d(HsD,Hs))==0: ## combine the near partitions
                          if len(np.where((parts!=0)&(parts!=sys1id))[0])!=0:
                              tmp_parts=parts[np.where((parts!=0)&(parts!=sys1id))[0]]
                              sys_ind1=np.where((btimelist_tid== timelist[tid])&(bsyslist ==tmp_parts))[0]
                              tmp_delta_dir=abs(bdirlist[sys_ind1]-tmp_dir)
                              tmp_delta_dir[tmp_delta_dir>180]=360-tmp_delta_dir[tmp_delta_dir>180]
                              Dsys=(np.log(bfplist[sys_ind1])-np.log(tmp_fp))**2*9\
                                        + (tmp_delta_dir/180.0)**2
                              if len(np.intersect1d(D,Dsys))!=1:                                         																															
                                  break
                              Dsys_ind1=np.where(D==Dsys)[0]
                              D=np.delete(D,[Dsys_ind1],None)  
                              Hs=np.delete(Hs,[Dsys_ind1],None)  
                              hs_buoy=np.delete(hs_buoy,[Dsys_ind1],None)  
                              fp_buoy=np.delete(fp_buoy,[Dsys_ind1],None)  
                              dir_buoy=np.delete(dir_buoy,[Dsys_ind1],None) 																														
                              if len(D)==0:																														
                                  break
                          if len(np.where(parts==0)[0])!=0:
                              sys_ind2=np.where((btimelist_tid== timelist[tid])&(bsyslist ==0))[0]
                              wind_flag=bwindlist[sys_ind2]
                              wind2ind=np.where(wind_flag==1)[0]
                              if len(wind2ind)!=0:                                     
                                  tmp_delta_dir=abs(bdirlist[sys_ind2[wind2ind]]-tmp_dir)
                                  tmp_delta_dir[tmp_delta_dir>180]=360-tmp_delta_dir[tmp_delta_dir>180]                                          
                                  Dsys=(np.log(bfplist[sys_ind2[wind2ind]])-np.log(tmp_fp))**2*9\
                                            + (tmp_delta_dir/180.0)**2
                                  if len(np.intersect1d(D,Dsys))!=1:
                                      break          
                                  Dsys_ind2=np.where(D==Dsys)[0]
                                  D=np.delete(D,[Dsys_ind2],None)  
                                  Hs=np.delete(Hs,[Dsys_ind2],None)  
                                  hs_buoy=np.delete(hs_buoy,[Dsys_ind2],None)  
                                  fp_buoy=np.delete(fp_buoy,[Dsys_ind2],None)  
                                  dir_buoy=np.delete(dir_buoy,[Dsys_ind2],None)
                              if len(D)==0:                                       																															
                                  break  
                          if len(D)-1>len(np.where((btimelist_tid== timelist[tid])&(bsyslist == 0)&(bwindlist!= 1))[0]):
                             continue        
#                          if sys_timelist_date[ind]==datetime(2016,4,19,6):
#                                pdb.set_trace()                                                           
                          tmp_ind=np.argmin(D)
                          if (D[tmp_ind]+abs(Hs[tmp_ind])**2/9)>(Db+abs(HsD)**2/9+0.06):
                              break																													
                          if 	Dsys_ind1 is not None:																												
                              sss[0][sys_ind1]['parts']=buoy_parts[Dsys_ind1] 																									
                          if 	Dsys_ind2 is not None:																																																									
                              sss[0][sys_ind2[wind2ind]]['parts']=buoy_parts[Dsys_ind2] 
                          if 	Dsys_ind1 is not None:																																																										
                              del buoy_parts[Dsys_ind1]    
                          if 	Dsys_ind2 is not None:																																																										
                              del buoy_parts[Dsys_ind2]    																													
                          sys_ind=np.where((btimelist_tid== timelist[tid])&(bsyslist == sys1id))[0]
                          sss[0][sys_ind[0]]['parts']=buoy_parts[tmp_ind]                         
                          bfplist[sys_ind]=fp_buoy[tmp_ind]
                          bdirlist[sys_ind]=dir_buoy[tmp_ind]                      
                          bhslist[sys_ind]=hs_buoy[tmp_ind] 
                          thrlist[sys_ind[0]]=thr
                          if len(np.where(parts==0)[0])!=0:
                              sys_ind=np.where((btimelist_tid== timelist[tid])&(bsyslist == 0)&(bwindlist!=1))[0]
                              if len(hs_buoy)==1:
                                  flag_merge[sys_ind]=1   ## the rest 0 will not exist                                  
                              else:
                                  tmp_ind =np.arange(0,len(D))
                                  tmp_ind=np.delete(tmp_ind,[np.argmin(D)],None)   
                                  for idx in range(0,len(tmp_ind)):
                                      bfplist[sys_ind[idx]]=fp_buoy[tmp_ind[idx]]
                                      bdirlist[sys_ind[idx]]=dir_buoy[tmp_ind[idx]]                      
                                      bhslist[sys_ind[idx]]=hs_buoy[tmp_ind[idx]]      
                                  if len(sys_ind)>len(tmp_ind):
                                     flag_merge[sys_ind[(idx+1):]]=1   ## the rest 0 will not exist  
                          break
                      
            Db=(np.log(sys_fplist[ind])-np.log(tmp_fp))**2*9\
                                        + (tmp_delta_dir/180.0)**2
            HsD=(tmp_hs-sys_hslist[ind])/tmp_hs
												
    _recordlist=[]
    tmp_sss=np.array(sss[2])  
    for tid in range(0,len(btimelist)):
        if (flag_merge[tid]==1)&(tmp_sss[tid]==0):
            tmp_sss[tid+1]=0
            continue
        if flag_merge[tid]==0: ## the partitions have been merged
            if tmp_sss[tid]==0:
                _recordlist.append([])
                _recordlist[sss[1][tid]].append({ 'time':btimelist_tid[tid]\
                         ,'datetime':btimelist[tid], 'fp':bfplist[tid]\
                                 , 'hs':bhslist[tid], 'dir':bdirlist[tid] \
                                     ,'parts':sss[0][tid]['parts'], 'sysid':bsyslist[tid],'wind_flag':bwindlist[tid],'thr':thrlist[tid]})
                
            else:
                 _recordlist[sss[1][tid]].append({ 'time':btimelist_tid[tid]\
                         ,'datetime':btimelist[tid], 'fp':bfplist[tid]\
                                 , 'hs':bhslist[tid], 'dir':bdirlist[tid] \
                                      ,'parts':sss[0][tid]['parts'], 'sysid':bsyslist[tid],'wind_flag':bwindlist[tid],'thr':thrlist[tid]})             
    return  _recordlist,sysid                 
def track_connect(recordlist,sysid,id,time,f,k,sf,sp_list,wtime,wspd,wdir,start_thr,start=True):

    sss = zip(*[(recordlist[tid][prtid],tid,prtid) for tid in range(0,len(recordlist)) for prtid in range(0,len(recordlist[tid]))])
    btimelist = np.array([sss[0][x]['datetime'] for x in range(0,len(sss[0]))])
    btimelist_tid=np.array([sss[0][x]['time'] for x in range(0,len(sss[0]))])
    bfplist =np.array([sss[0][x]['fp'] for x in range(0,len(sss[0]))])
    bhslist = np.array([sss[0][x]['hs'] for x in range(0,len(sss[0]))])
    bdirlist = np.array([sss[0][x]['dir'] for x in range(0,len(sss[0]))])
    bwindlist = np.array([sss[0][x]['wind_flag'] for x in range(0,len(sss[0]))])    
    thrlist = np.array([sss[0][x]['thr'] for x in range(0,len(sss[0]))])    
    
    bsyslist_tmp = np.array([sss[0][x]['sysid'] for x in range(0,len(sss[0]))])
    bsyslist=np.copy(bsyslist_tmp)
##  assign the non associated parts with sysid 0    
    num=1
    for  sys1id in range(1,sysid):
        if len(np.where(bsyslist_tmp == sys1id)[0])>=6:
            slope, intercept, r_value, p_value, std_err = stats.linregress(btimelist_tid[np.where(bsyslist_tmp == sys1id)[0]],bfplist[np.where(bsyslist_tmp == sys1id)[0]])
            if (p_value<0.01)and(slope>0):
               bsyslist[np.where(bsyslist_tmp == sys1id)[0]]=num
               num=num+1
            else:
               bsyslist[np.where(bsyslist_tmp == sys1id)[0]]=0                    
        else:
            bsyslist[np.where(bsyslist_tmp == sys1id)[0]]=0       
             
#       connect the tracked swell (num>10) belonging to the same storm          
    for  sys1id in range(1,num):
        for  sys2id in range(sys1id+1,num):            
             time1=btimelist_tid[np.where(bsyslist == sys1id)[0]]
             time2=btimelist_tid[np.where(bsyslist == sys2id)[0]]
             if (len(time1)==0)or(len(time2)==0):
                 continue
             if time1[0]>time2[0]:
                 tempsid=sys1id
                 sys1id=sys2id
                 sys2id=tempsid																
             time1=btimelist_tid[np.where(bsyslist == sys1id)[0]]
             time2=btimelist_tid[np.where(bsyslist == sys2id)[0]]                 
             time_min=np.min(abs(time1-time2[0]))             
             if time_min>7:
                 continue
             fp1=bfplist[np.where(bsyslist == sys1id)[0]]
             fp2=bfplist[np.where(bsyslist == sys2id)[0]]
             dir1=bdirlist[np.where(bsyslist == sys1id)[0]]
             dir2=bdirlist[np.where(bsyslist == sys2id)[0]]   
             tmp_index=np.argmin(abs(time1-time2[0]))
             delta_fp=fp2[0]-fp1[tmp_index]
             tmp_index=heapq.nsmallest(3,np.where(abs(time1-time2[0]))[0])
             meandir1=dir1[tmp_index[0]]/3
             for idx in range(1,3):
                 if dir1[tmp_index[idx]]-dir1[tmp_index[0]]>180:
                         meandir1=meandir1+(dir1[tmp_index[idx]]-360)/3
                 elif dir1[tmp_index[idx]]-dir1[tmp_index[0]]<-180:
                         meandir1=meandir1+(dir1[tmp_index[idx]]+360)/3 
                 else:
                         meandir1=meandir1+dir1[tmp_index[idx]]/3     
             meandir2=dir2[0]/3
             for idx in range(1,3):
                 if dir2[idx]-dir2[0]>180:
                         meandir2=meandir2+(dir2[idx]-360)/3
                 elif dir2[tmp_index[idx]]-dir2[0]<-180:
                         meandir2=meandir2+(dir2[idx]+360)/3 
                 else:
                         meandir2=meandir2+dir2[idx]/3                                   
             delta_dir=abs(meandir1-meandir2)                     
             if delta_dir>180:
                delta_dir = 360-delta_dir                                                                  
             if  (abs(delta_fp)< 0.007) and (delta_dir <60):
                 bsyslist[np.where(bsyslist == sys2id)[0]]=np.unique(bsyslist[np.where(bsyslist == sys1id)[0]])                  

    ##  renumber                   
    sysid=1
    for  sys1id in range(1,num):
        time_sys=btimelist_tid[np.where(bsyslist == sys1id)[0]]
        if len(time_sys)==0:
            continue
        if np.max(time_sys)-np.min(time_sys)>=24:
           bsyslist[np.where(bsyslist == sys1id)[0]]=sysid
           sysid=sysid+1
        else:
           bsyslist[np.where(bsyslist == sys1id)[0]]=0             
    flag_merge=np.zeros_like(bsyslist)
    print sysid        
    ## same time two near partitions hs diferent.
    for  sys1id in range(1,sysid):
         sys_timelist=np.copy(btimelist_tid[np.where(bsyslist == sys1id)[0]])
         sys_timelist_date=np.copy(btimelist[np.where(bsyslist == sys1id)[0]])
         sys_fplist=np.copy(bfplist[np.where(bsyslist == sys1id)[0]])
         sys_dirlist=np.copy(bdirlist[np.where(bsyslist == sys1id)[0]])
         sys_hslist=np.copy(bhslist[np.where(bsyslist == sys1id)[0]])        
         timelist=sorted(np.unique(sys_timelist))
         for tid in range(0,len(timelist)):										
            ind=np.where(sys_timelist==timelist[tid])[0]		
            if len(ind)>1:
                tmp_dir=np.mean(np.concatenate((sys_dirlist[ind[0]-3:ind[0]],sys_dirlist[ind[0]+len(ind):ind[0]+3+len(ind)]), axis=0))
                tmp_delta_dir=abs(sys_dirlist[ind]-tmp_dir)
                tmp_delta_dir[tmp_delta_dir>180]=360-tmp_delta_dir[tmp_delta_dir>180]
                tmp_fp=np.mean(np.concatenate((sys_fplist[ind[0]-3:ind[0]],sys_fplist[ind[0]+len(ind):ind[0]+3+len(ind)]), axis=0))
                tmp_hs=np.mean(np.concatenate((sys_hslist[ind[0]-3:ind[0]],sys_hslist[ind[0]+len(ind):ind[0]+3+len(ind)]), axis=0))
                Db=(np.log(sys_fplist[ind])-np.log(tmp_fp))**2*9\
                                            + (tmp_delta_dir/180.0)**2
                HsD=(tmp_hs-sys_hslist[ind])/tmp_hs
                time_ind=np.where(time==sys_timelist_date[ind[0]])[0]  ## find the time of buoy spectrum
                parts=bsyslist[np.where(btimelist_tid== timelist[tid])[0]]  ## find the all partition sysid in time timelist[tid]
                merge=0
                first_partition=0
                for thr in range(start_thr,11):
                     [hs_buoy,dir_buoy,fp_buoy,D,Hs,buoy_parts]=buoy_partition(id,time[time_ind],f,k,sf,sp_list[time_ind],thr*0.05,Db,HsD,tmp_dir,tmp_hs,tmp_fp,'combine')
                     if  hs_buoy is None:
                         break
                     if thr==start_thr:
                         first_partition=len(D)                
                     if (len(np.intersect1d(D,Db))<=1)&(first_partition>len(D)):
                              if len(np.where((parts!=0)&(parts!=sys1id))[0])!=0:
                                  sys_ind=np.where((btimelist_tid== timelist[tid])&(bsyslist ==parts[np.where((parts!=0)&(parts!=sys1id))[0]]))[0]
                                  tmp_delta_dir=abs(bdirlist[sys_ind]-tmp_dir)
                                  tmp_delta_dir[tmp_delta_dir>180]=360-tmp_delta_dir[tmp_delta_dir>180]
                                  Dsys=(np.log(bfplist[sys_ind])-np.log(tmp_fp))**2*9\
                                            + (tmp_delta_dir/180.0)**2
                                  if len(np.intersect1d(D,Dsys))!=1:
                                      break          
                                  Dsys_ind=np.where(D==Dsys)[0]
                                  if len(Dsys_ind)>1:
										continue																									
                                  D=np.delete(D,[Dsys_ind],None)  
                                  Hs=np.delete(Hs,[Dsys_ind],None)  
                                  hs_buoy=np.delete(hs_buoy,[Dsys_ind],None)  
                                  fp_buoy=np.delete(fp_buoy,[Dsys_ind],None)  
                                  dir_buoy=np.delete(dir_buoy,[Dsys_ind],None)
                                  del buoy_parts[Dsys_ind]																																	
                              if len(np.where(parts==0)[0])!=0:
                                  sys_ind=np.where((btimelist_tid== timelist[tid])&(bsyslist ==0))[0]
                                  wind_flag=bwindlist[sys_ind]
                                  wind2ind=np.where(wind_flag==1)[0]
                                  if len(wind2ind)!=0:                                     
                                      tmp_delta_dir=abs(bdirlist[sys_ind[wind2ind]]-tmp_dir)
                                      tmp_delta_dir[tmp_delta_dir>180]=360-tmp_delta_dir[tmp_delta_dir>180]                                          
                                      Dsys=(np.log(bfplist[sys_ind[wind2ind]])-np.log(tmp_fp))**2*9\
                                                + (tmp_delta_dir/180.0)**2
                                      if len(np.intersect1d(D,Dsys))!=1:
                                          break          
                                      Dsys_ind=np.where(D==Dsys)[0]
                                      D=np.delete(D,[Dsys_ind],None)  
                                      Hs=np.delete(Hs,[Dsys_ind],None)  
                                      hs_buoy=np.delete(hs_buoy,[Dsys_ind],None)  
                                      fp_buoy=np.delete(fp_buoy,[Dsys_ind],None)  
                                      dir_buoy=np.delete(dir_buoy,[Dsys_ind],None) 
#                                      print D,Dsys_ind																																						
#                                      del buoy_parts[Dsys_ind]																																											
                              if len(D)-1>len(np.where((btimelist_tid== timelist[tid])&(bsyslist == 0)&(bwindlist!= 1))[0]):
                                 continue
                              if len(D)==0:
                                 break                              																																
                              tmp_ind=np.argmin(D)
                              sys_ind=np.where((btimelist_tid== timelist[tid])&(bsyslist == sys1id))[0]
                              sss[0][sys_ind[0]]['parts']=buoy_parts[tmp_ind]																														
                              bfplist[sys_ind[0]]=fp_buoy[tmp_ind]
                              bdirlist[sys_ind[0]]=dir_buoy[tmp_ind]                      
                              bhslist[sys_ind[0]]=hs_buoy[tmp_ind]  
                              thrlist[np.where(btimelist_tid== timelist[tid])[0]]=thr
                              flag_merge[sys_ind[1:]]=1   ## the rest will not exist      
                              if len(np.where(parts==0)[0])!=0:
                                  sys_ind=np.where((btimelist_tid== timelist[tid])&(bsyslist == 0)&(bwindlist!= 1))[0]
                                  tmp_ind =np.arange(0,len(D))
                                  tmp_ind=np.delete(tmp_ind,[np.argmin(D)],None)  
                                  for idx in range(0,len(tmp_ind)):
                                      bfplist[sys_ind[idx]]=fp_buoy[tmp_ind[idx]]
                                      bdirlist[sys_ind[idx]]=dir_buoy[tmp_ind[idx]]                      
                                      bhslist[sys_ind[idx]]=hs_buoy[tmp_ind[idx]]   
                                      sss[0][sys_ind[idx]]['parts']=buoy_parts[tmp_ind[idx]]																																						
                                  if len(sys_ind)>len(tmp_ind):
                                      flag_merge[sys_ind[len(tmp_ind):]]=1   ## the rest 0 will not exist  
                              merge=1
                              break
                if merge==0:
                     idx =np.arange(0,len(Db))
                     idx=np.delete(idx,[np.argmin(Db)],None)   
                     sys_ind=np.where((btimelist_tid== timelist[tid])&(bsyslist == sys1id))[0]
                     bsyslist[sys_ind[idx]]=0
																																			
#            if timelist[tid]==219.:
#				print bdirlist[np.where((btimelist_tid== timelist[tid]))[0]]
#				print bfplist[np.where((btimelist_tid== timelist[tid]))[0]]                              
    _recordlist = []
    tmp_sss=np.array(sss[2])  
    for tid in range(0,len(btimelist)):
        if (flag_merge[tid]==1)&(tmp_sss[tid]==0):
            tmp_sss[tid+1]=0
            continue
        if flag_merge[tid]==0: ## the partitions have been merged
            if tmp_sss[tid]==0:
                _recordlist.append([])
                _recordlist[sss[1][tid]].append({ 'time':btimelist_tid[tid]\
                         ,'datetime':btimelist[tid], 'fp':bfplist[tid]\
                                 , 'hs':bhslist[tid], 'dir':bdirlist[tid] \
                                    ,'parts':sss[0][tid]['parts']  , 'sysid':bsyslist[tid],'wind_flag':bwindlist[tid],'thr':thrlist[tid],'flag_merge':0})
                
            else:
                _recordlist[sss[1][tid]].append({ 'time':btimelist_tid[tid]\
                         ,'datetime':btimelist[tid], 'fp':bfplist[tid]\
                                 , 'hs':bhslist[tid], 'dir':bdirlist[tid] \
                                   ,'parts':sss[0][tid]['parts']   , 'sysid':bsyslist[tid],'wind_flag':bwindlist[tid],'thr':thrlist[tid],'flag_merge':0})

    return _recordlist,sysid  
def repartition(recordlist,tid,prtid,sysid,sys_not_merge,id,time,f,k,sf,sp_list,wtime,wspd,wdir):  
    sss = zip(*[(recordlist[tid][x],x) for x in range(0,len(recordlist[tid]))])    
    syslist = np.array([recordlist[tid][x]['sysid'] for x in range(0,len(recordlist[tid]))])  
    windlist = np.array([recordlist[tid][x]['wind_flag'] for x in range(0,len(recordlist[tid]))]) 
    fplist =np.array([recordlist[tid][x]['fp'] for x in range(0,len(recordlist[tid]))])
    hslist = np.array([recordlist[tid][x]['hs'] for x in range(0,len(recordlist[tid]))])
    dirlist = np.array([recordlist[tid][x]['dir'] for x in range(0,len(recordlist[tid]))])  
    thrlist=np.array([recordlist[tid][x]['thr'] for x in range(0,len(recordlist[tid]))]) 
    parts= [recordlist[tid][x]['parts'] for x in range(0,len(recordlist[tid]))]
    timelist = np.array([recordlist[tid][x]['datetime'] for x in range(0,len(recordlist[tid]))])  
    timelist_tid = np.array([recordlist[tid][x]['time'] for x in range(0,len(recordlist[tid]))])      
    flag_merge=np.zeros_like(syslist)
    Db=0;HsD=0;tmp_dir=recordlist[tid][prtid]['dir'] ;tmp_hs=recordlist[tid][prtid]['hs'] ;tmp_fp=recordlist[tid][prtid]['fp']  
    D0=[]
    Hs0=[]
    merge=0
    for ind in range(0,len(syslist)):
        delta_dir=abs(tmp_dir-dirlist[ind])
        if delta_dir>180:
            delta_dir=360-delta_dir
        D0.append((np.log(fplist[ind])-np.log(tmp_fp))**2*9 + (delta_dir/180.0)**2)
        Hs0.append((tmp_hs-hslist[ind])/tmp_hs)							
    time_ind=np.where(time==recordlist[tid][prtid]['datetime'])[0]  ## find the time of buoy spectrum
    for thr in range(recordlist[tid][prtid]['thr']+1,11):     
         [hs_buoy,dir_buoy,fp_buoy,D,Hs,buoy_parts]=buoy_partition(id,time[time_ind],f,k,sf,sp_list[time_ind],thr*0.05,Db,HsD,tmp_dir,tmp_hs,tmp_fp,'repartition')
         if hs_buoy is None:
             break
         flag=0
         if len(sys_not_merge)!=0:
	         for ind in range(0,len(sys_not_merge)):
	            if (len(np.intersect1d(D0[sys_not_merge[ind]],D))==0)|(len(np.where(D0==D0[sys_not_merge[ind]])[0])>1)|(len(np.where(D==D0[sys_not_merge[ind]])[0])>1):              
	                flag=1
	                break
         if flag==1:
             break
																					
         if len(np.intersect1d(HsD,Hs))==0:       
              for ind in range(0,len(sys_not_merge)):   
                  Dsys_ind=np.where(D==D0[sys_not_merge[ind]])[0] 																			 
                  fplist[sys_not_merge[ind]]=fp_buoy[Dsys_ind]
                  dirlist[sys_not_merge[ind]]=dir_buoy[Dsys_ind]                      
                  hslist[sys_not_merge[ind]]=hs_buoy[Dsys_ind] 
																		
                  D=np.delete(D,[Dsys_ind],None)  
                  Hs=np.delete(Hs,[Dsys_ind],None)  
                  hs_buoy=np.delete(hs_buoy,[Dsys_ind],None)  
                  fp_buoy=np.delete(fp_buoy,[Dsys_ind],None)  
                  dir_buoy=np.delete(dir_buoy,[Dsys_ind],None)
                  del buoy_parts[Dsys_ind]																			
              if len(D)==0:
			     break
              sys_tmp=np.where(syslist != sysid)[0] 							
              if len(np.setdiff1d(sys_tmp,sys_not_merge))<len(D)-1:
                continue							
              tmp_ind=np.argmin(D)												
              if D[tmp_ind]>(Db+0.06):
                   break    																	
              sys_ind=np.where(syslist == sysid)[0]
              parts[sys_ind]=buoy_parts[tmp_ind]                      
              fplist[sys_ind]=fp_buoy[tmp_ind]
              dirlist[sys_ind]=dir_buoy[tmp_ind]                      
              hslist[sys_ind]=hs_buoy[tmp_ind] 
              syslist[sys_ind]=sysid
              thrlist[:]=thr 
              sys_ind=np.setdiff1d(sys_tmp,sys_not_merge)
             
              if len(sys_ind)!=0:
                  if len(hs_buoy)==1:
                      flag_merge[sys_ind]=1   ## the rest 0 will not exist                                  
                  else:
                      tmp_ind =np.arange(0,len(D))
                      tmp_ind=np.delete(tmp_ind,[np.argmin(D)],None)   
                      for idx in range(0,len(tmp_ind)):																						
                          fplist[sys_ind[idx]]=fp_buoy[tmp_ind[idx]]
                          dirlist[sys_ind[idx]]=dir_buoy[tmp_ind[idx]]                      
                          hslist[sys_ind[idx]]=hs_buoy[tmp_ind[idx]]   
                          parts[sys_ind[idx]]=buoy_parts[tmp_ind[idx]] 																										
                      if len(sys_ind)>len(tmp_ind):
                         flag_merge[sys_ind[(idx+1):]]=1   ## the rest 0 will not exist  
              merge=1																									
              break
												
    if merge==1:           
	    tmp_sss=np.array(sss[1])  
	    for ind in range(0,len(syslist)):
	        if (flag_merge[ind]==1)&(tmp_sss[ind]==0):
	            tmp_sss[ind+1]=0
	            continue
	        if flag_merge[ind]==0: ## the partitions have been merged       
	            if tmp_sss[ind]==0:
	                recordlist[tid]=[]
	                recordlist[tid].append({ 'time':timelist_tid[ind]\
	                         ,'datetime':timelist[ind], 'fp':fplist[ind]\
	                                 , 'hs':hslist[ind], 'dir':dirlist[ind] \
	                                     ,'parts':parts[ind], 'sysid':syslist[ind],'wind_flag':windlist[ind],'thr':thrlist[ind]})
	                
	            else:
	                recordlist[tid].append({ 'time':timelist_tid[ind]\
	                         ,'datetime':timelist[ind], 'fp':fplist[ind]\
	                                 , 'hs':hslist[ind], 'dir':dirlist[ind] \
	                                     ,'parts':parts[ind], 'sysid':syslist[ind],'wind_flag':windlist[ind],'thr':thrlist[ind]})           
	
								
def remerge(recordlist,sysid,tid,prtid,id,time,f,k,sf,sp_list,wtime,wspd,wdir):  
    sys_not_merge=[]           
    for x in range(0,len(recordlist[tid])):
       fp0=recordlist[tid][x]['fp']
       dir0=recordlist[tid][x]['dir']
       if 1./fp0<11.0:
            sys_not_merge.append(x)
            continue												
       if (recordlist[tid][x]['sysid'] == 0)&(recordlist[tid][x]['wind_flag'] !=1):
            count=0
            flag_sys=0
            for deltatid in range(-1,-7,-1):    
                if (tid-deltatid)<0:
                    break
                for prt2id in range(0,len(recordlist[tid+deltatid])):
                    if recordlist[tid+deltatid][prt2id]['sysid'] == sysid:
                        delta_fp=recordlist[tid+deltatid][prt2id]['fp'] - fp0
                        delta_dir = abs(recordlist[tid+deltatid][prt2id]['dir'] - dir0) 
                        delta_time=recordlist[tid+deltatid][prt2id]['time']-recordlist[tid][prtid]['time']
                        if delta_dir>180:
                           delta_dir = 360-delta_dir
                        if  (delta_fp < 0.006) and (delta_fp >=0.00) and (delta_dir <60)and (abs(delta_time) <= 6):
                             flag_sys=1
                             break
                        if  (delta_fp < 0.00) and (delta_fp >=-0.003) and (delta_dir <60)and (abs(delta_time) <= 6):  
                             flag_sys=1                       
                             break                        
                    if (recordlist[tid+deltatid][prt2id]['sysid'] == sysid)|(recordlist[tid+deltatid][prt2id]['wind_flag'] == 1):
                        continue               
                    if (recordlist[tid+deltatid][prt2id]['sysid'] != sysid)&(recordlist[tid+deltatid][prt2id]['sysid'] != 0):
                        delta_fp=recordlist[tid+deltatid][prt2id]['fp'] - fp0
                        delta_dir = abs(recordlist[tid+deltatid][prt2id]['dir'] - dir0) 
                        if delta_dir>180:
                           delta_dir = 360-delta_dir
                        if  (delta_fp < 0.006) and (delta_fp >=0.00) and (delta_dir <60):
                             flag_sys=1
                             sys_not_merge.append(x)
                             break
                        if  (delta_fp < 0.00) and (delta_fp >=-0.003) and (delta_dir <60):   
                             flag_sys=1
                             sys_not_merge.append(x)                             
                             break
                    if recordlist[tid+deltatid][prt2id]['sysid'] == 0:
                        delta_fp=recordlist[tid+deltatid][prt2id]['fp'] - fp0
                        delta_dir = abs(recordlist[tid+deltatid][prt2id]['dir'] - dir0) 
                        if delta_dir>180:
                           delta_dir = 360-delta_dir
                        if  (delta_fp < 0.006) and (delta_fp >=0.00) and (delta_dir <60):  
                             fp0=recordlist[tid+deltatid][prt2id]['fp']
                             dir0=recordlist[tid+deltatid][prt2id]['dir']
                             count=count+1
                             break
                        if  (delta_fp < 0.00) and (delta_fp>=-0.003) and (delta_dir <60):
                             fp0=recordlist[tid+deltatid][prt2id]['fp']
                             dir0=recordlist[tid+deltatid][prt2id]['dir']
                             count=count+1
                             break                         
                if flag_sys==1:
                    break
            if flag_sys==1:
                    continue  
            fp0=recordlist[tid][x]['fp']
            dir0=recordlist[tid][x]['dir']                
            for deltatid in range(1,7):     
                if (tid+deltatid)>=(len(recordlist)-1):
                      break    # inner deltatid                
                for prt2id in range(0,len(recordlist[tid+deltatid])):
                    if recordlist[tid+deltatid][prt2id]['sysid'] == sysid:
                        delta_fp=recordlist[tid+deltatid][prt2id]['fp'] - fp0
                        delta_dir = abs(recordlist[tid+deltatid][prt2id]['dir'] - dir0) 
                        delta_time=recordlist[tid+deltatid][prt2id]['time']-recordlist[tid][prtid]['time']
                        if delta_dir>180:
                           delta_dir = 360-delta_dir
                        if  (delta_fp < 0.006) and (delta_fp >=0.00) and (delta_dir <60)and (abs(delta_time) <= 6):
                             flag_sys=1                             
                             break
                        if  (delta_fp < 0.00) and (delta_fp >=-0.003) and (delta_dir <60)and (abs(delta_time) <= 6):  
                             flag_sys=1                     
                             break                      
                    if (recordlist[tid+deltatid][prt2id]['sysid'] == sysid)|(recordlist[tid+deltatid][prt2id]['wind_flag'] == 1):
                        continue               
                    if (recordlist[tid+deltatid][prt2id]['sysid'] != sysid)&(recordlist[tid+deltatid][prt2id]['sysid'] != 0):
                        delta_fp=recordlist[tid+deltatid][prt2id]['fp'] - fp0
                        delta_dir = abs(recordlist[tid+deltatid][prt2id]['dir'] - dir0) 
                        if delta_dir>180:
                           delta_dir = 360-delta_dir
                        if  (delta_fp < 0.006) and (delta_fp >=0.00) and (delta_dir <60):
                             flag_sys=1
                             sys_not_merge.append(x)                             
                             break
                        if  (delta_fp < 0.00) and (delta_fp >=-0.003) and (delta_dir<60):     
                             flag_sys=1
                             sys_not_merge.append(x)                             
                             break
                    if recordlist[tid+deltatid][prt2id]['sysid'] == 0:
                        delta_fp=recordlist[tid+deltatid][prt2id]['fp'] - fp0
                        delta_dir = abs(recordlist[tid+deltatid][prt2id]['dir'] - dir0) 
                        if delta_dir>180:
                           delta_dir = 360-delta_dir
                        if  (delta_fp < 0.006) and (delta_fp>=0.00) and (delta_dir<60):     
                             fp0=recordlist[tid+deltatid][prt2id]['fp']
                             dir0=recordlist[tid+deltatid][prt2id]['dir']                            
                             count=count+1
                             break
                        if  (delta_fp < 0.00) and (delta_fp>=-0.003) and (delta_dir<60):
                             fp0=recordlist[tid+deltatid][prt2id]['fp']
                             dir0=recordlist[tid+deltatid][prt2id]['dir']
                             count=count+1
                             break                         
                if flag_sys==1:
                    break
            if flag_sys==1:
                   continue              
            if count>5:
               sys_not_merge.append(x)    
       if ((recordlist[tid][x]['sysid'] !=0)&(recordlist[tid][x]['sysid'] !=sysid))|(recordlist[tid][x]['wind_flag'] ==1):
           sys_not_merge.append(x)
    if (len(sys_not_merge)+1)<len(recordlist[tid]):
        repartition(recordlist,tid,prtid,sysid,sys_not_merge,id,time,f,k,sf,sp_list,wtime,wspd,wdir)
   
def track_buoy_grow_consistent(recordlist,sysid,id,time,f,k,sf,sp_list,wtime,wspd,wdir,meanlen=1):
    
    sss = zip(*[(recordlist[tid][prtid],tid,prtid) for tid in range(0,len(recordlist)) for prtid in range(0,len(recordlist[tid]))])
    bsyslist = np.array([sss[0][x]['sysid'] for x in range(0,len(sss[0]))])  
    
    for  sys1id in range(1,sysid):
        sys_ind=np.where(bsyslist==sys1id)[0]
        startid=sss[1][sys_ind[0]]
        endtid=sss[1][sys_ind[-1]]
        tid=startid        
        deltatid = 0  
        groupe_fp=[];groupe_dir=[];groupe_fp=[];groupe_dir=[]              
        while deltatid < 6:
           if (tid+deltatid)>=(len(recordlist)-1):
                  break    # inner deltatid
           delta_fp=[]; delta_dir=[];delta_time=[];delta_dist=[]
           empty_flag=1
           for prt2id in range(0,len(recordlist[tid+deltatid])):
                if recordlist[tid+deltatid][prt2id]['sysid'] ==sys1id:                        
                    tid=tid+deltatid; prtid=prt2id; deltatid=1                        
                    empty_flag=0
#                    other sysid 0
                    for x in range(0,len(recordlist[tid])):
                       if (recordlist[tid][x]['sysid'] == 0)&(recordlist[tid][x]['wind_flag'] !=1):
                           remerge(recordlist,sys1id,tid,prtid,id,time,f,k,sf,sp_list,wtime,wspd,wdir)
                           for y in range(0,len(recordlist[tid])):
                               if recordlist[tid][y]['sysid'] ==sys1id:                                  
                                   prtid=y
                           break
                    groupe_fp.append(recordlist[tid][prtid]['fp'])
                    groupe_dir.append(recordlist[tid][prtid]['dir']) 
                    break
                else:       
                    if len(groupe_fp)==0:
                        continue
                    if (recordlist[tid+deltatid][prt2id]['sysid'] != 0)|(recordlist[tid+deltatid][prt2id]['wind_flag'] ==1):
                        delta_fp.append(99);delta_dir.append(99);delta_dist.append(99);delta_time.append(99)
                        continue
                    if len(groupe_fp)>meanlen:
                        mfp=sum(groupe_fp[-meanlen:])/meanlen
                        mdir=sum(groupe_dir[-meanlen:])/meanlen
                    else:
                        mfp=sum(groupe_fp)/len(groupe_fp)
                        mdir=sum(groupe_dir)/len(groupe_dir)                     
                    delta_fp.append(recordlist[tid+deltatid][prt2id]['fp'] - mfp)
                    delta_time.append(recordlist[tid+deltatid][prt2id]['time']-recordlist[tid][prtid]['time'])
                    tmp_delta_dir = abs(recordlist[tid+deltatid][prt2id]['dir'] - mdir)                          
                    if tmp_delta_dir>180:
                        tmp_delta_dir = 360-tmp_delta_dir
                    delta_dir.append(tmp_delta_dir)
                    delta_dist.append((np.log(recordlist[tid+deltatid][prt2id]['fp'])-np.log(mfp))**2*9\
                                                + ((delta_dir[-1])/180.0)**2)
           if empty_flag==1:
                nextprtid = np.argmin(delta_dist) 
                if  (delta_fp[nextprtid] < 0.006) and (delta_fp[nextprtid] >=0.00) and (delta_dir[nextprtid] <60)\
                                and (abs(delta_time[nextprtid]) <= 6):  
                    recordlist[tid+deltatid][nextprtid]['sysid'] = recordlist[tid][prtid]['sysid'] 
                    groupe_fp.append(recordlist[tid+deltatid][nextprtid]['fp']);groupe_dir.append(recordlist[tid+deltatid][nextprtid]['dir'])                                                 
                    tid = tid+deltatid ; prtid = nextprtid ; deltatid = 1
                    for x in range(0,len(recordlist[tid])):
                       if (recordlist[tid][x]['sysid'] == 0)&(recordlist[tid][x]['wind_flag'] !=1):
                           remerge(recordlist,sys1id,tid,prtid,id,time,f,k,sf,sp_list,wtime,wspd,wdir)
                           for y in range(0,len(recordlist[tid])):
                               if recordlist[tid][y]['sysid'] ==sys1id:                                  
                                   prtid=y
                           break
                    continue
                
                elif (delta_fp[nextprtid] < 0.00) and (delta_fp[nextprtid] >-0.003) and (delta_dir[nextprtid] <60)\
                                and (abs(delta_time[nextprtid]) <= 6):                
                    recordlist[tid+deltatid][nextprtid]['sysid'] = recordlist[tid][prtid]['sysid']
                    groupe_fp.append(recordlist[tid+deltatid][nextprtid]['fp']);groupe_dir.append(recordlist[tid+deltatid][nextprtid]['dir'])                                                                   
                    tid = tid+deltatid ; prtid = nextprtid ; deltatid = 1
                    for x in range(0,len(recordlist[tid])):
                       if (recordlist[tid][x]['sysid'] == 0)&(recordlist[tid][x]['wind_flag'] !=1):
                           remerge(recordlist,sys1id,tid,prtid,id,time,f,k,sf,sp_list,wtime,wspd,wdir)
                           for y in range(0,len(recordlist[tid])):
                               if recordlist[tid][y]['sysid'] ==sys1id:                                  
                                   prtid=y
                           break                   
                else:
                    deltatid = deltatid + 1
    print 'backward'
    sss = zip(*[(recordlist[tid][prtid],tid,prtid) for tid in range(0,len(recordlist)) for prtid in range(0,len(recordlist[tid]))])
    bsyslist = np.array([sss[0][x]['sysid'] for x in range(0,len(sss[0]))])  
    
    for  sys1id in range(1,sysid):
        sys_ind=np.where(bsyslist==sys1id)[0]
        startid=sss[1][sys_ind[0]]
        endtid=sss[1][sys_ind[-1]]                   
        tid=endtid        
        deltatid = 0  
        groupe_fp=[];groupe_dir=[];groupe_fp=[];groupe_dir=[]              
        while deltatid < 6:
           if  (tid-deltatid)<0:
                  break    # inner deltatid
           delta_fp=[]; delta_dir=[];delta_time=[];delta_dist=[]
           empty_flag=1
           for prt2id in range(0,len(recordlist[tid-deltatid])):
                if recordlist[tid-deltatid][prt2id]['sysid'] ==sys1id:                        
                    tid=tid-deltatid; prtid=prt2id; deltatid=1
                    groupe_fp.append(recordlist[tid][prtid]['fp'])
                    groupe_dir.append(recordlist[tid][prtid]['dir'])                         
                    empty_flag=0
#                    other sysid 0
                    for x in range(0,len(recordlist[tid])):
                       if (recordlist[tid][x]['sysid'] == 0)&(recordlist[tid][x]['wind_flag'] !=1):
                           remerge(recordlist,sys1id,tid,prtid,id,time,f,k,sf,sp_list,wtime,wspd,wdir)
                           for y in range(0,len(recordlist[tid])):
                               if recordlist[tid][y]['sysid'] ==sys1id:                                  
                                   prtid=y
                           break
                    break
                else:    
                    if len(groupe_fp)==0:
                        continue                      
                    if (recordlist[tid-deltatid][prt2id]['sysid'] != 0)|(recordlist[tid-deltatid][prt2id]['wind_flag'] ==1):
                        delta_fp.append(99);delta_dir.append(99);delta_dist.append(99);delta_time.append(99)
                        continue
                    if len(groupe_fp)>meanlen:
                        mfp=sum(groupe_fp[-meanlen:])/meanlen
                        mdir=sum(groupe_dir[-meanlen:])/meanlen
                    else:
                        mfp=sum(groupe_fp)/len(groupe_fp)
                        mdir=sum(groupe_dir)/len(groupe_dir)                     
                    delta_fp.append(recordlist[tid-deltatid][prt2id]['fp'] - mfp)
                    delta_time.append(recordlist[tid-deltatid][prt2id]['time']-recordlist[tid][prtid]['time'])
                    tmp_delta_dir = abs(recordlist[tid-deltatid][prt2id]['dir'] - mdir)                          
                    if tmp_delta_dir>180:
                        tmp_delta_dir = 360-tmp_delta_dir
                    delta_dir.append(tmp_delta_dir)
                    delta_dist.append((np.log(recordlist[tid-deltatid][prt2id]['fp'])-np.log(mfp))**2*9\
                                                + ((delta_dir[-1])/180.0)**2)
           if empty_flag==1:         
                nextprtid = np.argmin(delta_dist) 
                if  (delta_fp[nextprtid] < 0.003) and (delta_fp[nextprtid] >=0.00) and (delta_dir[nextprtid] <60)\
                                and (abs(delta_time[nextprtid]) <= 6):  
                    recordlist[tid-deltatid][nextprtid]['sysid'] = recordlist[tid][prtid]['sysid'] 
                    groupe_fp.append(recordlist[tid-deltatid][nextprtid]['fp']);groupe_dir.append(recordlist[tid-deltatid][nextprtid]['dir'])                                                 
                    tid = tid-deltatid ; prtid = nextprtid ; deltatid = 1
                    for x in range(0,len(recordlist[tid])):
                       if (recordlist[tid][x]['sysid'] == 0)&(recordlist[tid][x]['wind_flag'] !=1):
                           remerge(recordlist,sys1id,tid,prtid,id,time,f,k,sf,sp_list,wtime,wspd,wdir)
                           for y in range(0,len(recordlist[tid])):
                               if recordlist[tid][y]['sysid'] ==sys1id:                                  
                                   prtid=y
                           break              
                    continue
                
                elif (delta_fp[nextprtid] < 0.00) and (delta_fp[nextprtid] >-0.006) and (delta_dir[nextprtid] <60)\
                                and (abs(delta_time[nextprtid]) <= 6):                
                    recordlist[tid-deltatid][nextprtid]['sysid'] = recordlist[tid][prtid]['sysid']
                    groupe_fp.append(recordlist[tid-deltatid][nextprtid]['fp']);groupe_dir.append(recordlist[tid-deltatid][nextprtid]['dir'])                                                                   
                    tid = tid-deltatid ; prtid = nextprtid ; deltatid = 1
                    for x in range(0,len(recordlist[tid])):
                       if (recordlist[tid][x]['sysid'] == 0)&(recordlist[tid][x]['wind_flag'] !=1):
                           remerge(recordlist,sys1id,tid,prtid,id,time,f,k,sf,sp_list,wtime,wspd,wdir)
                           for y in range(0,len(recordlist[tid])):
                               if recordlist[tid][y]['sysid'] ==sys1id:                                  
                                   prtid=y
                           break               
                else:
                    deltatid = deltatid + 1                   
    return recordlist,sysid      
def track_criteria(recordlist,delta_fp_max=0.03,delta_fp_min=-0.01,delta_dir_max=30,sysid=1):
    meanlen=4    
    time_interval_max=6    
    ###Assignment
    starttid = 0; tid = 0
    while tid < len(recordlist):
        tid = starttid ; prtid = 0 ;  deltatid = 1 ;
        mark_time_viewed = 1  
        ####finding a initial searching point and define as a new system
        while prtid < len(recordlist[tid]):
            if (recordlist[tid][prtid]['sysid'] != 0)|(recordlist[tid][prtid]['wind_flag'] == 1)\
                 |(1.0/recordlist[tid][prtid]['fp']<12):    #partition been assigned
                prtid = prtid + 1 
                continue
            mark_time_viewed = 0                   #there are still partition useful left
            recordlist[tid][prtid]['sysid'] = sysid
            break  
            
        if mark_time_viewed == 1:             # no non-assigned parttion in this time
            starttid = starttid + 1
            if starttid == len(recordlist):
                break
            continue
        #####end of initial

        ###Begin of assignment
        groupe_fp=[];groupe_dir=[];groupe_time=[]
        groupe_fp.append(recordlist[tid][prtid]['fp'])
        groupe_dir.append(recordlist[tid][prtid]['dir'])
        groupe_time.append(recordlist[tid][prtid]['time'])
        while deltatid < time_interval_max:
            if (tid+deltatid)>=len(recordlist):
                break    # inner deltatid
            delta_fp=[]; delta_dir=[];delta_time=[];delta_dist=[]
            for prt2id in range(0,len(recordlist[tid+deltatid])):
                if (recordlist[tid+deltatid][prt2id]['sysid'] != 0)|(recordlist[tid+deltatid][prt2id]['wind_flag'] == 1):
                    delta_fp.append(99);delta_dir.append(99);delta_dist.append(99);delta_time.append(99)
                    continue               
                    
                delta_fp.append(recordlist[tid+deltatid][prt2id]['fp'] - recordlist[tid][prtid]['fp'] )
                delta_time.append(recordlist[tid+deltatid][prt2id]['time']-recordlist[tid][prtid]['time'])
                tmp_delta_dir = abs(recordlist[tid+deltatid][prt2id]['dir'] - recordlist[tid][prtid]['dir']) 
                if tmp_delta_dir>180:
                    tmp_delta_dir = 360-tmp_delta_dir
                delta_dir.append(tmp_delta_dir)
                delta_dist.append((np.log(recordlist[tid+deltatid][prt2id]['fp'])-np.log(recordlist[tid][prtid]['fp']))**2*9\
                                            + ((delta_dir[-1])/180.0)**2)
            nextprtid = np.argmin(delta_dist)  
            matches_ind=np.array([x for x in range(0,len(groupe_time)) if recordlist[tid+deltatid][nextprtid]['time']-groupe_time[x]<meanlen],dtype='int')
            tmp_group_fp=np.array(groupe_fp)
            mfp=np.mean(tmp_group_fp[matches_ind])
            if (recordlist[tid+deltatid][nextprtid]['fp']-mfp>0.006)or (recordlist[tid+deltatid][nextprtid]['fp']-mfp<-0.003):
                deltatid = deltatid + 1
                continue
                
            if  (delta_fp[nextprtid] < delta_fp_max) and (delta_fp[nextprtid] >=0.00) and (delta_dir[nextprtid] <delta_dir_max)\
                            and (abs(delta_time[nextprtid]) <= time_interval_max):     
                recordlist[tid+deltatid][nextprtid]['sysid'] = recordlist[tid][prtid]['sysid']  
                groupe_fp.append(recordlist[tid+deltatid][nextprtid]['fp']);groupe_dir.append(recordlist[tid+deltatid][nextprtid]['dir'])
                groupe_time.append(recordlist[tid+deltatid][nextprtid]['time'])
                tid = tid+deltatid ; prtid = nextprtid ; deltatid = 1
            
            elif (delta_fp[nextprtid] < 0.00) and (delta_fp[nextprtid] >delta_fp_min) and (delta_dir[nextprtid] <delta_dir_max)\
                            and (abs(delta_time[nextprtid]) <= time_interval_max):                
                recordlist[tid+deltatid][nextprtid]['sysid'] = recordlist[tid][prtid]['sysid']        
                groupe_fp.append(recordlist[tid+deltatid][nextprtid]['fp']);groupe_dir.append(recordlist[tid+deltatid][nextprtid]['dir'])      
                groupe_time.append(recordlist[tid+deltatid][nextprtid]['time'])
                tid = tid+deltatid ; prtid = nextprtid ; deltatid = 1
            else:
                deltatid = deltatid + 1

        sysid = sysid + 1
    return recordlist,sysid   
def track_buoy_grow_forward(recordlist,sysid,meanlen=6):
     for  sys1id in range(1,sysid):
            ####finding a initial searching point 
            startid=0
            for tid in range(0,len(recordlist)):    
                for prtid in range(0,len(recordlist[tid])):
                    if recordlist[tid][prtid]['sysid']== sys1id: 
                        startid=1
                        break
                if startid==1:
                    break
            
            deltatid = 1 
            groupe_fp=[];groupe_dir=[]
            groupe_fp.append(recordlist[tid][prtid]['fp'])
            groupe_dir.append(recordlist[tid][prtid]['dir'])                  
            while deltatid < 6:
               if (tid+deltatid)>=(len(recordlist)-1):
                      break    # inner deltatid
               delta_fp=[]; delta_dir=[];delta_time=[];delta_dist=[]
               empty_flag=1
               for prt2id in range(0,len(recordlist[tid+deltatid])):
                    if recordlist[tid+deltatid][prt2id]['sysid'] ==sys1id:                        
                        tid=tid+deltatid; prtid=prt2id; deltatid=1
                        groupe_fp.append(recordlist[tid][prtid]['fp'])
                        groupe_dir.append(recordlist[tid][prtid]['dir'])                         
                        empty_flag=0
                        break
                    else:                     
                        if (recordlist[tid+deltatid][prt2id]['sysid'] != 0)|(recordlist[tid+deltatid][prt2id]['wind_flag'] ==1):
                            delta_fp.append(99);delta_dir.append(99);delta_dist.append(99);delta_time.append(99)
                            continue
                        if len(groupe_fp)>meanlen:
                            mfp=sum(groupe_fp[-meanlen:])/meanlen
                            mdir=sum(groupe_dir[-meanlen:])/meanlen
                        else:
                            mfp=sum(groupe_fp)/len(groupe_fp)
                            mdir=sum(groupe_dir)/len(groupe_dir)                     
                        delta_fp.append(recordlist[tid+deltatid][prt2id]['fp'] - mfp)
                        delta_time.append(recordlist[tid+deltatid][prt2id]['time']-recordlist[tid][prtid]['time'])
                        tmp_delta_dir = abs(recordlist[tid+deltatid][prt2id]['dir'] - mdir)                          
                        if tmp_delta_dir>180:
                            tmp_delta_dir = 360-tmp_delta_dir
                        delta_dir.append(tmp_delta_dir)
                        delta_dist.append((np.log(recordlist[tid+deltatid][prt2id]['fp'])-np.log(mfp))**2*9\
                                                    + ((delta_dir[-1])/180.0)**2)
               if empty_flag==1:
                    nextprtid = np.argmin(delta_dist) 
                    if  (delta_fp[nextprtid] < 0.006) and (delta_fp[nextprtid] >=0.00) and (delta_dir[nextprtid] <60)\
                                    and (abs(delta_time[nextprtid]) <= 6):  
                        recordlist[tid+deltatid][nextprtid]['sysid'] = recordlist[tid][prtid]['sysid'] 
                        groupe_fp.append(recordlist[tid+deltatid][nextprtid]['fp']);groupe_dir.append(recordlist[tid+deltatid][nextprtid]['dir'])                                                 
                        tid = tid+deltatid ; prtid = nextprtid ; deltatid = 1
                        continue
                    
                    elif (delta_fp[nextprtid] < 0.00) and (delta_fp[nextprtid] >-0.003) and (delta_dir[nextprtid] <60)\
                                    and (abs(delta_time[nextprtid]) <= 6):                
                        recordlist[tid+deltatid][nextprtid]['sysid'] = recordlist[tid][prtid]['sysid']
                        groupe_fp.append(recordlist[tid+deltatid][nextprtid]['fp']);groupe_dir.append(recordlist[tid+deltatid][nextprtid]['dir'])                                                                   
                        tid = tid+deltatid ; prtid = nextprtid ; deltatid = 1
                    else:
                        deltatid = deltatid + 1
     return recordlist,sysid                            
def track_buoy_grow_backward(recordlist,sysid,meanlen=6):
     for  sys1id in range(1,sysid):
            ####finding a initial searching point 
            startid=0
            for tid in np.arange(len(recordlist)-1,-1,-1):    
                for prtid in np.arange(len(recordlist[tid])-1,-1,-1):   
                    if recordlist[tid][prtid]['sysid']== sys1id: 
                        startid=1
                        break
                if startid==1:
                    break
            
            deltatid = 1 
            groupe_fp=[];groupe_dir=[]
            groupe_fp.append(recordlist[tid][prtid]['fp'])
            groupe_dir.append(recordlist[tid][prtid]['dir'])            
            while deltatid < 6:
               if (tid-deltatid)<0:
                      break    # inner deltatid
               delta_fp=[]; delta_dir=[]; delta_time=[];delta_dist=[]
               empty_flag=1
               for prt2id in range(0,len(recordlist[tid-deltatid])):
                    if recordlist[tid-deltatid][prt2id]['sysid'] ==sys1id:                        
                        tid=tid-deltatid; prtid=prt2id; deltatid=1
                        groupe_fp.append(recordlist[tid][prtid]['fp'])
                        groupe_dir.append(recordlist[tid][prtid]['dir'])                         
                        empty_flag=0
                        break
                    else:                     
                        if (recordlist[tid-deltatid][prt2id]['sysid'] != 0)|(recordlist[tid-deltatid][prt2id]['wind_flag']==1):
                            delta_fp.append(99);delta_dir.append(99);delta_dist.append(99);delta_time.append(99)
                            continue
                        if len(groupe_fp)>meanlen:
                            mfp=sum(groupe_fp[-meanlen:])/meanlen
                            mdir=sum(groupe_dir[-meanlen:])/meanlen
                        else:
                            mfp=sum(groupe_fp)/len(groupe_fp)
                            mdir=sum(groupe_dir)/len(groupe_dir)                     
                        delta_fp.append(recordlist[tid-deltatid][prt2id]['fp'] - mfp)
                        delta_time.append(recordlist[tid-deltatid][prt2id]['time']-recordlist[tid][prtid]['time'])
                        tmp_delta_dir = abs(recordlist[tid-deltatid][prt2id]['dir'] - mdir)                          
                        if tmp_delta_dir>180:
                            tmp_delta_dir = 360-tmp_delta_dir
                        delta_dir.append(tmp_delta_dir)
                        delta_dist.append((np.log(recordlist[tid-deltatid][prt2id]['fp'])-np.log(mfp))**2*9\
                                                    + ((delta_dir[-1])/180.0)**2)
               if empty_flag==1:               
                    nextprtid = np.argmin(delta_dist)                 
                    if  (delta_fp[nextprtid] < 0.003) and (delta_fp[nextprtid] >=0.00) and (delta_dir[nextprtid] <45)\
                                    and (abs(delta_time[nextprtid]) <= 6):  
                        recordlist[tid-deltatid][nextprtid]['sysid'] = recordlist[tid][prtid]['sysid']    
                        groupe_fp.append(recordlist[tid-deltatid][nextprtid]['fp']);groupe_dir.append(recordlist[tid-deltatid][nextprtid]['dir'])                         
                        tid = tid-deltatid ; prtid = nextprtid ; deltatid = 1
                        continue
                    
                    elif (delta_fp[nextprtid] < 0.00) and (delta_fp[nextprtid] >-0.006) and (delta_dir[nextprtid] <45)\
                                    and (abs(delta_time[nextprtid]) <= 6):                
                        recordlist[tid-deltatid][nextprtid]['sysid'] = recordlist[tid][prtid]['sysid']  
                        groupe_fp.append(recordlist[tid-deltatid][nextprtid]['fp']);groupe_dir.append(recordlist[tid-deltatid][nextprtid]['dir'])                                                 
                        tid = tid-deltatid ; prtid = nextprtid ; deltatid = 1
                    else:
                        deltatid = deltatid + 1   
     return recordlist,sysid            
def plot_middle(recordlist,fig_name,id,time,f,k,sf,sp_list,Method): 
    mytitle=Method
    mcolor=['b','m','r','w','g','c','y']     
    sss = zip(*[(recordlist[tid][prtid],tid,prtid) for tid in range(0,len(recordlist)) for prtid in range(0,len(recordlist[tid]))])
    btimelist = np.array([sss[0][x]['datetime'] for x in range(0,len(sss[0]))])
    bfplist =np.array([sss[0][x]['fp'] for x in range(0,len(sss[0]))])
    bhslist = np.array([sss[0][x]['hs'] for x in range(0,len(sss[0]))])
    bdirlist = np.array([sss[0][x]['dir'] for x in range(0,len(sss[0]))])
    bsyslist= np.array([sss[0][x]['sysid'] for x in range(0,len(sss[0]))])
    if Method=='G':
       #dir_fig='//home/cercache/users/xwang/SAR_validation/pic_swell_G/'+id+'/'
       dir_fig='/home/cercache/users/mlebars/SAR_validation/pic_swell_G/'+id+'/'
    if Method=='G3':
       #dir_fig='//home/cercache/users/xwang/SAR_validation/pic_swell_G3/'+id+'/'
       dir_fig='/home/cercache/users/mlebars/SAR_validation/pic_swell_G3/'+id+'/'
    split=True
    if split is None:                         
        fig1 = plt.figure(figsize=(16,9));
        ax11 = fig1.add_axes([0.05, 0.8, 0.9, 0.15]) 
        ax12 = fig1.add_axes([0.05, 0.6, 0.9, 0.15])
        ax13 = fig1.add_axes([0.05, 0.4, 0.9, 0.15])
        ax14 = fig1.add_axes([0.05, 0.2, 0.9, 0.15]) 
        
        outid = 0
        for sysid in range(1,max(bsyslist)+1):
            if len(np.where(bsyslist == sysid)[0])<5:
                 continue           
            outid = outid + 1            
            pos = np.where(bsyslist == sysid)[0]
            timelist = btimelist[pos]
            fplist = bfplist[pos]
            dirlist = bdirlist[pos]
            hslist = bhslist[pos]
            wllist=9.81/2/np.pi/(fplist**2)  
            
            if np.max(dirlist)-np.min(dirlist)>270:
                if len(dirlist[dirlist>180])>(len(dirlist)/2):
                    dirlist[dirlist<180]=dirlist[dirlist<180]+360
                else:
                    dirlist[dirlist>180]=dirlist[dirlist>180]-360
                
            plot_Partition(np.min(btimelist),np.max(btimelist),time,f,k,sf,sp_list,timelist,hslist,wllist,dirlist,mcolor[(outid-1)%7],fig1,ax11,ax12,ax13,ax14,outid-1,mytitle,myTick='day')
          
        plt.savefig(dir_fig+datetime.strftime(btimelist[0],'%Y-%m-%d %H:%M:%S')+fig_name) 
        plt.close(fig1)   
    if split is True:
        start_time=np.min(btimelist)-timedelta(30.)
        Flag=0
        while Flag==0:
            start_time=start_time+timedelta(30.)
            end_time=start_time+timedelta(30.)
            if np.max(btimelist)<=end_time+timedelta(30.):
                Flag=1
                end_time=np.max(btimelist)
                  
            fig1 = plt.figure(figsize=(16,9));
            ax11 = fig1.add_axes([0.05, 0.8, 0.9, 0.15]) 
            ax12 = fig1.add_axes([0.05, 0.6, 0.9, 0.15])
            ax13 = fig1.add_axes([0.05, 0.4, 0.9, 0.15])
            ax14 = fig1.add_axes([0.05, 0.2, 0.9, 0.15]) 
            
            outid = 0
            for sysid in range(1,max(bsyslist)+1):
                if len(np.where(bsyslist == sysid)[0])<6:
                     continue                   
                outid = outid + 1
                pos = np.where(bsyslist == sysid)[0]
                timelist = btimelist[pos]
                fplist = bfplist[pos]
                dirlist = bdirlist[pos]
                hslist = bhslist[pos]
                wllist=9.81/2/np.pi/(fplist**2)  
                
                if np.max(dirlist)-np.min(dirlist)>270:
                    if len(dirlist[dirlist>180])>(len(dirlist)/2):
                        dirlist[dirlist<180]=dirlist[dirlist<180]+360
                    else:
                        dirlist[dirlist>180]=dirlist[dirlist>180]-360
                    
                plot_Partition(start_time,end_time,time,f,k,sf,sp_list,timelist,hslist,wllist,dirlist,mcolor[(outid-1)%7],fig1,ax11,ax12,ax13,ax14,outid-1,mytitle,myTick='day')
            
            start_date_string = datetime.strftime(start_time,'%Y%m%dT%H%M%S')
            end_date_string   = datetime.strftime(end_time,'%Y%m%dT%H%M%S')
            #Modifs MLB  (ajout verif creation folder)
            if not os.path.exists(dir_fig):
                os.makedirs(dir_fig)
            plt.savefig(dir_fig+id+'from_' + start_date_string + '_to_' + end_date_string +'_'+fig_name) 
            

            plt.close(fig1)             
            
def plot_buoy_partitions_cluster( btimelist, bfplist, bhslist, bdirlist, bsyslist,bwindlist,time,f,k,sf,sp_list,mcolor,dir_fig,mytitle,wtime,wspd,wdir):
       
    fig1 = plt.figure(figsize=(16,9));
    ax11 = fig1.add_axes([0.05, 0.8, 0.9, 0.15]) 
    ax12 = fig1.add_axes([0.05, 0.6, 0.9, 0.15])
    ax13 = fig1.add_axes([0.05, 0.4, 0.9, 0.15])
    ax14 = fig1.add_axes([0.05, 0.2, 0.9, 0.15])   
    
    outid = 0
    for sysid in range(1,max(bsyslist)+1):
        if len(np.where(bsyslist == sysid)[0])<6:
             continue
        outid = outid + 1
        pos = np.where(bsyslist == sysid)[0]
        timelist = btimelist[pos]
        fplist = bfplist[pos]
        dirlist = bdirlist[pos]
        hslist = bhslist[pos]
        wllist=9.81/2/np.pi/(fplist**2)  
        
        if np.max(dirlist)-np.min(dirlist)>270:
            if len(dirlist[dirlist>180])>(len(dirlist)/2):
                dirlist[dirlist<180]=dirlist[dirlist<180]+360
            else:
                dirlist[dirlist>180]=dirlist[dirlist>180]-360
            
        plot_Partition(np.min(btimelist),np.max(btimelist),time,f,k,sf,sp_list,timelist,hslist,wllist,dirlist,mcolor[(outid-1)%7],fig1,ax11,ax12,ax13,ax14,outid-1,mytitle,myTick='month')
    
    
    mcolor=['k']         
    dirdeg=[]
    T=[]
    hs=[]
    time=[]
    for tid in range(0,len(btimelist)):
        if (bsyslist[tid]==0)&(bwindlist[tid]!=1):
            if bdirlist[tid]>180:
              dirdeg.append(bdirlist[tid]-360)
            else:
              dirdeg.append(bdirlist[tid])  
            T.append(1.0/bfplist[tid])  
            hs.append(bhslist[tid])   
            time.append(btimelist[tid])
                       
    ax12.scatter(time,T,c=mcolor)     
    ax13.scatter(time,hs,c=mcolor)    
    ax14.scatter(time,dirdeg,c=mcolor)  
     
    plt.savefig(dir_fig+'buoy_tracked') 
    plt.close(fig1)


def  create_buoy_nc( btimelist, bfplist, bhslist, bdirlist, bsyslist,bwindlist,thr,partlist,wtime,wspd,wdir,id,lon,lat,dir_output):

    from netCDF4 import Dataset

    if os.path.isdir(dir_output)==False:
            command_line = 'mkdir ' + dir_output
            os.system(command_line)           

    filename=dir_output+'BuoySwell'+id+'from'+ datetime.strftime(np.min(btimelist),'%Y_%m_%d_%H')+'to'+datetime.strftime(np.max(btimelist),'%Y_%m_%d_%H')+'.nc' 
       
    nc = Dataset(filename, 'w', format='NETCDF4')
    #Global attributes        
    nc.title='BuoySwell Partition product(storm)'
    nc.sourceProduct='NDBC'
    nc.processingStartTime=datetime.strftime(np.min(btimelist),'%Y/%m/%d %H:%M:%S')
    nc.processingEndTime=datetime.strftime(np.max(btimelist),'%Y/%m/%d %H:%M:%S')
    nc.processingTime=datetime.strftime(datetime.now(),'%Y/%m/%d %H:%M:%S')
    nc.station_lon=lon
    nc.station_lat=lat
    
    # Create dimensions
    nc.createDimension('time',len(btimelist))
    nc.createDimension('k_buoy',partlist[0].shape[0]);nc.createDimension('phi_buoy',partlist[0].shape[1])
    # Create variables
    fp=nc.createVariable('fp', 'f4',('time') ) 
    dirdeg=nc.createVariable('dirdeg', 'f4', ('time') ) 
    hs=nc.createVariable('hs', 'f4', ('time') )   
    sysid=nc.createVariable('sysid', 'i4', ('time') ) 
    windsea_flag=nc.createVariable('windsea_flag', 'i4', ('time') ) 
    threshold=nc.createVariable('thr', 'i4', ('time') ) 
    year=nc.createVariable('year', 'i4', ('time') )
    month=nc.createVariable('month', 'i4', ('time') )
    day=nc.createVariable('day', 'i4', ('time') )
    hour=nc.createVariable('hour', 'i4', ('time') )  
    
    parts_buoy=nc.createVariable('parts_buoy', 'f4', ('time','k_buoy','phi_buoy') )    
    
    # Variables 
    nc.variables['fp'].units='Hz'
    nc.variables['fp'].long_name='frequency'
    
    nc.variables['dirdeg'].units='deg'
    nc.variables['dirdeg'].long_name='wave direction '
    
    nc.variables['hs'].units='m'
    nc.variables['hs'].long_name='wave height'
    
    # flag
    nc.variables['sysid'].description='identify partitions belong to different storms.0 means partition belongs to no storm'
    nc.variables['windsea_flag'].description='-1 no wind information. 0 swell. 1 windseas'    
    nc.variables['thr'].description='merge_thr,noise_level=10'
     
    year[:]=np.array([int(t.year) for t in btimelist])
    month[:]=np.array([int(t.month) for t in btimelist])
    day[:]=np.array([int(t.day) for t in btimelist])
    hour[:]=np.array([int(t.hour) for t in btimelist])
    fp[:]=bfplist
    dirdeg[:]=bdirlist
    hs[:]=bhslist
    sysid[:]=bsyslist
    windsea_flag[:]=bwindlist
    threshold[:]=thr
    parts_buoy[:]=partlist
    nc.close()
def integral(sp,k_grid,phi_grid,dk_grid,k,phi):
    spec=sp*dk_grid*k_grid*(phi[1]-phi[0])/180*np.pi
    hsmax=4.0*np.sqrt(np.sum(spec))    
    myspec=sp*k_grid*(phi[1]-phi[0])/180*np.pi				
    Sk=[]
    for tmp_k in k:
       if len(myspec[k_grid==tmp_k])==0:
			Sk.append(0)
       else:
			Sk.append(sum(myspec[k_grid==tmp_k]))
    if max(Sk)<=0:
		kmax=None
		Lmax=None
    else:
       kmax=k[Sk.index(max(Sk))]
       Lmax = 2*np.pi/kmax									
    myspec=sp*dk_grid*k_grid			
    Sphi=[]
    for tmp_phi in phi:
       if len(myspec[phi_grid==tmp_phi])==0:
			Sphi.append(0)
       else:
			Sphi.append(sum(myspec[phi_grid==tmp_phi]))
    if max(Sphi)<=0:
		phimax=None
    else:		
       phimax=phi[Sphi.index(max(Sphi))]      					
    return Lmax,phimax,hsmax
def interpolate_as_sar(phi,k,sp,phi_sar,k_sar):
     nphi = sp.shape[0]
     nphih = nphi / 2    
     sp= np.vstack((sp[-nphih:, :], sp, sp[0:nphih, :]))
     phi = np.hstack((phi[-nphih:] - 360, phi, phi[0:nphih] + 360))    
     wavelength=2*np.pi/k
     wavelength_sar=2*np.pi/k_sar
     xgrid=np.log(wavelength/wavelength[-1]) 
     xgrid_sar=np.log(wavelength_sar/wavelength[-1])     # new axis
     grid_x, grid_y = np.meshgrid(xgrid_sar, phi_sar)
     x,y = np.meshgrid(xgrid, phi)
     from scipy.interpolate import griddata
     grid_z= griddata((x.flatten(),y.flatten()), sp.flatten(),(grid_x, grid_y),method='linear')
     grid_z=np.nan_to_num(grid_z)
     return grid_z                               								
def buoy_partition(id,time,f,k,sf,sp, thr,Db,HsD,tmp_dir,tmp_hs,tmp_fp,subtitle):
    ocn_file = '/home/cercache/project/mpc-sentinel1/data/esa/sentinel-1a/L2/WV/S1A_WV_OCN__2S/2017/005/S1A_WV_OCN__2SSV_20170105T200855_20170105T201405_014701_017EB9_6E58.SAFE/measurement/s1a-wv1-ocn-vv-20170105t201249-20170105t201252-014701-017eb9-017.nc'
    ocn_param = get_oswl2ocn_param(ocn_file,get_spec=True)
    ocn_k   = ocn_param['oswk']
    ocn_phi = ocn_param['oswphi']#*np.pi/180.
    #Modif MLB
    #phi = np.arange(0,360,5)			
    phi = np.arange(0,360,10)
    sp=sp.transpose()				
    sp=interpolate_as_sar(phi,k,sp,ocn_phi,ocn_k) 				
    phi=ocn_phi;k=ocn_k
    dim = (phi.size, k.size)
    k_grid = np.tile(k, (dim[0], 1))
    phi_grid = np.tile(phi, (dim[1], 1)).transpose()
    dk=[]
    dk.append(k[1]-k[0])
    for ind in range(len(k)-2):
       dk.append(1.0/2*(k[ind+2]-k[ind]))
    dk.append(k[-1]-k[-2])
    dk_grid = np.tile(dk, (dim[0], 1))   					
    from wave_spectrum_plot import *
    hs_buoy = []; wl_buoy = []; dir_buoy = [];parts_buoy=[]
    noise_level=10  
    try:
        labels = partition_spectrum(sp,
                                    k, phi, 
                                    twod=True, 
                                    klow=None, khigh=None,
                                    #noise_level=50., # WV1 !
                                    noise_level=noise_level,  #changed
                                    noise_thr=1., # relative to noise_levelHs
                                    merge_thr1=thr, # relative to noise_level
                                    merge_thr2=np.inf, # relative to noise_level
                                    discard_thr=0., # in %
                                    discard_dphi=360.) # in degrees
    except Exception, e:
                print e
                return None,None,None,None,None,None
    if labels is not None:
        nlabels = labels.max()
        for lab in (np.arange(nlabels)+1):
                        myspec=sp.flatten()[labels.flatten()==lab]         
                        kspec=k_grid.flatten()[labels.flatten()==lab]
                        phispec=phi_grid.flatten()[labels.flatten()==lab] 																
                        dkspec=dk_grid.flatten()[labels.flatten()==lab]  
                        Lmax,phimax,hsmax= integral(myspec,kspec,phispec,dkspec,k,phi)                          
                        parts=copy.copy(labels)
                        parts[labels!=lab]=0
                        hs_buoy.append(hsmax)
                        wl_buoy.append(Lmax)
                        dir_buoy.append(phimax%360)
                        parts_buoy.append(parts)

        fp_buoy=np.sqrt(9.81/2/np.pi/np.array(wl_buoy))
        delta_dir=abs(tmp_dir-dir_buoy)
        delta_dir[delta_dir>180]=360-delta_dir[delta_dir>180]
        D=(np.log(tmp_fp)-np.log(fp_buoy))**2*9 + (delta_dir/180.0)**2
        Hs=(tmp_hs-hs_buoy)/tmp_hs
             
        return np.array(hs_buoy),np.array(dir_buoy),np.array(fp_buoy),D,Hs,parts_buoy	
    else:
        return None,None,None,None,None,None												