# -*- coding: utf-8 -*-
"""
Created on Wed Dec  7 17:33:52 2016

@author: xwang
"""


import matplotlib
matplotlib.use('Agg')
import numpy as np
import os
import matplotlib as mpl
import matplotlib.pyplot as plt
from datetime import datetime,timedelta
import pdb
import matplotlib.dates as mdates
from palette import *
from ocean_wave_spectrum_partitioning import partition_spectrum
import copy
from get_oswl2ocn_param import get_oswl2ocn_param

#MLB
#Function that create a colorMap from a rgb file
def getColorMap( rgbFile = "medspiration.rgb" ):
    '''
    Load a RGB palette provided in ascii file
    '''
    colors = []
    nbCol=0
    for line in open( rgbFile ):
        r,g,b = [int(c) for c in line.split()]
        colors.append( [r/255.,g/255.,b/255.] )
        nbCol += 1
    return( mpl.colors.ListedColormap(colors, name="custom", N=nbCol) )


def interpolate_as_sar(phi,k,sp,phi_sar,k_sar):
     nphi = sp.shape[0]
     nphih = nphi / 2    
     sp= np.vstack((sp[-nphih:, :], sp, sp[0:nphih, :]))
     phi = np.hstack((phi[-nphih:] - 360, phi, phi[0:nphih] + 360))
     wavelength=2*np.pi/k
     wavelength_sar=2*np.pi/k_sar
     xgrid=np.log(wavelength/wavelength[-1]) 
     xgrid_sar=np.log(wavelength_sar/wavelength[-1])     # new axis
     grid_x, grid_y = np.meshgrid(xgrid_sar, phi_sar)
     x,y = np.meshgrid(xgrid, phi)
     from scipy.interpolate import griddata
     grid_z= griddata((x.flatten(),y.flatten()), sp.flatten(),(grid_x, grid_y),method='linear')
     grid_z=np.nan_to_num(grid_z)
     return grid_z
         
                
def integral(sp,k_grid,phi_grid,dk_grid,k,phi):
    spec=sp*dk_grid*k_grid*(phi[1]-phi[0])/180*np.pi
    hsmax=4.0*np.sqrt(np.sum(spec))    
    myspec=sp*k_grid*(phi[1]-phi[0])/180*np.pi				
    Sk=[]
    for tmp_k in k:
       if len(myspec[k_grid==tmp_k])==0:
			Sk.append(0)
       else:
			Sk.append(sum(myspec[k_grid==tmp_k]))
    if max(Sk)<=0:
		kmax=None
		Lmax=None
    else:
       kmax=k[Sk.index(max(Sk))]
       Lmax = 2*np.pi/kmax									
    myspec=sp*dk_grid*k_grid			
    Sphi=[]
    for tmp_phi in phi:
       if len(myspec[phi_grid==tmp_phi])==0:
			Sphi.append(0)
       else:
			Sphi.append(sum(myspec[phi_grid==tmp_phi]))
    if max(Sphi)<=0:
		phimax=None
    else:		
       phimax=phi[Sphi.index(max(Sphi))]      					
    return Lmax,phimax,hsmax
def buoy_partition_plot(time,f,k,sf,sp_list,mytitle,fileout,buoy_flag,thr1=5,myTick='day',split=None):
    #palette = '/home3/homedir7/perso/xwang/Documents/wangxuan/fwk_new/wave_spec.pal'
    palette = '/home3/homedir7/perso/mlebars/workspace/developpements/buoy_track/additionals/wave_spec.pal'
    cmap = getColorMap( rgbFile = palette )
    g=9.8
    #Modif MLB
    #phi = np.arange(0,360,5)
    phi = np.arange(0,360,10)
    ocn_file = '/home/cercache/project/mpc-sentinel1/data/esa/sentinel-1a/L2/WV/S1A_WV_OCN__2S/2017/005/S1A_WV_OCN__2SSV_20170105T200855_20170105T201405_014701_017EB9_6E58.SAFE/measurement/s1a-wv1-ocn-vv-20170105t201249-20170105t201252-014701-017eb9-017.nc'
    ocn_param = get_oswl2ocn_param(ocn_file,get_spec=True)
    ocn_k   = ocn_param['oswk']
    ocn_phi = ocn_param['oswphi']#*np.pi/180.			
    dim = (ocn_phi.size, ocn_k.size)
    k_grid = np.tile(ocn_k, (dim[0], 1))
    phi_grid = np.tile(ocn_phi, (dim[1], 1)).transpose()
    dk=[]
    dk.append(ocn_k[1]-ocn_k[0])
    for ind in range(len(ocn_k)-2):
       dk.append(1.0/2*(ocn_k[ind+2]-ocn_k[ind]))
    dk.append(ocn_k[-1]-ocn_k[-2])
    dk_grid = np.tile(dk, (dim[0], 1))   				
    hs_buoy = []; wl_buoy = []; dir_buoy = [];time_buoy=[];parts_buoy=[];
    from wave_spectrum_plot import *
    for cpt,sp in enumerate(sp_list):
        if buoy_flag[cpt]==0:
            sp=sp.transpose()
            sp=interpolate_as_sar(phi,k,sp,ocn_phi,ocn_k) 																	
            try:
                labels = partition_spectrum(sp,
                                            ocn_k, ocn_phi, 
                                            twod=True, 
                                            klow=None, khigh=None,
                                            #noise_level=50., # WV1 !
                                            noise_level=10.,  #changed
                                            noise_thr=1., # relative to noise_levelHs
                                            merge_thr1=thr1, # relative to noise_level
                                            merge_thr2=np.inf, # relative to noise_level
                                            discard_thr=0., # in %
                                            discard_dphi=360.) # in degrees
            except Exception, e:
                        print e
                        print datetime.strftime(time[cpt],'%Y%m%dT%H%M%S')
                        continue
            
            if labels is not None:
                nlabels = labels.max()
                if nlabels==0:
                    continue
                for lab in (np.arange(nlabels)+1):
                        myspec=sp.flatten()[labels.flatten()==lab]         
                        kspec=k_grid.flatten()[labels.flatten()==lab]
                        phispec=phi_grid.flatten()[labels.flatten()==lab] 																
                        dkspec=dk_grid.flatten()[labels.flatten()==lab]  
                        Lmax,phimax,hsmax= integral(myspec,kspec,phispec,dkspec,ocn_k,ocn_phi)                          
                        parts=copy.copy(labels)
                        parts[labels!=lab]=0
                        hs_buoy.append(hsmax)
                        wl_buoy.append(Lmax)																							
                        dir_buoy.append(phimax%360)
                        time_buoy.append(time[cpt])
                        parts_buoy.append(parts)
        else:
            continue                    
    if split is None:
        fig = plt.figure(figsize=(16,9));
        ax1 = fig.add_axes([0.05, 0.8, 0.9, 0.15]) 
        ax2 = fig.add_axes([0.05, 0.6, 0.9, 0.15])
        ax3 = fig.add_axes([0.05, 0.4, 0.9, 0.15])
        ax4 = fig.add_axes([0.05, 0.2, 0.9, 0.15])   
        ind=0;mcolor='k'      
#        plot_Partition(np.min(np.concatenate((np.array(time_buoy),time), axis=0)),np.max(np.concatenate((np.array(time_buoy),time), axis=0)),time,f,k,sf,sp_list,time_buoy,hs_buoy,np.array(wl_buoy),dir_buoy,mcolor,fig,ax1,ax2,ax3,ax4,ind,mytitle,myTick)  
        plot_Partition(np.min(time),np.max(time),time,f,k,sf,sp_list,time_buoy,hs_buoy,np.array(wl_buoy),dir_buoy,mcolor,fig,ax1,ax2,ax3,ax4,ind,mytitle,myTick)   
        #Modif MLB
        #plt.savefig(fileout+'_0_'+'buoy_partitions') 
        plt.savefig(fileout) 
        plt.close(fig)
    if split is True:
        start_time=np.min(time)-timedelta(30.)
        Flag=0
        while Flag==0:
            start_time=start_time+timedelta(30.)
            end_time=start_time+timedelta(30.)
            if np.max(time)<=end_time+timedelta(30.):
                Flag=1
                end_time=np.max(time)
            fig = plt.figure(figsize=(16,9));
            ax1 = fig.add_axes([0.05, 0.8, 0.9, 0.15]) 
            ax2 = fig.add_axes([0.05, 0.6, 0.9, 0.15])
            ax3 = fig.add_axes([0.05, 0.4, 0.9, 0.15])
            ax4 = fig.add_axes([0.05, 0.2, 0.9, 0.15])   
            ind=0;mcolor='k'      
            start_date_string = datetime.strftime(start_time,'%Y%m%dT%H%M%S')
            end_date_string   = datetime.strftime(end_time,'%Y%m%dT%H%M%S')
            plot_Partition(start_time,end_time,time,f,k,sf,sp_list,time_buoy,hs_buoy,np.array(wl_buoy),dir_buoy,mcolor,fig,ax1,ax2,ax3,ax4,ind,mytitle,myTick)   
            plt.savefig(fileout+'from_' + start_date_string + '_to_' + end_date_string +'_'+'buoy_partitions') 
            plt.close(fig)           
        
    return hs_buoy,wl_buoy,dir_buoy,time_buoy,parts_buoy

def plot_Partition(Time_start,Time_end,time,f,k,sf,sp_list,time_p,hs_p,wl_p,dirdeg_p,mcolor,fig,ax1,ax2,ax3,ax4,ind,mytitle,myTick='day'):
    if ind==0:
        #palette = '/home3/homedir7/perso/xwang/Documents/wangxuan/fwk_new/wave_spec.pal'
        palette = '/home3/homedir7/perso/mlebars/workspace/developpements/buoy_track/additionals/wave_spec.pal'
    
        cmap = getColorMap( rgbFile = palette )
        g=9.8
        # Buoy 
        x_lims = list([np.min(time),np.max(time)])
        x_lims = mdates.date2num(x_lims)
        y_lims = [8, 21.]#np.max(1./f)]
        T = 1./f
        indice = (T<=21) & (T>=8)
        SSS = sf.T[::,::][indice,::]/np.max(sf.T[::,::][indice,::])
        ax1.imshow(SSS, extent = [x_lims[0], x_lims[1],  y_lims[0], y_lims[1]], aspect='auto',cmap=cmap)         
        ax1.set_ylabel('wave period [s]')
        ax1.xaxis_date()
        xlocs,xlabels=plt.xticks()
        if myTick is 'month':
            ax1.xaxis.set_major_locator(mdates.MonthLocator(bymonth=range(1,13)))
            ax1.xaxis.set_major_formatter(mdates.DateFormatter('%m'))          
        if myTick is 'day':
            ax1.xaxis.set_major_locator(mdates.DayLocator(bymonthday=range(1,32),interval=3))
            ax1.xaxis.set_major_formatter(mdates.DateFormatter('%m-%d'))        
        if myTick is 'hour':
            ax1.xaxis.set_major_locator(mdates.HourLocator(interval=6))           
            ax1.xaxis.set_major_formatter(mdates.DateFormatter('%m-%dT%H'))
        ax1.scatter(time_p,np.sqrt(2*np.pi*wl_p/g),c=mcolor)  
        ax1.grid()
        ax1.set_ylim([8,20])
        ax1.set_title(mytitle)
        ax1.set_xlim([Time_start,Time_end])

        ax2.scatter(time_p,np.sqrt(2*np.pi*np.array(wl_p)/g),c=mcolor)     
        ax2.grid()
        ax2.set_ylabel('wave period [s]')
        if myTick is 'month':
            ax2.xaxis.set_major_locator(mdates.MonthLocator(bymonth=range(1,13)))
            ax2.xaxis.set_major_formatter(mdates.DateFormatter('%m-%dT%H'))          
        if myTick is 'day':
            ax2.xaxis.set_major_locator(mdates.DayLocator(bymonthday=range(1,32),interval=3))
            ax2.xaxis.set_major_formatter(mdates.DateFormatter('%m-%d'))        
        if myTick is 'hour':
            ax2.xaxis.set_major_locator(mdates.HourLocator(interval=6))           
            ax2.xaxis.set_major_formatter(mdates.DateFormatter('%m-%dT%H'))   
        ax2.set_ylim([8,20])
        ax2.set_xlim([Time_start,Time_end])
        
        ax3.scatter(time_p,hs_p,c=mcolor)  
        ax3.grid()
        ax3.set_ylabel('wave height [m]')
        if myTick is 'month':
            ax3.xaxis.set_major_locator(mdates.MonthLocator(bymonth=range(1,13)))
            ax3.xaxis.set_major_formatter(mdates.DateFormatter('%m'))          
        if myTick is 'day':
            ax3.xaxis.set_major_locator(mdates.DayLocator(bymonthday=range(1,32),interval=3))
            ax3.xaxis.set_major_formatter(mdates.DateFormatter('%m-%d'))        
        if myTick is 'hour':
            ax3.xaxis.set_major_locator(mdates.HourLocator(interval=6))           
            ax3.xaxis.set_major_formatter(mdates.DateFormatter('%m-%dT%H'))  
        ax3.set_ylim([0,4])
        ax3.set_xlim([Time_start,Time_end])
        
        ax4.scatter(time_p,dirdeg_p,c=mcolor)  
        ax4.grid()
#        ax4.set_ylim([-50,50])
        ax4.set_ylabel('wave direction [deg]')
        if myTick is 'month':
            ax4.xaxis.set_major_locator(mdates.MonthLocator(bymonth=range(1,13)))
            ax4.xaxis.set_major_formatter(mdates.DateFormatter('%m'))          
        if myTick is 'day':
            ax4.xaxis.set_major_locator(mdates.DayLocator(bymonthday=range(1,32),interval=3))
            ax4.xaxis.set_major_formatter(mdates.DateFormatter('%m-%d'))        
        if myTick is 'hour':
            ax4.xaxis.set_major_locator(mdates.HourLocator(interval=6))           
            ax4.xaxis.set_major_formatter(mdates.DateFormatter('%m-%dT%H'))    
        ax4.set_xlim([Time_start,Time_end])

    else:
        g=9.8
        ax1.scatter(time_p,np.sqrt(2*np.pi*wl_p/g),c=mcolor)  
#        vmin=0.5;vmax=5
        ax2.scatter(time_p,np.sqrt(2*np.pi*np.array(wl_p)/g),c=mcolor)  
        
        ax3.scatter(time_p,hs_p,c=mcolor)  
        
        ax4.scatter(time_p,dirdeg_p,c=mcolor)     

       
        