# -*- coding: utf-8 -*-
"""
Created on Thu Jan 26 10:55:29 2017

@author: xwang
Update by Mlebars
Exemple of command to launch
python main2.py --provider ndbc_nrt --ids 32012,46041 --startdate 20170101-080000 --enddate 20170102-080000
"""

#python main2.py --provider ndbc_nrt --ids 32012,46041 --startdate 20170101-080000 --enddate 20170102-080000
import matplotlib
import numpy as np
from bz_pick import zip_load,zip_save 
import matplotlib as mpl
import matplotlib.pyplot as plt
from datetime import datetime,timedelta
from buoy_nc_reader import read_buoy_nc,read_nc_wind,spectrum_mean
import pdb
from data_path import cdip_globwave_Path, ndbc_globwave_Path,ndbc_nrt_Path,ndbc_archive_Path
from palette import *
from buoy_and_SAR_storm_plot import buoy_partition_plot, plot_Partition
from glob import glob
#MLB Correction
import os
import argparse
import sys

Method='G'
g=9.8
mcolor=['b','m','r','w','g','c','y']
#provider='ndbc_globwave'
provider = 'ndbc_nrt'
dirfig='/home/cercache/users/mlebars/buoyspec/'+provider+'/pic_swell_G/'
dir_output ='/home/cercache/users/mlebars/buoyspec/'+provider+'/BuoySwell_G/'




def cmdlineparser():
    """
    define the options of the scripts and set func depending of the inputs
    """
    #OBJCHOICES = ('WV', 'SAFE')

    parser = argparse.ArgumentParser(description='Sentinel L2 Indexer')

    parser.add_argument('--provider', action='store', default=None,choices=['ndbc_nrt','ndbc_globwave'],required=True, help='Mode that determine to search data')
    parser.add_argument('--ids', action='store',type=str, default=None, help='give the buoys ids ex: 32012,46041,46076',required=True)
    parser.add_argument('--startdate', action='store',type=str, default=None, help='startDate to process YYYYMMDD-hhmmss',required=True)
    parser.add_argument('--enddate', action='store',type=str, default=None, help='endDate to process YYYYMMDD-hhmmss',required=True)
    #parser.add_argument('data_type', type=str.upper, choices=OBJCHOICES)

    #subparsers = parser.add_subparsers()

    #parser_wv = subparsers.add_parser('wv')
    #parser_wv.set_defaults(func=run_l2_wv_indexation)

    #parser_wv_phx = subparsers.add_parser('wvphx')
    #parser_wv_phx.set_defaults(func=run_l2_wv_indexation_phoenix)

    return parser

    
def main():
    parser = cmdlineparser()
    args = parser.parse_args()
    ids =  args.ids.split(',')
    time_start=datetime.strptime(args.startdate, '%Y%m%d-%H%M%S') 
    time_end=datetime.strptime(args.enddate, '%Y%m%d-%H%M%S') 

    if Method=='G':
            print time_start,time_end
            print "started at",datetime.now()
            #MODIF MLB
            #from waveCluster_buoy_G import track_buoy,plot_buoy_partitions_cluster,create_buoy_nc
            from waveCluster_buoy_G_try import track_buoy,plot_buoy_partitions_cluster,create_buoy_nc
            for id in ids:
                print 'Buoy Id:', id
                time_wd = {'type':'t1t2','startDate': time_start-timedelta(days = 1),'stopDate':time_end+timedelta(days = 1)}
                phi = np.arange(0,360,10)
                lon,lat,time,f,k,sf,sp_list,buoy_flag = read_buoy_nc(id,time_wd,phi = phi,con = 'to', provider = provider,smooth=True)
                if lon is None:
                    print "lon not Found"
                    continue
                wtime,wspd,wdir = read_nc_wind(id,time_wd)
                _lon = np.mean(lon) % 360
                _lat = np.mean(lat)
                #dirfig='//home/cercache/users/xwang/buoyspec/pic_swell_G/'
                
                if os.path.isdir(dirfig)==False:
                    command_line = 'mkdir ' + dirfig
                    os.system(command_line)
                mytitle=id+'  lon: {:3.2f}'.format(_lon)+'  lat: {:3.2f}'.format(_lat)
                fileout = dirfig + id +'_'+datetime.strftime(time_start,'%Y-%m-%d %H:%M:%S')+'buoy_partitions.png'
                #Modif MLB
                #[hs_buoy,wl_buoy,dir_buoy,time_buoy]=buoy_partition_plot(time,f,k,sf,sp_list,mytitle,fileout,buoy_flag,myTick='day')
                [hs_buoy,wl_buoy,dir_buoy,time_buoy,parts_buoy]=buoy_partition_plot(time,f,k,sf,sp_list,mytitle,fileout,buoy_flag,myTick='day')      
                
                print "after buoy_partition_plot",fileout
                if len(time_buoy)==0:
                    continue
                #Modif MLB
                #[btimelist, bfplist, bhslist, bdirlist, bsyslist,bwindlist,thr]=track_buoy(hs_buoy,wl_buoy,dir_buoy,time_buoy,id,time,f,k,sf,sp_list,wtime,wspd,wdir)  
                [btimelist, bfplist, bhslist, bdirlist, bsyslist,bwindlist,thr,partlist]=track_buoy(hs_buoy,wl_buoy,dir_buoy,time_buoy,parts_buoy,id,time,f,k,sf,sp_list,wtime,wspd,wdir,Method)  
                
                if max(bsyslist)<1:
                    continue        
                plot_buoy_partitions_cluster( btimelist, bfplist, bhslist, bdirlist, bsyslist,bwindlist,time,f,k,sf,sp_list,mcolor,dirfig + id,mytitle,wtime,wspd,wdir)
                #dir_output ='//home/cercache/users/xwang/buoyspec/BuoySwell_G/'
                
                #Modif MLB 
                #create_buoy_nc( btimelist, bfplist, bhslist, bdirlist, bsyslist,bwindlist,thr,wtime,wspd,wdir,id,_lon,_lat,dir_output)
                create_buoy_nc( btimelist, bfplist, bhslist, bdirlist, bsyslist,bwindlist,thr,partlist,wtime,wspd,wdir,id,_lon,_lat,dir_output)
                print (dir_output)
                print datetime.now()

            print "End",datetime.now()
    
    sys.exit(0)

    

if __name__ == '__main__':
    main()

def myFunction():
    #archivePath = ndbc_globwave_Path
    #filepattern = 'WMO*spectrum*.nc'
    #monthDir = os.path.join(archivePath, str(time_end.year), "%02d" % time_end.month)
    #files = glob(monthDir+'/'+filepattern)
    ids = []
    #for _fi in files:
    #    id = str.split(str.split(os.path.basename(_fi),'_')[0],'WMO')[1]
    #    ids.append(id)
    ids = ['32012','46041','46076','46002','46042','46078','46006','46047','46083','46011','46053','46084','46012','46054','46086','46014','46059','46087','46015','46060','46089','46022','46061','51000','46026','46066','51003','46027','46069','51004','46028','46070','51101','46029','46071','46035','46072']
    #ids = ['32012']

    