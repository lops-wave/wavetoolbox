import numpy as np
from numpy import sin,cos,pi
import pdb

def buoy_spectrum2d(sf,a1,a2,b1,b2,f,smooth,dirs = np.arange(0,365,10)):
# Maximum entropy method to estimate the Directional Distribution
# Maximum Entropy Method - Lygre & Krogstad (1986 - JPO)
# Eqn. 13:
# phi1 = (c1 - c2c1*)/(1 - abs(c1)^2)
# phi2 = c2 - c1phi1
# 2piD = (1 - phi1c1* - phi2c2*)/abs(1 - phi1exp(-itheta) -phi2exp(2itheta))^2
# c1 and c2 are the complex fourier coefficients

# inputs : dirs in degrees

   nfreq = sf.size
   nbin = dirs.size

   c1 = a1+1j*b1
   c2 = a2+1j*b2
   p1 = (c1-c2*c1.conjugate())/(1.-abs(c1)**2)
   p2 = c2-c1*p1

   # numerator(2D) : x
   x = 1.-p1*c1.conjugate()-p2*c2.conjugate()  
   x= np.tile(np.real(x),(nbin,1)).T

   # denominator(2D): y
   a = dirs*pi/180.
   e1 = np.tile(cos(a)-1j*sin(a),(nfreq,1))
   e2 = np.tile(cos(2*a)-1j*sin(2*a),(nfreq,1))

   p1e1 = np.tile(p1,(nbin,1)).T*e1
   p2e2 = np.tile(p2,(nbin,1)).T*e2
   y = abs(1-p1e1-p2e2)**2

   D = x/(y*2*pi)
   
# normalizes the spreading function,
# so that int D(theta,f) dtheta = 1 for each f  

   dth = dirs[1]-dirs[0]
   tot = np.tile(np.sum(D, axis=1)*dth/180.*pi,(nbin,1)).T
   D = D/tot

   sp2d = np.tile(sf,(nbin,1)).T*D
#   sp2d=np.nan_to_num(sp2d)
# smooth
   if smooth==True:
      from scipy.signal import fftconvolve
      df=[]
      df.append(f[1]-f[0])
      for ind in range(len(f)-2):
        df.append(1.0/2*(f[ind+2]-f[ind]))
      df.append(f[-1]-f[-2])
      df_grid = np.tile(df, (len(dirs), 1))
#      pdb.set_trace()
      sp2d=sp2d*df_grid.T
      K = np.array([[1./np.sqrt(2),1,1./np.sqrt(2)],[1,2,1],[1./np.sqrt(2),1,1./np.sqrt(2)]])/(6+4./np.sqrt(2))
      sp2d = fftconvolve(sp2d,K,mode = 'same')
      sp2d = sp2d*(1.0/df_grid.T)
#      pdb.set_trace()
   return sp2d,D


def spfphi2kphi(spfphi,f):
   g = 9.81
   k = (2*pi*f)**2/g
   dk = np.zeros(k.size)
   dk[0] = k[1]-k[0]
   dk[-1] = k[-1]-k[-2]
   dk[1:-1] = (k[2:]-k[0:-2])/2.

   df = np.zeros(f.size)
   df[0] = f[1]-f[0]
   df[-1] = f[-1]-f[-2]
   df[1:-1] = (f[2:]-f[0:-2])/2.

   spkphi = []
   for sp in spfphi:
      df2kdk = (np.tile(df/(k*dk), (sp.shape[1], 1))).T
      sp2 = sp*df2kdk
      spkphi.append(sp2)

   return spkphi,k

def spfrom2to(sp_in,phi):

   phi2 = (180.+phi)%360
   ind = np.argsort(phi2)

   sp_out = []
   for sp in sp_in:
      sp_out.append(sp[:,ind])

   return sp_out

def th2ab(stheta1,stheta2,theta1,theta2,construct=None):
   # convert theta & stheta(globwave data used) to a1,b1,a2,b2
   r1 = abs(1-0.5*(stheta1/180.*pi)**2)
   r2 = abs(1-2.0*(stheta2/180.*pi)**2)  
#   r1 = 1-0.5*(stheta1/180.*pi)**2
#   r2 = 1-2.0*(stheta2/180.*pi)**2
  
   a1 = r1*cos(theta1/180.*pi)
   b1 = r1*sin(theta1/180.*pi)

   a2 = r2*cos(2*theta2/180.*pi)
   b2 = r2*sin(2*theta2/180.*pi) 

   return a1,a2,b1,b2

def ar2ab(alpha1,alpha2,r1,r2):
   # convert alpha & r(ndbc data used) to a1,b1,a2,b2
   a1 = r1*cos(alpha1/180.*pi)
   b1 = r1*sin(alpha1/180.*pi)
   a2 = r2*cos(2*alpha2/180.*pi)
   b2 = r2*sin(2*alpha2/180.*pi)

   return a1,a2,b1,b2   