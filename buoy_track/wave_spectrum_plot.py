import numpy as np
import matplotlib.pyplot as plt
import matplotlib as mpl
from mpl_toolkits.axes_grid1 import make_axes_locatable
import itertools
import matplotlib
import pdb

cdict = {'red': ((0., 1, 1),
                 (0.05, 1, 1),
                 (0.11, 0, 0),
                 (0.66, 1, 1),
                 (0.89, 1, 1),
                 (1, 0.5, 0.5)),
         'green': ((0., 1, 1),
                   (0.05, 1, 1),
                   (0.11, 0, 0),
                   (0.375, 1, 1),
                   (0.64, 1, 1),
                   (0.91, 0, 0),
                   (1, 0, 0)),
         'blue': ((0., 1, 1),
                  (0.05, 1, 1),
                  (0.11, 1, 1),
                  (0.34, 1, 1),
                  (0.65, 0, 0),
                  (1, 0, 0))}

my_cmap = matplotlib.colors.LinearSegmentedColormap('my_colormap',cdict,256)

ccc = ['yellow','black','red','blue','green','orange','purple','cyan']


def init_sp(k,phi,spec):
	if np.shape(spec)[0]!=len(phi):		spec = spec.T

	# # adding 360deg
	ind =np.argsort(phi)
	phi = np.sort(phi)    
	phi = np.append(phi,360)
#	pdb.set_trace()
	sp = spec[ind,:]
	sp = np.concatenate((sp,sp[0:1,:]), axis=0)

	#print np.min(sp)
	# converting the direction to rad
	phi = phi/180.0*np.pi

	radius,thetas=np.meshgrid(k,phi)

	return thetas,radius,sp

def init_ax(ax):
	# Go clockwise
	ax.set_theta_direction(-1)
	# Start from the top
	ax.set_theta_offset(np.pi/2)

	ax.set_aspect('equal', adjustable='box', anchor='C')
	ax.radii_angle = 90. # position of label name on radial axis
	ax.theta_angles = np.arange(0, 360, 90)
	ax.theta_labels = ['N', 'E', 'S', 'W']
	ax.set_thetagrids(angles=ax.theta_angles, labels=ax.theta_labels,frac=1.02)
	ax.set_axis_off
	ax.set_frame_on(False)
	ax.xaxis.set_visible(True)
	return ax 	

def circle_plot(ax,r,freq=0):
	# Radial Circles and their label
	theta = 2*np.pi*np.arange(360)/360.
	
	labels = []

	if freq==0:
		for i in r:
			plt.plot(theta, np.arange(360)*0 + 2*np.pi/i,'--k')
			labels.append(str(i)+' m')
		ax.set_rgrids([2*np.pi/i for i in r], labels=labels, angle=45.)

	if freq==1:
		for i in r:
			plt.plot(theta, np.arange(360)*0 + np.sqrt(2*np.pi/i*9.81/(2*np.pi)**2),'--k')
			labels.append(str(i)+' m')
		ax.set_rgrids([np.sqrt(2*np.pi/i*9.81/(2*np.pi)**2) for i in r], labels=labels, angle=45.)
		

	    
def AzRaAx_add(ax,tra,cut_off,wsys=None):
        #pdb.set_trace()
	ax.plot([(tra-180)*np.pi/180.,(tra)*np.pi/180.],[2*np.pi/cut_off,2*np.pi/cut_off],zorder=100000,color='black',lw=1.5)
	ax.plot([(tra-90)*np.pi/180.,(tra+90)*np.pi/180.],[2*np.pi/cut_off,2*np.pi/cut_off],zorder=100000,color='black',lw=1.5)
	ax.annotate('Range',xy=((90+tra+2)*np.pi/180.,2*np.pi/cut_off),color='black',zorder=1000000)
	ax.annotate('Azimuth',xy=((tra-2)*np.pi/180.,2*np.pi/cut_off),color='black',zorder=1000000)

#	if wsys is not None:
#		theta1=(tra-45)%360;theta2=(tra+45)%360
#		proj=abs(np.cos((theta1-tra)%360/180.0*np.pi))
#		ax.plot([theta1/180.0*np.pi,theta2/180.0*np.pi],[ 2*np.pi/cut_off/proj,2*np.pi/cut_off/proj],zorder=100000,color='grey',lw=1.5,alpha=0.5)
#
#		theta1=(tra-180-45)%360;theta2=(tra-180+45)%360
#		proj=abs(np.cos((theta1-tra)%360/180.0*np.pi))
#		ax.plot([theta1/180.0*np.pi,theta2/180.0*np.pi],[ 2*np.pi/cut_off/proj,2*np.pi/cut_off/proj],zorder=100000,color='grey',lw=1.5,alpha=0.5)	
#		pdb.set_trace()		
#		R = 2*np.pi/cut_off
#		#Azc = 2*np.pi/wsys['Azc']
#                Azc = 2*np.pi/cut_off
#		ttt = tra-180.
#		theta0 = np.arcsin(Azc/R)
#		theta1 = (np.pi/2.)-theta0
#		theta2 = -theta1
#		theta3 =  theta1 + ttt*np.pi/180.
#		theta4 = -theta1 + ttt*np.pi/180.
#		#ax.plot([theta0],[2*np.pi/cut_off],'o', zorder=100000,color='green',lw=1.5)
#		#ax.plot([theta1],[2*np.pi/cut_off],'o', zorder=100000,color='blue',lw=1.5)
#		#ax.plot([theta1+ttt*np.pi/180.],[2*np.pi/cut_off],'s', zorder=100000,color='blue',lw=1.5)
#		#ax.plot([theta2],[2*np.pi/cut_off],'o',zorder=100000,color='red',lw=1.5)
#		#ax.plot([theta2+ttt*np.pi/180.],[2*np.pi/cut_off],'s',zorder=100000,color='red',lw=1.5)
#		#ax.plot([theta1,theta2],[2*np.pi/cut_off,2*np.pi/cut_off],zorder=100000,color='grey',lw=1.5)
#		ax.plot([theta3,theta4],[2*np.pi/cut_off,2*np.pi/cut_off],zorder=100000,color='grey',lw=1.5,alpha=0.5)
#
#		theta0 = np.arcsin(Azc/R) 
#		theta1 = (np.pi/2.)+theta0
#		theta2 = -theta1 
#		theta3 =  theta1 + ttt*np.pi/180.
#		theta4 = -theta1 + ttt*np.pi/180.
#		#ax.plot([theta3],[2*np.pi/cut_off],'o', zorder=100000,color='blue',lw=1.5)
#		#ax.plot([theta4],[2*np.pi/cut_off],'o',zorder=100000,color='red',lw=1.5)
#		ax.plot([theta3,theta4],[2*np.pi/cut_off,2*np.pi/cut_off],zorder=100000,color='grey',lw=1.5,alpha=0.5)
#		#pdb.set_trace()

def ax_invisible(ax):
	ax.set_frame_on(False)
	ax.xaxis.set_visible(False)
	ax.yaxis.set_visible(False) 	

def spec_plot(sp,k, phi,ax_plot,ax_cb,title='',tra = None,cut_off = 200, colormap= my_cmap,	freq = 0,hs_on = 1,c_txt=None,
				wsys = None, spi_list = None, 
				wind = None,ax_legend = None,sp_log = 0,vmax = None,vmin = None,cb_lable = 'spectral energy',
				fs_title = 20,fs_legend = 12,k0=None,phi0=None, markersize=10,part=None,oswSpecRes=None,ocn_AzCutoff=None):	
	if sp_log :
		sp[sp>0] = np.log(sp[sp>0])
		sp[sp<0] = 0
		cb_lable = cb_lable+'(nature log)'

	if vmin is not None:
		sp[sp<vmin] = vmin
	else:
		vmin = np.min(sp)
	
	if vmax is not None:
		sp[sp>vmax] = vmax
	else:
		vmax = np.max(sp)


	ax_invisible(ax_plot)
	divider = make_axes_locatable(ax_plot)
	ax_plot = divider.append_axes('top', size='400%')


	ax_invisible(ax_cb)				
	divider = make_axes_locatable(ax_cb)
	ax_cb = divider.append_axes('right', size='3%', pad=0.3)

	if ax_legend is not None:
		ax_invisible(ax_legend)
		divider = make_axes_locatable(ax_legend)
		ax = divider.append_axes('bottom', size='20%')

	ax_plot = init_ax(ax_plot)
	plt.sca(ax_plot)

	plt.title(title, fontsize=fs_title)
	# Radial Circle    
	circle_plot(ax_plot,[50,100,200,400,800],freq= freq)
	# range & azimuth axis
	if tra is not None: 
		if wsys is not None:
                        #pdb.set_trace()
			AzRaAx_add(ax_plot,tra,cut_off,wsys=wsys)
		else: 
			AzRaAx_add(ax_plot,tra,cut_off)

	# plot the contour of spectrum
	if np.amax(sp)>np.amin(sp):	
		# contour	
		#pdb.set_trace()
		
		#levels = [0.] + list(np.linspace(vmin,vmax,50))
		#levels[1] = 20.
		levels = list(np.linspace(vmin,vmax,100))
#		pdb.set_trace()
		plt.contourf(*init_sp(k,phi,sp),cmap=colormap,levels = levels)
#		plt.pcolor(*init_sp(k,phi,sp),cmap=colormap)
		#pdb.set_trace()
		if part is not None:
#			pdb.set_trace()
			part = part+1
			pu = np.unique(part)
			pu = pu[pu<200]
			#pdb.set_trace()
			for cpt in pu:
				#pdb.set_trace()
				ppart=part*0
				ppart[part==cpt]=cpt
                                #print cpt
                                #pdb.set_trace()
				plt.contour(*init_sp(k,phi,ppart),levels=[0],colors=ccc[np.int(cpt)%6])
				#print ccc[np.int(cpt)]

		# colorbar
		norm = mpl.colors.Normalize(vmin=vmin,vmax=vmax)	
		cb = mpl.colorbar.ColorbarBase(ax_cb,cmap=colormap,norm=norm,orientation='vertical', format='%.0e')			
		cb.set_label(cb_lable)
		if oswSpecRes is not None:
			plt.plot(2*np.pi*phi/360., 2*np.pi/oswSpecRes,'--k',linewidth=2.0)
#			pdb.set_trace()		
	else:
		ax_invisible(ax_cb)

	if ocn_AzCutoff is not None:
		theta1=(tra-45)%360;theta2=(tra+45)%360
		proj=abs(np.cos((theta1-tra)%360/180.0*np.pi))
		plt.plot([theta1/180.0*np.pi,theta2/180.0*np.pi],[ 2*np.pi/ocn_AzCutoff /proj,2*np.pi/ocn_AzCutoff /proj],zorder=100000,color='grey',lw=1.5,alpha=0.5)

		theta1=(tra-180-45)%360;theta2=(tra-180+45)%360
		proj=abs(np.cos((theta1-tra)%360/180.0*np.pi))
		plt.plot([theta1/180.0*np.pi,theta2/180.0*np.pi],[ 2*np.pi/ocn_AzCutoff /proj,2*np.pi/ocn_AzCutoff /proj],zorder=100000,color='grey',lw=1.5,alpha=0.5)							
	if k0 is not None and phi0 is not None:
		plt.plot(phi0/180.*np.pi,k0,c = 'k',marker='o',alpha = 0.9,markersize=markersize)	

	if wsys is not None:
		
		# colors = itertools.cycle(['0.75',"y", "g", "c","m","r"])
		
		for j,iwsys in enumerate(wsys['hs']):
			# c = next(colors)
			c = 'k'
			c_txt = ccc[np.int(pu[j])]
			# add_wsys_contour_wsys(ax_plot,sp,k,phi,iwsys,cut_off,c_filled = c)
			#pdb.set_trace()
			if c_txt is None:
				add_wsys_label(ax,wsys,j,c_txt=c,x_txt = j*0.2,fs_legend = fs_legend,hs = hs_on)
			else:
				add_wsys_label(ax,wsys,j,c_txt=c_txt,x_txt = j*0.2,fs_legend = fs_legend,hs = hs_on)
			ax_invisible(ax)

	if spi_list is not None:	
		colors = itertools.cycle(['0.75',"y", "g", "c","m","r"])
		for j,spi in enumerate(spi_list):
			c = next(colors) 
			add_wsys_contour_sp(ax_plot,k,phi,spi,cut_off,c_filled = c)

			# if c_txt is None:
			# 	add_wsys_label(ax,spi,c_txt=c,x_txt = j*0.2,fs_legend = fs_legend,hs = hs_on)
			# else:
			# 	add_wsys_label(ax,spi,c_txt=c_txt,x_txt = j*0.2,fs_legend = fs_legend,hs = hs_on)
			# ax_invisible(ax)
						

	# plot the wind vector    
	if wind is not None:
		U = wind['U'] #zonal
		V = wind['V'] #meridional
		wspd = np.sqrt(U**2+V**2)
		ax_plot.quiver(np.arctan2(U,V),0.5*2*np.pi/cut_off,
						U/wspd,V/wspd,
						scale=5,width=0.015,color='red')

		ax_plot.annotate('U10 = {:3.2f}'.format(wspd)+ ' m/s',
						xy=(np.arctan2(U,V),0.9*2*np.pi/cut_off),color='red',zorder=1000000)


	
	if freq == 0: 
		ax_plot.set_rmax(rmax=2*np.pi/cut_off) 
	if freq ==1:
		ax_plot.set_rmax(rmax=np.sqrt(2*np.pi/cut_off*9.81/(2*np.pi)**2)) 





def add_wsys_contour_sp(ax_plot,k,phi,spi,cut_off,c_filled = '0.75'):	
	# boundary of each wave system

	sys = np.zeros_like(spi.sp,dtype = np.float)           
	sys[spi.sp>0]=1   

	thetas, radius, sys2 = init_sp(k,phi,sys)

	# sys2[sys2==0]= np.nan

	ax_plot.contour(thetas, radius, sys2,colors=c_filled,levels = [0],linewidths=2)
	
	ax_plot.set_rmax(rmax=2*np.pi/cut_off) 	
	


def add_wsys_contour_wsys(ax_plot,sp,k,phi,iwsys,cut_off,c_filled = '0.75'):	
	# boundary of each wave system
	isys = iwsys.isystem 
	if len(isys)>0:
		sys = np.zeros_like(sp,dtype = np.float)           
		sys[isys]=1            
		thetas, radius, sys2 = init_sp(k,phi,sys)

		sys2[sys2==0]= np.nan

		ax_plot.contourf(thetas, radius, sys2,colors=c_filled,alpha = 0.8)
	
	ax_plot.set_rmax(rmax=2*np.pi/cut_off) 

def add_wsys_label(ax_legend,wsys,j,c_txt='k',x_txt = 0,fs_legend = 12,hs = 1):
	#pdb.set_trace()
	# Legend of each wave system
	_Hs  = 'Hs'  + ': {:3.2f}'.format(wsys['hs'][j]) + ' m'
	_Wl  = 'Wl'  + ': {:3.2f}'.format(wsys['wl'][j]) + ' m'
	_Dir = 'Dir' + ': {:3.2f}'.format(wsys['dirad'][j]*180.0/np.pi) + ' $^\circ$'
	#_Hs  = 'Hs'  + ': {:3.2f}'.format(iwsys['hs) + ' m'
	#_Wl  = 'Wl'  + ': {:3.2f}'.format(iwsys.wl) + ' m'
	#_Dir = 'Dir' + ': {:3.2f}'.format(iwsys.dirad) + ' $^\circ$'
	#pdb.set_trace()
	if hs:
		ax_legend.annotate(_Hs,xy=(x_txt, 0.3),color=c_txt, fontsize=fs_legend,xycoords='axes fraction')

	ax_legend.annotate(_Dir,xy=(x_txt, 0.6),color=c_txt, fontsize=fs_legend,xycoords='axes fraction')	
	ax_legend.annotate(_Wl,xy=(x_txt, 0.9),color=c_txt, fontsize=fs_legend,xycoords='axes fraction')

	if j==0:
		_Lon = 'Lon : {:3.2f}'.format(wsys['lon']) + ' deg'
		_Lat = 'Lat : {:3.2f}'.format(wsys['lat']) + ' deg'
		ax_legend.annotate(_Lon,xy=(x_txt, -0.1),color='k', fontsize=fs_legend,xycoords='axes fraction')
		ax_legend.annotate(_Lat,xy=(x_txt, -0.4),color='k', fontsize=fs_legend,xycoords='axes fraction')

		_SnR = 'SnR : {:3.2f}'.format(wsys['SnR'])
		_Nv  = 'Nv  : {:3.2f}'.format(wsys['Nv'])
		ax_legend.annotate(_SnR,xy=(0.2, -0.1),color='k', fontsize=fs_legend,xycoords='axes fraction')
		ax_legend.annotate(_Nv,xy=(0.2, -0.4),color='k', fontsize=fs_legend,xycoords='axes fraction')

		_Nrcs  = 'NRCS  : {:3.2f}'.format(wsys['Nrcs']) + ' dB'
		_Tra   = 'Track : {:3.2f}'.format(wsys['Tra']) + ' deg'
		ax_legend.annotate(_Nrcs,xy=(0.375, -0.1),color='k', fontsize=fs_legend,xycoords='axes fraction')
		ax_legend.annotate(_Tra ,xy=(0.375, -0.4),color='k', fontsize=fs_legend,xycoords='axes fraction')

		_Azc  = 'Az. Cut Off : {:3.2f}'.format(wsys['Azc']) + ' m'
		_inc  = 'Incidence   : {:3.2f}'.format(wsys['Inc']) + ' deg'
		ax_legend.annotate(_Azc,xy=(0.6, -0.1),color='k', fontsize=fs_legend,xycoords='axes fraction')
		ax_legend.annotate(_inc,xy=(0.6, -0.4),color='k', fontsize=fs_legend,xycoords='axes fraction')



	# if hasattr(iwsys, 'Rpb'):
	# 	_Rpb  = 'Rpb'  + ': {:3.2f}'.format(iwsys.Rpb) 
	# 	ax_legend.annotate(_Rpb,xy=(x_txt, 0.0),color=c_txt, fontsize=fs_legend,xycoords='axes fraction')

