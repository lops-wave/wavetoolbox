# -*- coding: utf-8 -*-

# pour decoder la date, voir float2strdate

import netCDF4 as nc
from glob import glob
import numpy as np
from datetime import *
import pdb

def get_oswl2ocn_param(file,get_spec=False,norut=None):

    data = nc.Dataset(file)
   
    #pdb.set_trace()

    wv_param={}
    wv_param['oswHs']             = (data.variables['oswHs'][:])
    wv_param['oswWl']            = (data.variables['oswWl'][:])
    wv_param['oswWindSeaHs']      = (data.variables['oswWindSeaHs'][:])
    wv_param['oswAzCutoff']       = (data.variables['oswAzCutoff'][:])
    wv_param['oswRaCutoff']       = (data.variables['oswRaCutoff'][:])
    wv_param['oswSnr']            = (data.variables['oswSnr'][:])
    wv_param['oswNv']             = (data.variables['oswNv'][:])
    wv_param['oswInten']          = (data.variables['oswInten'][:])
    wv_param['oswWindSpeed']      = (data.variables['oswWindSpeed'][:])
    wv_param['oswWindDirection']  = (data.variables['oswWindDirection'][:])
    wv_param['oswIncidenceAngle'] = (data.variables['oswIncidenceAngle'][:])
    wv_param['oswLandFlag']       = (data.variables['oswLandFlag'][:])
    wv_param['oswDepth']          = (data.variables['oswDepth'][:])
    wv_param['oswLandCoverage']   = (data.variables['oswLandCoverage'][:])
    wv_param['oswLat']            = (data.variables['oswLat'][:])
    wv_param['oswLon']            = (data.variables['oswLon'][:])
    wv_param['oswIconf']          = (data.variables['oswIconf'][:])
    wv_param['oswDirmet']         = (data.variables['oswDirmet'][:])
    wv_param['oswNrcs']           = (data.variables['oswNrcs'][:])
    wv_param['oswHeading']        = (data.variables['oswHeading'][:])
    wv_param['oswSpecRes']        = (data.variables['oswSpecRes'][:][0,0,:])
    wv_param['oswWaveAge']        = (data.variables['oswWaveAge'][:])
    #wv_param['rvlDcObs']          = (data.variables['rvlDcObs'][:])    
    #wv_param['rvlDcGeo']          = (data.variables['rvlDcGeo'][:])      
    #wv_param['rvlDcMiss']         = (data.variables['rvlDcMiss'][:])
    #try:
    #    wv_param['oswEcmwfWindDirection']  = (data.variables['oswEcmwfWindDirection'][:])
    #except KeyError:
    #    wv_param['oswEcmwfWindDirection']  = np.nan

    if get_spec==True:
        if norut is None:
            wv_param['oswk']          = (data.variables['oswK'][:])
            wv_param['oswphi']        = (data.variables['oswPhi'][:])
            wv_param['oswspec']       = (data.variables['oswPolSpec'][0,0,:,:])
            wv_param['xspec_re']      = (data.variables['oswQualityCrossSpectraRe'][0,0,:,:])
            wv_param['xspec_im']      = (data.variables['oswQualityCrossSpectraIm'][0,0,:,:])
            wv_param['oswPartitions'] = (data.variables['oswPartitions'][0,0,:,:])
        else:
            wv_param['oswk']          = (data.variables['oswK'][:])
            wv_param['oswphi']        = (data.variables['oswPhi'][:])
            wv_param['oswspec']       = (data.variables['oswPolSpec'][:,0,:,:])
            wv_param['xspec_re']      = (data.variables['oswQualityCrossSpectraRe'][:,0,:,:])
            wv_param['xspec_im']      = (data.variables['oswQualityCrossSpectraIm'][:,0,:,:])
            wv_param['oswPartitions'] = (data.variables['oswPartitions'][:,0,:,:])
            
    #pdb.set_trace()
    data.close()

    #pdb.set_trace()

    return wv_param
