#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
    Author:  mickael.lebars@ifremer.fr
    Purpose: exploit script data driven L2 to produce xspec
    Creation:  2016-10-19
    Arguments:    
    Outputs:     
    Dependencies:
    History: Based on Antoine Grouazel script to produce spectrum
    --------------------------------------------------------------
    Date:
    Author:
    Modification:
    --------------------------------------------------------------
'''
import os
import logging
import sys
import datetime
import glob
import shutil
import fnmatch
import collections
import matplotlib

import re
from dateutil import rrule

#from operator import or_

#sys.path.insert(0,'/home3/homedir7/perso/satwave/sources_en_exploitation/mpc-sentinel/mpc-sentinel/mpcsentinellibs/graphics')

#from shared_information import mission_start_s1a

mission_start_s1a = "20140101"
DIR_PROJECT = '/home/datawork-cersat-public/project/mpc-sentinel1/'
# rep_trigger = '/home/cercache/project/mpc-sentinel1/workspace/trigger_chains/xspec_L2_plots/'
rep_trigger = os.path.join(DIR_PROJECT,'workspace','trigger_chains','xspec_L2_plots')

SAT = {"S1A":'sentinel-1a',
       "S1B":'sentinel-1b',
       "S1C":'sentinel-1c',
       "S1D":'sentinel-1d',
       }
# ROOT_OUT = '/home/cercache/project/mpc-sentinel1/analysis/s1_data_analysis/L2_V2/'
ROOT_OUT = os.path.join(DIR_PROJECT,'analysis','s1_data_analysis','L2_%s')
# LOG_FOLDER = '/home/cercache/project/mpc-sentinel1/analysis/s1_data_analysis/L2_V2/LOGS/'
# LOG_FOLDER = os.path.join(DIR_PROJECT,'analysis','s1_data_analysis','L2_%s','LOGS')
LOG_FOLDER = os.path.join('/home1/scratch/satwave/s1_data_analysis','L2_%s','wavetoolbox-logs')
modes = {'SAT':['satfull','satreal','satimaginary'],'WW3':['ww3']}
fileprefix = {'satfull':'ocean_swell_spectra_',
              'satreal':'realcrossspectra_',
              'satimaginary':'imaginarycrossspectra_',
              'ww3':''
              }



def getoutputfolder(platform,version_outputdir):
    """
    #Return the basic folder where the listing will be stored
    args:
        version_outputdir (str): V2 for instance
    """
    return os.path.join(ROOT_OUT%version_outputdir,'WV',platform+'_WV_OCN__2S/')

productNamePattern = re.compile('.*\/(.*SAFE)\/')
def getoutputlines(inputfile,outputbase,source):
    """
    # return a list with lines that give elements to generate outputfile  
    # each line is composed with :
    #    -the input file complete path
    #    -the output file complete path
    #    -the mode to obtain it("representation used to show the spectrum)
    #
    # if more than one mode is possible one line will be generated for each one 
    #
    # inputfile: the NETCDF file that will be used to generate spectrum
    # outputfile: the png file that will be generated
    # mode: the mode that will be used to generate the png
    """
    linelist=[]
    m = re.search('-(\d{8})t(\d{6})',inputfile)
    if m is None:
        return linelist
    mydate = datetime.datetime.strptime(m.group(1),'%Y%m%d')
    year = str(mydate.year).zfill(4)
    doy = str(mydate.timetuple().tm_yday).zfill(3)
    outputname = os.path.basename(os.path.splitext(inputfile)[0])
    
    if 'ww3' in inputfile.lower() :
        foldername = 'ww3_corresponding_spectra'
        #08-12-2016 Will be changed when the .SAFE will be available
        outputfolder = os.path.join(outputbase,"ww3spectra",year,doy,foldername)
    else:
        foldername = 'xspec'
        safefolder = os.path.basename(os.path.dirname(os.path.dirname(inputfile)))
        outputfolder = os.path.join(outputbase,year,doy,safefolder,foldername)
        
    for mode in modes[source]:
        outputfile = os.path.join(outputfolder,fileprefix[mode]+outputname+'.png')
        linelist.append(inputfile+' '+outputfile+' '+mode)
    
    return linelist

def getinputfilelist(repdata,options,startdate,enddate,version_outputdir,write_to_file=True):
    """
    walk through the file system to get the measurement files
    #return a list 
    """
    if options.source == 'SAT': 
        repdata += 'WV/'+options.platform+"_WV_OCN__2S"
        pattern = options.platform.lower()+'*'+"ocn"+'*.nc'
    else: 
        pattern = options.source.lower()+'*'+"ocn"+'*.nc'
        
    logname = options.source+options.platform+'_'+startdate+'_'+enddate+'_onsea.log'

    startdate = datetime.datetime.strptime(startdate,'%Y%m%d')
    enddate = datetime.datetime.strptime(enddate,'%Y%m%d')
    enddate += datetime.timedelta(hours=23,minutes=59,seconds=59) #added agrouaze July 2020 to allow all acquisition of the last day given

    tifflist = []
    
    if write_to_file:
        logpath = LOG_FOLDER%version_outputdir+logname
        fid = open(logpath,'w')
    else:
        logpath = None
        
    listrepdatatype_date=[]
    #List the folders that may contains data
    if options.source == 'SAT': 
        for oneday in rrule.rrule(rrule.DAILY,dtstart=startdate,until=enddate):
            repdatatype_date = repdata
            year = str(oneday.year)
            dayofyear = str(oneday.timetuple().tm_yday).zfill(3)
            repdatatype_date = os.path.join(repdata,year,dayofyear+'/')
            listrepdatatype_date.append(repdatatype_date)    
                
    if options.source == 'WW3':
        #force to start at the month beginning to be sure not missing a month
        for oneday in rrule.rrule(rrule.MONTHLY,dtstart=startdate.replace(day=1),until=enddate):
            year = str(oneday.year)
            month = str(oneday.month).zfill(2)
            repdatatype_date = os.path.join(repdata,year+month,'netCDF_L2_daily'+'/')
            listrepdatatype_date.append(repdatatype_date) 
    #Walk throught the folder list to search corresponding files
    for repdatatype_date in listrepdatatype_date:    
        for root, dirnames, filenames in os.walk(repdatatype_date):
            for filename in fnmatch.filter(filenames,pattern):
                fullpath = os.path.join(root,filename)
                datesar = os.path.splitext(filename)[0].split('-')[4]
                datesar = datetime.datetime.strptime(datesar,'%Y%m%dt%H%M%S')
                if datesar>= startdate and datesar <= enddate:
                    tifflist.append(fullpath)
                    if write_to_file:
                        fid.write(fullpath+'\n')
    if write_to_file:
        fid.close()
        logging.info('output %s',logpath)
    #return logpath,tifflist
    return tifflist


def generatecommandline(options,listing_gogo):    
    """
    #Return the command line used to launch gogolist
    """
    #ssher script used to connect using ssh on distant computer
    ssher = '/home/losafe/users/agrouaze/PROGRAMMES/EXPLOIT/cron_ssher.py'
    script = '/home3/homedir7/perso/satwave/sources_en_exploitation/wavetoolbox/runner_plot_spectra_v2.bash'
    gogoscript = '/home/cersat5/tools/gogolist/bin/gogolist.py'
    
    options_gogo = "--mem=3G --name "+options.platform+"_WVL2_xspec --background --split-max-jobs 100 -e "+script+"--stdin "#+listing_gogo

    return "python "+ssher+" cerhouse1 "+gogoscript+' '+options_gogo


def getinputfolder(options):
    """
    #return the folder where to find netcdf files for the platform and the source
    """
    inputfolder = ""
    if options.source == 'SAT':
        inputfolder =  '/home/datawork-cersat-public/project/mpc-sentinel1/data/esa/'+SAT[options.platform]+'/L2/'
    elif options.source == "WW3":
        sat = options.platform.lower()
        inputfolder = '/home/datawork-cersat-public/project/mpc-sentinel1/data/colocation/'+SAT[options.platform]+'/sar-model/ww3spectra'
    elif options.source == "BUOY":
        inputfolder = ''
    return inputfolder

def parsearguments(parser):
    """
    # Parse the arguments and create an object options that contains it 
    """
    parser.add_option("-b","--beginningdate",
                        action="store", type="string",
                        dest="begdate", metavar="string",
                        help="starting date YYYYMMDD [default=begining of mission]")
    parser.add_option("-e","--endingdate",
                        action="store", type="string",
                        dest="enddate", metavar="string",
                        help="ending date YYYYMMDD [default=current day]")
    parser.add_option("-q","--quiet",
                        action="store_false", default=True,dest="quiet",
                        help="quiet mode [OPTIONAL default=True]")
    parser.add_option("-g","--gogolist",
                        action="store_false", default=True,dest="gogo",
                        help="turn off gogolist [OPTIONAL default=True]")
    parser.add_option("-p","--platform",
                        action="store", type="choice",choices=[uu for uu in SAT.keys()],
                        dest="platform",
                        help="satellite: S1A or S1B or ... [mandatory]")
    parser.add_option("-s","--source",
                        action="store", type="choice",choices=['SAT','WW3','BUOY'],
                        dest="source",
                        help="source: SAT or WW3 or BUOY [mandatory]")
    parser.add_option("-v","--verbose",
                      action='store_true',default=False,
                      help="verbose mode [default is quiet]")
    parser.add_option("-r","--redo-all",
                      action='store_true',default=False,dest='redo',
                      help="redo all plot even the one existing [default is to produce only the one missing]")
    parser.add_option("-o","--output-filepath",
                      action='store',type="string",
                      default=None,dest='outputFilepath',
                      help="the path where to generate the listing")
    parser.add_option("--output-folder",
                      action='store',type="string",
                      default=None,dest='outputFolder',
                      help="the path where you want the spectra to be generated")
    parser.add_option("--stdin",
                      action='store_true',
                      default=False,dest='stdin',
                      help="using stdin to generate the listing")
    parser.add_option("--version-outputdir",
                      action='store',
                      default='V2',
                      help="by default V2 but can be V3 etc ...")
    (options, args) = parser.parse_args()

    return options

#USELESS FOR NOW
#Return a Dict with the id as key and the L1 tiff path as value
def getl1indexeddictionary(startdate,enddate,platform):
    ci  = hbr.CersatIndexClient()
    
    index_name = platform+"WV.v3"
    
    tmpList = list(ci.getIndexFiles(index_name, start=startdate,stop=enddate, columns=['cf:fpath']))
    resultDict={}
    for element in tmpList:
        #Associate the Id to the filepath
        resultDict[element[0]]=element[1]['cf:fpath']
    return resultDict

#USELESS FOR NOW
def getkeyfromfilename(filename):

    if "sentinel-1b" in filename.lower():
        collection = "S1BWV.v3"
    else:
        collection = "S1AWV.v3"
            
    basename = os.path.basename(os.path.splitext(filename)[0])
    fname_fields = basename.split('-')
    
    quicklook_datetime = fname_fields[-5].replace('t', '')
    
    orbit = fname_fields[-3]
    fid = fname_fields[-2]
    inc = fname_fields[-1].split("_")[0]
    quicklook_id = "%s-%s-%s"%(orbit, fid, inc)
    
    #Construct key with the differents elements
    key = "%s#DT#%s#%s"%(collection, quicklook_datetime, quicklook_id)
    
    return key

def createlisting(options):
    cpt = collections.defaultdict(int)
    #Configure the logger -----------
    root = logging.getLogger()
    if root.handlers:
        for handler in root.handlers:
            root.removeHandler(handler) 
    if options.verbose:
        logging.basicConfig(level=logging.DEBUG)
    else:
        logging.basicConfig(level=logging.INFO)
    #----------------------------------- 
        
    #Configure the start and end date---------
    if options.enddate == None:
        enddate = datetime.datetime.now()
    else:
        enddate = datetime.datetime.strptime(options.enddate,'%Y%m%d')
    if options.begdate == None:
        startdate =  datetime.datetime.strptime(mission_start_s1a,'%Y%m%d')
    else:
        startdate = datetime.datetime.strptime(options.begdate,'%Y%m%d')
    logging.info('%s %s',enddate,startdate)
    enddate_str = datetime.datetime.strftime(enddate,'%Y%m%d')
    startdate_str = datetime.datetime.strftime(startdate,'%Y%m%d')
    #---------------------------------------
    if options.outputFolder is None:
        repout = getoutputfolder(options.platform,version_outputdir=options.version_outputdir)
    else:
        repout = options.outputFolder

    if not options.stdin:
        inputfolder = getinputfolder(options)
        logging.info('inputfolder = %s',inputfolder)

    logging.info('options.version_outputdir = %s',options.version_outputdir)
    if options.stdin:
        listnc = sys.stdin
    #get all the files that contains data for the specified interval
    else:
        listnc = getinputfilelist(inputfolder,options,startdate_str,enddate_str,version_outputdir=options.version_outputdir,write_to_file=True)
        logging.info('generate listing for L2 xspectrum from %s to %s',startdate,enddate)

    if "outputFilepath" in dir(options):
        if options.outputFilepath:
            listing_togenerate = options.outputFilepath
        else:
#             listing_togenerate = '/home/cercache/project/mpc-sentinel1/analysis/s1_data_analysis/L2_V2/listing/xspec_l2_'+startdate_str+'_'+enddate_str+'_'+options.platform+'_'+options.source+'.lst'
            listing_togenerate = os.path.join(DIR_PROJECT,'analysis','s1_data_analysis','L2_%s'%options.version_outputdir,'listing','xspec_l2_'+startdate_str+'_'+enddate_str+'_'+options.platform+'_'+options.source+'.lst')
            
    if os.path.exists(listing_togenerate):
        os.remove(listing_togenerate)
    dirpathlisting = os.path.dirname(listing_togenerate)
    if os.path.exists(dirpathlisting) is False:
        os.makedirs(dirpathlisting,0o0775)
        logging.info('create dir %s',dirpathlisting)
    fid = open(listing_togenerate,'w')
        #logging.info('%s nc found on period %s-%s',len(listnc),startdate,enddate)
    filecounteroutput = 0
     
    for lii,inputfile in enumerate(listnc):
        inputfile = inputfile.strip('\n')
        linestogenerate = getoutputlines(inputfile,repout,options.source)
        for currentline in linestogenerate:
            #Check if the file to generate exist or if the option redo is active
            if (os.path.exists(currentline.split()[1])==False) or options.redo:
                fid.write(currentline+"\n")
                filecounteroutput += 1
                cpt['to_be_generated'] +=1
            else:
                if os.path.exists(currentline.split()[1]):
                    cpt['already_exist'] +=1
        if lii%5000==0:
            logging.info('listing preparation %s/%s %s line  %s',lii,len(listnc),cpt,linestogenerate)
    logging.info('%s nc will have xspec plot generated for period %s-%s',filecounteroutput,startdate,enddate)
        
    fid.close()
    os.chmod(listing_togenerate,0o0775)
    logging.debug('listing gogo %s',listing_togenerate)
    logging.info('counter %s',cpt)
    return listing_togenerate

if __name__ == "__main__":
    from optparse import OptionParser
    
    #Get the options--------
    parser = OptionParser()
    options = parsearguments(parser)
    #-----------------------
    createlisting(options)
    #generate the command line that will be called
    #meta_sentence = generatecommandline(options, listing_togenerate)
    #logging.info('meta sentence %s',meta_sentence)
    #os.system(meta_sentence)
    

