import sys
import pdb
#from reportlab.lib.testutils import outputfile
sys.path.append('/home/satwave/sources_en_exploitation/mpc-sentinel/mpc-sentinel/mpcsentinellibs/indexation_in_hbase')
import hbaserequests as hbr
from msgpack import unpackb
import msgpack_numpy as msgnum
import datetime
from shapely.geometry import Polygon
import pandas as pd
from collections import defaultdict
import calendar
from dateutil.relativedelta import relativedelta
import os
import re
import pickle
from netCDF4 import Dataset
import numpy as np
import json
import collections

import time

import argparse

# sys.path.append('/home/mlebars/workspace/git/cerberecontrib-cfosat')

sys.path.append('/home/satwave/sources_en_exploitation/cerbere')
sys.path.append('/home/satwave/sources_en_exploitation/ceraux/ceraux')
import landmask

import phoenixdb
import phoenixdb.cursor

indexPhoenix = "INDEX_COLLECTIONFILES_PHX4"
urlDatabasePhoenix = "http://br156-175:8765"

ci = hbr.CersatIndexClient()

index_name = 'S1AWV.v3'
# For Test
#htmlOutputPathA = '/home/mlebars/workspace/developpements/Bazar/'
#htmlOutputPathB = '/home/mlebars/workspace/developpements/Bazar/'
htmlOutputPathA = '/home/cersat5/www/public/oceanwavesremotesensing/s1a/completion_checker/'
htmlOutputPathB = '/home/cersat5/www/public/oceanwavesremotesensing/s1b/completion_checker/'

platform = 'S1A'
platformB = 'S1B'

picklerPathA = '/home/cercache/users/mlebars/output_test/checkgeneration_result/picklers/sat-a'
picklerPathB = '/home/cercache/users/mlebars/output_test/checkgeneration_result/picklers/sat-b'

landmaskPath = '/home/satwave/sources_en_exploitation/fichier_references_cotes/PREVIMER_WW3-GLOBAL-30MIN_20160530T00Z.nc'

dictdbcolnames = {'L1': 'cf:fpath',
                  'roughness': 'cf:ql:l1_roughness',
                  'a': {'ww3_V2': 'cf:ql:' + platform + '_WV_OCN__2S_ww3' + '_V2',
                        'imaginary_V2': 'cf:ql:' + platform + '_WV_OCN__2S_imaginarycrossspectra_' + platform.lower() + '_V2',
                        'real_V2': 'cf:ql:' + platform + '_WV_OCN__2S_realcrossspectra_' + platform.lower() + '_V2',
                        'full_V2': 'cf:ql:' + platform + '_WV_OCN__2S_ocean_swell_spectra_' + platform.lower() + '_V2'

                        }, 'b': {'ww3_V2': 'cf:ql:' + platformB + '_WV_OCN__2S_ww3' + '_V2',
                                 'imaginary_V2': 'cf:ql:' + platformB + '_WV_OCN__2S_imaginarycrossspectra_' + platformB.lower() + '_V2',
                                 'real_V2': 'cf:ql:' + platformB + '_WV_OCN__2S_realcrossspectra_' + platformB.lower() + '_V2',
                                 'full_V2': 'cf:ql:' + platformB + '_WV_OCN__2S_ocean_swell_spectra_' + platformB.lower() + '_V2'
                                 }}


# Retrieve data indexed in hbase
def getPathListInIndex(month_to_check, satellite='a'):

    ql_list = [dictdbcolnames['L1'], dictdbcolnames['roughness'],
               dictdbcolnames[satellite]['ww3_V2'],
               dictdbcolnames[satellite]['imaginary_V2'],
               dictdbcolnames[satellite]['real_V2'],
               dictdbcolnames[satellite]['full_V2']]

    print("Retrieve data from DB")
    index_name = 'S1' + satellite.upper() + 'WV.v3'
    start_date = datetime.datetime.strptime(month_to_check, '%Y%m')
    # The end_date is also included that's why one day is substracted
    end_date = start_date + relativedelta(months=+1) - relativedelta(days=+1)

    # avoid the future days if month is not finished yet
    if start_date > datetime.datetime.today():
        start_date = datetime.datetime.today()

    elementsIndexedInDatabase = defaultdict(list)

    flist = list(ci.getIndexFiles(index_name, start=start_date.strftime(
        '%Y%m%d'), stop=end_date.strftime('%Y%m%d'), columns=['cf:']))

    MISSING_FDATE = 0
    for r in flist:
        for qlcolname in ql_list:
            if qlcolname in r[1] and r[1][qlcolname] != 'unknown':
                elementsIndexedInDatabase[qlcolname].append(r[1][qlcolname])
    return elementsIndexedInDatabase


# Retrieve data indexed in hbase
def getPathListInPhoenix(month_to_check, satellite='a'):

    start_date = datetime.datetime.strptime(month_to_check, '%Y%m')
    # The end_date is not also included with phoenix
    end_date = start_date + relativedelta(months=+1)

    # avoid the future days if month is not finished yet
    if start_date > datetime.datetime.today():
        start_date = datetime.datetime.today()

    conn = phoenixdb.connect(urlDatabasePhoenix, autocommit=True)
    cursor = conn.cursor()
    myRequest = "SELECT FNAME FROM " + indexPhoenix
    whereClause = " WHERE FDATEDT BETWEEN TO_TIMESTAMP('" + str(
        start_date) + "','yyyy-MM-dd') AND TO_TIMESTAMP('" + str(end_date) + "','yyyy-MM-dd')"
    if satellite == 'a':
        whereClause += " AND SENSOR IN('S1A') "
    elif satellite == 'b':
        whereClause += " AND SENSOR IN('S1B') "

    myRequest += whereClause
    print myRequest
    cursor.execute(myRequest)
    elementsIndexed = cursor.fetchall()

    tmpTab = []
    # Transforme les tuple de fname en donnees simple en retirant l'extension
    for tmpValue in elementsIndexed:
        tmpTab.append(os.path.splitext(tmpValue[0])[0])

    elementsIndexed = list(tmpTab)
    return elementsIndexed


# Will search throught all the folder for the specified month
# The day before and after will be checked to get missing elements
# the first and the last day will be checked to avoid other month elements
def getListFromPNGOnHardDrive(month_to_check, satellite='a'):
    originaldate = datetime.datetime.strptime(month_to_check, '%Y%m')

    # Pattern used in beginning and ending of month to get only concerned data
    searchpattern = str(originaldate.year) + str(originaldate.month).zfill(2) + \
        "[0-9]{2}t[0-9]{6}-" + str(originaldate.year)

    replacestring = 's1' + satellite.lower() + "-"

    currentdate = originaldate + relativedelta(months=+1)
    finishdate = originaldate - relativedelta(days=+1)
    print("getListFromPNGOnHardDrive", currentdate, finishdate)

    # avoid the future days if month is not finished yet
    if currentdate > datetime.datetime.today():
        currentdate = datetime.datetime.today()

    elementsOnHardDrive = defaultdict(list)
    fullPath = defaultdict(list)

    while currentdate >= finishdate:

        year = str(currentdate.year)
        dayofyear = str(currentdate.timetuple().tm_yday).zfill(3)
        basedir = '/home/cercache/project/mpc-sentinel1/analysis/s1_data_analysis/L2_V2/WV/S1' + \
            satellite.upper() + '_WV_OCN__2S/'
        mydir = basedir + year + '/' + dayofyear + '/'
        ww3dir = basedir + 'ww3spectra/' + year + '/' + dayofyear + '/'

        # check if it is an extremity to retrieve only data for the asked month
        if currentdate.day == 1 or currentdate.day == (calendar.monthrange(currentdate.year, currentdate.month)[1]):
            notAnExtremity = False
        # if not an extremity, no need to complexify treatement, take all elements
        else:
            notAnExtremity = True

        for root, dirs, files in os.walk(mydir):
            for file in files:
                if notAnExtremity or re.search(searchpattern, file):

                    # remove prefix and extension to have the same baseName
                    if file.startswith("imaginarycrossspectra") and file.endswith(".png"):
                        # add the fileNameBase
                        m = re.search('(s1.*)(\.png)', file)
                        elementsOnHardDrive[dictdbcolnames[satellite]['imaginary_V2']].append(
                            m.group(1))
                        fullPath[dictdbcolnames[satellite]['imaginary_V2']].append(
                            os.path.join(root, file))
                    if file.startswith("realcrossspectra") and file.endswith(".png"):
                        # add the fileNameBase
                        m = re.search('(s1.*)(\.png)', file)
                        elementsOnHardDrive[dictdbcolnames[satellite]['real_V2']].append(
                            m.group(1))
                        fullPath[dictdbcolnames[satellite]['real_V2']].append(
                            os.path.join(root, file))
                    if file.startswith("ocean_swell_spectra") and file.endswith(".png"):
                        # add the fileNameBase
                        m = re.search('(s1.*)(\.png)', file)
                        elementsOnHardDrive[dictdbcolnames[satellite]['full_V2']].append(
                            m.group(1))
                        fullPath[dictdbcolnames[satellite]['full_V2']].append(
                            os.path.join(root, file))

        for root, dirs, files in os.walk(ww3dir):
            for file in files:
                if notAnExtremity or re.search(searchpattern, file):
                    if file.startswith("ww3") and file.endswith(".png"):
                        # add the fileNameBase --> change ww3 to s1a to have the exact same name
                        filename = os.path.splitext(file)[0]
                        elementsOnHardDrive[dictdbcolnames[satellite]['ww3_V2']].append(
                            filename.replace('ww3-', replacestring))
                        fullPath[dictdbcolnames[satellite]['ww3_V2']].append(
                            os.path.join(root, file))

        # change the date
        currentdate = currentdate - relativedelta(days=+1)
    return elementsOnHardDrive, fullPath

# The day before and after will be checked to get missing elements
# the first and the last day will be checket to avoid other month elements


def getListFromNCOnHardDrive(month_to_check, satellite='a'):

    basedir = '/home/cercache/project/mpc-sentinel1/data/esa/sentinel-1' + \
        satellite + '/L2/WV/S1' + satellite.upper() + '_WV_OCN__2S/'

    originaldate = datetime.datetime.strptime(month_to_check, '%Y%m')
    # Pattern used in beginning and ending of month to get only concerned data
    searchpattern = str(originaldate.year) + str(originaldate.month).zfill(2) + \
        "[0-9]{2}t[0-9]{6}-" + str(originaldate.year)

    currentdate = originaldate + relativedelta(months=+1)
    finishdate = originaldate - relativedelta(days=+1)

    print("getListFromNCOnHardDrive", currentdate, finishdate)

    # avoid the future days if month is not finished yet
    if currentdate > datetime.datetime.today():
        currentdate = datetime.datetime.today()

    ncFiles = []
    ncSafeFolders = []
    ncFullPath = {}
    while currentdate >= finishdate:

        year = str(currentdate.year)
        dayofyear = str(currentdate.timetuple().tm_yday).zfill(3)

        if currentdate.day == 1 or currentdate.day == (calendar.monthrange(currentdate.year, currentdate.month)[1]):
            notAnExtremity = False
        # if not an extremity, no need to complexify treatement, take all elements
        else:
            notAnExtremity = True

        mydir = basedir + year + '/' + dayofyear + '/'
        for root, dirs, files in os.walk(mydir):
            for file in files:
                if notAnExtremity or re.search(searchpattern, file):
                    # remove prefix and extension to have the same baseName
                    if file.startswith("s1") and file.endswith(".nc"):

                        regexpResult = re.search('(\w+\.SAFE)', root)
                        safeName = regexpResult.group(1)
                        if not safeName in ncSafeFolders:
                            ncSafeFolders.append(safeName)
                        # add the fileNameBase
                        m = re.search('(s1.*)(\.nc)', file)
                        ncFiles.append(m.group(1))
                        ncFullPath[m.group(1)] = os.path.join(root, file)
        currentdate = currentdate - relativedelta(days=+1)

    return ncFiles, ncFullPath, ncSafeFolders

# Retrieve NC for the WW3 elements and check elements on earth
def getListFromWW3OnHardDrive(month_to_check, satellite='a', fullNCLatLonOverwrite=False):

    if satellite == 'a':
        picklerPath = picklerPathA
    elif satellite == 'b':
        picklerPath = picklerPathB

    currentdate = datetime.datetime.strptime(month_to_check, '%Y%m')
    replacestring = 's1' + satellite.lower() + "-"
    print("getListFromWW3OnHardDrive", month_to_check)

    #mylandmask = landmask.Landmask(filename='/home/oo12/oo/modeles_previmer/ww3/GLOBAL-30MIN/best_estimate/2016/PREVIMER_WW3-GLOBAL-30MIN_20160530T00Z.nc')
    #mylandmask = landmask.Landmask(filename=landmaskPath)

    ncFiles = []
    ncFilesOnWater = []
    ncFullPath = {}
    ncLatLon = {}
    year = str(currentdate.year)
    month = str(currentdate.month).zfill(2)

    basedir = '/home/cercache/project/mpc-sentinel1/data/colocation/sentinel-1' + \
        satellite + '/sar-model/ww3spectra/' + year + month + '/netCDF_L2'

    cursor = 0
    # IncorrectLatLon=0

    #latLonPicklePath = picklerPath+"/"+month_to_check+"www3NCLatLonList.p"

    # If file does not exist or overwrite mode create a new dictionary else load data from pickle
    # if fullNCLatLonOverwrite or not os.path.exists(latLonPicklePath):
    #    ncLatLon={}
    # else:
    #    ncLatLon = pickle.load(open(latLonPicklePath,"rb"))

    for root, dirs, files in os.walk(basedir):
        for file in files:
            if file.startswith("ww3") and file.endswith(".nc"):
                # print("reading",file)
                # add the fileNameBase --> change ww3 to s1a to have the exact same name
                filename = file.replace('ww3-', replacestring)

                m = re.search('(s1.*)(\.nc)', filename)
                ncFiles.append(m.group(1))
                ncFullPath[m.group(1)] = os.path.join(root, file)

                # if m.group(1) in ncLatLon:
                #    lat,lon = ncLatLon[m.group(1)].split('_')
                # else:
                #    fileToRead = Dataset(os.path.join(root,file), 'r')
                #    try:
                #        lon =  np.squeeze(fileToRead.variables['lon'][:])
                #        lat =  np.squeeze(fileToRead.variables['lat'][:])
                #        ncLatLon[m.group(1)] = str(float(lat)) + '_' + str(float(lon))
                #
                #    except TypeError:
                #        IncorrectLatLon+=1

                # Check if it's on the sea
                # try:
                #    lon = float(lon)
                #    lat = float(lat)
                #    if not mylandmask.is_land(lon=lon,lat=lat):
                #        ncFilesOnWater.append(m.group(1))
                # except TypeError:
                #    IncorrectLatLon+=1
    # if IncorrectLatLon>0:
    #    print("Fichiers avec lat/lon incorrects:",IncorrectLatLon)

    # pickle.dump(ncLatLon,open(latLonPicklePath,"wb"))
    # return ncFiles,ncFullPath,ncFilesOnWater
    return ncFiles, ncFullPath


import glob


def getAllListNCFromMPCListing(outputdir, satellites=['S1A', 'S1B']):
    # outputdir  --> For the moment outputdir is /home/cercache/project/mpc-sentinel1/workspace/MPC-CC_archive_listings/

    mpcSafeDictPicklePath = picklerPathA + "/" + "allListNCFromMPC.p"

    # If file does not exist or overwrite mode create a new dictionary else load data from pickle
    if not os.path.exists(mpcSafeDictPicklePath):
        mpcSafeDict = {
            'S1A': {'L0': {}, 'L1': {}, 'L2': {}},
            'S1B': {'L0': {}, 'L1': {}, 'L2': {}}
        }
    else:
        mpcSafeDict = pickle.load(open(mpcSafeDictPicklePath, "rb"))

    # read the latest file
    mpc_csv_listing = glob.glob(outputdir + '*.csv')
    print('Nber of csv found: %s', len(mpc_csv_listing))
    mostRecentDate = datetime.datetime(1900, 1, 1)
    # csv give full informations since the mission beginning
    # Search the most recent csv
    for current_csv in mpc_csv_listing:
        currentElementDate = datetime.datetime.strptime(os.path.basename(current_csv).split(
            '_')[3] + os.path.basename(current_csv).split('_')[4].replace('.csv', ''), '%Y%m%d%H%M%S')
        if currentElementDate > mostRecentDate:
            most_recent_listing = current_csv
            mostRecentDate = currentElementDate
    #print('latest csv: %s',most_recent_listing)

    # most_recent_listing=os.path.join(outputdir,'mpc_product_list_20170124_063002.csv')

    fid = open(most_recent_listing)
    data_mpc = fid.readlines()
    fid.close()
    # remove header To get only Data
    data_mpc = data_mpc[1:]
    print('Nber of SAFE at CLS: %s', len(data_mpc))

    list_safe_cls = []

    for currentLine in data_mpc:
        fname = currentLine.split(';')[3]
        missionId = currentLine.split(';')[4]
        acquisition_start_date = datetime.datetime.strptime(
            currentLine.split(';')[12], '%Y-%m-%d')
        processing_level = 'L' + currentLine.split(';')[9]
        monthDate = str(acquisition_start_date.year) + \
            str(acquisition_start_date.month).zfill(2)
        if (not (monthDate in mpcSafeDict[missionId][processing_level])):
            mpcSafeDict[missionId][processing_level][monthDate] = []
        if (not (fname in mpcSafeDict[missionId][processing_level][monthDate])):
            mpcSafeDict[missionId][processing_level][monthDate].append(fname)

    pickle.dump(mpcSafeDict, open(mpcSafeDictPicklePath, "wb"))
    return mpcSafeDict


def getFilesFromTrack(month_to_check, satellite='a'):

    trackfile = '/home/cercache/project/mpc-sentinel1/data/colocation/sentinel-1' + satellite + '/sar-model/ww3spectra/' + \
        month_to_check + '/trackfile_S1' + satellite.upper() + '_L2_' + month_to_check + \
        '_original_sar_info.txt.from_index'
    if not os.path.exists(trackfile):
        trackfileList = glob.glob('/home/cercache/project/mpc-sentinel1/data/colocation/sentinel-1' + satellite +
                                  '/sar-model/ww3spectra/' + month_to_check + '/S1' + satellite.upper() + '_L2*' + month_to_check + '*withname.txt')
        if len(trackfileList) > 0:
            trackfile = trackfileList[0]
    ncFilesFromTrack = []
    ncFilesFromTrackOnWater = []
    isFirstLine = True
    IncorrectLatLon = 0
    mylandmask = landmask.Landmask(landmaskPath)

    if os.path.exists(trackfile):
        with open(trackfile) as inputfile:
            for currentline in inputfile:
                # Code to Ignore First Line(Header)
                if isFirstLine:
                    isFirstLine = False
                    continue
                tmpSplit = currentline.split(" ")
                currentValue = tmpSplit[4]
                lon = tmpSplit[2]
                lat = tmpSplit[3]

                # Remove extension if existing
                if currentValue.endswith(".nc"):
                    currentValue = os.path.splitext(currentValue)[0]
                ncFilesFromTrack.append(currentValue)
                try:
                    lon = float(lon)
                    lat = float(lat)
                    if not mylandmask.is_land(lon=lon, lat=lat):
                        ncFilesFromTrackOnWater.append(currentValue)
                except TypeError:
                    IncorrectLatLon += 1
        if IncorrectLatLon > 0:
            print("Fichiers avec lat/lon incorrects:", IncorrectLatLon)
    return ncFilesFromTrack, ncFilesFromTrackOnWater


def getNCFileNameFromCalvalIndex(month_to_check, satellite='a'):

    #ci = hbr.CersatIndexClient()
    if satellite.upper() == 'A':
        index_name = 'FRED/S1A_WV_SL2_OCN/calval1d/v1'
    else:
        index_name = 'FRED/S1B_WV_SL2_OCN/calval1d/v2'
    params = ['cf:osw','cf:ww3']
    
    #start_date = datetime.datetime.strptime('20180122', '%Y%m%d')
    start_date = datetime.datetime.strptime(month_to_check, '%Y%m')
    # The end_date is also included that's why one day is substracted
    end_date = start_date + relativedelta(months=+1) - relativedelta(days=+1)
    #end_date = start_date

    # avoid the future days if month is not finished yet
    if start_date > datetime.datetime.today():
        start_date = datetime.datetime.today()

    indexElementInCalvalList = list(ci.getIndexFiles(index_name, start=start_date.strftime(
        '%Y%m%d'), stop=end_date.strftime('%Y%m%d'), columns=params))

    cursor = 0
    geolocalizedElementsFromCalvalIndex = []
    notGeolocalizedElementsFromCalvalIndex = []
    WW3IndexedInCalvalIndex = []
    
    while cursor < len(indexElementInCalvalList):
        currentValuesOsw = unpackb(indexElementInCalvalList[cursor][1]['cf:osw'])
        if 'cf:ww3' in indexElementInCalvalList[cursor][1]:
            currentValuesWW3 = unpackb(indexElementInCalvalList[cursor][1]['cf:ww3'])
            if len(currentValuesWW3)>0:
                WW3IndexedInCalvalIndex.append(os.path.splitext(indexElementInCalvalList[cursor][0].split('#')[-1])[0])
        
        if currentValuesOsw.has_key('oswLat') and currentValuesOsw.has_key('oswLon'):
            # Retrieve the name of the NCFile and remove it's extension like it will be in the trackfile
            geolocalizedElementsFromCalvalIndex.append(os.path.splitext(indexElementInCalvalList[cursor][0].split('#')[-1])[0])
        else:
            notGeolocalizedElementsFromCalvalIndex.append(os.path.splitext(
                indexElementInCalvalList[cursor][0].split('#')[-1])[0])
        cursor += 1

    return geolocalizedElementsFromCalvalIndex, notGeolocalizedElementsFromCalvalIndex,WW3IndexedInCalvalIndex


def getListFromLevel1XmlOnHardDrive(month_to_check, satellite='a'):

    basedir = '/home/cercache/project/mpc-sentinel1/data/esa/sentinel-1' + \
        satellite + '/L1/WV/S1' + satellite.upper() + '_WV_SLC__1S/'

    originaldate = datetime.datetime.strptime(month_to_check, '%Y%m')
    # Pattern used in beginning and ending of month to get only concerned data
    searchpattern = str(originaldate.year) + str(originaldate.month).zfill(2) + \
        "[0-9]{2}t[0-9]{6}-" + str(originaldate.year)

    currentdate = originaldate + relativedelta(months=+1)
    finishdate = originaldate - relativedelta(days=+1)

    print("getListFromLevel1XmlOnHardDrive", currentdate, finishdate)

    # avoid the future days if month is not finished yet
    if currentdate > datetime.datetime.today():
        currentdate = datetime.datetime.today()

    tiffFilePath = []
    baseName = []
    xmlFullPathDict = {}
    while currentdate >= finishdate:

        year = str(currentdate.year)
        dayofyear = str(currentdate.timetuple().tm_yday).zfill(3)

        if currentdate.day == 1 or currentdate.day == (calendar.monthrange(currentdate.year, currentdate.month)[1]):
            notAnExtremity = False
        # if not an extremity, no need to complexify treatement, take all elements
        else:
            notAnExtremity = True

        mydir = basedir + year + '/' + dayofyear + '/'
        for root, dirs, files in os.walk(mydir):
            for file in files:
                if notAnExtremity or re.search(searchpattern, file):
                    # remove prefix and extension to have the same baseName
                    if file.startswith("s1") and file.endswith(".xml") and os.path.basename(root) == "annotation":
                        # add the fileNameBase
                        baseName.append(os.path.splitext(file)[0])
                        currentValue = os.path.join(root.replace("/annotation", "/measurement"), file.replace('.xml', '.tiff'))
                        tiffFilePath.append(currentValue)
                        #xmlFullPathDict[os.path.splitext(file)[0]] = currentValue
                        xmlFullPathDict[os.path.splitext(file)[0]] = os.path.join(root, file)
        currentdate = currentdate - relativedelta(days=+1)
    return tiffFilePath, baseName, xmlFullPathDict


def getListFromLevel1RoughnessOnHardDrive(month_to_check, satellite='a'):

    basedir = '/home/cercache/project/mpc-sentinel1/analysis/s1_data_analysis/L1/WV/S1' + \
        satellite.upper() + '_WV_SLC__1S/'
    originaldate = datetime.datetime.strptime(month_to_check, '%Y%m')
    # Pattern used in beginning and ending of month to get only concerned data
    searchpattern = str(originaldate.year) + str(originaldate.month).zfill(2) + \
        "[0-9]{2}t[0-9]{6}-" + str(originaldate.year)

    currentdate = originaldate + relativedelta(months=+1)
    finishdate = originaldate - relativedelta(days=+1)

    print("getListFromLevel1RoughnessOnHardDrive", currentdate, finishdate)

    # avoid the future days if month is not finished yet
    if currentdate > datetime.datetime.today():
        currentdate = datetime.datetime.today()

    baseName = []
    filePath = []
    roughnessDict = {}
    # ncFullPath={}
    while currentdate >= finishdate:

        year = str(currentdate.year)
        dayofyear = str(currentdate.timetuple().tm_yday).zfill(3)

        if currentdate.day == 1 or currentdate.day == (calendar.monthrange(currentdate.year, currentdate.month)[1]):
            notAnExtremity = False
        # if not an extremity, no need to complexify treatement, take all elements
        else:
            notAnExtremity = True

        mydir = basedir + year + '/' + dayofyear + '/'
        for root, dirs, files in os.walk(mydir):
            for file in files:
                if notAnExtremity or re.search(searchpattern, file):
                    # remove prefix and extension to have the same baseName
                    if file.endswith("roughness.png") and os.path.basename(root) == "roughness":
                        # add the fileNameBase
                        filePath.append(os.path.join(root, file))
                        baseName.append(file.replace("-roughness.png", ""))
                        roughnessDict[file.replace(
                            "-roughness.png", "")] = os.path.join(root, file)
        currentdate = currentdate - relativedelta(days=+1)
    return filePath, baseName, roughnessDict

# The write Functions Get The data return it and write it in a pickle on harddrive


def writeListFromPNGOnHardDrive(datemonth, picklerPath, satellite='a'):
    pngBaseList, pngFullPathList = getListFromPNGOnHardDrive(
        datemonth, satellite)
    pickle.dump(pngBaseList, open(picklerPath + "/" +
                                  datemonth + "pngBaseList.p", "wb"))
    pickle.dump(pngFullPathList, open(picklerPath + "/" +
                                      datemonth + "pngFullPathList.p", "wb"))
    return pngBaseList, pngFullPathList


def writeListFromNCOnHardDrive(datemonth, picklerPath, satellite='a'):
    ncBaseList, ncFullPathList, ncSafeFolders = getListFromNCOnHardDrive(
        datemonth, satellite)
    pickle.dump(ncBaseList, open(picklerPath + "/" +
                                 datemonth + "ncBaseList.p", "wb"))
    pickle.dump(ncFullPathList, open(picklerPath + "/" +
                                     datemonth + "ncFullPathList.p", "wb"))
    pickle.dump(ncSafeFolders, open(picklerPath + "/" +
                                    datemonth + "ncSafeFolders.p", "wb"))

    return ncBaseList, ncFullPathList


def writeListFromWW3OnHardDrive(datemonth, picklerPath, satellite='a'):
    #ww3NcBaseList,ww3ncFullPathList,ww3NcFilesOnWater = getListFromWW3OnHardDrive(datemonth,satellite)
    ww3NcBaseList, ww3ncFullPathList = getListFromWW3OnHardDrive(
        datemonth, satellite)
    pickle.dump(ww3NcBaseList, open(picklerPath + "/" +
                                    datemonth + "ww3NcBaseList.p", "wb"))
    pickle.dump(ww3ncFullPathList, open(picklerPath + "/" +
                                        datemonth + "ww3ncFullPathList.p", "wb"))
    # pickle.dump(ww3NcFilesOnWater,open(picklerPath+"/"+datemonth+"ww3NcFilesOnWater.p","wb"))
    # return ww3NcBaseList,ww3ncFullPathList,ww3NcFilesOnWater
    return ww3NcBaseList, ww3ncFullPathList


def writeFilesFromTrack(datemonth, picklerPath, satellite='a'):
    ncBaseFromTrack, ncFilesFromTrackOnWater = getFilesFromTrack(
        datemonth, satellite)
    pickle.dump(ncBaseFromTrack, open(picklerPath + "/" +
                                      datemonth + "ncBaseFromTrack.p", "wb"))
    pickle.dump(ncFilesFromTrackOnWater, open(picklerPath + "/" +
                                              datemonth + "ncBaseFromTrackOnWater.p", "wb"))
    return ncBaseFromTrack, ncFilesFromTrackOnWater


def writePathListInIndex(datemonth, picklerPath, satellite='a'):
    indexedFullPathList = getPathListInIndex(datemonth, satellite)
    pickle.dump(indexedFullPathList, open(picklerPath + "/" +
                                          datemonth + "indexedFullPathList.p", "wb"))
    return indexedFullPathList


def writePathListInPhoenix(datemonth, picklerPath, satellite='a'):
    indexedPhoenixNCBaseList = getPathListInPhoenix(datemonth, satellite)
    print "len(indexedPhoenixNCBaseList):", len(indexedPhoenixNCBaseList)
    pickle.dump(indexedPhoenixNCBaseList, open(
        picklerPath + "/" + datemonth + "indexedPhoenixNCBaseList.p", "wb"))
    print "after dump", picklerPath, datemonth
    return indexedPhoenixNCBaseList


def writeListFromLevel1XmlOnHardDrive(datemonth, picklerPath, satellite='a'):
    filePath, baseName, xmlFullPathDict = getListFromLevel1XmlOnHardDrive(
        datemonth, satellite)
    pickle.dump(filePath, open(picklerPath + "/" +
                               datemonth + "L1XmlFullPathList.p", "wb"))
    pickle.dump(baseName, open(picklerPath + "/" +
                               datemonth + "L1XmlBaseNameList.p", "wb"))
    pickle.dump(xmlFullPathDict, open(picklerPath + "/" +
                                      datemonth + "L1XmlFullPathDict.p", "wb"))
    return baseName, filePath, xmlFullPathDict


def writeListFromLevel1RoughnessOnHardDrive(datemonth, picklerPath, satellite='a'):
    filePath, baseName, roughnessDict = getListFromLevel1RoughnessOnHardDrive(
        datemonth, satellite)
    pickle.dump(filePath, open(picklerPath + "/" +
                               datemonth + "L1RoughnessFullPathList.p", "wb"))
    pickle.dump(baseName, open(picklerPath + "/" +
                               datemonth + "L1roughnessBaseNameList.p", "wb"))
    pickle.dump(roughnessDict, open(picklerPath + "/" +
                                    datemonth + "L1RoughnessFullPathDict.p", "wb"))
    return baseName, filePath, roughnessDict


def writeNCFileNameFromCalvalIndex(datemonth, picklerPath, satellite='a'):
    geolocalizedElementsFromCalvalIndex, notGeolocalizedElementsFromCalvalIndex,WW3IndexedInCalvalIndex = getNCFileNameFromCalvalIndex(datemonth, satellite)
    pickle.dump(geolocalizedElementsFromCalvalIndex, open(picklerPath + "/" + datemonth + "geolocalizedElementsFromCalvalIndex.p", "wb"))
    pickle.dump(notGeolocalizedElementsFromCalvalIndex, open(picklerPath + "/" + datemonth + "notGeolocalizedElementsFromCalvalIndex.p", "wb"))
    pickle.dump(WW3IndexedInCalvalIndex, open(picklerPath + "/" + datemonth + "WW3IndexedInCalvalIndex.p", "wb"))


    return geolocalizedElementsFromCalvalIndex, notGeolocalizedElementsFromCalvalIndex,WW3IndexedInCalvalIndex


def writeAllData(datemonth, picklerPath, satellite='a', showData=True):
    pngBaseList, pngFullPathList = writeListFromPNGOnHardDrive(
        datemonth, picklerPath, satellite)
    ncBaseList, ncFullPathList = writeListFromNCOnHardDrive(
        datemonth, picklerPath, satellite)
    #ww3NcBaseList,ww3NcFullPathList,ww3NcFilesOnWater = writeListFromWW3OnHardDrive(datemonth,picklerPath,satellite)
    ww3NcBaseList, ww3NcFullPathList = writeListFromWW3OnHardDrive(
        datemonth, picklerPath, satellite)
    ncBaseFromTrack, ncFilesFromTrackOnWater = writeFilesFromTrack(
        datemonth, picklerPath, satellite)
    indexedFullPathList = writePathListInIndex(
        datemonth, picklerPath, satellite)
    indexedInPhoenixNCBaseList = writePathListInPhoenix(
        datemonth, picklerPath, satellite)
    l1XmlBaseList, l1XmlFullPathList, L1XmlFullPathDict = writeListFromLevel1XmlOnHardDrive(
        datemonth, picklerPath, satellite)
    l1RoughnessBaseList, l1RoughnessFullPathList, l1RoughnessFullPathDict = writeListFromLevel1RoughnessOnHardDrive(
        datemonth, picklerPath, satellite)
    geolocalizedElementsFromCalvalIndex, notGeolocalizedElementsFromCalvalIndex,WW3IndexedInCalvalIndex = writeNCFileNameFromCalvalIndex(
        datemonth, picklerPath, satellite)
    
    listElementsInError(datemonth, picklerPath, satellite)

##################################################
##############  PART FOR IPF   ###################
##################################################

def writeAllDataIPF(ncfolder,pngfolder,version, picklerPath,start_date,end_date,satellite='a',calvalIndex=None):
    #NCFiles
    ncBaseList, ncFullPathList = getListFromNCOnHardDriveBlock(ncfolder, satellite)
    
    pickle.dump(ncBaseList, open(picklerPath + "/" + version + "ncBaseList.p", "wb"))
    pickle.dump(ncFullPathList, open(picklerPath + "/" + version + "ncFullPathList.p", "wb"))
    
    #PNGFiles
    pngBaseList, pngFullPathList = getListFromPNGOnHardDriveBlock(pngfolder, satellite)
    
    pickle.dump(pngBaseList, open(picklerPath + "/" + version + "pngBaseList.p", "wb"))
    pickle.dump(pngFullPathList, open(picklerPath + "/" + version + "pngFullPathList.p", "wb"))
    
    ql_list = [dictdbcolnames[satellite]['imaginary_V2']+version,
        dictdbcolnames[satellite]['real_V2']+version,
        dictdbcolnames[satellite]['full_V2']+version]

    indexedFullPathList = getPathListInIndexHbaseBlock(start_date,end_date,satellite,ql_list)
    pickle.dump(indexedFullPathList, open(picklerPath + "/" + version + "indexedFullPathList.p", "wb"))
    
    indexedInPhoenixNCBaseList = getPathListInPhoenixBlock(start_date,end_date,satellite)
    pickle.dump(indexedInPhoenixNCBaseList, open(picklerPath + "/" + version + "indexedInPhoenixNCBaseList.p", "wb"))

    geolocalizedElementsFromCalvalIndex, notGeolocalizedElementsFromCalvalIndex = getNCFileNameFromCalvalIndexBlock(start_date,end_date, satellite,calvalIndex)
    pickle.dump(geolocalizedElementsFromCalvalIndex, open(picklerPath + "/" + version + "geolocalizedElementsFromCalvalIndex.p", "wb"))
    pickle.dump(notGeolocalizedElementsFromCalvalIndex, open(picklerPath + "/" + version + "notGeolocalizedElementsFromCalvalIndex.p", "wb"))

def getListFromNCOnHardDriveBlock(folder, satellite='a'):
        
    ncFiles = []
    ncFullPath = {}
    for root, dirs, files in os.walk(folder):
        for file in files:
            if file.startswith('s1'+satellite) and file.endswith(".nc"):
                if root.split('/')[-1]=='measurement':
                    # add the fileNameBase
                    m = re.search('(s1.*)(\.nc)', file)
                    ncFiles.append(m.group(1))
                    ncFullPath[m.group(1)] = os.path.join(root, file)
    
    return ncFiles, ncFullPath
def getListFromPNGOnHardDriveBlock(pngFolder, satellite='a'):
    
    elementsOnHardDrive = defaultdict(list)
    fullPath = defaultdict(list)
    key_sat = '(s1'+satellite+'.*)(\.png)'
    for root, dirs, files in os.walk(pngFolder):
        for file in files:

            # remove prefix and extension to have the same baseName
            if file.startswith("imaginarycrossspectra") and file.endswith(".png"):
                # add the fileNameBase
                m = re.search(key_sat, file)
                elementsOnHardDrive[dictdbcolnames[satellite]['imaginary_V2']].append(m.group(1))
                fullPath[dictdbcolnames[satellite]['imaginary_V2']].append(os.path.join(root, file))
            if file.startswith("realcrossspectra") and file.endswith(".png"):
                # add the fileNameBase
                m = re.search(key_sat, file)
                elementsOnHardDrive[dictdbcolnames[satellite]['real_V2']].append(m.group(1))
                fullPath[dictdbcolnames[satellite]['real_V2']].append(os.path.join(root, file))
            if file.startswith("ocean_swell_spectra") and file.endswith(".png"):
                # add the fileNameBase
                m = re.search(key_sat, file)
                elementsOnHardDrive[dictdbcolnames[satellite]['full_V2']].append(m.group(1))
                fullPath[dictdbcolnames[satellite]['full_V2']].append(os.path.join(root, file))
    return elementsOnHardDrive, fullPath

#
def getPathListInIndexHbaseBlock(start_date,end_date,satellite='a',ql_list=None,index_name=None):
    if ql_list == None:
        ql_list = [dictdbcolnames['L1'], dictdbcolnames['roughness'],
                dictdbcolnames[satellite]['ww3_V2'],
                dictdbcolnames[satellite]['imaginary_V2'],
                dictdbcolnames[satellite]['real_V2'],
                dictdbcolnames[satellite]['full_V2']]

    print("Retrieve data from DB")
    if index_name == None:
        index_name = 'S1' + satellite.upper() + 'WV.v3'
    
    elementsIndexedInDatabase = defaultdict(list)

    flist = list(ci.getIndexFiles(index_name, start=start_date, stop=end_date, columns=['cf:']))

    MISSING_FDATE = 0
    for r in flist:
        for qlcolname in ql_list:
            if qlcolname in r[1] and r[1][qlcolname] != 'unknown':
                elementsIndexedInDatabase[qlcolname].append(r[1][qlcolname])
    return elementsIndexedInDatabase

def getNCFileNameFromCalvalIndexBlock(start_date,end_date, satellite='a',calvalIndex=None):

    #ci = hbr.CersatIndexClient()
    if calvalIndex is None:
        if satellite.upper() == 'A':
            index_name = 'FRED/S1A_WV_SL2_OCN/calval1d/v1'
        else:
            index_name = 'FRED/S1B_WV_SL2_OCN/calval1d/v2'
    else:
        index_name=calvalIndex
    params = ['cf:osw']

    indexElementInCalvalList = list(ci.getIndexFiles(index_name, start=start_date, stop=end_date, columns=params))

    cursor = 0
    geolocalizedElementsFromCalvalIndex = []
    notGeolocalizedElementsFromCalvalIndex = []

    while cursor < len(indexElementInCalvalList):
        currentValuesOsw = unpackb(
            indexElementInCalvalList[cursor][1]['cf:osw'])

        if currentValuesOsw.has_key('oswLat') and currentValuesOsw.has_key('oswLon'):
            # Retrieve the name of the NCFile and remove it's extension like it will be in the trackfile
            geolocalizedElementsFromCalvalIndex.append(os.path.splitext(
                indexElementInCalvalList[cursor][0].split('#')[-1])[0])
        else:
            notGeolocalizedElementsFromCalvalIndex.append(os.path.splitext(
                indexElementInCalvalList[cursor][0].split('#')[-1])[0])
        cursor += 1

    return geolocalizedElementsFromCalvalIndex, notGeolocalizedElementsFromCalvalIndex


# Retrieve data indexed in hbase
def getPathListInPhoenixBlock(start_date,end_date, satellite='a'):

    conn = phoenixdb.connect(urlDatabasePhoenix, autocommit=True)
    cursor = conn.cursor()
    myRequest = "SELECT FNAME FROM " + indexPhoenix
    whereClause = " WHERE FDATEDT BETWEEN TO_TIMESTAMP('" + str(
        start_date) + "','yyyyMMdd') AND TO_TIMESTAMP('" + str(end_date) + "','yyyyMMdd')"
    if satellite == 'a':
        whereClause += " AND SENSOR IN('S1A') "
    elif satellite == 'b':
        whereClause += " AND SENSOR IN('S1B') "

    myRequest += whereClause
    print myRequest
    cursor.execute(myRequest)
    elementsIndexed = cursor.fetchall()

    tmpTab = []
    # Transforme les tuple de fname en donnees simple en retirant l'extension
    for tmpValue in elementsIndexed:
        tmpTab.append(os.path.splitext(tmpValue[0])[0])

    elementsIndexed = list(tmpTab)
    return elementsIndexed


def createHTMLOverviewIPF(picklerPath, satellite,versionList):

    if satellite == 'b':
        path = htmlOutputPathB
    else:
        path = htmlOutputPathA

    # satellite='a'
    # picklerPath=picklerPathA

    startDate = datetime.datetime.strptime('201506', '%Y%m')
    #startDate = datetime.datetime.strptime('201710', '%Y%m')

    #endDate = datetime.datetime.strptime('201702', '%Y%m')
    endDate = datetime.datetime.now()

    outputfilepath = os.path.join(
        path, 'generalOverviewIPF_' + satellite.upper() + ".html")
    tmpoutputfilepath = os.path.join(
        path, 'generalOverviewIPF_' + satellite.upper() + ".tmp")

    outputfile = open(tmpoutputfilepath, 'w')
    outputfile.write('<HTML>')
    outputfile.write(
        '<HEAD><link rel="stylesheet" type="text/css" href="checkgeneration.css"/></HEAD>')
    outputfile.write('<BODY>')

    outputfile.write(
        'The current element status are based between Nc files on file system and number of indexed elements.<br/>')
    outputfile.write('File generated at:' + endDate.strftime('%Y%m%d-%H%M%S'))

    outputfile.write('<TABLE>')
    # Generate head of the table
    outputfile.write('<TR class="mytitle">')
    outputfile.write('<TD><img class="image-icon" title="Everything allright" src="Bobarcade.png" height="50px"></TD>')
    outputfile.write(
        '<TD><img class="image-icon" title="Everything allright" src="images/green_dot.png"></TD><TD>Everything processed</TD>')
    outputfile.write(
        '<TD><img class="image-icon" title="Less than 1% files have not been processed" src="images/yellow_dot.png"></TD><TD>&lt;1% elements have not been processed</TD>')
    outputfile.write('<TD><img class="image-icon" title="More than 1% files And Less than 10% files have not been processed" src="images/orange_dot.png"></TD><TD> &gt;1% &lt;10% elements have not been processed</TD>')
    outputfile.write('<TD><img class="image-icon" title="More than 10% files And Less than 30% files have not been processed" src="images/lightred_dot.png"></TD><TD>&gt;10% &lt;30% elements have not been processed</TD>')
    outputfile.write(
        '<TD><img class="image-icon" title="More than 30% files have not been processed" src="images/red_dot.png"></TD><TD>&gt;30% elements have not been processed</TD>')
    outputfile.write('</TR>')
    outputfile.write('<TR class="mytitle">')
    outputfile.write(
        '<TD><img class="image-icon" title="UNKNOWN STATUS" src="images/brown_dot.png"></TD><TD>UNKNOWN STATUS</TD>')
    outputfile.write(
        '<TD><img class="image-icon" title="Less than 1% files Exceeding" src="images/cyan_dot.png"></TD><TD>Less than 1% files Exceeding</TD>')
    outputfile.write('<TD><img class="image-icon" title="More than 1% files And Less than 10% files Exceeding" src="images/bluesky_dot.png"></TD><TD>More than 1% files And Less than 10% files Exceeding</TD>')
    outputfile.write(
        '<TD><img class="image-icon" title="More than 10% files Exceeding" src="images/blue_dot.png"></TD><TD>More than 10% files Exceeding</TD>')
    outputfile.write('<TD></TD><TD></TD><TD><img class="image-icon" title="Everything allright" src="Bubarcade.png" height="50px"></TD>')
    outputfile.write('</TR>')

    outputfile.write('</TABLE>')

    outputfile.write('<TABLE>')

    # Generate head of the table
    outputfile.write('<TR class="mytitle">')
    outputfile.write('<TH rowspan="2" ></TH>')

    outputfile.write('<TH colspan="8" >Sentinel 1 L2</TH>')

    # LV2 Data
    outputfile.write('</TR><TR class="mytitle">')
    outputfile.write('<TH>Duplicate L2 NC files</TH>')
    #outputfile.write('<TH>Duplicate L2 elements indexed Phoenix</TH>')
    #outputfile.write('<TH>Nc Files --> Unique Fname indexed in Phoenix</TH>')
    outputfile.write('<TH>Real_V2 -> Generation des png par rapport aux fichiers nc du filesystem</TH>')
    outputfile.write('<TH>Real_V2 -> Indexation des vignettes dans hbase</TH>')
    # outputfile.write('<TH>Imaginary</TH>')
    outputfile.write('<TH>Imaginary_V2 -> Generation des png par rapport aux fichiers nc du filesystem</TH>')
    outputfile.write('<TH>Imaginary_V2 -> Indexation des vignettes dans hbase</TH>')

    # outputfile.write('<TH>Full</TH>')
    outputfile.write('<TH>Full_V2 -> Generation des png par rapport aux fichiers nc du filesystem</TH>')
    outputfile.write('<TH>Full_V2 -> Indexation des vignettes dans hbase</TH>')

    # LV1 Data

    outputfile.write('<TH> NC Files To calval Index </TH>')
    outputfile.write('</TR>')

    # Loop to generate Data

    for currentVersion in versionList:
        print currentVersion

        ncBaseList = pickle.load( open(picklerPath + "/" + currentVersion + "ncBaseList.p", "rb"))
        ncFullPathList = pickle.load( open(picklerPath + "/" + currentVersion + "ncFullPathList.p", "rb"))
        pngBaseList = pickle.load( open(picklerPath + "/" + currentVersion + "pngBaseList.p", "rb"))
        pngFullPathList = pickle.load( open(picklerPath + "/" + currentVersion + "pngFullPathList.p", "rb"))
        #doublons=[item for item, count in collections.Counter(ncBaseList).items() if count > 1]
        doublonsLength = len(ncBaseList) - len(set(ncBaseList))

        if len(ncBaseList) > 0:
            outputfile.write('<TR>')
            outputfile.write('<TD><a>' + currentVersion + '</a></TD>')

            # Duplicate NC Files
            outputfile.write(createHTMLDotStatusDuplicate(
                len(ncBaseList), doublonsLength,'duplicateL2NCFiles' + satellite.upper() + currentVersion + '.txt'))

            indexedFullPathList = pickle.load( open(picklerPath + "/" + currentVersion + "indexedFullPathList.p", "rb"))
            indexedInPhoenixNCBaseList = pickle.load( open(picklerPath + "/" + currentVersion + "indexedInPhoenixNCBaseList.p", "rb"))
            
            indexedInPhoenixNCBaseSet = set(indexedInPhoenixNCBaseList)
            #Duplicate in phoenix
            #duplicateInPhoenix = len(indexedInPhoenixNCBaseList) - len(indexedInPhoenixNCBaseSet)
            #outputfile.write(createHTMLDotStatusDuplicate(len(indexedInPhoenixNCBaseList), duplicateInPhoenix,'duplicateElementsIndexedInPhoenix' + satellite.upper() + currentVersion + '.txt'))

            #Indexation in phoenix
            #outputfile.write(createHTMLDotStatus(len(ncFullPathList), len(indexedInPhoenixNCBaseSet),0,'listings.html?' + currentVersion + '_NCFilesInPhoenix_' + satellite.upper() ))

            # Sentinel 1 Data L2

            indexedFullPathList = retrievePathListInIndex(
                currentVersion, picklerPath)

            doublonsLength=0
            # for currentType in ['real','real_V2','imaginary','imaginary_V2','full','full_V2']:
            for currentType in ['real_V2', 'imaginary_V2', 'full_V2']:
                outputfile.write(createHTMLDotStatus(len(ncBaseList), len(pngFullPathList[dictdbcolnames[satellite][currentType]]),0,'listings.html?' + currentVersion +"_L2Png"+currentType.capitalize()[:-3]+"Generation" +'_'+ satellite.upper()))
                outputfile.write(createHTMLDotStatus(len(pngFullPathList[dictdbcolnames[satellite][currentType]]), len(indexedFullPathList[dictdbcolnames[satellite][currentType]+currentVersion]), doublonsLength,'listings.html?' + currentVersion +"_L2Png"+currentType.capitalize()[:-3]+"Indexation" +'_'+ satellite.upper()))

            # Elements in calval
            geolocalizedElementsFromCalvalIndex = pickle.load( open(picklerPath + "/" + currentVersion + "geolocalizedElementsFromCalvalIndex.p", "rb"))
            notGeolocalizedElementsFromCalvalIndex = pickle.load( open(picklerPath + "/" + currentVersion + "notGeolocalizedElementsFromCalvalIndex.p", "rb"))
            # Difference between Tiff files and calval index
            outputfile.write(createHTMLDotStatus(len(ncBaseList), len(geolocalizedElementsFromCalvalIndex) + len(notGeolocalizedElementsFromCalvalIndex), doublonsLength,'listings.html?' + currentVersion + '_L2InCalvalIndex_' + satellite.upper()))
            

            outputfile.write('</TR>')


    outputfile.write('</TABLE>')

    outputfile.close()
    os.rename(tmpoutputfilepath, outputfilepath)
    print outputfilepath

##################################################
############  END OF PART FOR IPF   ##############
##################################################


def retrieveListFromPNGOnHardDrive(datemonth, picklerPath):
    pngBaseList = pickle.load(
        open(picklerPath + "/" + datemonth + "pngBaseList.p", "rb"))
    pngFullPathList = pickle.load(
        open(picklerPath + "/" + datemonth + "pngFullPathList.p", "rb"))
    return pngBaseList, pngFullPathList


def retrieveListFromNCOnHardDrive(datemonth, picklerPath):
    ncBaseList = pickle.load(
        open(picklerPath + "/" + datemonth + "ncBaseList.p", "rb"))
    ncFullPathList = pickle.load(
        open(picklerPath + "/" + datemonth + "ncFullPathList.p", "rb"))
    return ncBaseList, ncFullPathList


def retrieveListFromWW3OnHardDrive(datemonth, picklerPath):
    ww3NcBaseList = pickle.load(
        open(picklerPath + "/" + datemonth + "ww3NcBaseList.p", "rb"))
    ww3NcFullPathList = pickle.load(
        open(picklerPath + "/" + datemonth + "ww3ncFullPathList.p", "rb"))
    #ww3NcFilesOnWater = pickle.load(open(picklerPath+"/"+datemonth+"ww3NcFilesOnWater.p","rb"))
    # return ww3NcBaseList,ww3NcFullPathList,ww3NcFilesOnWater
    return ww3NcBaseList, ww3NcFullPathList


def retrieveFilesFromTrack(datemonth, picklerPath):
    ncBaseFromTrack = pickle.load(
        open(picklerPath + "/" + datemonth + "ncBaseFromTrack.p", "rb"))
    ncFilesFromTrackOnWater = pickle.load(
        open(picklerPath + "/" + datemonth + "ncBaseFromTrackOnWater.p", "rb"))
    return ncBaseFromTrack, ncFilesFromTrackOnWater


def retrievePathListInIndex(datemonth, picklerPath):
    indexedFullPathList = pickle.load(
        open(picklerPath + "/" + datemonth + "indexedFullPathList.p", "rb"))
    return indexedFullPathList


def retrievePathListInPhoenix(datemonth, picklerPath):
    indexedInPhoenixNCBaseList = pickle.load(
        open(picklerPath + "/" + datemonth + "indexedPhoenixNCBaseList.p", "rb"))
    return indexedInPhoenixNCBaseList


def retrieveListFromLevel1XmlOnHardDrive(datemonth, picklerPath, satellite='a'):
    filePath = pickle.load(
        open(picklerPath + "/" + datemonth + "L1XmlFullPathList.p", "rb"))
    baseName = pickle.load(
        open(picklerPath + "/" + datemonth + "L1XmlBaseNameList.p", "rb"))
    l1XmlFullPathDict = pickle.load(
        open(picklerPath + "/" + datemonth + "L1XmlFullPathDict.p", "rb"))
    return baseName, filePath, l1XmlFullPathDict


def retrieveListFromLevel1RoughnessOnHardDrive(datemonth, picklerPath, satellite='a'):
    filePath = pickle.load(
        open(picklerPath + "/" + datemonth + "L1RoughnessFullPathList.p", "rb"))
    baseName = pickle.load(
        open(picklerPath + "/" + datemonth + "L1roughnessBaseNameList.p", "rb"))
    l1RoughnessFullPathDict = pickle.load(
        open(picklerPath + "/" + datemonth + "L1RoughnessFullPathDict.p", "rb"))
    return baseName, filePath, l1RoughnessFullPathDict


def retrieveListGeolocalizedElementsFromCalvalIndex(datemonth, picklerPath, satellite='a'):
    geolocalizedElementsFromCalvalIndex = pickle.load(open(picklerPath + "/" + datemonth + "geolocalizedElementsFromCalvalIndex.p", "rb"))
    notGeolocalizedElementsFromCalvalIndex = pickle.load(open(picklerPath + "/" + datemonth + "notGeolocalizedElementsFromCalvalIndex.p", "rb"))
    WW3IndexedInCalvalIndex = pickle.load(open(picklerPath + "/" + datemonth + "WW3IndexedInCalvalIndex.p", "rb"))
    return geolocalizedElementsFromCalvalIndex, notGeolocalizedElementsFromCalvalIndex, WW3IndexedInCalvalIndex


def retrieveAllData(datemonth, picklerPath, satellite='a', showData=True):
    pngBaseList, pngFullPathList = retrieveListFromPNGOnHardDrive(
        datemonth, picklerPath)
    ncBaseList, ncFullPathList = retrieveListFromNCOnHardDrive(
        datemonth, picklerPath)
    #ww3NcBaseList,ww3NcFullPathList,ww3NcFilesOnWater = retrieveListFromWW3OnHardDrive(datemonth,picklerPath)
    ww3NcBaseList, ww3NcFullPathList = retrieveListFromWW3OnHardDrive(
        datemonth, picklerPath)
    ncBaseFromTrack, ncBaseFromTrackOnWater = retrieveFilesFromTrack(
        datemonth, picklerPath)
    indexedFullPathList = retrievePathListInIndex(datemonth, picklerPath)
    indexedInPhoenixNCBaseList = retrievePathListInPhoenix(
        datemonth, picklerPath)
    l1XmlBaseList, l1XmlFullPathList, l1XmlFullPathDict = retrieveListFromLevel1XmlOnHardDrive(
        datemonth, picklerPath)
    l1RoughnessBaseList, l1RoughnessFullPathList, l1RoughnessFullPathDict = retrieveListFromLevel1RoughnessOnHardDrive(
        datemonth, picklerPath)
    geolocalizedElementsFromCalvalIndex, notGeolocalizedElementsFromCalvalIndex,WW3IndexedInCalvalIndex = retrieveListGeolocalizedElementsFromCalvalIndex(
        datemonth, picklerPath)
    # if showData:
    #    showAllData(pngBaseList,pngFullPathList,ncBaseList,ncFullPathList,ww3NcFilesOnWater,ww3NcBaseList,ww3NcFullPathList,ncBaseFromTrack,indexedFullPathList,l1XmlBaseList,l1XmlFullPathList,l1RoughnessBaseList,l1RoughnessFullPathList,satellite)
    return pngBaseList, pngFullPathList, ncBaseList, ncFullPathList, ww3NcBaseList, ww3NcFullPathList, ncBaseFromTrack, indexedFullPathList, indexedInPhoenixNCBaseList, l1XmlBaseList, l1XmlFullPathList, l1XmlFullPathDict, l1RoughnessBaseList, l1RoughnessFullPathList, l1RoughnessFullPathDict, geolocalizedElementsFromCalvalIndex


def showAllData(pngBaseList, pngFullPathList, ncBaseList, ncFullPathList, ww3NcBaseList, ww3NcFullPathList, ncBaseFromTrack, indexedFullPathList, l1XmlBaseList, l1XmlFullPathList, l1RoughnessBaseList, l1RoughnessFullPathList, satellite='a'):
    platform = 'S1A'
    platformB = 'S1B'

    dictdbcolnames = {'L1': 'cf:fpath',
                      'roughness': 'cf:ql:l1_roughness',
                      'a': {'ww3_V2': 'cf:ql:' + platform + '_WV_OCN__2S_ww3' + '_V2',
                            'imaginary_V2': 'cf:ql:' + platform + '_WV_OCN__2S_imaginarycrossspectra_' + platform.lower() + '_V2',
                            'real_V2': 'cf:ql:' + platform + '_WV_OCN__2S_realcrossspectra_' + platform.lower() + '_V2',
                            'full_V2': 'cf:ql:' + platform + '_WV_OCN__2S_ocean_swell_spectra_' + platform.lower() + '_V2'
                            }, 'b': {'ww3_V2': 'cf:ql:' + platformB + '_WV_OCN__2S_ww3' + '_V2',
                                     'imaginary_V2': 'cf:ql:' + platformB + '_WV_OCN__2S_imaginarycrossspectra_' + platformB.lower() + '_V2',
                                     'real_V2': 'cf:ql:' + platformB + '_WV_OCN__2S_realcrossspectra_' + platformB.lower() + '_V2',
                                     'full_V2': 'cf:ql:' + platformB + '_WV_OCN__2S_ocean_swell_spectra_' + platformB.lower() + '_V2'
                                     }
                      }
    print "\nElements for real spectra:"
    print "Number of NC Files:%i    Number of generated PNG:%i    Number of indexed elements:%i" % (len(ncBaseList), len(pngBaseList[dictdbcolnames[satellite]["real_V2"]]), len(indexedFullPathList[dictdbcolnames[satellite]["real_V2"]]))
    print "\nElements for imaginary spectra:"
    print "Number of NC Files:%i    Number of generated PNG:%i    Number of indexed elements:%i" % (len(ncBaseList), len(pngBaseList[dictdbcolnames[satellite]["imaginary_V2"]]), len(indexedFullPathList[dictdbcolnames[satellite]["imaginary_V2"]]))
    print "\nElements for full spectra"
    print "Number of NC Files:%i    Number of generated PNG:%i    Number of indexed elements:%i" % (len(ncBaseList), len(pngBaseList[dictdbcolnames[satellite]["full_V2"]]), len(indexedFullPathList[dictdbcolnames[satellite]["full_V2"]]))
    print "\nElements for WW3 spectra"
    print "Number of NC Files(used for others spectra):%i " % (len(ncBaseList))
    print "Number of elements described In trackfile:%i   Number of generated NCfiles:%i   \nNumber of generated PNG:%i   Number of elements indexed:%i" % (len(ncBaseFromTrack), len(ww3NcBaseList), len(pngBaseList[dictdbcolnames[satellite]["ww3_V2"]]), len(indexedFullPathList[dictdbcolnames[satellite]["ww3_V2"]]))
    print "\nLevel 1 Elements"
    print "Number of Level1 XML files:%i    Number of Indexed XML LV1:%i" % (len(l1XmlFullPathList), len(indexedFullPathList[dictdbcolnames["L1"]]))
    print "Number of Png  Roughness LV1:%i  Number of Indexed Roughness:%i" % (len(l1RoughnessFullPathList), len(indexedFullPathList[dictdbcolnames["roughness"]]))


# Angular or other stuff must be a better and cleaner solution but don't have time to learn them
def createHTMLOutput(datemonth, picklerPath, satellite='a'):
    pngBaseList, pngFullPathList, ncBaseList, ncFullPathList, ww3NcBaseList, ww3NcFullPathList, ncBaseFromTrack, indexedFullPathList, indexedInPhoenixNCBaseList, l1XmlBaseList, l1XmlFullPathList, l1XmlFullPathDict, l1RoughnessBaseList, l1RoughnessFullPathList, l1RoughnessFullPathDict, geolocalizedElementsFromCalvalIndex = retrieveAllData(
        datemonth, picklerPath, satellite)

    checkDifferences = 0

    if satellite == 'b':
        path = htmlOutputPathB
    else:
        path = htmlOutputPathA

    errorListPath = os.path.join(path, 'errorFiles')

    if not os.path.exists(errorListPath):
        os.makedirs(errorListPath)

    outputfilepath = os.path.join(
        path, str(datemonth) + '_' + satellite.upper() + ".html")
    tmpoutputfilepath = os.path.join(
        path, str(datemonth) + '_' + satellite.upper() + ".tmp")

    outputfile = open(tmpoutputfilepath, 'w')
    outputfile.write('<HTML>')
    outputfile.write(
        '<HEAD><link rel="stylesheet" type="text/css" href="checkgeneration.css"/></HEAD>')
    outputfile.write('<BODY>')

    outputfile.write('<TABLE>')
    outputfile.write('<TR class="mytitle">')
    outputfile.write('<TD></TD>')
    outputfile.write('<TD>Number of NCFiles</TD>')
    outputfile.write('<TD>Number Of generated PNG</TD>')
    outputfile.write('<TD>Number Of indexed elements</TD>')
    outputfile.write('<TD>Error Files</TD>')
    outputfile.write('</TR>')

    for elementType in ['real_V2', 'imaginary_V2', 'full_V2']:
        outputfile.write('<TR>')
        outputfile.write('<TD>Elements for ' + elementType + ' spectra:</TD>')
        outputfile.write('<TD>' + str(len(ncBaseList)) + '</TD>')
        outputfile.write(
            '<TD>' + str(len(pngBaseList[dictdbcolnames[satellite][elementType]])) + '</TD>')
        outputfile.write(
            '<TD>' + str(len(indexedFullPathList[dictdbcolnames[satellite][elementType]])) + '</TD>')
        outputfile.write('<TD class="errorElement">')
        fileNamePrefix = str(datemonth) + '_' + \
            satellite.upper() + "_" + elementType.capitalize()
        outputfile.write(checkErrors(ncBaseList, "NCUsedFor" + elementType.capitalize(),
                                     pngBaseList[dictdbcolnames[satellite][elementType]], elementType + "PNG", errorListPath, fileNamePrefix))
        outputfile.write(checkErrors(pngFullPathList[dictdbcolnames[satellite][elementType]], elementType.capitalize(
        ) + "PNG", indexedFullPathList[dictdbcolnames[satellite][elementType]], elementType.capitalize() + "Indexed", errorListPath, fileNamePrefix))

        outputfile.write('</TD>')
        outputfile.write('</TR>')

    outputfile.write('</TABLE>')

    outputfile.write('<BR><TABLE>')
    outputfile.write('<TR class="mytitle">')
    outputfile.write('<TD>Number of Level1 XML files</TD>')
    outputfile.write('<TD>Number of Indexed XML LV1</TD>')
    outputfile.write('<TD>Error Files</TD>')
    outputfile.write('</TR><TR>')
    outputfile.write('<TD>' + str(len(l1XmlFullPathList)) + '</TD>')
    outputfile.write(
        '<TD>' + str(len(indexedFullPathList[dictdbcolnames["L1"]])) + '</TD>')

    outputfile.write('<TD class="errorElement">')
    outputfile.write(checkErrors(l1XmlFullPathList, "L1XML",
                                 indexedFullPathList[dictdbcolnames["L1"]], "indexedL1", errorListPath, fileNamePrefix))
    outputfile.write('</TD>')
    outputfile.write('</TR>')
    outputfile.write('</TABLE>')

    outputfile.write('<BR><TABLE>')
    outputfile.write('<TR class="mytitle">')
    outputfile.write('<TD>Number of Png Roughness LV1</TD>')
    outputfile.write('<TD>Number of Indexed Roughness</TD>')
    outputfile.write('<TD>Error Files</TD>')
    outputfile.write('</TR>')
    outputfile.write('<TR>')
    outputfile.write('<TD>' + str(len(l1RoughnessFullPathList)) + '</TD>')
    outputfile.write(
        '<TD>' + str(len(indexedFullPathList[dictdbcolnames["roughness"]])) + '</TD>')

    outputfile.write('<TD class="errorElement">')
    outputfile.write(checkErrors(l1RoughnessFullPathList, "L1XML",
                                 indexedFullPathList[dictdbcolnames["roughness"]], "indexedRoughness", errorListPath, fileNamePrefix))
    outputfile.write('</TD>')

    outputfile.write('</TR>')
    outputfile.write('</TABLE>')

    outputfile.write('<BR><TABLE>')
    outputfile.write('<TR class="mytitle">')
    outputfile.write('<TD></TD>')
    outputfile.write('<TD>Number of NCFiles<BR>(used for other spectra)</TD>')
    outputfile.write(
        '<TD>Number of NCFiles indexed in calval<BR>(Used for create trackfile)</TD>')
    outputfile.write(
        '<TD>Number of elements described In trackfile<BR>(Used To generate NCFiles)</TD>')
    outputfile.write(
        '<TD>Number of expected NCFiles<BR>(earth elements are remove with LandMask)</TD>')
    outputfile.write('<TD>Number of generated NCfiles</TD>')
    outputfile.write('<TD>Number of generated PNG</TD>')
    outputfile.write('<TD>Number of elements indexed</TD>')
    outputfile.write('<TD>Error Files</TD>')

    outputfile.write('</TR>')
    outputfile.write('<TR>')
    outputfile.write('<TD>Elements for WW3 spectra:</TD>')
    outputfile.write('<TD>' + str(len(ncBaseList)) + '</TD>')
    outputfile.write(
        '<TD>' + str(len(geolocalizedElementsFromCalvalIndex)) + '</TD>')
    outputfile.write('<TD>' + str(len(ncBaseFromTrack)) + '</TD>')
    # outputfile.write('<TD>'+str(len(ww3NCFilesOnWater))+'</TD>')
    outputfile.write('<TD>' + str(len(ww3NcBaseList)) + '</TD>')
    outputfile.write(
        '<TD>' + str(len(pngBaseList[dictdbcolnames[satellite]["ww3_V2"]])) + '</TD>')
    outputfile.write(
        '<TD>' + str(len(indexedFullPathList[dictdbcolnames[satellite]["ww3_V2"]])) + '</TD>')

    outputfile.write('<TD class="errorElement">')

    fileNamePrefix = str(datemonth) + '_' + satellite.upper() + "_WW3"
    # Errors between indexCalval and trackfile
    # outputfile.write(checkErrors(geolocalizedElementsFromCalvalIndex,"NCIndexedInCalval",ncBaseFromTrack,"NCInTrackFile",errorListPath,fileNamePrefix))
    # Errors between Landmask NC and generated NCFiles
    # outputfile.write(checkErrors(ww3NCFilesOnWater,"OnWaterNCInTrackFile",ww3NcBaseList,"NC",errorListPath,fileNamePrefix))
    # Errors Between Generated NCFiles and PNG
    outputfile.write(checkErrors(ww3NcBaseList, "NC",
                                 pngBaseList[dictdbcolnames[satellite]["ww3_V2"]], "WW3PNG", errorListPath, fileNamePrefix))
    # Errors between PNG and index
    outputfile.write(checkErrors(pngFullPathList[dictdbcolnames[satellite]["ww3_V2"]], "PNG",
                                 indexedFullPathList[dictdbcolnames[satellite]["ww3_V2"]], "Indexed", errorListPath, fileNamePrefix))
    outputfile.write('</TD>')

    outputfile.write('</TR>')

    outputfile.write('</TABLE>')
    outputfile.close()
    os.rename(tmpoutputfilepath, outputfilepath)
    print(outputfilepath)

# Get the difference length between the 2 elements and generate a list of elements in error


def checkErrors(originList, originListLabel, resultList, resultListLabel, path, fileNamePrefix):

    vanishedElements = list(set(resultList) - set(originList))
    vanishedElementsPath = os.path.join(
        path, fileNamePrefix + "_" + originListLabel + "Vanished.txt")

    outputfile = open(vanishedElementsPath, 'w')
    for currentElement in vanishedElements:
        outputfile.write(currentElement + "\n")
    outputfile.close()

    notGeneratedElements = list(set(originList) - set(resultList))
    notGeneratedElementsPath = os.path.join(
        path, fileNamePrefix + "_" + resultListLabel + "NotProcessed.txt")

    outputfile = open(notGeneratedElementsPath, 'w')
    for currentElement in notGeneratedElements:
        outputfile.write(currentElement + "\n")
    outputfile.close()

    outputString = ""
    if len(notGeneratedElements) > 0:
        outputString += '<a href="' + notGeneratedElementsPath + '">Nb Elements in error:' + \
            str(len(notGeneratedElements)) + "<BR/> Listing File: " + \
            os.path.basename(notGeneratedElementsPath) + '</a><br>'
    if len(vanishedElements) > 0:
        outputString += '<a href="' + vanishedElementsPath + '">Nb Elements in error:' + \
            str(len(vanishedElements)) + "<BR/> Listing File: " + \
            os.path.basename(vanishedElementsPath) + '</a><br>'
    return outputString

def writeDuplicate(inputList,outputPath,outputFileName):
    outputfilepath = os.path.join(outputPath,outputFileName)
    print outputfilepath
    outputfile = open(outputfilepath+".tmp", 'w')
    duplicate = []
    uniq = []
    for x in inputList:
        if x not in uniq:
            uniq.append(x)
        else:
            outputfile.write(x+ "\n")
    outputfile.close()
    os.rename(outputfilepath+".tmp", outputfilepath+".txt")
    return outputfilepath
# Write missing elements in 2 files 
# replacingTab waiting a tab of tab to made replacement in written file used for L1 Tiff/xml
def writeMissing(originList,resultList,outputPath,outputFileName,fullPathListing=None,replacingTab=[]):
    outputfilepath = os.path.join(outputPath,"Missing"+outputFileName)
    outputfile = open(outputfilepath+".tmp", 'w')
    
    tmpElementsList = list(set(originList)-set(resultList))
    for currentElement in tmpElementsList:
        if fullPathListing:
            if currentElement in fullPathListing:
                currentElement = fullPathListing[currentElement]
        for currentReplacement in replacingTab:
            currentElement = currentElement.replace(currentReplacement[0],currentReplacement[1])
            
        outputfile.write(currentElement + "\n")
    os.rename(outputfilepath+".tmp", outputfilepath+".txt")

    outputfilepath = os.path.join(outputPath,"Vanished"+outputFileName)
    outputfile = open(outputfilepath+".tmp", 'w')
    
    tmpElementsList = list(set(resultList) - set(originList))
    for currentElement in tmpElementsList:
        if fullPathListing:
            if currentElement in fullPathListing:
                currentElement = fullPathListing[currentElement]
            for currentReplacement in replacingTab:
                currentElement = currentElement.replace(currentReplacement[0],currentReplacement[1])
        outputfile.write(currentElement + "\n")
    outputfile.close()
    os.rename(outputfilepath+".tmp", outputfilepath+".txt")

    return outputfilepath

def listElementsInError(dateToUse, picklerPath, satellite='a'):

    if satellite == 'b':
        path = htmlOutputPathB
    else:
        path = htmlOutputPathA

    pngBaseList, pngFullPathList = retrieveListFromPNGOnHardDrive(dateToUse, picklerPath)
    ncBaseList, ncFullPathList = retrieveListFromNCOnHardDrive(dateToUse, picklerPath)
    indexedInPhoenixNCBaseList = retrievePathListInPhoenix(dateToUse, picklerPath)
    
    # duplicateL2NCFiles
    writeDuplicate(ncBaseList,path,'duplicateL2NCFiles' + satellite.upper() + dateToUse)
    # duplicateElementsIndexedInPhoenix
    writeDuplicate(indexedInPhoenixNCBaseList,path,'duplicateElementsIndexedInPhoenix' + satellite.upper() + dateToUse)
    
    # MissingNCFilesInPhoenix
    writeMissing(ncBaseList,indexedInPhoenixNCBaseList,path,'NCFilesInPhoenix' + satellite.upper() + dateToUse, ncFullPathList)
    # MissingL2PngRealGeneration
    writeMissing(ncBaseList,pngBaseList[dictdbcolnames[satellite]['real_V2']],path,'L2PngRealGeneration' + satellite.upper() + dateToUse,ncFullPathList)
    
    indexedFullPathList = retrievePathListInIndex(dateToUse, picklerPath)
    # MissingL2RealPngIndexation
    writeMissing(pngFullPathList[dictdbcolnames[satellite]['real_V2']],indexedFullPathList[dictdbcolnames[satellite]['real_V2']],path,'L2PngRealIndexation' + satellite.upper() + dateToUse)
    # MissingL2PngImaginaryGeneration
    writeMissing(ncBaseList,pngBaseList[dictdbcolnames[satellite]['imaginary_V2']],path,'L2PngImaginaryGeneration' + satellite.upper() + dateToUse,ncFullPathList)
    # MissingL2PngImaginaryIndexation
    writeMissing(pngFullPathList[dictdbcolnames[satellite]['imaginary_V2']],indexedFullPathList[dictdbcolnames[satellite]['imaginary_V2']],path,'L2PngImaginaryIndexation' + satellite.upper() + dateToUse)
    # MissingL2PngFullGeneration
    writeMissing(ncBaseList,pngBaseList[dictdbcolnames[satellite]['full_V2']],path,'L2PngFullGeneration' + satellite.upper() + dateToUse, ncFullPathList)
    # MissingL2PngFullIndexation
    writeMissing(pngFullPathList[dictdbcolnames[satellite]['full_V2']],indexedFullPathList[dictdbcolnames[satellite]['full_V2']],path,'L2PngFullIndexation' + satellite.upper() + dateToUse)
    

    # MissingXmlIndexation
    l1XmlBaseList, l1XmlFullPathList, l1XmlFullPathDict = retrieveListFromLevel1XmlOnHardDrive(dateToUse, picklerPath)
    #XML files are used to index but Tiff path are indexed in hbase, 
    # use Tiff filepath to compare but change them in xml filepath for the listing (replacingTab)
    writeMissing(l1XmlFullPathList,indexedFullPathList[dictdbcolnames['L1']],path,'XmlIndexation' + satellite.upper() + dateToUse,replacingTab=[['/measurement/','/annotation/'],['.tiff','.xml']])
    
    # MissingRoughnessGeneration
    l1RoughnessBaseList, l1RoughnessFullPathList, l1RoughnessFullPathDict = retrieveListFromLevel1RoughnessOnHardDrive(dateToUse, picklerPath)
    writeMissing(l1XmlBaseList,l1RoughnessBaseList,path,'RoughnessGeneration' + satellite.upper() + dateToUse, l1XmlFullPathDict,replacingTab=[['/annotation/','/measurement/'],['.xml','.tiff']])
    # MissingRoughnessIndexation
    writeMissing(l1RoughnessFullPathList,indexedFullPathList[dictdbcolnames["roughness"]],path,'RoughnessIndexation' + satellite.upper() + dateToUse)
    
    #WW3 Part
    # MissingL2InCalvalIndex
    geolocalizedElementsFromCalvalIndex, notGeolocalizedElementsFromCalvalIndex,WW3IndexedInCalvalIndex = retrieveListGeolocalizedElementsFromCalvalIndex(dateToUse, picklerPath)
    fullElementsFromCalvalIndex = geolocalizedElementsFromCalvalIndex + notGeolocalizedElementsFromCalvalIndex
    writeMissing(ncBaseList,fullElementsFromCalvalIndex,path,'L2InCalvalIndex' + satellite.upper() + dateToUse, ncFullPathList)
    # MissingL2InTrackfile
    ncBaseFromTrack, ncBaseFromTrackOnWater = retrieveFilesFromTrack(dateToUse, picklerPath)
    writeMissing(geolocalizedElementsFromCalvalIndex,ncBaseFromTrack,path,'L2InTrackfile' + satellite.upper() + dateToUse, ncFullPathList)
    # MissingWW3NCFiles
    ww3NcBaseList, ww3NcFullPathList = retrieveListFromWW3OnHardDrive(dateToUse, picklerPath)
    writeMissing(ncBaseFromTrackOnWater,ww3NcBaseList,path,'WW3NCFiles' + satellite.upper() + dateToUse)
    # MissingL2PngWW3Generation
    writeMissing(ww3NcBaseList,pngBaseList[dictdbcolnames[satellite]['ww3_V2']],path,'L2PngWW3Generation' + satellite.upper() + dateToUse,ww3NcFullPathList)
    # MissingL2PngWW3Indexation
    writeMissing(pngFullPathList[dictdbcolnames[satellite]['ww3_V2']],indexedFullPathList[dictdbcolnames[satellite]['ww3_V2']],path,'L2PngWW3Indexation' + satellite.upper() + dateToUse)

def createHTMLOutputGraph(dateToUse, picklerPath, satellite='a'):

    if satellite == 'b':
        path = htmlOutputPathB
    else:
        path = htmlOutputPathA
    # errorListPath=os.path.join(path,'errorFiles')

    # if not os.path.exists(errorListPath):
    #    os.makedirs(errorListPath)

    outputfilepath = os.path.join(
        path, str(dateToUse) + '_' + satellite.upper() + "_graph.json")
    tmpoutputfilepath = os.path.join(
        path, str(dateToUse) + '_' + satellite.upper() + "_graph.tmp")

    with open(tmpoutputfilepath, 'w') as outputfile:
        # Load json structure
        jsonbasepath = os.path.join(path, 'base_graph.json')
        jsonbase = open(jsonbasepath)
        jsondata = json.load(jsonbase)
        jsonbase.close()

        geolocalizedElementsFromCalvalIndex, notGeolocalizedElementsFromCalvalIndex,WW3IndexedInCalvalIndex = retrieveListGeolocalizedElementsFromCalvalIndex(
            dateToUse, picklerPath)
        l1XmlBaseList, l1XmlFullPathList, l1XmlFullPathDict = retrieveListFromLevel1XmlOnHardDrive(
            dateToUse, picklerPath)
        ncBaseList, ncFullPathList = retrieveListFromNCOnHardDrive(
            dateToUse, picklerPath)

        #indexation in calval
        status, description = comparelistLength(
            ncBaseList, geolocalizedElementsFromCalvalIndex)
        jsondata['children'][0]['children'][0]['status'] = status
        jsondata['children'][0]['children'][0]['description'] = description

        ncBaseFromTrack, ncBaseFromTrackOnWater = retrieveFilesFromTrack(
            dateToUse, picklerPath)
        # extraction to trackfile
        status, description = comparelistLength(
            geolocalizedElementsFromCalvalIndex, ncBaseList)
        jsondata['children'][0]['children'][0]['children'][0]['status'] = status
        jsondata['children'][0]['children'][0]['children'][0]['description'] = description

        ww3NcBaseList, ww3NcFullPathList = retrieveListFromWW3OnHardDrive(
            dateToUse, picklerPath)
        # WW3 NC Files generation
        status, description = comparelistLength(ncBaseList, ww3NcBaseList)
        jsondata['children'][0]['children'][0]['children'][0]['children'][0]['status'] = status
        jsondata['children'][0]['children'][0]['children'][0]['children'][0]['description'] = description

        pngBaseList, pngFullPathList = retrieveListFromPNGOnHardDrive(
            dateToUse, picklerPath)
        # WW3 PNG Generation
        status, description = comparelistLength(
            ww3NcBaseList, pngBaseList[dictdbcolnames[satellite]['ww3_V2']])
        jsondata['children'][0]['children'][0]['children'][0]['children'][0]['children'][0]['status'] = status
        jsondata['children'][0]['children'][0]['children'][0]['children'][0]['children'][0]['description'] = description

        indexedFullPathList = retrievePathListInIndex(dateToUse, picklerPath)
        # WW3 PNG indexation
        status, description = comparelistLength(
            pngBaseList[dictdbcolnames[satellite]['ww3_V2']], indexedFullPathList[dictdbcolnames[satellite]['ww3_V2']])
        jsondata['children'][0]['children'][0]['children'][0]['children'][0]['children'][0]['children'][0]['status'] = status
        jsondata['children'][0]['children'][0]['children'][0]['children'][0]['children'][0]['children'][0]['description'] = description

        # Tiff indexation in Hbase
        status, description = comparelistLength(
            l1XmlBaseList, indexedFullPathList[dictdbcolnames['L1']])
        jsondata['children'][0]['children'][1]['status'] = status
        jsondata['children'][0]['children'][1]['description'] = description

        l1RoughnessBaseList, l1RoughnessFullPathList, l1RoughnessFullPathDict = retrieveListFromLevel1RoughnessOnHardDrive(
            dateToUse, picklerPath)
        # roughness files generation
        status, description = comparelistLength(
            l1XmlBaseList, l1RoughnessBaseList)
        jsondata['children'][0]['children'][2]['status'] = status
        jsondata['children'][0]['children'][2]['description'] = description
        # roughness files indexation
        status, description = comparelistLength(
            l1RoughnessBaseList, indexedFullPathList[dictdbcolnames['roughness']])
        jsondata['children'][0]['children'][2]['children'][0]['status'] = status
        jsondata['children'][0]['children'][2]['children'][0]['description'] = description

        # Real PNG generation
        status, description = comparelistLength(
            ncBaseList, pngFullPathList[dictdbcolnames[satellite]['real_V2']])
        jsondata['children'][1]['children'][0]['status'] = status
        jsondata['children'][1]['children'][0]['description'] = description
        # Real PNG indexation
        status, description = comparelistLength(
            pngFullPathList[dictdbcolnames[satellite]['real_V2']], indexedFullPathList[dictdbcolnames[satellite]['real_V2']])
        jsondata['children'][1]['children'][0]['children'][0]['status'] = status
        jsondata['children'][1]['children'][0]['children'][0]['description'] = description
        # Imaginary PNG generation
        status, description = comparelistLength(
            ncBaseList, pngFullPathList[dictdbcolnames[satellite]['imaginary_V2']])
        jsondata['children'][1]['children'][1]['status'] = status
        jsondata['children'][1]['children'][1]['description'] = description
        # Imaginary PNG indexation
        status, description = comparelistLength(
            pngFullPathList[dictdbcolnames[satellite]['imaginary_V2']], indexedFullPathList[dictdbcolnames[satellite]['imaginary_V2']])
        jsondata['children'][1]['children'][1]['children'][0]['status'] = status
        jsondata['children'][1]['children'][1]['children'][0]['description'] = description
        # Full PNG generation
        status, description = comparelistLength(
            ncBaseList, pngFullPathList[dictdbcolnames[satellite]['full_V2']])
        jsondata['children'][1]['children'][2]['status'] = status
        jsondata['children'][1]['children'][2]['description'] = description
        # Full PNG indexation
        status, description = comparelistLength(
            pngFullPathList[dictdbcolnames[satellite]['full_V2']], indexedFullPathList[dictdbcolnames[satellite]['full_V2']])
        jsondata['children'][1]['children'][2]['children'][0]['status'] = status
        jsondata['children'][1]['children'][2]['children'][0]['description'] = description

        outputfile.write(json.dumps(jsondata))
        os.rename(tmpoutputfilepath, outputfilepath)


def createHTMLDotStatus(inputLen, outputLen, duplicateLen=0,fileLink=None):
    resultString = '<TD>'
    if fileLink:
        resultString += ('<a href="'+fileLink+'" target="_blank">')
    
    resultString += ('<img class="image-icon" ')
    if outputLen == (inputLen - duplicateLen):
        resultString += 'src="images/green_dot.png"'
    # Case when there is missing elements
    elif outputLen < (inputLen - duplicateLen):
        if (inputLen == duplicateLen):
            percentageInError = 100
        else:
            percentageInError = 100 * \
                (inputLen - duplicateLen - outputLen) / (inputLen - duplicateLen)
        if percentageInError > 30:
            resultString += 'src="images/red_dot.png"'
        if percentageInError > 10:
            resultString += 'src="images/lightred_dot.png"'
        if percentageInError > 1:
            resultString += 'src="images/orange_dot.png"'
        else:
            resultString += 'src="images/yellow_dot.png"'
    # Case When there is too much elements
    elif outputLen > (inputLen - duplicateLen):
        if (inputLen == duplicateLen):
            percentageInError = 100
        else:
            percentageInError = 100 * \
                (outputLen - inputLen - duplicateLen) / (inputLen - duplicateLen)
        if percentageInError > 10:
            resultString += 'src="images/blue_dot.png"'
        if percentageInError > 1:
            resultString += 'src="images/bluesky_dot.png"'
        else:
            resultString += 'src="images/cyan_dot.png"'

    # Must not happen case,just in case
    else:
        resultString += 'src="images/brown_dot.png"'
    missingElements = inputLen - outputLen - duplicateLen
    resultString += ' title="' + str(outputLen) + '/' + str(
        inputLen - duplicateLen) + '\n' + str(missingElements) + ' Missing' + '">'
    if fileLink:
        resultString += ('</a>')
    resultString += '</TD>'
    return resultString


def createHTMLDotStatusDuplicate(inputLen, duplicateLen,fileLink=None):
    #resultString = ('<TD><img class="image-icon" ')
    resultString = '<TD>'
    if fileLink:
        resultString += ('<a href="'+fileLink+'" target="_blank">')
    resultString += '<img class="image-icon" '
    if duplicateLen == 0:
        resultString += 'src="images/green_dot.png"'
    # Case when there is missing elements
    elif duplicateLen > 0:
        percentageInError = 100 * (duplicateLen) / (inputLen)
        if percentageInError > 30:
            resultString += 'src="images/red_dot.png"'
        if percentageInError > 10:
            resultString += 'src="images/lightred_dot.png"'
        if percentageInError > 1:
            resultString += 'src="images/orange_dot.png"'
        else:
            resultString += 'src="images/yellow_dot.png"'
    # Must not happen case,just in case
    else:
        resultString += 'src="images/brown_dot.png"'
    resultString += ' title="' + str(duplicateLen) + '/' + str(
        inputLen) + '\n' + str(duplicateLen) + ' Duplicates' + '">'
    if fileLink:
        resultString += ('</a>')
    resultString += '</TD>'

    return resultString


def createHTMLOverview(picklerPath, satellite):

    if satellite == 'b':
        path = htmlOutputPathB
    else:
        path = htmlOutputPathA

    # satellite='a'
    # picklerPath=picklerPathA

    startDate = datetime.datetime.strptime('201506', '%Y%m')
    #startDate = datetime.datetime.strptime('201710', '%Y%m')

    #endDate = datetime.datetime.strptime('201702', '%Y%m')
    endDate = datetime.datetime.now()

    outputfilepath = os.path.join(
        path, 'generalOverview2_' + satellite.upper() + ".html")
    tmpoutputfilepath = os.path.join(
        path, 'generalOverview_' + satellite.upper() + ".tmp")

    outputfile = open(tmpoutputfilepath, 'w')
    outputfile.write('<HTML>')
    outputfile.write(
        '<HEAD><link rel="stylesheet" type="text/css" href="checkgeneration.css"/></HEAD>')
    outputfile.write('<BODY>')

    outputfile.write(
        'The current element status are based between Nc files on file system and number of indexed elements.<br/>')
    outputfile.write('File generated at:' + endDate.strftime('%Y%m%d-%H%M%S'))

    outputfile.write('<TABLE>')
    # Generate head of the table
    outputfile.write('<TR class="mytitle">')
    outputfile.write('<TD><img class="image-icon" title="Everything allright" src="Bobarcade.png" height="50px"></TD>')
    outputfile.write(
        '<TD><img class="image-icon" title="Everything allright" src="images/green_dot.png"></TD><TD>Everything processed</TD>')
    outputfile.write(
        '<TD><img class="image-icon" title="Less than 1% files have not been processed" src="images/yellow_dot.png"></TD><TD>&lt;1% elements have not been processed</TD>')
    outputfile.write('<TD><img class="image-icon" title="More than 1% files And Less than 10% files have not been processed" src="images/orange_dot.png"></TD><TD> &gt;1% &lt;10% elements have not been processed</TD>')
    outputfile.write('<TD><img class="image-icon" title="More than 10% files And Less than 30% files have not been processed" src="images/lightred_dot.png"></TD><TD>&gt;10% &lt;30% elements have not been processed</TD>')
    outputfile.write(
        '<TD><img class="image-icon" title="More than 30% files have not been processed" src="images/red_dot.png"></TD><TD>&gt;30% elements have not been processed</TD>')
    outputfile.write('</TR>')
    outputfile.write('<TR class="mytitle">')
    outputfile.write(
        '<TD><img class="image-icon" title="UNKNOWN STATUS" src="images/brown_dot.png"></TD><TD>UNKNOWN STATUS</TD>')
    outputfile.write(
        '<TD><img class="image-icon" title="Less than 1% files Exceeding" src="images/cyan_dot.png"></TD><TD>Less than 1% files Exceeding</TD>')
    outputfile.write('<TD><img class="image-icon" title="More than 1% files And Less than 10% files Exceeding" src="images/bluesky_dot.png"></TD><TD>More than 1% files And Less than 10% files Exceeding</TD>')
    outputfile.write(
        '<TD><img class="image-icon" title="More than 10% files Exceeding" src="images/blue_dot.png"></TD><TD>More than 10% files Exceeding</TD>')
    outputfile.write('<TD></TD><TD></TD><TD><img class="image-icon" title="Everything allright" src="Bubarcade.png" height="50px"></TD>')
    outputfile.write('</TR>')

    outputfile.write('</TABLE>')

    outputfile.write('<TABLE>')

    # Generate head of the table
    outputfile.write('<TR class="mytitle">')
    outputfile.write('<TH rowspan="2" ></TH>')

    outputfile.write('<TH colspan="9" >Sentinel 1 L2</TH>')
    outputfile.write('<TH colspan="3" >Sentinel 1 L1</TH>')
    outputfile.write('<TH colspan="6" >WW3</TH>')

    # LV2 Data
    outputfile.write('</TR><TR class="mytitle">')
    outputfile.write('<TH>L2 NC files sur le filesystem en doublon</TH>')
    outputfile.write('<TH>L2 elements indexes en doublons dans Phoenix (difference LON/LAT)</TH>')
    outputfile.write('<TH>Nombre de fichier indexes dans phoenix par rapport au nombre de fichier nc sur le filesystem en excluant les doublons</TH>')
    outputfile.write('<TH>Real_V2 -> Generation des png par rapport aux fichiers nc du filesystem</TH>')
    outputfile.write('<TH>Real_V2 -> Indexation des vignettes dans hbase</TH>')
    # outputfile.write('<TH>Imaginary</TH>')
    outputfile.write('<TH>Imaginary_V2 -> PNG Generation des png par rapport aux fichiers nc du filesystem</TH>')
    outputfile.write('<TH>Imaginary_V2 -> Indexation des vignettes dans hbase</TH>')

    # outputfile.write('<TH>Full</TH>')
    outputfile.write('<TH>Full_V2 -> Generation des png par rapport aux fichiers nc du filesystem</TH>')
    outputfile.write('<TH>Full_V2 -> Indexation des vignettes dans hbase</TH>')

    # LV1 Data

    outputfile.write('<TH>Nombre de lignes dans l index hbase par rapport aux fichier L1 du filesystem</TH>')
    outputfile.write('<TH>Roughness -> Generation des png par rapport aux fichiers nc du filesystem</TH>')
    outputfile.write('<TH>Roughness -> Indexation des vignettes dans hbase</TH>')

    # WW3 Data
    outputfile.write('<TH>L2 -> Index calval</TH>')
    outputfile.write('<TH>Element geolocalise dans l Index calval (HBASE) compare au elements du Trackfile</TH>')
    outputfile.write('<TH>ncFiles generes compares aux nombres d elements sur l eau dans le TrackFile</TH>')
    outputfile.write('<TH>nombre d elements WW3 indexes dans l index de calval compare aux ncFiles generes</TH>')
    outputfile.write('<TH>WW3 -> PNG Generation des png par rapport aux fichiers nc du filesystem</TH>')
    outputfile.write('<TH>WW3 -> PNG Indexation des vignettes dans hbase</TH>')

    outputfile.write('</TR>')

    # Loop to generate Data

    while endDate >= startDate:
        dateToUse = str(endDate.year) + str(endDate.month).zfill(2)
        print dateToUse

        pngBaseList, pngFullPathList = retrieveListFromPNGOnHardDrive(
            dateToUse, picklerPath)
        ncBaseList, ncFullPathList = retrieveListFromNCOnHardDrive(
            dateToUse, picklerPath)
        #doublons=[item for item, count in collections.Counter(ncBaseList).items() if count > 1]
        doublonsLength = len(ncBaseList) - len(set(ncBaseList))

        if len(ncBaseList) > 0:
            outputfile.write('<TR>')
            outputfile.write('<TD><a href=monthly_graphic_overview.html?' +
                             dateToUse + '_' + satellite.upper() + '>' + dateToUse + '</a></TD>')

            # Duplicate NC Files
            outputfile.write(createHTMLDotStatusDuplicate(
                len(ncBaseList), doublonsLength,'duplicateL2NCFiles' + satellite.upper() + dateToUse + '.txt'))
            #outputfile.write('<TD><img class="image-icon" ')
            # if  doublonsLength>0:
            #    outputfile.write('src="images/red_dot.png"')
            # else:
            #    outputfile.write('src="images/green_dot.png"')
            #outputfile.write(' title="'+str(doublonsLength)+'/'+str(len(ncBaseList))+","+str(len(ncFullPathList))+'"></TD>')

            indexedInPhoenixNCBaseList = retrievePathListInPhoenix(
                dateToUse, picklerPath)
            indexedInPhoenixNCBaseSet = set(indexedInPhoenixNCBaseList)
            #Duplicate in phoenix
            duplicateInPhoenix = len(
                indexedInPhoenixNCBaseList) - len(indexedInPhoenixNCBaseSet)
            outputfile.write(createHTMLDotStatusDuplicate(
                len(indexedInPhoenixNCBaseList), duplicateInPhoenix,'duplicateElementsIndexedInPhoenix' + satellite.upper() + dateToUse + '.txt'))

            #Indexation in phoenix
            outputfile.write(createHTMLDotStatus(
                len(ncFullPathList), len(indexedInPhoenixNCBaseSet),0,'listings.html?' + dateToUse + '_NCFilesInPhoenix_' + satellite.upper() ))

            # Sentinel 1 Data L2

            indexedFullPathList = retrievePathListInIndex(
                dateToUse, picklerPath)

            # for currentType in ['real','real_V2','imaginary','imaginary_V2','full','full_V2']:
            for currentType in ['real_V2', 'imaginary_V2', 'full_V2']:
                outputfile.write(createHTMLDotStatus(len(ncBaseList), len(
                    pngFullPathList[dictdbcolnames[satellite][currentType]]),0,'listings.html?' + dateToUse +"_L2Png"+currentType.capitalize()[:-3]+"Generation" +'_'+ satellite.upper()))
                outputfile.write(createHTMLDotStatus(len(pngFullPathList[dictdbcolnames[satellite][currentType]]), len(
                    indexedFullPathList[dictdbcolnames[satellite][currentType]]), doublonsLength,'listings.html?' + dateToUse +"_L2Png"+currentType.capitalize()[:-3]+"Indexation" +'_'+ satellite.upper()))

            # Sentinel 1 Data Lv1

            l1XmlBaseList, l1XmlFullPathList, l1XmlFullPathDict = retrieveListFromLevel1XmlOnHardDrive(
                dateToUse, picklerPath)
            # L1 XML Indexation in hbase
            outputfile.write(createHTMLDotStatus(len(l1XmlFullPathList), len(
                indexedFullPathList[dictdbcolnames["L1"]]),0,'listings.html?' + dateToUse +'_XmlIndexation_' + satellite.upper()))

            l1RoughnessBaseList, l1RoughnessFullPathList, l1RoughnessFullPathDict = retrieveListFromLevel1RoughnessOnHardDrive(
                dateToUse, picklerPath)
            # L1 XML --> Roughness png
            outputfile.write(createHTMLDotStatus(
                 len(l1XmlFullPathList),len(l1RoughnessFullPathList),0,'listings.html?' + dateToUse+'_RoughnessGeneration_' + satellite.upper()))
            # Roughness indexation in hbase
            outputfile.write(createHTMLDotStatus(
                len(l1RoughnessFullPathList), len(indexedFullPathList[dictdbcolnames["roughness"]]),0,'listings.html?' + dateToUse + '_RoughnessIndexation_' + satellite.upper()))

            # WW3 Data
            geolocalizedElementsFromCalvalIndex, notGeolocalizedElementsFromCalvalIndex,WW3IndexedInCalvalIndex = retrieveListGeolocalizedElementsFromCalvalIndex(
                dateToUse, picklerPath)
            # Difference between Tiff files and calval index
            outputfile.write(createHTMLDotStatus(len(ncBaseList), len(
                geolocalizedElementsFromCalvalIndex) + len(notGeolocalizedElementsFromCalvalIndex), doublonsLength,'listings.html?' + dateToUse + '_L2InCalvalIndex_' + satellite.upper()))
            ncBaseFromTrack, ncBaseFromTrackOnWater = retrieveFilesFromTrack(
                dateToUse, picklerPath)
            ww3NcBaseList, ww3NcFullPathList = retrieveListFromWW3OnHardDrive(
                dateToUse, picklerPath)

            # Difference between gelocalized Elements in calval index and NC files in track on water
            outputfile.write(createHTMLDotStatus(
                len(geolocalizedElementsFromCalvalIndex), len(ncBaseFromTrack),0,'listings.html?' + dateToUse + '_L2InTrackfile_' + satellite.upper()))
            # Difference NC files in tracks on water and  Generated NC
            outputfile.write(createHTMLDotStatus(
                len(ncBaseFromTrackOnWater), len(ww3NcBaseList),0,'listings.html?' + dateToUse + '_WW3NCFiles_' + satellite.upper()))
            #Difference between Generated NC and WWW3 in calval
            outputfile.write(createHTMLDotStatus(len(ww3NcBaseList), len(WW3IndexedInCalvalIndex),0,'listings.html?' + dateToUse + '_L2WW3CalvalIndexation_' + satellite.upper()))
            

            # Difference NC files and Generated PNG
            outputfile.write(createHTMLDotStatus(len(ww3NcBaseList), len(
                pngBaseList[dictdbcolnames[satellite]['ww3_V2']]),0,'listings.html?' + dateToUse + '_L2PngWW3Generation_' + satellite.upper()))
            # Difference NC files Generated PNG and indexed in Hbase
            outputfile.write(createHTMLDotStatus(len(pngBaseList[dictdbcolnames[satellite]['ww3_V2']]), len(
                indexedFullPathList[dictdbcolnames[satellite]['ww3_V2']]),0,'listings.html?' + dateToUse + '_L2PngWW3Indexation_' + satellite.upper()))

            outputfile.write('</TR>')

        endDate = endDate - relativedelta(months=+1)

    outputfile.write('</TABLE>')

    outputfile.close()
    os.rename(tmpoutputfilepath, outputfilepath)
    print outputfilepath


def comparelistLength(inputList, outputList):
    if len(inputList) > len(outputList):
        if len(set(inputList)) == len(set(outputList)):
            status = "warn"
            description = str(len(inputList) - len(set(inputList))) + \
                ' duplicates (' + str(len(outputList)) + \
                '/' + str(len(inputList)) + ')'
        else:
            status = "error"
            description = str(len(inputList) - len(outputList)) + ' missing (' + \
                str(len(outputList)) + '/' + str(len(inputList)) + ')'
    elif len(inputList) < len(outputList):
        status = "anomaly"
        description = str(len(outputList) - len(inputList)) + ' vanished (' + \
            str(len(outputList)) + '/' + str(len(inputList)) + ')'
    else:
        status = "ok"
        description = str(len(inputList)) + ' processed (' + \
            str(len(outputList)) + '/' + str(len(inputList)) + ')'
    return status, description


def cmdlineparser():
    """
    define the options of the scripts and set func depending of the inputs
    """
    parser = argparse.ArgumentParser(description='Sentinel L2 Indexer')

    parser.add_argument('-d', '--debug', action='store_true',
                        default=False, help='Debug logs')
    parser.add_argument('--monthToProcess', action='store',
                        default=None, help='monthToprocess', required=False)
    parser.add_argument('--satellite', action='store', default='a',
                        help='satellite to work on data', required=False)
    parser.add_argument('--function', action='store', default='overview',
                        help='function to launch in overview,monthlygraph,monthlyhtml,writealldata')

    return parser


if __name__ == "__main__":
    parser = cmdlineparser()
    args = parser.parse_args()
    if args.debug:
        logging.getLogger().setLevel(logging.DEBUG)
    #logging.debug('Parser : %s'%(args))
    # args.func(args)

    print args.satellite
    if args.satellite == 'a':
        picklerPath = picklerPathA
    if args.satellite == 'b':
        picklerPath = picklerPathB

    listMonthToprocess=[]


    if args.function == 'overview':
        createHTMLOverview(picklerPath, args.satellite)
    else:
        currentMonth = args.monthToProcess
        if currentMonth == 'ALL':
            endDate = datetime.datetime.now()
            month = endDate.month
            year = endDate.year
            while year >2014:
                listMonthToprocess.append(str(year)+str(month).zfill(2))
                month-=1
                if month<1:
                    month=12
                    year-=1
        else:
            listMonthToprocess.append(args.monthToProcess)
        
        print listMonthToprocess
        for currentMonth in listMonthToprocess:
            print picklerPath

            if args.function == 'monthlygraph':
                createHTMLOutputGraph(currentMonth, picklerPath, args.satellite)
            elif args.function == 'monthlyhtml':
                createHTMLOutput(currentMonth, picklerPath, args.satellite)
            elif args.function == 'writealldata':
                writeAllData(currentMonth, picklerPath, args.satellite)
            else:
                locals()[args.function](currentMonth, picklerPath, args.satellite)
        
    sys.exit(0)
