from matplotlib import pyplot as plt
import numpy as np
def add_labeled_image_axis(ax,heading,k):
    """

    :param ax: pyplot obj
    :param heading: float from North clockiwse (can be negative -16 ascending -166 descending)
    :param k: nd.array 1D array wave number vector
    :return:
    """
    ax.plot(np.radians([heading, heading]), (0.01, max(k)), 'r-')
    ax.plot(np.radians([heading + 180, heading + 180]), (0.01, max(k)), 'r-')
    plt.plot(np.radians([heading + 90, heading + 90]), (0.01, max(k)), 'r-')
    plt.plot(np.radians([heading + 270, heading + 270]), (0.01, max(k)), 'r-')
    ax.text(np.radians(heading), (0.65 + (np.cos(np.radians(heading)) < 0) * 0.25) * max(k),
            ' Azimuth', size=18, color='red', rotation=-heading + 90 + (np.sin(np.radians(heading)) < 0) * 180,
            ha='center')
    ax.text(np.radians(heading + 90), 0.80 * max(k), ' Range', size=18, color='red',
            rotation=-heading + (np.cos(np.radians(heading)) < 0) * 180, ha='center')