import sys
#sys.path.append('/home/mlebars/workspace/developpements/wavetools')
sys.path.append('/home/satwave/sources_en_exploitation/wavetoolbox/')

import plot_spec_V2
import Spectrum
from SpectrumReader_V2 import SpectrumReader

import matplotlib

from datetime import datetime, timedelta
from dateutil import rrule
import os
import glob
import netCDF4 as netcdf 
import numpy as np

#provider path
ndbc_nrt_wind_Path ='/home/cercache/project/globwave/data/provider/ndbc/meteo/nrt/'
ndbc_nrt_Path = '/home/cercache/project/globwave/data/provider/ndbc/spectra/nrt/'
ndbc_archive_Path = '/home/cercache/project/globwave/data/provider/ndbc/spectra/'
cdip_txt_Path = '/home/cercache/project/globwave/data/provider/cdip/'

#globwave path

ndbc_globwave_nrt_wave_path = '/home/cercache/project/globwave/data/globwave/ndbc/nrt_spectra/wave_sensor/'
ndbc_globwave_Path = '/home/cercache/project/globwave/data/globwave/ndbc/archive/wave_sensor/'
cdip_globwave_Path = '/home/cercache/project/globwave/data/globwave/cdip/wave_sensor/'

path_globwave = {
                 "ndbc_source_nrt_meteo":ndbc_nrt_wind_Path,
                 "ndbc_source_nrt_spectra":ndbc_nrt_Path,
                 'ndbc_source_archive_spectra':ndbc_archive_Path,
                 'cdip_source':cdip_txt_Path,
                 'ndbc_globwave_nrt_spectra':ndbc_globwave_nrt_wave_path,
                 'ndbc_globwave_archive_spectra':ndbc_globwave_Path,
                 'cdip_globwave_wave':cdip_globwave_Path
                }

provider_to_type = {'cdip_globwave_wave':'buoyglobwavecdip',
                    'ndbc_globwave_nrt_spectra':'buoyglobwavendbc',
                    'ndbc_globwave_archive_spectra':'buoyglobwavendbc'
                    }

#getFilePattern according to the provider
def getfilepattern(id,provider):
    filepattern = None
    
    if 'ndbc_globwave' in provider :
            filepattern = 'WMO'+id+'*_spectrum.nc'

    if 'cdip_globwave' in provider:
        if len(id)==5:      filepattern = 'WMO'+id+'*.nc'
        elif len(id)==3:      filepattern = id+'_*.nc'
    
    return filepattern

def getfilelist(filepattern,startdate,stopdate,provider):
    filelist = []

    archivePath = path_globwave[provider]
    
    if 'globwave' in  provider:
        for dd in rrule.rrule(rrule.MONTHLY,dtstart=startdate,until=stopdate):
            month_str  = str(dd.month).zfill(2)
            monthDir = os.path.join(archivePath, str(dd.year),month_str  )
            if os.path.exists(monthDir):
                tmp = os.path.join(monthDir,filepattern)
                filelist += glob.glob(tmp)

    filelist.sort()
    return filelist
    

#Find nearest Element in fileSystem 
#Step 1 search file that could match
#Step 2 search in files the index in and the file
#step 3 transform it to Spectrum object and return it
def findBuoyElement( id, mydatetime, provider ='cdip_globwave_wave',intervaldays=1):
    #getFilePattern according to the provider
    filepattern = getfilepattern(id,provider)
    
    startdate =  mydatetime - timedelta(days=intervaldays)
    stopdate =  mydatetime + timedelta(days=intervaldays)
    
    filelist =  getfilelist(filepattern,startdate,stopdate,provider)
    print len(filelist),"Found in",filepattern
    index=-1
    filepath=""
    #init the interval to 365 days
    #if no element is in 2 years interval that won't work
    timeinterval=timedelta(days=365)
    #timeinterval
    for buoyFile in filelist:
        data = netcdf.Dataset( buoyFile, 'r' )
        time = netcdf.num2date( data.variables['time'][:], data.variables['time'].units )

        data.close()
        # check the time in the time window or not
        tmpidx = (np.abs(time-mydatetime)).argmin()
        tmptimeinterval = abs(time[tmpidx]-mydatetime)
        #the new element is a better one
        if tmptimeinterval < timeinterval:
            timeinterval = tmptimeinterval
            index = tmpidx
            filepath = buoyFile
    print index,filepath,provider_to_type[provider]
    return SpectrumReader.read_spectrum(filepath,provider_to_type[provider],index)     
    #return index,filepath

def returnBuoyElement( buoyFile, indexSpectrum, provider ='cdip_globwave_wave'):
    if provider in provider_to_type.keys():
        provider=provider_to_type[provider]
    return SpectrumReader.read_spectrum(buoyFile,provider,indexSpectrum)   


#Find all Element in fileSystem between startDate and endDate 
#Step 1 search file that could match
#Step 2 search in files the elements
#step 3 transform it to Spectrum object and add it to return list
#return list spectra
def findBuoyElementListFromFiles( id, startDate,endDate=None, provider ='cdip_globwave_wave',limit=50,returnSpectra=True):
    #getFilePattern according to the provider
    filepattern = getfilepattern(id,provider)
    
    if endDate is None:
        endDate = startDate + timedelta(days=365)
    #startdate =  datetosearch - timedelta(hours=intervalhours)
    #stopdate =  datetosearch + timedelta(hours=intervalhours)
    
    filelist =  getfilelist(filepattern,startDate,endDate,provider)
    print len(filelist),"File Found in",filepattern
    index=-1

    listSpectra = []
    cpt=0
    for buoyFile in filelist:
        data = netcdf.Dataset( buoyFile, 'r' )
        time = netcdf.num2date( data.variables['time'][:], data.variables['time'].units )
        print "time",time
        data.close()
        # Get the nearest elements from start and end
        startIndex = (np.abs(time-startDate)).argmin()
        endIndex = (np.abs(time-endDate)).argmin()
        #If the nearest startelement is before start date take the next one
        if time[startIndex]<startDate:
            startIndex+=1
        #If the nearest endelement is after end date take the previous one
        if time[endIndex]>endDate:
            endIndex-=1
        #get all the elements
        while startIndex<=endIndex:
            if returnSpectra:
                listSpectra.append(SpectrumReader.read_spectrum(buoyFile,provider_to_type[provider],startIndex))
            else:
                listSpectra.append([buoyFile,provider,startIndex])
            cpt+=1
            #If limit exceed return list
            if cpt>limit:
                return listSpectra            
            startIndex+=1
    return listSpectra